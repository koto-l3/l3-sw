
import os,sys
import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS
import numpy as np
import glob
import time
from shutil import disk_usage as du
from psutil import virtual_memory as vm
from scp import SCPClient
import paramiko



bucket = "daq"
org = "koto"
token = "kJfsktlhyrnsoT6HmfSzVQHlml_1MH4JcKxnXsgOtGPIOk14Nc1NxXg7odhaF3jJb6ZKejKMAlsCzWZ-ggJhRw=="
url="192.168.10.180:8086"


l3db_ip="192.168.10.180"

#bucket = "daq"
#org = "koto"
#token = "YqiHq3EunEMHYZbgehheAAb-Qd4GUasqlJ3VU61qjo1av5XtFH6Kf4AimwNHFVCquGQ_vlpIidGCvOFwvbVRgQ=="
#url="http://192.168.13.99:8086"


client = influxdb_client.InfluxDBClient(
   url=url,
   token=token,
   org=org
)
write_api = client.write_api(write_options=SYNCHRONOUS)


def WriteToDB(points):
    try:
        write_api.write(bucket=bucket, org=org, record=points)
    except Exception as e:
        print("Error writing to influxdb:")
        print(e)
        




counter = 0
while (True):
    
    
    l3_target_vdisk_file="/home/koto/config/run_target_vdisks/l3_target_vdisk.conf"
    
    # get L3 target vdisk path
    
    l3_target_vdisk_id = -1
    
    try:
        l3_target_vdisk_id = int(np.loadtxt(l3_target_vdisk_file))
    except:
        print("Unexpected contents found in %s." % l3_target_vdisk_file)
        time.sleep(10)
        continue
    
    print("L3 target vdisk id: ", l3_target_vdisk_id)
    if (l3_target_vdisk_id != 0 and l3_target_vdisk_id != 1):
        print("Unexpected value found in %s." % l3_target_vdisk_file)
        time.sleep(10)
        continue

    stats_dir_in =  "/mnt/vdisk" + str(l3_target_vdisk_id) + "/stats/l3_out/"
    stats_dir_out = "/mnt/vdisk" + str(l3_target_vdisk_id) + "/stats/uploadedToInfluxDB/"
    stats_dir_err = "/mnt/vdisk" + str(l3_target_vdisk_id) + "/stats/processedWithErrors"
    
    counter += 1
    if counter  > 10:
        
        # get used memory in GB
        used_ram = vm().used/(1<<30)
        
        
        # Get disk space and upload it to influxDB
        # get used space in disk node in TB
        vdisk0_used_space = du("/mnt/vdisk0")[1]/(1<<40)
        vdisk1_used_space = du("/mnt/vdisk1")[1]/(1<<40)
        
        points = []
        
        points.append(influxdb_client.Point("perf_monitor" ).tag("Node", "dn1").field("vdisk0_used_disk_space", vdisk0_used_space).time(time.time_ns(), write_precision="ns"))
        points.append(influxdb_client.Point("perf_monitor" ).tag("Node", "dn1").field("vdisk1_used_disk_space", vdisk1_used_space).time(time.time_ns(), write_precision="ns"))
        points.append(influxdb_client.Point("perf_monitor" ).tag("Node", "dn1").field("used_ram",        used_ram).time(time.time_ns(), write_precision="ns"))
        WriteToDB(points)
        
        counter = 0
        

    
    
    files = glob.glob(stats_dir_in + 'run_*stats.txt')

    # get spill ids
    #ids = [int(files[k].split("_")[-1].split(".")[0]) for k in range(len(files))]

    # sort them
    #ids.sort()
    files.sort()



    if (len(files) == 0):
        print("Waiting for stats files to come...")
        time.sleep(2)
        continue

    for i, file in enumerate(files):

        print("Processing file: ", file)
        
        try:
            M = np.loadtxt(file)
        except Exception as e:
            print("Error reading file %s: %s" % (file, e))
            # move the file to the "processed with errors" folder
            cmd = "mv " + file + " " + stats_dir_err
            os.system(cmd)
            os.system(cmd)
            continue 
        
        if (len(M) == 0):
            print("File %s is empty" % file)
            cmd = "mv " + file + " " + stats_dir_err
            os.system(cmd)
            os.system(cmd)
            continue
        
        runNo = int(M[0][0])
        spillNo = int(M[0][1])
        nProcessedEvents = int(M[0][2])
        timestamp = int(M[0][3])
        print("RunNo: ", runNo, "SpillNo: ", spillNo, "spill_start_time", timestamp, "nProcessedEvents", nProcessedEvents)
        
        
        statsPerMode = M[1:]
        print(statsPerMode)
        
        #print("nAcceptedEventsByMode: ", nAcceptedEventsByMode)
        #print("nRejectedEventsByMode_minXY: ", nRejectedEventsByMode_minXY)
        #print("nRejectedEventsByMode_MaxR: ", nRejectedEventsByMode_MaxR)
        #print("nRejectedEventsByMode_TotalEnergy: ", nRejectedEventsByMode_TotalEnergy)
        #print("nRejectedEventsByMode_COE: ", nRejectedEventsByMode_COE)
        
        reductionFactors = np.zeros(len(statsPerMode))
        for i in range(len(statsPerMode)):
            if (np.sum(statsPerMode[i][:]) != 0):
                # reduction factor = nProcessedEvents / nAcceptedEventss
                reductionFactors[i] = np.sum(statsPerMode[i][0:2]) / statsPerMode[i][0]

        points = []

        n_rejected_events_5g_minXY =    statsPerMode[1][2]/statsPerMode[1][1]*100 if statsPerMode[1][1] != 0 else 0.0
        n_rejected_events_5g_maxR =     statsPerMode[1][3]/statsPerMode[1][1]*100 if statsPerMode[1][1] != 0 else 0.0
        n_rejected_events_5g_totalE =   statsPerMode[1][4]/statsPerMode[1][1]*100 if statsPerMode[1][1] != 0 else 0.0
        n_rejected_events_5g_coe =      statsPerMode[1][5]/statsPerMode[1][1]*100 if statsPerMode[1][1] != 0 else 0.0
        n_rejected_events_kp_minXY =    statsPerMode[2][2]/statsPerMode[2][1]*100 if statsPerMode[2][1] != 0 else 0.0
        n_rejected_events_kp_maxR =     statsPerMode[2][3]/statsPerMode[2][1]*100 if statsPerMode[2][1] != 0 else 0.0
        n_rejected_events_kp_totalE =   statsPerMode[2][4]/statsPerMode[2][1]*100 if statsPerMode[2][1] != 0 else 0.0
        n_rejected_events_pi0ee_minXY =  statsPerMode[3][2]/statsPerMode[3][1]*100 if statsPerMode[3][1] != 0 else 0.0
        n_rejected_events_pi0ee_maxR =   statsPerMode[3][3]/statsPerMode[3][1]*100 if statsPerMode[3][1] != 0 else 0.0
        n_rejected_events_pi0ee_totalE = statsPerMode[3][4]/statsPerMode[3][1]*100 if statsPerMode[3][1] != 0 else 0.0
        n_rejected_events_pi0ee_coe =    statsPerMode[3][5]/statsPerMode[3][1]*100 if statsPerMode[3][1] != 0 else 0.0
        
        n_selected_events_total_count = nProcessedEvents - sum(statsPerMode[:,1])

        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("run_no",             runNo).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("spill_no",           spillNo).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_processed_events", nProcessedEvents).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_selected_events_total_count", n_selected_events_total_count).time(timestamp, write_precision="ns"))
        
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_accepted_events_5g", statsPerMode[1][0]).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_rejected_events_5g_minXY", n_rejected_events_5g_minXY).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_rejected_events_5g_maxR", n_rejected_events_5g_maxR).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_rejected_events_5g_totalE", n_rejected_events_5g_totalE).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_rejected_events_5g_coe", n_rejected_events_5g_coe).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("reduction_factor_5g", reductionFactors[1]).time(timestamp, write_precision="ns"))
        
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_accepted_events_kp", statsPerMode[2][0]).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_rejected_events_kp_minXY", n_rejected_events_kp_minXY).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_rejected_events_kp_maxR", n_rejected_events_kp_maxR).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_rejected_events_kp_totalE", n_rejected_events_kp_totalE).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("reduction_factor_kp", reductionFactors[2]).time(timestamp, write_precision="ns"))
        
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_accepted_events_pi0ee", statsPerMode[3][0]).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_rejected_events_pi0ee_minXY", n_rejected_events_pi0ee_minXY).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_rejected_events_pi0ee_maxR", n_rejected_events_pi0ee_maxR).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_rejected_events_pi0ee_totalE", n_rejected_events_pi0ee_totalE).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("n_rejected_events_pi0ee_coe", n_rejected_events_pi0ee_coe).time(timestamp, write_precision="ns"))
        points.append(influxdb_client.Point("spill_monitor").tag("Node", "dn1").field("reduction_factor_pi0ee", reductionFactors[3]).time(timestamp, write_precision="ns"))
        
        
        WriteToDB(points)
        
        # scp file to db server
        
        outputDir = "/data/daqlog/l3/stats_in_from_dn/"
        
        try:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(l3db_ip, username="koto")
            scp = SCPClient(ssh.get_transport())
            scp.put(file, outputDir)
            scp.close()
            ssh.close()

        except Exception as e:
            print("Could not copy stats file to db server: ", e)
            print("Moving it to %s instead" % stats_dir_out)

        # move the file to the processed folder
        cmd = "mv " + file + " " + stats_dir_out
        os.system(cmd)

        
        counter += 1        
        time.sleep(0.2)

