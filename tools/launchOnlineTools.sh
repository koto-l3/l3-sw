
set -ex


# Make sure we exit the background processes when we exit
trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

thisRunOutDir=$(cat /home/koto/git-repos/l3-sw/config/dn_out_base_dir.txt)

# saveL3Conf.sh: Save the L3 configuration to a file
if [ $# -eq 1 ]; then
    bash /home/koto/git-repos/l3-sw/tools/saveL3Conf.sh $1 $thisRunOutDir
fi

# monitorStats.py: Upload statistics to InfluxDB
python /home/koto/git-repos/l3-sw/tools/monitorStats.py &

# sendMonitorFiles.sh: Send monitor bin files to the monitor PC
bash /home/koto/git-repos/l3-sw/tools/sendMonitorFiles.sh ${thisRunOutDir}/monitor koto 192.168.10.204 /data/online_monitor_data &

wait
