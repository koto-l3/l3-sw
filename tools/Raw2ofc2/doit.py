from ROOT import TFile, TTree, TH1F
from array import array
import uproot
import numpy as np
from datetime import datetime
from time import sleep
from pprint import pprint as pp


runNo = 90
targetSpillNo = -1 # -1 for all spills




def ProcessBatch(entry_start, entry_stop, tree, batch_id):

    arrays = tree.arrays("EventData/EventData.Data[16][128]", entry_start=entry_start, entry_stop=entry_stop, library="np")
    
    data = arrays["EventData/EventData.Data[16][128]"]
    

    ModuleID = tree.arrays("EventData.ModuleID", entry_start=entry_start, entry_stop=entry_stop, library="np")["EventData.ModuleID"]
    CrateID = tree.arrays("EventData.CrateID", entry_start=entry_start, entry_stop=entry_stop, library="np")["EventData.CrateID"]
    SlotNo = tree.arrays("EventData.SlotNo", entry_start=entry_start, entry_stop=entry_stop, library="np")["EventData.SlotNo"]
    SpillNo = tree.arrays("EventData.SpillID", entry_start=entry_start, entry_stop=entry_stop, library="np")["EventData.SpillID"]
    L1TrigNo = tree.arrays("EventData.L1TrigNo", entry_start=entry_start, entry_stop=entry_stop, library="np")["EventData.L1TrigNo"]

    
    
    # ROOT tree containing packets
    out_fname = "out_" + str(batch_id) + ".root"
    f = TFile(out_fname,'recreate')
    t = TTree('T','ofc2_out_data')


    #print(len(data), len(data[0]), len(data[0][0]))



    m_adc_data = np.zeros((288, 16, 64), dtype=np.uint16)
    m_adc_hdrs = np.zeros((288, 6), dtype=np.uint16)
    
    

    t.Branch('m_adc_data', m_adc_data, 'm_adc_data[288][16][64]/s')
    t.Branch('m_adc_hdrs', m_adc_hdrs, 'm_adc_hdrs[288][6]/s')


    # timestamp
    dt = datetime.now()
    timestamp = int(datetime.timestamp(dt))
    

    ## print trigger ADC and exit
    #for fadc_ch in range(16):
    #    #print(counter)
    #    print("ch ", fadc_ch, ": ", end="")
    #    for wfmEntry in range(64):
    #        #print("writing entry ", counter + wfmEntry*2, "and", counter + wfmEntry*2+1, "i_fadc_sorted", i_fadc_sorted, "ch", fadc_ch, "entry", wfmEntry)
    #        #b0 = data[20][70][fadc_ch * 128 + wfmEntry] & 0xFF
    #        #b1= (data[20][70][fadc_ch * 128 + wfmEntry] >> 8) & 0xFF
    #        print(hex(data[15][70][fadc_ch * 128 + wfmEntry])[2:], end=" ")    
    #    print()


    for event in range(len(data)):

        print("event", event, "of", len(data))

        #print(len(data[event][70]), data[event][70][0])
        
    
        if targetSpillNo > 0 and SpillNo[event][0] != targetSpillNo:
            print("Skipping event", event, "SpillNo", SpillNo[event][0])
            continue



        for i_fadc in range(288):


            spill_no = SpillNo[event][i_fadc]
            event_no = L1TrigNo[event][i_fadc]
            module_id = ModuleID[event][i_fadc]
            crate_no = CrateID[event][i_fadc]
            slot_no = SlotNo[event][i_fadc]
            timestamp = 999
            clusBit = 999

            # Make sure the FADCs come sorted
            i_fadc_sorted = i_fadc
            
            if runNo == 87:
                
                i_fadc_sorted = -1
                
                for i in range(288):

                    # Fuck this
                    crate_no = CrateID[event][i]
                    module_id = ModuleID[event][i]
                    slot_no = SlotNo[event][i]
                    if crate_no == 10:
                        module_id -= 1
                        slot_no -= 1
                    if crate_no == 15 and module_id > 8:
                        module_id -= 1
                        slot_no -= 1
                    recModId = crate_no * 16 + module_id
                    if recModId == i_fadc:
                        i_fadc_sorted = i
                        break
                    #print("CrateID", crate_no, "ModuleID", module_id, "SlotNo", slot_no)

                if i_fadc_sorted == -1:
                    print("ERROR: FADC %d not found in event %d" % (i_fadc, event))
                    exit()

                if module_id != slot_no-3:
                    print("ERROR: ModuleID %d != SlotNo %d - 2 in event %d" % (module_id, slot_no, event))
                    exit()
                
            
            #print("i_fadc: %d, module_id: %d, crate_no: %d, slot_no: %d, spill_no: %d, event_no: %d" % (i_fadc, module_id, crate_no, slot_no, spill_no, event_no))
                  
                  
            # ADC Header
            m_adc_hdrs[i_fadc][0] = (1 << 15) + (1 << 14) + ((spill_no & 0xF) << 10) + ((module_id & 0x1F) << 5) + (crate_no & 0x1F)
            m_adc_hdrs[i_fadc][1] = (1 << 15) + (1 << 14) + ((event_no & 0xFF) << 6) + (spill_no >> 4 & 0x3F)
            m_adc_hdrs[i_fadc][2] = (1 << 15) + (1 << 14) + ((timestamp & 0x3F) << 8) + (event_no >> 8 & 0xFF)
            m_adc_hdrs[i_fadc][3] = (1 << 15) + (1 << 14) + (timestamp >> 6 & 0x3FFF)
            m_adc_hdrs[i_fadc][4] = (1 << 15) + (1 << 14) + ((clusBit & 0x1F) << 9) + (timestamp >> 20 & 0x1FF)
            m_adc_hdrs[i_fadc][5] = (1 << 15) + (1 << 14) + (0 << 13) + (clusBit >> 5 & 0x7FF)

            #print("i_fadc_sorted", i_fadc_sorted, "i_fadc", i_fadc, "recModId", recModId)
            #print("CrateID", crate_no, "ModuleID", module_id, "SlotNo", slot_no, "SpillNo", spill_no, "EventNo", event_no, "Timestamp", timestamp, "ClusBit", clusBit)

                    #packet[counter + wfmEntry*2] = data[event][i_fadc_sorted][fadc_ch * 128 + wfmEntry] & 0xFF
                    #packet[counter + wfmEntry*2+1] = (data[event][i_fadc_sorted][fadc_ch * 128 + wfmEntry] >> 8) & 0xFF
    
            #print(m_adc_hdrs[i_fadc][0])
            
            # ADC Data
            for i_ch in range(16):
                for i_sample in range(64):
                    m_adc_data[i_fadc][i_ch][i_sample] = data[event][i_fadc_sorted][i_ch * 128 + i_sample]
                
                
             

        #for i in range(0, 1000, 2):
        #    print(i/2, (packet[i] << 8) | packet[i+1])


        t.Fill()


    f.Write()
    f.Close()











#fnames = ["data/run35000_node20_file0.root:T"] # Physics
#fnames = ["data/run34975_node20_file0.root:T"] # Physics
fnames = ["data/run38026_spill10_file0.root:T"] # Physics
#fnames = ["data/fullout_KL3pi0_0.root:runTree"] # Physics
#fnames = ["/home/mario/work/koto-l3/selection/2212_MinBiasTreeProducer/data/run36484_node20_file0.root:T"] # Cosmic
nEntriesPerBatch = 1000




for i, val in enumerate(uproot.num_entries(fnames)):

    tree = uproot.open(fnames[i])

    nEntries = val[2]
    #nEntries = 2000
    
    
    if nEntriesPerBatch > nEntries:
        nEntriesPerBatch = nEntries
    nBatches = int(np.ceil(nEntries / nEntriesPerBatch))
    

    print("nEntries:", nEntries, "nBatches:", nBatches)
    # Process all batches
    for batch in range(nBatches):
        print("Processing full batch", batch, "of", nBatches)
        
        entry_start = batch*nEntriesPerBatch
        entry_stop = (batch+1)*nEntriesPerBatch

        print("entry_start", entry_start, "entry_stop", entry_stop)
        ProcessBatch(entry_start, entry_stop, tree, batch)

    print("Processing remaining events, or not")
    # Process last batch
    entry_start = nBatches*nEntriesPerBatch
    entry_stop = nEntries
    print("entry_start", entry_start, "entry_stop", entry_stop)
    
    if entry_start != entry_stop:
        ProcessBatch(entry_start, entry_stop, tree, nBatches)
    














