
set -e




localMonitorDir=$1
remoteUser=$2
remoteHost=$3
remoteTargetDir=$4


# Check number of arguments
if [ $# -ne 4 ]; then
    echo "Usage: $0 <localMonitorDir> <remoteUser> <remoteHost> <remoteTargetDir>"
    exit 1
fi

# Check if the monitor files directory exists
if [ ! -d "$localMonitorDir" ]; then
    echo "ERROR: directory $localMonitorDir does not exist"
    exit 1
fi

while true
do

    # See if the directory contains any bin files at all
    count=`ls -1 $localMonitorDir/*.bin 2>/dev/null | wc -l`
    if [ $count == 0 ]; then
        echo "Waiting for monitor files to appear in $localMonitorDir ..."
        sleep 5
        continue
    fi

    # Get the list of bin files in the monitor files directory
    files=$(ls $localMonitorDir/*.bin)

    # Send the files
    for file in $files; do

        filename="${file##*/}"

        echo "Sending file $file to $remoteUser@$remoteHost:$remoteTargetDir/$filename"

        # Send the file with temporary extension
        scp -o HostKeyAlgorithms=+ssh-rsa -o PubkeyAcceptedAlgorithms=+ssh-rsa $localMonitorDir/$filename $remoteUser@$remoteHost:$remoteTargetDir/$filename.tmp

        # Rename the file
        ssh -o HostKeyAlgorithms=+ssh-rsa -o PubkeyAcceptedAlgorithms=+ssh-rsa $remoteUser@$remoteHost "mv $remoteTargetDir/$filename.tmp $remoteTargetDir/$filename"

        # Remove the file locally
        rm $localMonitorDir/$filename

        echo "File $filename sent"
    done

done


