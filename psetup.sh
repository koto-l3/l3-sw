
set -e

# Get pwd from cli
pwd=$1

if [ -z "$pwd" ]; then
    echo "Please provide the password as the first argument"
    exit 1
fi


## mk the target dir
#sshpass -p $pwd pssh -h ./pssh-hosts -l koto -A -i "mkdir -p /home/koto/common/l3sw"
#
# scp everything

psshHostFiles="pssh-spillnodes pssh-disknodes"

# Commands to run in all nodes
for file in $psshHostFiles; do
    echo "Copying to $file"
    sshpass -p $pwd prsync -v -r -h ./$file -l koto -vA /home/koto/git-repos/l3-sw /home/koto/git-repos/  
    echo "Updating git submodules in $file"
    sshpass -p $pwd pssh -h ./$file -l koto -A -i "cd /home/koto/git-repos/l3-sw/ && git submodule update --init --recursive"
done

# Commands to run only in the spill nodes
sshpass -p $pwd pssh -h ./pssh-spillnodes -l koto -A -i "cd /home/koto/git-repos/l3-sw/build && cmake -DIS_SPILLNODE=TRUE .. && make -j15" &

# Commands to run only in the disk nodes
sshpass -p $pwd pssh -h ./pssh-disknodes -l koto -A -i "cd /home/koto/git-repos/l3-sw/build && cmake -DIS_SPILLNODE=FALSE .. && make -j15" &

wait

# compile

# (also locally)
#cd /home/koto/git-repos/l3-sw/build && cmake .. && make