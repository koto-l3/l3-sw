#include "HnEveDisCSI.h"
//#include "GsimE14Detector/GsimE14UserGeom.h"
#include <iostream>
#include "TVector3.h"
#include "TMath.h"
#include "TGraph.h"
#include "TPaveText.h"

//using namespace E14;

namespace HnLib
{
  HnEveDisCSI::HnEveDisCSI(int userFlag) : HnEveDisDetector("CSI","csi",2716)
  {
    m_userFlag=userFlag;
    m_isZYPlaneVisible=true;
    double  KtevCsicFine_Len = 50.0;
    double  KtevCsic_Gap = 0.03;
    double  KtevCsicFine_Gap = KtevCsic_Gap*2.5/5.;
    double  Csir_Len     = 180;
    double  KtevCsicFine_XY  = 2.5;
    double  KtevCsic_XY  = 5.0;
    
    const double cm=10.;
    m_shiftPosition=TVector3(0,0,(609.8+180./2.+2)*cm);
    //    double Csir_Zmin      = 609.8;        //not used
    //    double Csic_Zmin      = 614.8;	//not used

    int nBlockFine=48;
    int nBeamHole=8;
    double csh1_len = KtevCsicFine_Len;

    //    double crystalZOffset = Csic_Zmin-Csir_Zmin;	//not used
    double c_csh1_z = -Csir_Len/2 + 5 + csh1_len/2;

    int id=0;
    for(int i=0;i<nBlockFine;i++) {
      double YBlock=
	(KtevCsicFine_XY+KtevCsicFine_Gap)*(i-nBlockFine/2.)
	+(KtevCsicFine_XY+KtevCsicFine_Gap)/2.;

      for(int j=0;j<nBlockFine;j++) {
	double XBlock=
	  (KtevCsicFine_XY+KtevCsicFine_Gap)*(j-nBlockFine/2.)
	  +(KtevCsicFine_XY+KtevCsicFine_Gap)/2.;

	if( i>=(nBlockFine-nBeamHole)/2. &&
	    i<(nBlockFine-nBeamHole)/2.+nBeamHole &&
	    j>=(nBlockFine-nBeamHole)/2. &&
	    j<(nBlockFine-nBeamHole)/2.+nBeamHole )
	  continue;

	XBlock*=-1;

	double ZBlock=(-csh1_len/2.+KtevCsicFine_Len/2.) + (c_csh1_z);

	m_localEnvelopeDS[id].push_back(TVector3(XBlock+KtevCsicFine_XY/2.,YBlock+KtevCsicFine_XY/2.,ZBlock-KtevCsicFine_Len/2.));
	m_localEnvelopeDS[id].push_back(TVector3(XBlock-KtevCsicFine_XY/2.,YBlock+KtevCsicFine_XY/2.,ZBlock-KtevCsicFine_Len/2.));
	m_localEnvelopeDS[id].push_back(TVector3(XBlock-KtevCsicFine_XY/2.,YBlock-KtevCsicFine_XY/2.,ZBlock-KtevCsicFine_Len/2.));
	m_localEnvelopeDS[id].push_back(TVector3(XBlock+KtevCsicFine_XY/2.,YBlock-KtevCsicFine_XY/2.,ZBlock-KtevCsicFine_Len/2.));

	m_localEnvelopeUS[id].push_back(TVector3(XBlock+KtevCsicFine_XY/2.,YBlock+KtevCsicFine_XY/2.,ZBlock+KtevCsicFine_Len/2.));
	m_localEnvelopeUS[id].push_back(TVector3(XBlock-KtevCsicFine_XY/2.,YBlock+KtevCsicFine_XY/2.,ZBlock+KtevCsicFine_Len/2.));
	m_localEnvelopeUS[id].push_back(TVector3(XBlock-KtevCsicFine_XY/2.,YBlock-KtevCsicFine_XY/2.,ZBlock+KtevCsicFine_Len/2.));
	m_localEnvelopeUS[id].push_back(TVector3(XBlock+KtevCsicFine_XY/2.,YBlock-KtevCsicFine_XY/2.,ZBlock+KtevCsicFine_Len/2.));

	//cm unit to mm unit
	for(std::vector<TVector3>::iterator it=m_localEnvelopeDS[id].begin();
	    it!=m_localEnvelopeDS[id].end();it++) {
	  (*it)=(*it)*10.;
	}
	for(std::vector<TVector3>::iterator it=m_localEnvelopeUS[id].begin();
	    it!=m_localEnvelopeUS[id].end();it++) {
	  (*it)=(*it)*10.;
	}

	m_moduleID[id]=id;
	m_mapIDtoIndex.insert( std::make_pair(id,id) );
	id++;
      }
    }


    int csiStack[38]
      = { 0,12,16,20,22,24,
	  26,28,30,32,32,34,
	  34,36,36,36,36,36,36,
	  36,36,36,36,36,36,34,
	  34,32,32,30,28,26,
	  24,22,20,16,12,0 };

    int nLayer=38;
    for(int i=0;i<nLayer;i++) {
      double YBlock=
	(KtevCsic_XY+KtevCsic_Gap)*(i-nLayer/2.)
	+(KtevCsic_XY+KtevCsic_Gap)/2.;

      int nBlock = csiStack[i];

      for(int j=0;j<nBlock;j++) {
	//KtevFine
	if( i>=(nLayer-nBlockFine/2.)/2. &&
	    i<(nLayer-nBlockFine/2.)/2.+nBlockFine/2. &&
	    j>=(nBlock-nBlockFine/2.)/2. &&
	    j<(nBlock-nBlockFine/2.)/2.+nBlockFine/2. )
	  continue;

	double XBlock=
	  (KtevCsic_XY+KtevCsic_Gap)*(j-nBlock/2.)
	  +(KtevCsic_XY+KtevCsic_Gap)/2.;
	// added by Sato 20120514
	XBlock*=-1;

	double ZBlock=(-csh1_len/2.+KtevCsicFine_Len/2.) + (c_csh1_z);

	m_localEnvelopeDS[id].push_back(TVector3(XBlock+KtevCsic_XY/2.,YBlock+KtevCsic_XY/2.,ZBlock-KtevCsicFine_Len/2.));
	m_localEnvelopeDS[id].push_back(TVector3(XBlock-KtevCsic_XY/2.,YBlock+KtevCsic_XY/2.,ZBlock-KtevCsicFine_Len/2.));
	m_localEnvelopeDS[id].push_back(TVector3(XBlock-KtevCsic_XY/2.,YBlock-KtevCsic_XY/2.,ZBlock-KtevCsicFine_Len/2.));
	m_localEnvelopeDS[id].push_back(TVector3(XBlock+KtevCsic_XY/2.,YBlock-KtevCsic_XY/2.,ZBlock-KtevCsicFine_Len/2.));
	m_localEnvelopeUS[id].push_back(TVector3(XBlock+KtevCsic_XY/2.,YBlock+KtevCsic_XY/2.,ZBlock+KtevCsicFine_Len/2.));
	m_localEnvelopeUS[id].push_back(TVector3(XBlock-KtevCsic_XY/2.,YBlock+KtevCsic_XY/2.,ZBlock+KtevCsicFine_Len/2.));
	m_localEnvelopeUS[id].push_back(TVector3(XBlock-KtevCsic_XY/2.,YBlock-KtevCsic_XY/2.,ZBlock+KtevCsicFine_Len/2.));
	m_localEnvelopeUS[id].push_back(TVector3(XBlock+KtevCsic_XY/2.,YBlock-KtevCsic_XY/2.,ZBlock+KtevCsicFine_Len/2.));

	//cm unit to mm unit
	for(std::vector<TVector3>::iterator it=m_localEnvelopeDS[id].begin();
	    it!=m_localEnvelopeDS[id].end();it++) {
	  (*it)=(*it)*10.;
	}
	for(std::vector<TVector3>::iterator it=m_localEnvelopeUS[id].begin();
	    it!=m_localEnvelopeUS[id].end();it++) {
	  (*it)=(*it)*10.;
	}
	m_moduleID[id]=id;
	m_mapIDtoIndex.insert(std::make_pair(id,id) );
	id++;

      }
    }
  }

  HnEveDisCSI::~HnEveDisCSI()
  {
    ;
  }

  bool HnEveDisCSI::setZYPlaneVisible(bool isVisible)
  {
    m_isZYPlaneVisible=isVisible;
    if(!g_hPoly) return false;
    TList* list = g_hPoly->GetBins();
    if(!list) return false;
    bool isModuleIDVisible=false;
    for(int i=0;i<m_nIndex;i++) {
      int id = m_moduleID[i];
      isModuleIDVisible=false;
      double x0,y0;
      if( id==0 ){
	x0=-9999;
	y0=-9999;
      }else{
	getModuleXY(id-1,x0,y0);
      }

      double x1,y1;
      getModuleXY(id,x1,y1);
      double diff = TMath::Abs(x1-x0);

      if( x0>=0 && x1<0 && diff<100 ){
	isModuleIDVisible=true;
      }

      TH2PolyBin* polybin = (TH2PolyBin*)list->At(i);
      if(!polybin) return false;
      if(!isModuleIDVisible)
	polybin->SetDrawOption("p");
      TObject* obj=polybin->GetPolygon();
      if(!obj) return false;
      TGraph* gr = (TGraph*)obj;
      if(isModuleIDVisible) {
	gr->SetFillStyle(1001);
	gr->SetLineStyle(1);
	gr->SetLineColor(1);
	gr->SetLineColorAlpha(1,1);
      } else {
	gr->SetFillStyle(0);
	gr->SetLineStyle(0);
	gr->SetLineColor(kWhite);
	gr->SetLineColorAlpha(kWhite,0);
      }
    }
    return true;
  }


}