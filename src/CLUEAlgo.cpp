#include "CLUEAlgo.h"

void CLUEAlgo::makeClusters()
{
  LayerTiles layerTiles;
  // start clustering
  auto start = std::chrono::high_resolution_clock::now();
  prepareDataStructures(layerTiles);
  auto finish = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> elapsed = finish - start;
  // std::cout << "--- prepareDataStructures:     " << elapsed.count() *1000 << " ms\n";

  start = std::chrono::high_resolution_clock::now();
  calculateLocalDensity(layerTiles);
  finish = std::chrono::high_resolution_clock::now();
  elapsed = finish - start;
  // std::cout << "--- calculateLocalDensity:     " << elapsed.count() *1000 << " ms\n";

  start = std::chrono::high_resolution_clock::now();
  calculateDistanceToHigher(layerTiles);
  finish = std::chrono::high_resolution_clock::now();
  elapsed = finish - start;
  // std::cout << "--- calculateDistanceToHigher: " << elapsed.count() *1000 << " ms\n";

  findAndAssignClusters();
}

void CLUEAlgo::prepareDataStructures(LayerTiles &tile)
{
  for (int i = 0; i < h_points_.n; i++)
  {
    // push index of points into tiles
    tile.fill(h_points_.x[i], h_points_.y[i], i);
  }
}

void CLUEAlgo::calculateLocalDensity(LayerTiles &tile)
{

  // loop over all points
  for (int i = 0; i < h_points_.n; i++)
  {
    LayerTiles &lt = tile;

    // get search box
    std::array<int, 4> search_box = lt.searchBox(h_points_.x[i] - dc_, h_points_.x[i] + dc_, h_points_.y[i] - dc_, h_points_.y[i] + dc_);

    // loop over bins in the search box
    for (int xBin = search_box[0]; xBin < search_box[1] + 1; ++xBin)
    {
      for (int yBin = search_box[2]; yBin < search_box[3] + 1; ++yBin)
      {

        // get the id of this bin
        int binId = lt.getGlobalBinByBin(xBin, yBin);
        // get the size of this bin
        int binSize = lt[binId].size();

        // interate inside this bin
        for (int binIter = 0; binIter < binSize; binIter++)
        {
          int j = lt[binId][binIter];
          // query N_{dc_}(i)
          float dist_ij = distance(i, j);
          if (dist_ij <= dc_)
          {
            // sum weights within N_{dc_}(i)
            h_points_.rho[i] += h_points_.weight[j];
          }
        } // end of interate inside this bin
      }
    } // end of loop over bins in search box
  }   // end of loop over points
}

void CLUEAlgo::calculateDistanceToHigher(LayerTiles &tile)
{
  // loop over all points
  for (int i = 0; i < h_points_.n; i++)
  {
    // default values of delta and nearest higher for i
    float maxDelta = std::numeric_limits<float>::max();
    float delta_i = maxDelta;
    int nearestHigher_i = -1;

    LayerTiles &lt = tile;

    // get search box
    std::array<int, 4> search_box = lt.searchBox(h_points_.x[i] - dm_, h_points_.x[i] + dm_, h_points_.y[i] - dm_, h_points_.y[i] + dm_);

    // loop over all bins in the search box
    for (int xBin = search_box[0]; xBin < search_box[1] + 1; ++xBin)
    {
      for (int yBin = search_box[2]; yBin < search_box[3] + 1; ++yBin)
      {

        // get the id of this bin
        int binId = lt.getGlobalBinByBin(xBin, yBin);
        // get the size of this bin
        int binSize = lt[binId].size();

        // interate inside this bin
        for (int binIter = 0; binIter < binSize; binIter++)
        {
          int j = lt[binId][binIter];
          // query N'_{dm_}(i)
          bool foundHigher = (h_points_.rho[j] > h_points_.rho[i]);
          // in the rare case where rho is the same, use detid
          foundHigher = foundHigher || ((h_points_.rho[j] == h_points_.rho[i]) && (j > i));
          float dist_ij = distance(i, j);
          if (foundHigher && dist_ij <= dm_)
          { // definition of N'_{dm_}(i)
            // find the nearest point within N'_{dm_}(i)
            if (dist_ij < delta_i)
            {
              // update delta_i and nearestHigher_i
              delta_i = dist_ij;
              nearestHigher_i = j;
            }
          }
        } // end of interate inside this bin
      }
    } // end of loop over bins in search box

    h_points_.delta[i] = delta_i;
    h_points_.nearestHigher[i] = nearestHigher_i;
  } // end of loop over points
}

void CLUEAlgo::findAndAssignClusters()
{
  auto start = std::chrono::high_resolution_clock::now();

  int nClusters = 0;

  // find cluster seeds and outlier
  std::vector<int> localStack;
  // loop over all points
  for (int i = 0; i < h_points_.n; i++)
  {
    // initialize clusterIndex
    h_points_.clusterIndex[i] = -1;
    // determine seed or outlier
    bool isSeed = (h_points_.delta[i] > deltac_) && (h_points_.rho[i] >= rhoc_);
    bool isOutlier = (h_points_.delta[i] > deltao_) && (h_points_.rho[i] < rhoc_);
    if (isSeed)
    {
      // set isSeed as 1
      h_points_.isSeed[i] = 1;
      // set cluster id
      h_points_.clusterIndex[i] = nClusters;
      // increment number of clusters
      nClusters++;
      // add seed into local stack
      localStack.push_back(i);
    }
    else if (!isOutlier)
    {
      // register as follower at its nearest higher
      h_points_.followers[h_points_.nearestHigher[i]].push_back(i);
    }
  }

  auto finish = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> elapsed = finish - start;
  // std::cout << "--- findSeedAndFollowers:      " << elapsed.count() *1000 << " ms\n";

  start = std::chrono::high_resolution_clock::now();
  // expend clusters from seeds
  while (!localStack.empty())
  {
    int i = localStack.back();
    auto &followers = h_points_.followers[i];
    localStack.pop_back();

    // loop over followers
    for (int j : followers)
    {
      // pass id from i to a i's follower
      h_points_.clusterIndex[j] = h_points_.clusterIndex[i];
      // push this follower to localStack
      localStack.push_back(j);
    }
  }
  finish = std::chrono::high_resolution_clock::now();
  elapsed = finish - start;
  // std::cout << "--- assignClusters:            " << elapsed.count() *1000 << " ms\n";
}

inline float CLUEAlgo::distance(int i, int j) const
{

  // 2-d distance on the layer
  const float dx = h_points_.x[i] - h_points_.x[j];
  const float dy = h_points_.y[i] - h_points_.y[j];
  return std::sqrt(dx * dx + dy * dy);
}
