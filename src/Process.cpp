

#include <fstream>
#include <algorithm>
#include <thread>
#include <future>

#include "Process.h"
#include "Detectors.h"
#include "GPUData.h"

#if D_ENABLE_CPU_DEBUG
#include "selector.h"
#endif

#include "compress_cpu.h"

#include <string>

#include <iterator>
#include <sstream>
#include <iostream>

#include "timer.h"
#include "common.h"
#include <unistd.h>
#include <mpi.h>

#include "nicola.h"

#include <chrono>
#include "nvtx3/nvToolsExt.h"




// Some small helper functions to deal with strings
template <typename Out>
void split(const std::string &s, char delim, Out result)
{
    std::istringstream iss(s);
    std::string item;
    while (std::getline(iss, item, delim))
    {
        if (!item.empty())
            *result++ = item;
    }
}
std::vector<std::string> split(const std::string &s, char delim)
{
    std::vector<std::string> elems;

    split(s, delim, std::back_inserter(elems));
    return elems;
}

uint16_t Process::GetRecoTagFromScaledTiggerBit(uint8_t scaledTrigBit)
{
    int l3tag = -1;


    for (int i = 0; i < 8; i++)
    {
        if (scaledTrigBit & (1 << i))
        {
            if (m_config.scaled_trig_bit_definitions[i] > l3tag)
            {
                l3tag = m_config.scaled_trig_bit_definitions[i];
            }
        }
    }

    if (l3tag == -1)
    {
        // if no bit is enabled
        l3tag = REC_PASS;
    }

    return (uint16_t)l3tag;

}

Process::Process(const Config &config)
{

    m_config = config;
    m_csi_info = new DetectorInfo(config);

    if (Initialize() != 0)
    {
        throw std::runtime_error("Error in Process::Initialize()");
    }

    for (u_int i = 0; i < m_config.n_pcap_threads; i++)
    {
        // There is only one computing thread per pcap thread. The computing
        // thread will be doing basic packet-level checks, and the actual big
        // calculations are done on GPU. 
        m_threadPoolListForCUDA.push_back(std::make_unique<BS::thread_pool>(1));
    }


    // Allocate memory...
    for (int i_stream = 0; i_stream < N_CUDA_STREAMS; i_stream++)
    {

        // Wfm data
        cudaMalloc((void **)&m_d_perFADCWfmData[i_stream], MAX_NEVENTS_PER_STREAM * 288 * 16 * 64 * sizeof(uint16_t));
        // ADC hdr data
        cudaMalloc((void **)&m_d_perEvtHeaderData[i_stream], MAX_NEVENTS_PER_STREAM * EVT_HDR_SIZE * sizeof(uint16_t));
        // idx and size of evety compressed event in the compressed data array
        cudaMalloc((void **)&m_d_perEvtIdxCompressed[i_stream], MAX_NEVENTS_PER_STREAM * sizeof(uint32_t));
        cudaMalloc((void **)&m_d_perEvtSizeCompressed[i_stream], MAX_NEVENTS_PER_STREAM * sizeof(uint32_t));
        cudaMallocHost((void **)&m_h_perEvtIdxCompressed[i_stream], MAX_NEVENTS_PER_STREAM * sizeof(uint32_t));
        cudaMallocHost((void **)&m_h_perEvtSizeCompressed[i_stream], MAX_NEVENTS_PER_STREAM * sizeof(uint32_t));
        // L3 event tag, depending on scaled trigg. bit.
        cudaMalloc((void **)&m_d_recoTag[i_stream], MAX_NEVENTS_PER_STREAM * sizeof(uint16_t));
        // Scaled trigger bits
        cudaMalloc((void **)&m_d_triggerBits[i_stream], MAX_NEVENTS_PER_STREAM * 8 * sizeof(uint16_t));
        // Wfm hdr for compression
        cudaMalloc((void **)&m_d_wfmInfo[i_stream], MAX_NEVENTS_PER_STREAM * 288 * 16 * 3 * sizeof(uint16_t));
        // Compressed data
        cudaMalloc((void **)&m_d_odata[i_stream], D_COMP_ADC_DATA_BUFF_SIZE * sizeof(uint8_t));
        // Compressed Bytes per Wfm
        cudaMalloc((void **)&m_d_compressedBytesPerWfm[i_stream], MAX_NEVENTS_PER_STREAM * (288 * 16 + 1) * sizeof(uint32_t));
        // Used to measure ped. sup. efficiency
        cudaMalloc((void **)&m_d_pedSuppressionFlag[i_stream], MAX_NEVENTS_PER_STREAM * 288 * 16 * sizeof(uint8_t));
        // Waveform data on the host
        cudaMallocHost((void **)&m_h_perFADCWfmData[i_stream], MAX_NEVENTS_PER_STREAM * 288 * 16 * 64 * sizeof(uint16_t));
        // Compressed data buffer on the host. Not used if doing RDMA.
        cudaMallocHost((void **)&m_h_compressedDataBuffer[i_stream], MPI_BUFF_SIZE * sizeof(uint8_t));

        // cluster variables (device)
        cudaMalloc((void **)&m_d_clusters[i_stream], sizeof(Clusters));
        m_d_clusters[i_stream] = new Clusters();
        cudaMalloc((void **)&m_d_clusters[i_stream]->x, sizeof(float) * MAX_NEVENTS_PER_STREAM * MAX_CSI_CLUS_PER_EVT);
        cudaMalloc((void **)&m_d_clusters[i_stream]->y, sizeof(float) * MAX_NEVENTS_PER_STREAM * MAX_CSI_CLUS_PER_EVT);
        cudaMalloc((void **)&m_d_clusters[i_stream]->E, sizeof(float) * MAX_NEVENTS_PER_STREAM * MAX_CSI_CLUS_PER_EVT);
        cudaMalloc((void **)&m_d_clusters[i_stream]->n, sizeof(int) * MAX_NEVENTS_PER_STREAM);
        cudaMalloc((void **)&m_d_clusters[i_stream]->evtID, sizeof(int) * MAX_NEVENTS_PER_STREAM);


#if D_ENABLE_CPU_DEBUG
        // total energy
        cudaMalloc((void **)&m_d_energies[i_stream], MAX_NEVENTS_PER_STREAM * sizeof(float));
        // cluster variables (host)
        m_h_clusters[i_stream] = new Clusters();
        cudaMallocHost((void **)&m_h_clusters[i_stream]->x, sizeof(float) * MAX_NEVENTS_PER_STREAM * MAX_CSI_CLUS_PER_EVT);
        cudaMallocHost((void **)&m_h_clusters[i_stream]->y, sizeof(float) * MAX_NEVENTS_PER_STREAM * MAX_CSI_CLUS_PER_EVT);
        cudaMallocHost((void **)&m_h_clusters[i_stream]->E, sizeof(float) * MAX_NEVENTS_PER_STREAM * MAX_CSI_CLUS_PER_EVT);
        cudaMallocHost((void **)&m_h_clusters[i_stream]->n, sizeof(int) * MAX_NEVENTS_PER_STREAM);
        cudaMallocHost((void **)&m_h_clusters[i_stream]->evtID, sizeof(int) * MAX_NEVENTS_PER_STREAM);
#endif

        l3log.log(L3_VERBOSE, L3_DEBUG, "Allocated CUDA buffers for stream %d\n", i_stream);
    }

    // CSI ch ID to ADC ch ID mapping
    cudaMalloc((void **)&m_d_CSIDetChIdToADCChId, m_config.csi_n_channels * sizeof(uint16_t));
    // Masked noisy channels (event reconstruction)
    cudaMalloc((void **)&m_d_csiChsMaskForReco, m_config.csi_n_channels * sizeof(uint8_t));
    // Masked noisy channels (pedestal suppression)
    cudaMalloc((void **)&m_d_csiChsMaskForPedSup, m_config.csi_n_channels * sizeof(uint8_t));
    // Gain mean coefficients (ADC counts to MeV)
    cudaMalloc((void **)&m_d_gainMeanCoefs, m_config.csi_n_channels * sizeof(float));
    // Constant for small and big crystals
    cudaMalloc((void **)&m_d_ADCMevCoefs, m_config.csi_n_channels * sizeof(float));

    // load above data into GPU
    cudaMemcpy(m_d_CSIDetChIdToADCChId, m_csi_info->GetAbsChIdPtr(), m_config.csi_n_channels * sizeof(uint16_t), cudaMemcpyHostToDevice);
    cudaMemcpy(m_d_gainMeanCoefs, m_csi_info->getGainMean().data(), m_config.csi_n_channels * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(m_d_ADCMevCoefs, m_csi_info->getMeVCoeff().data(), m_config.csi_n_channels * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(m_d_csiChsMaskForReco, m_csi_info->getChMaskForReco().data(), m_config.csi_n_channels * sizeof(uint8_t), cudaMemcpyHostToDevice);
    cudaMemcpy(m_d_csiChsMaskForPedSup, m_csi_info->getChMaskForPedSup().data(), m_config.csi_n_channels * sizeof(uint8_t), cudaMemcpyHostToDevice);


    for (u_int i_thread = 0; i_thread < m_config.n_pcap_threads; i_thread++)
    {
        m_packetsBeingProcessed[i_thread] = 0;
    }

    m_futures = std::vector<std::queue<std::future<uint32_t>>>(m_config.n_pcap_threads);
    m_recvdPktsPerQueue.resize(m_config.n_pcap_threads, 0);
    m_spillNo = 0;
    m_prevSpillNo = 0xffff-1;
}

Process::~Process()
{
    delete m_csi_info;

    for (u_int i = 0; i < m_fullDetectorList.size(); i++)
    {
        delete m_fullDetectorList.at(i);
    }
    m_fullDetectorList.clear();
}


void Process::AddDetector(const std::string name,
                          const bool is_500MHz,
                          const std::string ext_map_file)
{
    DetectorInfo *det = new DetectorInfo(m_config); // TODO: Is this duplcated?
    det->m_name = name;
    det->SetIs500MHz(is_500MHz);
    det->m_ext_map_file = ext_map_file;

    if (det->Is500MHz())
    {
        det->SetSamplesPerWfm(256);
    }
    else
    {
        det->SetSamplesPerWfm(64);
    }

    m_fullDetectorList.push_back(det);
}

int Process::Initialize()
{
    if (SetUpDetectors() != 0)
    {
        return 1;
    }

    for (int i_stream = 0; i_stream < N_CUDA_STREAMS; i_stream++)
    {
        m_clueAlgo[i_stream] = new CLUEAlgoGPU();
        
        m_clueAlgo[i_stream]->SetClusterParams(REC_RECO_5G, m_config.clus_cfg_5g.dc, m_config.clus_cfg_5g.deltao, m_config.clus_cfg_5g.deltac, m_config.clus_cfg_5g.rhoc);  
        m_clueAlgo[i_stream]->SetClusterParams(REC_RECO_KP, m_config.clus_cfg_kp.dc, m_config.clus_cfg_kp.deltao, m_config.clus_cfg_kp.deltac, m_config.clus_cfg_kp.rhoc);
        m_clueAlgo[i_stream]->SetClusterParams(REC_RECO_PI0EE, m_config.clus_cfg_pi0ee.dc, m_config.clus_cfg_pi0ee.deltao, m_config.clus_cfg_pi0ee.deltac, m_config.clus_cfg_pi0ee.rhoc);        
    }

    for (int i = 0; i < 8; i++)
    {
        m_gpuDataList[i] = new GPUData();
        m_gpuDataList[i]->AllocateDeviceMemory(m_config.csi_n_channels);
    }

    m_gpu_csi_info = new GPUDetectorInfo();
    m_gpu_csi_info->AllocateDeviceMemory(m_config.csi_n_channels);
    cudaMemcpy(m_gpu_csi_info->d_csi_x, m_csi_info->m_ChIdX.data(), m_config.csi_n_channels * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(m_gpu_csi_info->d_csi_y, m_csi_info->m_ChIdY.data(), m_config.csi_n_channels * sizeof(int), cudaMemcpyHostToDevice);
    return 0;
}

DetectorInfo *Process::GetDetectorInfo(const std::string name)
{
    for (u_int i = 0; i < m_fullDetectorList.size(); i++)
    {
        if (m_fullDetectorList.at(i)->m_name == name)
        {
            return m_fullDetectorList.at(i);
        }
    }
    printf("ERROR: Detector %s not found\n", name.c_str());
    return nullptr;
}

int Process::SetUpDetectors()
{
    std::string fin_name = "/home/koto/git-repos/l3-sw/config/DetectorList.txt";

    std::ifstream ifile;
    ifile.open(fin_name.c_str());
    if (!ifile)
    {
        std::cout << "cannot open file '" << fin_name << "'" << std::endl;
        return 1;
    }

    std::string line;
    while (std::getline(ifile, line))
    {
        // std::cout << line << std::endl;
        std::replace(std::begin(line), std::end(line), '\t', ' ');
        std::vector<std::string> words = split(line, ' ');

        if (words.size() == 0)
        {
            continue;
        }
        else if (words.size() == 1)
        {
            printf("---------%s\n", line.c_str());
        }
        else if (words.size() == 2)
        {
            // AddDetector(words.at(0), words.at(1));
            printf("here be dragons");
        }
        else if (words.size() == 3)
        {
            AddDetector(words.at(0), stoi(words.at(1)), words.at(2));
        }
        else
        {
            printf("Hi!! You got an error right here\n");
        }
    }
    ifile.close();

    // Add only the detectors useful for online selection
    // CSI - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    m_csi_info = GetDetectorInfo("CSI");
    m_csi_info->m_calibFile = m_config.csi_calib_file;
    m_csi_info->m_chPosFile = m_config.csi_ch_pos_file;
    m_csi_info->m_nChannels = m_config.csi_n_channels;
    if (m_csi_info->ReadCalibrationData() != 0)
    {
        printf("ERROR in reading calibration data\n");
        return 1;
    }
    if (m_csi_info->ReadCHPositions() != 0)
    {
        printf("ERROR in reading CSI ch positions\n");
        return 1;
    }
    if (m_csi_info->fillChMaskFromCfg() != 0)
    {
        l3log.log(L3_QUIET, L3_ERROR, "Got an error while parsing CSI Mask from config\n");
        return 1;
    }
    if (m_csi_info->fillMapperListFromTxt() != 0)
    {
        l3log.log(L3_QUIET, L3_ERROR, "error in parsing CSI mapper list\n");
        return 1;
    }

    // Other detectors (e.g. cv), may be initialized here

    return 0;
}

int Process::loadADCData(uint16_t *perFADCWfmData, FADCHeaderData &FADCHeaderData, uint8_t *srcPtr, int nFADCs, int firstFADCIdx, int i_event)
{
    int error_status = 0;

    nvtxRangePush("loadADCData");
    for (int i_fadc = 0; i_fadc < nFADCs; i_fadc++)
    {
        int glob_fadc_idx = firstFADCIdx + i_fadc;

        //printf("Loading ADC %d\n", i_fadc);
//        for (int i = 0; i < 6; i++)
//        {
//            //FADCHeaderData.data[i_event][i_fadc][i] = srcPtr[2 * i + 1] << 8 | srcPtr[2 * i]; // tmp
//
//            //FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + ADC_HDRS_O + i_fadc * 6 + i] = (srcPtr[2 * i] << 8) | srcPtr[2 * i + 1];
//
//            //printf("%x ", FADCHeaderData.data[i_fadc][i]);
//        }
        for (int i = 0; i < 12; i++)
        {
            FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + ADC_HDRS_O + i_fadc * 6*2 + i] = srcPtr[i];
        }
        ///printf("\n");

        ////print first 16 bytes
        // for (int i = 0; i < 32; i++){
        //     printf("%02x ", i_fadc_ptr[i]);
        // }
        // printf("\n");

        // Get FADC header data

        // TODO: Get this from config?
        // TODO: 16-bit words are transposed!!
        //uint8_t crateNo = FADCHeaderData.data[i_event][i_fadc][0] & 0x1F;
        //uint8_t moduleID = (srcPtr[1] << 8 | srcPtr[0]) >> 5 & 0x1F;
        //uint8_t crateNo = (srcPtr[1] << 8 | srcPtr[0]) & 0x1F;

        uint8_t moduleID = (FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + ADC_HDRS_O + i_fadc * 6*2 + 1] & 0x03) << 3 | (FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + ADC_HDRS_O + i_fadc * 6*2 + 0] >> 5 & 0x07);
        uint8_t crateNo = FADCHeaderData .evtHdrData[i_event * EVT_HDR_SIZE + ADC_HDRS_O + i_fadc * 6*2 + 0] & 0x1F;

        //uint16_t eventNo = (FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + ADC_HDRS_O + i_fadc * 6*2 + 4] & 0xFF) << 8 | (FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + ADC_HDRS_O + i_fadc * 6*2 + 3] & 0x3F) << 2 | (FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + ADC_HDRS_O + i_fadc * 6*2 + 2] >> 6 & 0x3);
        ////uint16_t spillNo = (FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + ADC_HDRS_O + i_fadc * 6*2 + 3] & 0x3F) << 4 | (FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + ADC_HDRS_O + i_fadc * 6*2 + 1] >> 2 & 0xF);
        //printf("moduleID: %d, crateNo: %d, eventNo: %d, spillNo: %d\n", moduleID, crateNo, eventNo, spillNo);


        //FADCHeaderData.crateNo[i_event][i_fadc] = crateNo;
        //FADCHeaderData.moduleID[i_event][i_fadc] = moduleID;

        // printf("crateNo: %d, moduleID: %d\n", crateNo, moduleID);

        // TODO: Get the 16 from config
        // ModuleID and crateNo (actually the whole ADC Header + Data) will be Fs if the ADC is missing.

        if (!MASK_ALL_ADCS)
        {
            if (crateNo * 16 + moduleID != firstFADCIdx + i_fadc)
            {
                //if ((moduleID != 0b11111)
                if (1)
                {
                    //if (crateNo != 0 && moduleID != 0)
                    if (1)
                    {
                        l3log.log(L3_NORMAL, L3_ERROR, "FADCs not coming in order. Expected ID %d (crate %d, module %d), got %d (crate %d, module %d)\n", firstFADCIdx + i_fadc, (firstFADCIdx + i_fadc) / 16, (firstFADCIdx + i_fadc) % 16, crateNo * 16 + moduleID, crateNo, moduleID);
                        error_status = 1;
                    }
                }
            }
        }

        //printf("srcPtr + m_config.adc_header_size: %p\n", srcPtr + m_config.adc_header_size);
        //printf("srcPtr + m_config.adc_header_size + 16 * 64 * 2: %p\n", srcPtr + m_config.adc_header_size + 16 * 64 * 2);
        //printf("perFADCWfmData : %p\n", (uint8_t *)perFADCWfmData);
        //printf("Im here, copying event %d; fadc %d\n", i_event, i_fadc);


        //perFADCWfmData[0] = 0;
        //printf("\n");

        //printf("Im here now!\n");

        std::copy(srcPtr + m_config.adc_header_size, srcPtr + m_config.adc_header_size + 16 * 64 * 2, (uint8_t *)(perFADCWfmData + i_event * 288 * 16 * 64 + glob_fadc_idx * 16 * 64));
        

        if ((glob_fadc_idx + 1) % 16 == 0)
        {
            // Last module of a crate. Skip crate footer.
            // printf("Skipping crate footer for module %d\n", glob_fadc_idx);
            srcPtr += m_config.crate_footer_size;
        }

        srcPtr += m_config.adc_header_size + 16 * 64 * 2 + m_config.adc_footer_size;
    }
    nvtxRangePop();
    //printf("Loaded All ADCs\n");
    return error_status;
}

void Process::Debug(int i_pcap_thread, uint16_t perFADCWfmData[][16][64], FADCHeaderData &FADCHeaderData, int refEventID)
{

    std::string filename_data = "pcap_thread_" + std::to_string(i_pcap_thread) + "_evt_" + std::to_string(refEventID) + "_ADCData.txt";
    std::string filename_hdr = "pcap_thread_" + std::to_string(i_pcap_thread) + "_evt_" + std::to_string(refEventID) + "_ADCHeader.txt";

    std::ofstream ofile_data(filename_data);
    std::ofstream ofile_hdr(filename_hdr);

    for (int i_fadc = 0; i_fadc < 288; i_fadc++)
    {
        ofile_hdr << "# FADC " << i_fadc << std::endl;
        for (int i = 0; i < 6; i++)
        {
            ofile_hdr << std::hex << FADCHeaderData.data[i_fadc][i] << " ";
        }
        ofile_hdr << std::endl;
    }

    for (int i_fadc = 0; i_fadc < 288; i_fadc++)
    {
        ofile_data << "# FADC " << i_fadc << std::endl;
        for (int i_sample = 0; i_sample < 64; i_sample++)
        {
            for (int i_ch = 0; i_ch < 16; i_ch++)
            {
                ofile_data << std::hex << perFADCWfmData[i_fadc][i_ch][i_sample] << " ";
            }
            ofile_data << std::endl;
        }
        ofile_data << std::endl;
    }

    ofile_data.close();
    ofile_hdr.close();

    printf("Wrote ADC data to %s\n", filename_data.c_str());
}

uint32_t Process::PrepareGPUData(uint32_t &totalProcessedPackets, int cudaStreamID, u_int nPackets, int firstPktIdx, std::vector<uint16_t> &eventIDs, std::vector<uint8_t> &scaledTrigBits, FADCHeaderData &FADCHeaderData, uint16_t &currSpillNo)
{
    nvtxRangePush("PrepareGPUData");

    // 8 cuda streams are set up, one for each pcap thread and for each pcap buffer
    int i_pcap_thread = cudaStreamID;
    int i_event = 0;

    uint16_t refSpillNo = 0;
    uint16_t refEventID = 0;

    while (totalProcessedPackets < nPackets)
    {

        if (i_event >= MAX_NEVENTS_PER_STREAM)
        {
            l3log.log(L3_DEBUG, L3_INFO, "INFO: i_event %d will be left for the next block.\n", i_event);
            break;
        }

        bool dropThisEvent = false;
        int firstPktIdxInBuff = m_circularBufferManager->at(cudaStreamID)->GetIdxFromIdx(firstPktIdx, totalProcessedPackets);

        uint8_t *i_fadc_ptr = m_rawPktDataBuffer->at(cudaStreamID)->at(firstPktIdxInBuff);
        uint16_t firstHdrWord = i_fadc_ptr[1] << 8 | i_fadc_ptr[0];
        uint16_t secondHdrWord = i_fadc_ptr[3] << 8 | i_fadc_ptr[2];
        uint16_t thirdHdrWord = i_fadc_ptr[5] << 8 | i_fadc_ptr[4];


        int packetID = m_rawPktHdrBuffer->at(cudaStreamID)->at(firstPktIdxInBuff).pktNo;
        

        if (packetID == 0)
        {
            refSpillNo = ((secondHdrWord & 0x3F) << 4) + ((firstHdrWord >> 10) & 0xF);
            refEventID = ((thirdHdrWord & 0xFF) << 8) | ((secondHdrWord >> 6) & 0xFF);

            if (refSpillNo != currSpillNo)
            {
                if (currSpillNo != 0xffff){
                    l3log.log(L3_VERBOSE, L3_DEBUG, "Got new spillNo %d, previous spillNo %d\n", refSpillNo, currSpillNo);
                }
                currSpillNo = refSpillNo;
                break;
            }
        }
        else
        {
            // printf("ERROR: (thread %d) Expected first packet of event %d is actually packet No. %d.\n", i_pcap_thread, refEventID, packetID);


            l3log.log(L3_NORMAL, L3_ERROR, "(thread %d) Expected first packet of current event is actually packet No. %d.\n", cudaStreamID, packetID);



            int skippedPackets = 0;
            for (int i_pkt = 0; i_pkt < PKTS_PER_EVT - packetID + 1; i_pkt++)
            {
                // l3log.log(0, L3_ERROR, "i+pkt = %d, PKTS_PER_EVT - packetID + 1 = %d\n", i_pkt, PKTS_PER_EVT - packetID + 1);
                int thisPktIdx = m_circularBufferManager->at(cudaStreamID)->GetIdxFromIdx(firstPktIdx, totalProcessedPackets + i_pkt);
                int thisPacketID = m_rawPktHdrBuffer->at(cudaStreamID)->at(thisPktIdx).pktNo;
                // printf("Packet %d has ID %d\n", i_pkt, thisPacketID);

                skippedPackets = i_pkt;
                if (thisPacketID == 0)
                {
                    l3log.log(L3_VERBOSE, L3_DEBUG, "Found next packet 0 after %d packets\n", i_pkt);
                    break;
                }
            }
            totalProcessedPackets += skippedPackets;
            continue;
        }
        
        //l3log.log(L3_NORMAL, L3_DEBUG, "RefEventID: %d, RefSpillNo: %d\n", refEventID, refSpillNo);

        // check that all packets in evt look good
        int thisPktIdx = 0; // id of last gfood packet
        for (int i_pkt = 0; i_pkt < PKTS_PER_EVT; i_pkt++)
        {
            thisPktIdx = m_circularBufferManager->at(cudaStreamID)->GetIdxFromIdx(firstPktIdx, totalProcessedPackets);
            // printf("totalProcessedPackets = %d, shift = %d, i_pkt = %d, thisPktIdx = %d\n", totalProcessedPackets, shift, i_pkt, thisPktIdx);
            //  printf("DEBUG: This packet index is %d, %d / %d\n", i_pkt, thisPktIdx, m_circularBufferManager->at(i_pcap_thread)->GetCapacity());
            if (thisPktIdx == m_circularBufferManager->at(cudaStreamID)->GetCapacity() - 1)
            {
                l3log.log(L3_VERBOSE, L3_DEBUG, "(thread %d) Event %d goes over the buffer boundary.\n", cudaStreamID, refEventID);

                // Copy the first packet to the end of the buffer, so we can read the last ADC form a contiguous block of memory.
                std::copy(m_rawPktDataBuffer->at(cudaStreamID)->at(0), m_rawPktDataBuffer->at(cudaStreamID)->at(0) + PKT_SIZE - OFC2_HDR_SIZE, m_rawPktDataBuffer->at(cudaStreamID)->at(m_circularBufferManager->at(cudaStreamID)->GetCapacity()));
            }
            // packet = m_rawPktDataBuffer->at(i_pcap_thread)->at(thisPktIdx);

            totalProcessedPackets++;
            if (i_pkt != m_rawPktHdrBuffer->at(cudaStreamID)->at(thisPktIdx).pktNo)
            {
                l3log.log(L3_NORMAL, L3_ERROR, "(thread %d) lost one packet in event %d (spill %d). Expected packet %d, got packet %d instead.\n", cudaStreamID, refEventID, refSpillNo, i_pkt, m_rawPktHdrBuffer->at(cudaStreamID)->at(thisPktIdx).pktNo);
                dropThisEvent = true;
                // badPktID = i_pkt;
                break;
            }
        }
        if (dropThisEvent)
        {
            // Packets from thisPktIdx until the next event are dropped at the beg. of the next iteration.
            l3log.log(L3_QUIET, L3_WARN, "Dropping evt %d\n", refEventID);
            continue;
        }

        // From this point, load FADC data into m_h_perFADCWfmData
        int adcSize = m_config.adc_header_size + 16 * 64 * 2 + m_config.adc_footer_size;

        int firstBlockNADCs = 288;
        int secondBlockNADCs = 0;

        // If this event goes over the buffer boundary
        if (i_fadc_ptr + 288 * adcSize > m_rawPktDataBuffer->at(i_pcap_thread)->at(m_circularBufferManager->at(i_pcap_thread)->GetCapacity()))
        {
            // Copy the first packet to the end of the buffer, so we can read the last ADC form a contiguous block of memory.
            std::copy(m_rawPktDataBuffer->at(i_pcap_thread)->at(0), m_rawPktDataBuffer->at(i_pcap_thread)->at(0) + PKT_SIZE - OFC2_HDR_SIZE, m_rawPktDataBuffer->at(i_pcap_thread)->at(m_circularBufferManager->at(i_pcap_thread)->GetCapacity()));

            // Get the number of fADCs in the second block
            firstBlockNADCs = (m_rawPktDataBuffer->at(i_pcap_thread)->at(m_circularBufferManager->at(i_pcap_thread)->GetCapacity()) - i_fadc_ptr) / adcSize + 1;
            secondBlockNADCs = 288 - firstBlockNADCs;

            //printf("DEBUG: Event %d goes over the buffer boundary.\n", refEventID);
            //printf("DEBUG: First block has %d ADCs, second block has %d ADCs.\n", firstBlockNADCs, secondBlockNADCs);
        }

        int err_status = loadADCData(m_h_perFADCWfmData[i_pcap_thread], FADCHeaderData, i_fadc_ptr, firstBlockNADCs, 0, i_event);
        if (err_status == 1)
        {
            l3log.log(0, L3_ERROR, "loadADCData exited with errors!\n");
        }
        int sizeProcessed = i_fadc_ptr - m_rawPktDataBuffer->at(i_pcap_thread)->at(m_circularBufferManager->at(i_pcap_thread)->GetCapacity());

        if (secondBlockNADCs > 0)
        {
            l3log.log(L3_VERBOSE, L3_DEBUG, "Loading second block of ADCs for event %d\n", refEventID);
            i_fadc_ptr = m_rawPktDataBuffer->at(i_pcap_thread)->at(0) + sizeProcessed + firstBlockNADCs * adcSize + (int)firstBlockNADCs / 16 * m_config.crate_footer_size;

            int err_status = loadADCData(m_h_perFADCWfmData[i_pcap_thread], FADCHeaderData, i_fadc_ptr, secondBlockNADCs, firstBlockNADCs, i_event);

            if (err_status == 1)
                printf("loadADCData exited with errors for event %d!\n", refEventID);
        }


        // One event has been loaded. Now we copy it to the GPU asynchronously,
        // while the next event is loaded.
        cudaMemcpyAsync(m_d_perFADCWfmData[cudaStreamID] + i_event * 288 * 16 * 64, m_h_perFADCWfmData[i_pcap_thread] + i_event * 288 * 16 * 64, 288 * 16 * 64 * sizeof(uint16_t), cudaMemcpyHostToDevice, m_streams[cudaStreamID]);


        uint8_t scaledTrigBit = m_h_perFADCWfmData[i_pcap_thread][i_event*288*16*64 + m_config.trigger_adc_ifadc*16*64 + m_config.scaled_trig_bit_ch*64 + m_config.scaled_trig_bit_sample];





        FADCHeaderData.recoTag[i_event] = GetRecoTagFromScaledTiggerBit(scaledTrigBit);
        l3log.log(L3_VERBOSE, L3_INFO, "Event %d is tagged as %d\n", refEventID, FADCHeaderData.recoTag[i_event]);


        // 1% of the events won't be reconstructed, and will pass directly. These are used for the trigger efficiency study.
        
        if (DEBUG_SELECTION){
            if (FADCHeaderData.recoTag[i_event] < SEL_THRESHOLD)
            {
                FADCHeaderData.recoTag[i_event] |= REC_LUCKY_BIT;
            }
        }
        else if (refEventID % 100 == 0)
        {
            FADCHeaderData.recoTag[i_event] |= REC_LUCKY_BIT;
            l3log.log(L3_VERBOSE, L3_INFO, "Event %d is re-tagged as LUCKY\n", refEventID);
        }


        //l3log.log(L3_QUIET, L3_INFO, "Event %d is tagged as %d (scaledTrigBit %d)\n", refEventID, FADCHeaderData.recoTag[i_event], scaledTrigBit);


        FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + TRIG_BIT_O] = scaledTrigBit;

        // TODO: Make recoTag uint8
        FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + L3_TAG_O] = FADCHeaderData.recoTag[i_event];

        FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + EVT_NO_O + 0] = (refEventID & 0xFF00) >> 8;
        FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + EVT_NO_O + 1] = refEventID & 0x00FF;

        FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + SPILL_NO_O + 0] = (refSpillNo & 0xFF00) >> 8;
        FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + SPILL_NO_O + 1] = refSpillNo & 0x00FF;

        FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + RUN_NO_O + 0] = (m_config.run_number & 0xFF00) >> 8;
        FADCHeaderData.evtHdrData[i_event * EVT_HDR_SIZE + RUN_NO_O + 1] = m_config.run_number & 0x00FF;

        eventIDs.at(i_event) = refEventID;
        scaledTrigBits.at(i_event) = scaledTrigBit;


        i_event++;
    }

    nvtxRangePop();
    return i_event;
}

#if D_ENABLE_CPU_DEBUG
void Process::DebugReconstruction(int nLoadedEvents, std::vector<uint16_t> &eventIDs, std::vector<uint8_t> &scaledTrigBits, int cudaStreamID, int spillNo, FADCHeaderData &FADCHeaderData)
{

    cudaDeviceSynchronize();
    launchCalcTotalEKernel(m_d_energies[cudaStreamID], m_gpuDataList[cudaStreamID]->d_csi_energies, m_gpuDataList[cudaStreamID]->d_csi_times, m_config.csi_n_channels, nLoadedEvents,  m_streams[cudaStreamID]);

    Selector selector(m_config);

    cudaMemcpy(m_h_clusters[cudaStreamID]->n, m_d_clusters[cudaStreamID]->n, nLoadedEvents * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(m_h_clusters[cudaStreamID]->x, m_d_clusters[cudaStreamID]->x, nLoadedEvents * MAX_CSI_CLUS_PER_EVT * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(m_h_clusters[cudaStreamID]->y, m_d_clusters[cudaStreamID]->y, nLoadedEvents * MAX_CSI_CLUS_PER_EVT * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(m_h_clusters[cudaStreamID]->E, m_d_clusters[cudaStreamID]->E, nLoadedEvents * MAX_CSI_CLUS_PER_EVT * sizeof(float), cudaMemcpyDeviceToHost);
    
    float h_energies[MAX_NEVENTS_PER_STREAM];
    cudaMemcpy(h_energies, m_d_energies[cudaStreamID], nLoadedEvents * sizeof(float), cudaMemcpyDeviceToHost);

    // cpy back recoTag from gpu
    cudaMemcpy(FADCHeaderData.recoTag, m_d_recoTag[cudaStreamID], nLoadedEvents * sizeof(uint16_t), cudaMemcpyDeviceToHost);


    // TODO: Here, do final reco and evt selection on CPU.
    int tmp_i_layer = 0;
    int tmp_flag = 0;
    for (int i_evt = 0; i_evt < nLoadedEvents; i_evt++)
    {

        uint8_t nCdtClusters = m_h_perFADCWfmData[cudaStreamID][i_evt*288*16*64 + m_config.trigger_adc_ifadc*16*64 + m_config.n_cdt_clus_ch*64 + m_config.n_cdt_clus_sample] & 0b00001111;
        FADCHeaderData.nCDTClusters[i_evt] = nCdtClusters;

        // printf("Event %d has %d L3 clusters; %d CDT clusters\n", eventIDs.at(i_evt), m_h_clusters[cudaStreamID]->n[tmp_i_layer], FADCHeaderData.nCDTClusters[i_evt]);

        // float minClusDistance;
        // selector.ComputeMinClusDistance(tmp_i_layer, m_h_clusters[cudaStreamID], minClusDistance);

        float clusCOE;
        float clusTotalE;
        float minClusE;
        float minXY;
        float maxR;
        selector.ComputeClusECOE(tmp_i_layer, m_h_clusters[cudaStreamID], clusTotalE, minClusE, clusCOE, minXY, maxR);

        // for inefficiency study of online reco
        // printf("Evt %d: %d L3 clusters; %d CDT clusters; recoTag %d\n", eventIDs.at(i_evt), m_h_clusters[cudaStreamID]->n[tmp_i_layer], FADCHeaderData.nCDTClusters[i_evt], FADCHeaderData.recoTag[i_evt]);
        if ((FADCHeaderData.recoTag[i_evt] & 0x00ff) < SEL_THRESHOLD || (FADCHeaderData.recoTag[i_evt] > REJ_THRESHOLD) || (FADCHeaderData.recoTag[i_evt] & REC_LUCKY_BIT))
        {
            printf("awkdata %d %d %d %d %f %f %f %f\n", eventIDs.at(i_evt), spillNo, FADCHeaderData.recoTag[i_evt], scaledTrigBits.at(i_evt), h_energies[i_evt], clusCOE, minXY, maxR);
            //if (h_energies[i_evt] == 0)
            //{
            //    exit(1);
            //}
        }

        if (eventIDs.at(i_evt) == 1460)
        {
            printf("Event %d has %d L3 clusters; %d CDT clusters\n", eventIDs.at(i_evt), m_h_clusters[cudaStreamID]->n[tmp_i_layer], FADCHeaderData.nCDTClusters[i_evt]);
            tmp_flag = 1;
        }

        tmp_i_layer++;
    }

    // CPU VERSION of the selection
    DetectorData csi_data = DetectorData(GetDetectorInfo("CSI"));
    SetWfmData(csi_data, m_h_perFADCWfmData[cudaStreamID], nLoadedEvents);
    std::vector<DetectorData> detectorDataList = {csi_data};
    selector.SetRawEntryID(-1);
    selector.Select(detectorDataList, nLoadedEvents, eventIDs, FADCHeaderData.recoTag, spillNo, FADCHeaderData.nCDTClusters, m_h_clusters[cudaStreamID]->n, m_h_clusters[cudaStreamID]->x, m_h_clusters[cudaStreamID]->y);

    PrintGPUData(nLoadedEvents, *m_gpuDataList[cudaStreamID], *m_h_clusters[cudaStreamID], eventIDs, m_d_recoTag[cudaStreamID]);

    if (tmp_flag == 1)
    {
        //exit(1);
    }

}
#endif

void Process::ReconstructAndSelect(int nLoadedEvents, int cudaStreamID)
{
    if (nLoadedEvents == 0)
    {
        l3log.log(L3_QUIET, L3_INFO, "Launched ReconstructAndSelect with 0 events!\n");
        return;
    }

    LaunchReconstructionKernel(m_d_perFADCWfmData[cudaStreamID], m_d_wfmInfo[cudaStreamID], m_gpuDataList[cudaStreamID]->d_csi_energies, m_gpuDataList[cudaStreamID]->d_csi_times, m_d_CSIDetChIdToADCChId, m_d_gainMeanCoefs, m_d_ADCMevCoefs, m_config.csi_n_channels, m_streams[cudaStreamID], nLoadedEvents, m_d_recoTag[cudaStreamID], m_d_csiChsMaskForReco);

    m_clueAlgo[cudaStreamID]->SetPointsFromGPU(m_gpuDataList[cudaStreamID], m_gpu_csi_info, m_config.csi_n_channels, m_streams[cudaStreamID], nLoadedEvents, m_d_recoTag[cudaStreamID], m_config.wfm_time_window);
    m_clueAlgo[cudaStreamID]->makeClusters(m_streams[cudaStreamID], m_d_clusters[cudaStreamID], m_d_recoTag[cudaStreamID], nLoadedEvents);

    launchSelectKernel(m_d_clusters[cudaStreamID], m_d_recoTag[cudaStreamID], m_streams[cudaStreamID], nLoadedEvents);
}

void Process::PrintGPUData(int nLoadedEvents, GPUData &gpudata, Clusters &clusters, std::vector<uint16_t> &eventIDs, uint16_t *d_recoTag)
{
    nvtxRangePush("PrintGPUData");

    float *h_csi_energies = new float[MAX_NEVENTS_PER_STREAM * m_config.csi_n_channels];
    float *h_csi_pedestals = new float[MAX_NEVENTS_PER_STREAM * m_config.csi_n_channels];
    float *h_csi_intADCs = new float[MAX_NEVENTS_PER_STREAM * m_config.csi_n_channels];
    float *h_csi_peakHeight = new float[MAX_NEVENTS_PER_STREAM * m_config.csi_n_channels];
    float *h_clus_E = new float[MAX_NEVENTS_PER_STREAM * MAX_CSI_CLUS_PER_EVT];
    int *h_clus_n = new int[MAX_NEVENTS_PER_STREAM];
    uint16_t *h_recoTag = new uint16_t[MAX_NEVENTS_PER_STREAM];

    cudaMemcpy(h_csi_energies, gpudata.d_csi_energies, nLoadedEvents * m_config.csi_n_channels * sizeof(float), cudaMemcpyDeviceToHost);
    //cudaMemcpy(h_csi_pedestals, gpudata.d_tmp_csi_pedestals, nLoadedEvents * m_config.csi_n_channels * sizeof(float), cudaMemcpyDeviceToHost);
    //cudaMemcpy(h_csi_intADCs, gpudata.d_tmp_csi_intADCs, nLoadedEvents * m_config.csi_n_channels * sizeof(float), cudaMemcpyDeviceToHost);
    //cudaMemcpy(h_csi_peakHeight, gpudata.d_tmp_csi_peakHeight, nLoadedEvents * m_config.csi_n_channels * sizeof(float), cudaMemcpyDeviceToHost);

    cudaMemcpy(h_clus_E, clusters.E, nLoadedEvents * MAX_CSI_CLUS_PER_EVT * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_clus_n, clusters.n, nLoadedEvents * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_recoTag, d_recoTag, nLoadedEvents * sizeof(uint16_t), cudaMemcpyDeviceToHost);

    for (int i_evt = 0; i_evt < nLoadedEvents; i_evt++)
    {
        if ((h_recoTag[i_evt] & 0x00ff) < REC_PEDSUP || h_recoTag[i_evt] > REJ_THRESHOLD)
        {
            float totalE = 0;


            for (int i_chi_ch = 0; i_chi_ch < m_config.csi_n_channels; i_chi_ch++)
            {
                totalE += h_csi_energies[i_evt * m_config.csi_n_channels + i_chi_ch];
                // evtid, chid, chE, chPed, chIntADC, chPeakHeight
                //printf("awkdata %d %d %f %f %f %f\n", eventIDs.at(i_evt), i_chi_ch, h_csi_energies[i_evt * m_config.csi_n_channels + i_chi_ch], h_csi_pedestals[i_evt * m_config.csi_n_channels + i_chi_ch], h_csi_intADCs[i_evt * m_config.csi_n_channels + i_chi_ch], h_csi_peakHeight[i_evt * m_config.csi_n_channels + i_chi_ch]);
                //printf("awkdata %d %d %f %f\n", eventIDs.at(i_evt), i_chi_ch, h_csi_energies[i_evt * m_config.csi_n_channels + i_chi_ch], h_csi_intADCs[i_evt * m_config.csi_n_channels + i_chi_ch]);
            }


        }


        //printf("evtID: %d; recoTag %d\n" , eventIDs.at(i_evt), h_recoTag[i_evt]);
        // printf("awkdataGPU %d %d %f\n", eventIDs.at(i_evt), h_clus_n[i_evt], totalE);
        // printf("awkdataGPU %d %f %f %f\n", eventIDs.at(i_evt), h_csi_energies[i_evt * m_config.csi_n_channels + 0], 0.0, 0.0);
    }

    delete[] h_csi_energies;
    delete[] h_csi_pedestals;
    delete[] h_csi_intADCs;
    delete[] h_csi_peakHeight;
    delete[] h_clus_E;
    delete[] h_clus_n;
    delete[] h_recoTag;

    nvtxRangePop();

}

void Process::WriteEvtHeader(uint8_t *_compressedDataPtr, uint8_t scaledTrigBit, int spillNo, uint16_t* EventHeaderDataPtr)
{
    nvtxRangePush("WriteEvtHeader");

    // trigger bit
    _compressedDataPtr[4] = scaledTrigBit;

    // Tag
    _compressedDataPtr[5] = 0xee;

    // Event No
    _compressedDataPtr[6] = (-1 >> 8) & 0xFF;
    _compressedDataPtr[7] = -1 & 0xFF;

    // Spill No
    _compressedDataPtr[8] = (spillNo >> 8) & 0xFF;
    _compressedDataPtr[9] = spillNo & 0xFF;

    // Run No
    _compressedDataPtr[10] = (m_config.run_number >> 8) & 0xFF;
    _compressedDataPtr[11] = m_config.run_number & 0xFF;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // timestamp
    //_compressedDataPtr[4] = (timestamp >> 56) & 0xFF;
    //_compressedDataPtr[5] = (timestamp >> 48) & 0xFF;
    //_compressedDataPtr[6] = (timestamp >> 40) & 0xFF;
    //_compressedDataPtr[7] = (timestamp >> 32) & 0xFF;
    //_compressedDataPtr[8] = (timestamp >> 24) & 0xFF;
    //_compressedDataPtr[9] = (timestamp >> 16) & 0xFF;
    //_compressedDataPtr[10] = (timestamp >> 8) & 0xFF;

    // FADC headers
    std::copy(EventHeaderDataPtr, EventHeaderDataPtr + 288 * 6, (uint16_t *)&_compressedDataPtr[12]);

    nvtxRangePop();
}

uint32_t Process::ProcessEventBlock(int i_pcap_thread, int firstPktIdx, u_int nPackets, int blockID)
{

    // One cuda stream per pcap buffer
    const int cudaStreamID = i_pcap_thread;


    uint32_t totalProcessedPackets = 0;
    uint64_t totalCompressedBytes = 0;

    // Pointer to track how much of m_h_compressedDataBuffer we have filled.
    // Not used at all if performing RDMA!
    uint8_t *_compressedDataPtr;
    _compressedDataPtr = m_h_compressedDataBuffer[cudaStreamID];

    // This shift is used to re-align the pointers in case some events have
    // missing packets
    int shift = 0;


    uint16_t prevSpillNo = 0xffff-2;
    uint16_t spillNo = 0xffff;


    // These could better be in a header.
    std::vector<uint16_t> eventIDs;
    std::vector<uint8_t> scaledTrigBits;
    eventIDs.resize(MAX_NEVENTS_PER_STREAM);
    scaledTrigBits.resize(MAX_NEVENTS_PER_STREAM);


    while (totalProcessedPackets < nPackets)
    {

        // prepare some events, until a new spill is found or we finish all packets
        FADCHeaderData FADCHeaderData;
        int nLoadedEvents = PrepareGPUData(totalProcessedPackets, cudaStreamID, nPackets, firstPktIdx, eventIDs, scaledTrigBits, FADCHeaderData, spillNo);
        //l3log.log(L3_QUIET, L3_DEBUG, "Loaded %d events in block %d\n", nLoadedEvents, blockID);

        if (nLoadedEvents == 0)
        {
            if (prevSpillNo != spillNo)
            {
                l3log.log(L3_VERBOSE, L3_DEBUG, "Found next spill on the first checked event. New SpillNo = %d, previous SpillNo = %d\n", spillNo, prevSpillNo);
                prevSpillNo = spillNo;
            }
            // Found next spill on the first checked event.
            // SpillNo is upadted now, so just start again from the new spill.
            continue;
        }


        // Copy FADC header data to GPU
        cudaMemcpy(m_d_perEvtHeaderData[cudaStreamID], &FADCHeaderData.evtHdrData, nLoadedEvents * EVT_HDR_SIZE * sizeof(uint8_t), cudaMemcpyHostToDevice);
        cudaMemcpy(m_d_recoTag[cudaStreamID], &FADCHeaderData.recoTag[0], nLoadedEvents * sizeof(uint16_t), cudaMemcpyHostToDevice);

        l3log.log(L3_VERBOSE, L3_DEBUG, "%d events will be sent to GPU from thread %d\n", nLoadedEvents, cudaStreamID);

        // reconstruct and select events
        if (nLoadedEvents > 0 && m_config.do_physics)
        {
            ReconstructAndSelect(nLoadedEvents, cudaStreamID);
        }

#if D_ENABLE_CPU_DEBUG
        DebugReconstruction(nLoadedEvents, eventIDs, scaledTrigBits, cudaStreamID, prevSpillNo, FADCHeaderData);
#endif



        // Pedestal suppression
        LaunchPedestalSuppressionKernel(m_d_perFADCWfmData[cudaStreamID], m_d_wfmInfo[cudaStreamID], m_d_CSIDetChIdToADCChId, m_d_gainMeanCoefs, m_d_ADCMevCoefs, m_config.csi_n_channels, m_streams[cudaStreamID], nLoadedEvents, m_d_recoTag[cudaStreamID], m_d_csiChsMaskForPedSup);


        // pre compress
        uint32_t compBytesThisChunk = launchPreCompressKernel(m_d_perFADCWfmData[cudaStreamID], m_d_perEvtSizeCompressed[cudaStreamID], m_d_perEvtIdxCompressed[cudaStreamID], m_h_perEvtSizeCompressed[cudaStreamID], m_h_perEvtIdxCompressed[cudaStreamID], m_d_wfmInfo[cudaStreamID], m_d_compressedBytesPerWfm[cudaStreamID], m_streams[cudaStreamID], nLoadedEvents, m_d_recoTag[cudaStreamID]);

        if (compBytesThisChunk > D_COMP_ADC_DATA_BUFF_SIZE)
        {
            l3log.log(L3_QUIET, L3_WARN, "post-processed data size is too big (%d bytes) to fit in the compressed data buffer (capacity: %d bytes)!\n", compBytesThisChunk, D_COMP_ADC_DATA_BUFF_SIZE);
            l3log.log(L3_QUIET, L3_WARN, "Increase D_COMP_ADC_DATA_BUFF_SIZE and recompile.\n");
            l3log.log(L3_QUIET, L3_WARN, "Discharging %d events\n", nLoadedEvents);
            continue;
        }
 
        // Compress all events at once. Event header is added to the compressed array on GPU
        LaunchCompressKernel(m_d_perFADCWfmData[cudaStreamID], m_d_perEvtHeaderData[cudaStreamID], m_d_odata[cudaStreamID], m_d_wfmInfo[cudaStreamID], m_d_perEvtIdxCompressed[cudaStreamID], m_d_perEvtSizeCompressed[cudaStreamID], m_d_compressedBytesPerWfm[cudaStreamID], m_streams[cudaStreamID], nLoadedEvents, m_d_recoTag[cudaStreamID]);

    
        // this sync needs to be here. Otherwise, MPI might start sending the buffer before the headers are written
        cudaStreamSynchronize(m_streams[cudaStreamID]);

        l3log.log(L3_VERBOSE, L3_DEBUG, "All GPU processing finished in block %d (%d events)\n", blockID, nLoadedEvents);


        if (m_config.do_mpi)
        {
            if (DO_RDMA)
            {
                // Copy m_d_odata directly to disk node (RDMA)
                
                l3log.log(L3_VERBOSE, L3_DEBUG, "Sending %d bytes to disk node rank %d (spill %d, block %d)\n", compBytesThisChunk, m_diskNodeRanks.at(prevSpillNo % m_config.n_ranks_per_disk_node), prevSpillNo, blockID);
                MPI_Send(m_d_odata[cudaStreamID], compBytesThisChunk, MPI_BYTE, m_diskNodeRanks.at(prevSpillNo % m_config.n_ranks_per_disk_node), prevSpillNo, MPI_COMM_WORLD);

                l3log.log(L3_VERBOSE, L3_DEBUG, "Sent %d bytes to disk node (spill %d, block %d)\n", compBytesThisChunk, prevSpillNo, blockID);
            }
            else
            {
                // Is there still space in the MPI buffer? We check this before retrieving the data from the GPU.
                // If there is not enough space, send the current buffered data to the disk node and reset the buffer.
                if (totalCompressedBytes + compBytesThisChunk > MPI_BUFF_SIZE)
                {
                    if (compBytesThisChunk > MPI_BUFF_SIZE)
                    {
                        l3log.log(0, L3_ERROR, "GPU compressed data is too big (%d bytes) to fit in the MPI buffer (capacity: %d bytes)!\n", compBytesThisChunk, MPI_BUFF_SIZE);
                        l3log.log(0, L3_ERROR, "Increase MPI_BUFF_SIZE and recompile.\n");
                        exit(1);
                    }
                    l3log.log(L3_VERBOSE, L3_INFO, "[MPI] Buffer full! Sending %d bytes to disk node (spill %d, block %d)\n", totalCompressedBytes, prevSpillNo, blockID);
                    if (m_config.do_mpi)
                    {
                        MPI_Send(m_h_compressedDataBuffer[cudaStreamID], totalCompressedBytes, MPI_BYTE, m_diskNodeRanks.at(prevSpillNo % m_config.n_ranks_per_disk_node), prevSpillNo, MPI_COMM_WORLD);
                    }

                    // Reset the compressed data array
                    _compressedDataPtr = m_h_compressedDataBuffer[cudaStreamID];
                    totalCompressedBytes = 0;
                }

                cudaMemcpy(_compressedDataPtr, m_d_odata[cudaStreamID], compBytesThisChunk, cudaMemcpyDeviceToHost);
                totalCompressedBytes += compBytesThisChunk;
                _compressedDataPtr += compBytesThisChunk;
                // If there is still space, but we are already on the next spill, send the current buffered data to the disk node and reset the buffer.
                if (spillNo != prevSpillNo)
                {
                    //l3log.log(L3_QUIET, L3_DEBUG, "Done with spill %d. Sending %d bytes to disk node and moving to spill %d\n", prevSpillNo, totalCompressedBytes, spillNo);
                    if (m_config.do_mpi)
                    {
                        MPI_Send(m_h_compressedDataBuffer[cudaStreamID], totalCompressedBytes, MPI_BYTE, m_diskNodeRanks.at(prevSpillNo % m_config.n_ranks_per_disk_node), prevSpillNo, MPI_COMM_WORLD);
                        
                        
                        _compressedDataPtr = m_h_compressedDataBuffer[cudaStreamID];
                        totalCompressedBytes = 0;
                    }   
                }
            }
        }
        else{
            cudaMemcpy(_compressedDataPtr, m_d_odata[cudaStreamID], (m_h_perEvtIdxCompressed[cudaStreamID][nLoadedEvents - 1] + m_h_perEvtSizeCompressed[cudaStreamID][nLoadedEvents - 1]), cudaMemcpyDeviceToHost);
        }


        if (m_config.do_decompress)
        {
            l3log.log(L3_NORMAL, L3_WARN, "Doing decompression! Remember to disable this in production!\n");
            if (m_config.do_mpi){
                cudaMemcpy(_compressedDataPtr, m_d_odata[cudaStreamID], (m_h_perEvtIdxCompressed[cudaStreamID][nLoadedEvents-1] + m_h_perEvtSizeCompressed[cudaStreamID][nLoadedEvents-1]), cudaMemcpyDeviceToHost);
            }

            for (int i_evt = 0; i_evt < nLoadedEvents; i_evt++)
            {

                //l3log.log(L3_VERBOSE, L3_DEBUG, "Decompressing event %d\n", i_evt);
                //printf("This event starts at %d and has %d bytes\n", m_dh_perEvtIdxCompressed[cudaStreamID][i_evt], m_dh_perEvtSizeCompressed[cudaStreamID][i_evt]);
                //printf("Size of evt HDR: %d\n", EVT_HDR_SIZE);

                int evtSize =   _compressedDataPtr[m_h_perEvtIdxCompressed[cudaStreamID][i_evt] + COMP_EVT_SIZE_O + 0] << 24 |
                                _compressedDataPtr[m_h_perEvtIdxCompressed[cudaStreamID][i_evt] + COMP_EVT_SIZE_O + 1] << 16 |
                                _compressedDataPtr[m_h_perEvtIdxCompressed[cudaStreamID][i_evt] + COMP_EVT_SIZE_O + 2] << 8 |
                                _compressedDataPtr[m_h_perEvtIdxCompressed[cudaStreamID][i_evt] + COMP_EVT_SIZE_O + 3];


                if (evtSize == L3_EVT_HDR_SIZE)
                {
                    continue;
                }
                else{
                    Decompress(&m_h_perFADCWfmData[cudaStreamID][288 * 16 * 64 * i_evt], &_compressedDataPtr[m_h_perEvtIdxCompressed[cudaStreamID][i_evt] + EVT_HDR_SIZE], 288 * 16, i_evt);
                }
            }
        }


#if D_ENABLE_CPU_DEBUG
        for (int i_evt = 0; i_evt < nLoadedEvents; i_evt++)
        {

            uint8_t recoTag;
            cudaMemcpy(&recoTag, m_d_recoTag[cudaStreamID] + i_evt, sizeof(uint8_t), cudaMemcpyDeviceToHost);
            uint32_t cbytes;
            cudaMemcpy(&cbytes, m_d_compressedBytesPerWfm[cudaStreamID] + i_evt * (288 * 16 + 1) + 288 * 16, sizeof(uint32_t), cudaMemcpyDeviceToHost);

            if (recoTag >= REJ_THRESHOLD)
            {
                // printf("Event %d has been rejected; tag = %d; cbytes = %d\n", eventIDs.at(i_evt), recoTag, cbytes);
                continue;
            }
            else
            {
                // printf("Event %d has been accepted; tag = %d; cbytes = %d\n", eventIDs.at(i_evt), recoTag, cbytes);
            }
        }
#endif

        if (spillNo != prevSpillNo)
        {
            l3log.log(L3_NORMAL, L3_INFO, "Finished processing spill %d. Moving to spill %d\n", prevSpillNo, spillNo);
            prevSpillNo = spillNo;
        }
    }

    l3log.log(L3_VERBOSE, L3_INFO, "Total Skipped pkts in thread %d block %d: %d\n", i_pcap_thread, blockID, shift);
    totalProcessedPackets += shift;
    
    if (!DO_RDMA)
    {
        // Send the remaining data to the disk nodes via MPI
        if (totalCompressedBytes > 0)
        {
            // This will be the case practically always
            l3log.log(L3_VERBOSE, L3_INFO, "[MPI] Event block completed! Sending %d bytes to disk node (spill %d, block %d)\n", totalCompressedBytes, prevSpillNo, blockID);
            if (m_config.do_mpi)
            {
                MPI_Send(m_h_compressedDataBuffer[cudaStreamID], totalCompressedBytes, MPI_BYTE, m_diskNodeRanks.at(prevSpillNo % m_config.n_ranks_per_disk_node), prevSpillNo, MPI_COMM_WORLD);
            }
        }
    }
    
    return nPackets;
}

/**
 * @brief Set Wfm data for a detector for a whole event
 *
 * @param det
 * @param index_map
 * @param mapper
 * @param wfmData Contains a full evt of wfms
 */
void Process::SetWfmData(DetectorData &detData,
                         // const IndexMap &index_map,
                         // E14Mapper *mapper,
                         // uint16_t wfmData[][16][64])
                         uint16_t *wfmData, int nLoadedEvents)
{

    // printf("Set Wfm data for detector", det->m_name.c_str());

    DetectorInfo *detInfo = detData.m_info;

    // detData.m_wfmData.clear();

    // for (int i_detCh = 0; i_detCh < 4096; i_detCh++)
    const int nDetectorChannels = detInfo->m_nChannels;
    //printf("nDetectorChannels: %d\n", nDetectorChannels);

    for (int i_event = 0; i_event < nLoadedEvents; i_event++)
    {

        for (int i_detCh = 0; i_detCh < nDetectorChannels; i_detCh++)
        {
            // if (mapper->GetCrateID(i_detCh)==9999) {
            //     printf("i_detCh=%d\n", i_detCh);
            //     continue;
            // }

            //t.StartLap(8);
            const int fadcCrateID = detInfo->GetCrateID(i_detCh);
            const int fadcModuleID = detInfo->GetFadcModuleId(i_detCh);
            const int fadcChId = detInfo->GetFadcChId(i_detCh);
            // std::cout << fadcChId << std::endl;

            // if (fadcCrateID == -1 || fadcModuleID == -1 || fadcChId == -1){
            //     // Some detectors channels are missing (this is not an error...)
            //     continue;
            // }

            // int fadcID = index_map.GetIndex(fadcCrateID, fadcModuleID);
            // if (fadcID != fadcCrateID*16+fadcModuleID){
            //     if (fadcCrateID != -1 && fadcModuleID != -1){
            //         printf("detch: %d, fadcID: [%d, %d], crateid = %d, moduleid = %d \n", i_detCh, fadcID, fadcCrateID*16+fadcModuleID, fadcCrateID, fadcModuleID);
            //         exit(1);
            //     }
            // }
            int fadcID = fadcCrateID * 16 + fadcModuleID;

            // if (i_detCh == 2385)
            //     printf("Ch %d of %s corresponds to fadcCrateID %d, fadcModuleID %d, fadcChId %d, fadcID %d\n",i_detCh, detInfo->m_name.c_str(), fadcCrateID, fadcModuleID, fadcChId, fadcID);

            //////t.FinishLap(8);

            // printf("---%d, %d, %d\n", fadcCrateID, fadcModuleID, fadcID); for(const auto&
            // elem : index_map->m_IndexMap)
            //{
            //    std::cout << elem.first.first << " " << elem.first.second << " " <<
            //    elem.second << "\n";
            // }

            // printf("fadcChId: %d \n", fadcChId);

            // move this mapping thing to the initialization stage so here you only
            // load the wfm data
            //////t.StartLap(9);
            if (detInfo->Is500MHz())
            {
                std::vector<short> waveform;
                printf("Detector %s is 500MHz!!\n", detInfo->m_name.c_str());
                waveform.resize(256);
                for (int i_detCh = 0; i_detCh < 4; ++i_detCh)
                {
                    std::vector<short> raw_waveform(detInfo->GetSamplesPerWfm());
                    for (int i = 0; i < detInfo->GetSamplesPerWfm(); i++)
                    {
                        // raw_waveform[i] = wfmData[fadcID][fadcChId+i_detCh*4][i];
                    }
                    for (int i_sample = 0; i_sample < detInfo->GetSamplesPerWfm(); ++i_sample)
                    {
                        waveform[i_detCh + i_sample * 4] = raw_waveform[i_sample];
                    }
                }
                printf("Here be dragons. SOME WORK NEDED HERE!!\n");
                // detData.SetChannelWfm(i_detCh, waveform);
            }
            else
            { // 125MHz
                // waveform.resize(64);
                // m_nSamples = 64;

                // printf("fadcCrateID: %d, fadcModuleID: %d, fadcChId: %d, fadcID: %d\n", fadcCrateID, fadcModuleID, fadcChId, fadcID);
                // for (int i = 0; i < m_nSamples; i++){
                //     waveform[i] = wfmData[fadcID][fadcChId][i];
                // }
                // printf("\nSomebody come and fix this\n");
                // sleep(1);
                // t.StartLap(6);
                // waveform.insert(waveform.end(), &wfmData[fadcID][fadcChId][0], &wfmData[fadcID][fadcChId][0]+detInfo->GetSamplesPerWfm());
                // t.FinishLap(6);
                if (fadcCrateID == -1 || fadcModuleID == -1)
                {
                    // Some detectors channels are missing (this is not an error...)
                    detData.SetChannelWfmPtr(i_event, i_detCh, nullptr);
                    // detData.SetChannelWfmPtr(i_detCh, wfmData);
                    // printf("fadcCrateID: %d, fadcModuleID: %d, fadcChId: %d, fadcID: %d\n", fadcCrateID, fadcModuleID, fadcChId, fadcID);
                    continue;
                }
                // detData.SetChannelWfmPtr(i_detCh, &wfmData[fadcID][fadcChId][0]);

                if (i_detCh > 2716)
                {
                    printf("ASDASDSADASDASDASDASDSA\n");
                    exit(1);
                }
                detData.SetChannelWfmPtr(i_event, i_detCh, wfmData + i_event * 288 * 16 * 64 + fadcID * 16 * 64 + fadcChId * 64);
                // printf("Event %d; wfm[0] = %d\n", i_event, wfmData[i_event*288*16*64 + fadcID*16*64 + fadcChId*64]);
                // printf("size od waveform: %d\n", detData.m_wfmData[i_detCh].size());
                // printf("Samples per wfm: %d\n", detInfo->GetSamplesPerWfm());
            }

            //t.FinishLap(9);
        }
    }
}

/**
 * @brief compress data per FADC
 * @param wfmData  all wfms for one event, with dimensions [nFADC][nCH][nSamples] = [288][16][64]
 */
int Process::LaunchCompress(uint16_t wfmData[][16][64], uint8_t h_odata[])
{
//
//    //t.StartLap(21);
//    int *h_wfmInfo = (int *)malloc(288 * 16 * 3 * sizeof(int)); // TODO: Convert this to new
//    //t.FinishLap(21);
//
//    //t.StartLap(18);
//    int maxOutputIndex = 0;
//
//    int outputIndex = 0;
//    for (int fadc_id = 0; fadc_id < 288; fadc_id++)
//    {
//        for (int wfmN = 0; wfmN < 16; wfmN++)
//        {
//
//            int min = 65536;
//            int max = 0;
//            int elemNow;
//            for (int i = 0; i < 64; i++)
//            {
//                elemNow = wfmData[fadc_id][wfmN][i];
//                if (elemNow < min)
//                {
//                    min = elemNow;
//                }
//                if (elemNow > max)
//                {
//                    max = elemNow;
//                }
//            }
//            h_wfmInfo[fadc_id * 16 * 3 + wfmN * 3 + 0] = min;
//
//            int r = 32 - __builtin_clz(max - min);
//            r = (r < 9) ? r : 16;
//
//            h_wfmInfo[fadc_id * 16 * 3 + wfmN * 3 + 1] = r;
//            h_wfmInfo[fadc_id * 16 * 3 + wfmN * 3 + 2] = outputIndex;
//            // printf("outputIndex: %d\n", outputIndex);
//
//            if (r != 16)
//            {
//                outputIndex += 3 + 64 * r / 8;
//                // bytesPerWfm[wfmN] = 3 + nSamplesPerWfm*r/8;
//            }
//            else
//            {
//                outputIndex += 1 + 64 * r / 8; // r=16
//                // bytesPerWfm[wfmN] = 1 + nSamplesPerWfm*r/8;
//            }
//
//            if (outputIndex > maxOutputIndex)
//            {
//                maxOutputIndex = outputIndex;
//            }
//            // printf("index = %d  %d  %d\n", fadc_id*16*3 + wfmN*3+0, fadc_id, wfmN);
//            // printf("index = %d  %d  %d\n", fadc_id*16*3 + wfmN*3+1, fadc_id, wfmN);
//            // printf("index = %d  %d  %d\n", fadc_id*16*3 + wfmN*3+2, fadc_id, wfmN);
//        }
//    }
//    //////t.FinishLap(18);
//
//    // Piece of code to see full info of a specific uncompressed wfm. Useful for debugging.
//    /*
//    for (int fadc_id = 0; fadc_id < 50; fadc_id++)
//    {
//        for (int wfmN = 0; wfmN < 16; wfmN ++){
//            printf("fadc_id: %d Wfm: %d, min: %d, r: %d, outputIndex: %d\n", fadc_id, wfmN, h_wfmInfo[fadc_id*16*3 + wfmN*3+0], h_wfmInfo[fadc_id*16*3 + wfmN*3+1], h_wfmInfo[fadc_id*16*3 + wfmN*3+2]);
//
//            if (fadc_id == 48 && wfmN == 15 && m_rawEntryID == 11)
//            {
//                printf("outputIndex: %d\n", maxOutputIndex);
//                for (int i = 0; i < 64; i++)
//                {
//                    printf("unc. wfm[%d] = %d\n", i, wfmData[fadc_id][wfmN][i]);
//                }
//            }
//        }
//    }
//    */
//
//    // printf("nCompressedBytes: %d\n", outputIndex);
//    maxOutputIndex = outputIndex;
//    // for (int i = 0; i < maxOutputIndex; i++)
//    //{
//    //     h_odata[i] = 0;
//    // }
//    memset(h_odata, 0, maxOutputIndex * sizeof(uint8_t));
//
//    // compressed host array
//    // h_odata = (uint8_t *)malloc(288*maxOutputIndex*sizeof(uint8_t)); // maximum uncompressed size
//
//    // uint8_t (*h_odata)[2048] = new uint8_t[288][2048];
//    ////t.StartLap(21);
//    // h_odata = new uint8_t[288][2064]; // maximum uncompressed size. 2064 = 128*16
//
//    // h_odata = std::make_unique<uint8_t[]>(2064*288); // Might require C++17
//
//    // this might not be neccessary
//    // for (int i = 0; i < 288*2064; i++)
//    //{
//    //    h_odata[10] = 0;
//    //}
//    // exit(0);
//
//    //t.FinishLap(21);
//
//    //t.StartLap(19);
//
//    compress_cpu *compressor = new compress_cpu();
//    compressor->Compress(wfmData, h_odata, 64, h_wfmInfo, 0, 288);
//
//    //t.FinishLap(19);
//
//    //t.StartLap(20);
//
//    // Decompress. Not needed in production but useful for debugging.
//    // > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >
//    if (m_config.do_decompress)
//    {
//
//        // printf("data16 unc. size: %d\n", outputIndex);
//        // int *data16_uncompressed = (int *)malloc(288*16*64*sizeof(int));
//        int data16_uncompressed[288][16 * 64];
//
//        int byteNow = 0; // TODO: This is a temporary thing, until the contents of the header are decided!!!
//        for (int fadc_id = 0; fadc_id < 288; fadc_id++)
//        {
//            for (int wfmN = 0; wfmN < 16; wfmN++)
//            {
//
//                int r = h_odata[byteNow];
//
//                uint16_t min = (h_odata[byteNow + 1] & 0x00FF) << 8 | (h_odata[byteNow + 2] & 0x00FF);
//                int i = byteNow + 3; // Where the wfm data starts in the 8 bit array
//
//                int bit = 0;
//                int byte = wfmN * 64;
//                // printf("r: %d; byteNow: %d; min: %d; i: %d; wfmN: %d\n", r, byteNow, min, i, wfmN);
//                if (r > 8)
//                {
//                    i = i - 2;
//                    while (byte < wfmN * 64 + 64)
//                    {
//                        data16_uncompressed[fadc_id][byte] = ((h_odata[i] << 8) | (h_odata[i + 1]));
//                        i += 2;
//                        byte++;
//                    }
//                }
//                else
//                {
//                    while (byte < wfmN * 64 + 64)
//                    {
//                        if (8 - r - bit >= 0) // if the element fits into this byte
//                        {
//                            data16_uncompressed[fadc_id][byte] = ((h_odata[i] >> (8 - r - bit)) & ((1 << r) - 1)) + min;
//                            bit += r;
//                            byte++;
//                        }
//                        else
//                        {
//                            i++;
//                            data16_uncompressed[fadc_id][byte] = ((h_odata[i - 1] & ((1 << (8 - bit)) - 1)) << (r + bit - 8) | h_odata[i] >> (8 - (r + bit - 8))) + min;
//                            byte++;
//                            bit = bit + r - 8;
//                        }
//                        // if (data16_uncompressed[byte-1] != h_idata[byte-1]){
//                        //     //printf("i=%d; byte=%d; unComp=%d; original = %d\n", i, byte-1, data16_uncompressed[byte-1], h_idata[byte-1]);
//                        // }
//                    }
//                    // printf("data16_uncompressedNico[%d] = %d\n", byte-1, data16_uncompressed[byte-1]);
//                }
//
//                int bytesPerWfm;
//                if (r != 16)
//                {
//                    bytesPerWfm = 3 + 64 * r / 8;
//                }
//                else
//                {
//                    bytesPerWfm = 1 + 64 * r / 8;
//                }
//                byteNow += bytesPerWfm;
//
//                // Comprobation
//                for (int j = 0; j < 64; j++)
//                {
//                    // printf("info: iFADC:%d wfmN:%d, j=%d; unComp=%d; original = %d\n", fadc_id, wfmN, j, data16_uncompressed[fadc_id][wfmN*64+j], wfmData[fadc_id][wfmN][j]);
//                    if (data16_uncompressed[fadc_id][wfmN * 64 + j] != wfmData[fadc_id][wfmN][j])
//                    {
//                        printf("err: iFADC:%d wfmN:%d, j=%d; unComp=%d; original = %d\n", fadc_id, wfmN, j, data16_uncompressed[fadc_id][wfmN * 64 + j], wfmData[fadc_id][wfmN][j]);
//                        printf("error!\n");
//                        exit(1);
//                    }
//                }
//            }
//        }
//    }
//    ////t.FinishLap(20);
//
//    // printf("lol no errors someone please take a screenshot of this.\n");
//
//    // Free memory
//    // free(h_idata);
//    // free(h_odata);
//    // free(h_wfmInfo);
//    // free(bytesPerWfm);
//    // delete compressor;
//
//    return maxOutputIndex;
    return -1;
}

void Process::LaunchProcessBlock(int i_pcap_thread, int nPktsInBlock, int blockID)
{

    // how many (broken) packets to skip (forwards) to align the start of the
    // block with the start of some event
    int shift = 0;

    int block_start_index = m_circularBufferManager->at(i_pcap_thread)->GetIdxFromTail(m_packetsBeingProcessed[i_pcap_thread]);
    int block_end_index = m_circularBufferManager->at(i_pcap_thread)->GetIdxFromTail(m_packetsBeingProcessed[i_pcap_thread] + nPktsInBlock - 1);

    int block_end_pkt_no = m_rawPktHdrBuffer->at(i_pcap_thread)->at(block_end_index).pktNo;
    //l3log.log(L3_VERBOSE, L3_DEBUG, "(thread %d) first packet's packetNo: %d\n", i_pcap_thread, block_start_pkt_no);
    //l3log.log(L3_VERBOSE, L3_DEBUG, "(thread %d) last packet's packetNo: %d\n", i_pcap_thread, block_end_pkt_no);

    if (block_end_pkt_no != PKTS_PER_EVT - 1)
    {

        l3log.log(L3_NORMAL, L3_ERROR, "(thread %d) Last packet of block has ID %d, which is not PKTS_PER_EVT - 1\n", i_pcap_thread, block_end_pkt_no);

        // The packet that should be the last packet of an event is not the last
        // packet of the event. Go back pkt by pkt until you we find it. If
        // after jmnping PKTS_PER_EVT pkts, or reaching the beginning of the
        // buffer, we haven't found it, then reject all scanned packets and
        // continue. This should not happen very often!
        while (block_end_pkt_no != PKTS_PER_EVT - 1 && block_end_index > 0 && shift < nPktsInBlock)
        {
            //l3log.log(0, L3_WARN, "Thread %d: Missing packets found here. Expected last pkt of some event (pktNo %d), but got pktNo = %d\n", i_pcap_thread, PKTS_PER_EVT - 1, block_end_pkt_no);
            shift++;
            block_end_index = m_circularBufferManager->at(i_pcap_thread)->GetIdxFromTail(m_packetsBeingProcessed[i_pcap_thread] + nPktsInBlock - 1 - shift);
            block_end_pkt_no = m_rawPktHdrBuffer->at(i_pcap_thread)->at(block_end_index).pktNo;
        }

        if (shift == nPktsInBlock || block_end_index == 0)
        {
            l3log.log(L3_NORMAL, L3_ERROR, "(thread %d) Did NOT find end of event after %d packets. Dropping %d packets\n", i_pcap_thread, shift, shift);
            m_packetsBeingProcessed[i_pcap_thread] += shift;
            // submit to the thread pool a lambda function that returns its argument
            m_futures.at(i_pcap_thread).push(std::move(m_threadPoolListForCUDA.at(i_pcap_thread)->submit([](uint32_t x)
                                                                                                         { return x; },
                                                                                                         shift)));
            return;
        }

        l3log.log(L3_VERBOSE, L3_DEBUG, "(thread %d) Found end of event after %d packets\n", i_pcap_thread, shift);
    }

    int nPacketsInBlock = nPktsInBlock - shift;

    m_packetsBeingProcessed[i_pcap_thread] += nPacketsInBlock;
    //printf("(thread %d) be m_packetsBeingProcessed[%d] += %d = %d\n", i_pcap_thread, i_pcap_thread, nPacketsInBlock, m_packetsBeingProcessed[i_pcap_thread]);

    
    if (m_config.do_single_thread)
    {
        // makes debugging much easier
        Process::ProcessEventBlock(i_pcap_thread, block_start_index, nPacketsInBlock, blockID);
        m_circularBufferManager->at(i_pcap_thread)->pop(nPacketsInBlock);
        m_packetsBeingProcessed[i_pcap_thread] -= nPacketsInBlock;
    }
    else
    {
        m_futures.at(i_pcap_thread).push(std::move(m_threadPoolListForCUDA.at(i_pcap_thread)->submit(&Process::ProcessEventBlock, this, i_pcap_thread, block_start_index, nPacketsInBlock, blockID)));
    }
}

int Process::Run(Capturer *capturer)
{

    m_rawPktDataBuffer = capturer->GetThreadBuff();
    m_rawPktHdrBuffer = capturer->GetRawHdrBuff();
    m_circularBufferManager = capturer->GetCircularBuffer();


    // Wait for Capturer to give the green light!
    std::unique_lock<std::mutex> lk(capturer->m);
    capturer->cv.wait(lk);
    l3log.log(L3_QUIET, L3_INFO, "Hi, Process here! Lock has been released. Running!\n");


    int maxNEventsPerBlock = 1000;

    // number of packets that each cuda stream will process per iteration
    std::vector<int> nPacketsToProcessPerThread(m_config.n_pcap_threads, 0);

    // Create CUDA streams
    for (int i = 0; i < N_CUDA_STREAMS; i++)
    {
        cudaStreamCreate(&m_streams[i]);
    }

    if (m_config.do_profiling)
    {
        // sleep enough for all packets to come
        l3log.log(L3_QUIET, L3_WARN, "Profiling mode! Sleeping some time for a full spill to come...\n");
        sleep(80);
    }

    bool lastLap = false;
    while (1)
    {

        // Free space from completed buffers
        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        for (u_int i_pcap_thread = 0; i_pcap_thread < m_config.n_pcap_threads; i_pcap_thread++)
        {
            while (m_futures.at(i_pcap_thread).size() > 0)
            {
                if (m_futures.at(i_pcap_thread).front().wait_for(std::chrono::seconds(0)) == std::future_status::ready)
                {
                    // get processedPkts count
                    std::future<uint32_t> &bb = m_futures.at(i_pcap_thread).front();
                    int processedPkts = bb.get();
                    m_futures.at(i_pcap_thread).pop();

                    l3log.log(L3_VERBOSE, L3_DEBUG, "(thread %d) Calling pop() with %d packets on a buffer containing %d packets\n", i_pcap_thread, processedPkts, m_circularBufferManager->at(i_pcap_thread)->GetNPackets());
                    
                    // pop processed packets from the circular buffer
                    l3log.log(L3_VERBOSE, L3_DEBUG, "(thread %d) nPackets being processed before poping: %d\n", i_pcap_thread, m_packetsBeingProcessed[i_pcap_thread]);
                    m_circularBufferManager->at(i_pcap_thread)->pop(processedPkts);
                    m_packetsBeingProcessed[i_pcap_thread] -= processedPkts;
                    l3log.log(L3_VERBOSE, L3_DEBUG, "(thread %d) nPackets being processed -= %d: %d\n", i_pcap_thread, processedPkts, m_packetsBeingProcessed[i_pcap_thread]);
                    l3log.log(L3_VERBOSE, L3_DEBUG, "popped %d packets from circular buffer %d\n", processedPkts, i_pcap_thread);
                }
                else
                {
                    break;
                }
            }
        }

        // Get the amount of packets that will be processed in this iteration,
        // and submit the tasks to the thread pool
        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        for (u_int i_pcap_thread = 0; i_pcap_thread < m_config.n_pcap_threads; i_pcap_thread++)
        {
            // l3log.log(L3_VERBOSE, L3_DEBUG, "nPackets being processed by thread %d: %d\n", i_pcap_thread, m_packetsBeingProcessed[i_pcap_thread]);
            // l3log.log(0, L3_DEBUG, "m_circularBufferManager->at(%d)->GetNPackets(): %d\n", i_pcap_thread, m_circularBufferManager->at(i_pcap_thread)->GetNPackets());
            // l3log.log(0, L3_DEBUG, "m_circularBufferManager->at(%d)->GetHead() - m_circularBufferManager->at(%d)->GetTail(): %d\n", i_pcap_thread, i_pcap_thread, m_circularBufferManager->at(i_pcap_thread)->GetHead() - m_circularBufferManager->at(i_pcap_thread)->GetTail());

            int nPacketsBefore = -1;
            int nPacketsInPCAPBuff = 0; // Number of packets in the PCAP buffer

            while (true)
            {
                int nItersWithoutPackets = 0;
                // Wait for packets to appear in the PCAP buffer. Do not
                // continue unless no more packets are coming or there are
                // enough packets to fill a processing block
                nPacketsInPCAPBuff = m_circularBufferManager->at(i_pcap_thread)->GetNPackets() - m_packetsBeingProcessed[i_pcap_thread];
                if (nPacketsInPCAPBuff > maxNEventsPerBlock * PKTS_PER_EVT)
                {
                    break;
                }
                else{
                    while (nItersWithoutPackets < 10)
                    {
                        usleep(1000);
                        nPacketsInPCAPBuff = m_circularBufferManager->at(i_pcap_thread)->GetNPackets() - m_packetsBeingProcessed[i_pcap_thread];
                        if (nPacketsInPCAPBuff != nPacketsBefore)
                        {
                            nPacketsBefore = nPacketsInPCAPBuff;
                            break;
                        }
                        nItersWithoutPackets++;
                    }
                    if (nItersWithoutPackets == 10)
                    {
                        break;
                    }
                }
            }

            // Number of events to be queued
            int nEvents = nPacketsInPCAPBuff / PKTS_PER_EVT;

            nPacketsToProcessPerThread[i_pcap_thread] = nEvents * PKTS_PER_EVT;

            // l3log.log(L3_VERBOSE, L3_DEBUG, "(thread %d) nPackets in circular buffer: %d\n",i_pcap_thread, m_circularBufferManager->at(i_pcap_thread)->GetNPackets());
            // l3log.log(L3_VERBOSE, L3_DEBUG, "(thread %d) nPackets being processed: %d\n",i_pcap_thread, m_packetsBeingProcessed[i_pcap_thread]);
            // l3log.log(L3_VERBOSE, L3_DEBUG, "(thread %d) nPacketsInPCAPBuff: %d; nEvents: %d\n",i_pcap_thread, nPacketsInPCAPBuff, nEvents);

            if (nPacketsToProcessPerThread[i_pcap_thread] != 0)
            {

                int nFullBlocks = nEvents / maxNEventsPerBlock;
                for (int i_block = 0; i_block < nFullBlocks; i_block++)
                {
                    l3log.log(L3_VERBOSE, L3_DEBUG, "Launching process block %d of %d\n", i_block, nFullBlocks);
                    LaunchProcessBlock(i_pcap_thread, nPacketsToProcessPerThread[i_pcap_thread], i_block);
                }
                if (nFullBlocks == 0)
                {
                    LaunchProcessBlock(i_pcap_thread, nPacketsToProcessPerThread[i_pcap_thread], 0);
                }
            }
        }

        if (lastLap)
        {
            printf("This was the last lap. Exiting...\n");
            break;
        }

        if ((m_config.do_profiling || m_config.exit_after_first_spill))
        {
            bool allThreadsHaveFinished = true;
            for (u_int i = 0; i < m_config.n_pcap_threads; i++)
            {
                if (capturer->m_pcap_finished == false)
                {
                    allThreadsHaveFinished = false;
                }
                if (m_futures.at(i).size() > 0) // if multi threaded
                {
                    allThreadsHaveFinished = false;
                }
                if (m_packetsBeingProcessed[i] > 0)
                {
                    allThreadsHaveFinished = false;
                }
            }

            if (allThreadsHaveFinished)
            {
                printf("No threads moving and PCAP finished. Doing a final iteration\n");
                lastLap = true;
            }
        }
    }

    // Destroy CUDA streams
    for (int i = 0; i < N_CUDA_STREAMS; i++)
    {
        printf("Destroying CUDA stream %d\n", i);
        cudaStreamDestroy(m_streams[i]);
    }

    // Free CUDA buffers
    printf("Freeing CUDA buffers\n");
    for (int i_stream = 0; i_stream < N_CUDA_STREAMS; i_stream++)
    {
        cudaFree(m_d_odata[i_stream]);
        cudaFree(m_d_perFADCWfmData[i_stream]);
        cudaFree(m_d_wfmInfo[i_stream]);
        cudaFree(m_d_compressedBytesPerWfm[i_stream]);
    }

    return 0;
}