#include "HnEveDisCV.h"
//#include "GsimE14Detector/GsimE14UserGeom.h"
#include <iostream>
#include <cstdio>
#include "TVector3.h"
#include "TMath.h"
#include "TGraph.h"

//using namespace E14;

namespace HnLib
{
  HnEveDisCV::HnEveDisCV(int userFlag) : HnEveDisDetector("CV","cv",184)
  {
    m_userFlag=userFlag;
    const double widthFactor=5;//Width is intentionally enlarged
    m_isFrontVisible=true;
    m_isRearVisible=true;
    const double mm = 1.;
    const double cm=10.;
    const double deg=TMath::Pi()/180.;

    m_shiftPosition=TVector3(0,0,(-5.23-0.85+614.8+2)*cm);

    double width=69*mm;
    double tCFRP=0.13*6*mm;
    double rearZ0=0*mm;//rearCV downstream surface
    double frontZ0=rearZ0-250*mm-tCFRP;//frontCV downstream surface
    double rearY0=-76.5*mm;
    double rearX0=-76.35*mm;
    double frontY0=120*mm;
    double frontX0=-119.85*mm;
    double yOffset=0.15*mm;;
    


    int    ridList[11]={
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10
    };
    double rwList[11]=
      {
	width,
	width,
	width,
	width,
	width,
	width,
	width,
	width,
	width,
	width,
	width
      };
    double rlList[11]=
      {
	916.4*mm,
	916.4*mm,
	887.7*mm,
	859*mm,
	830.4*mm,
	801.7*mm,
	773.1*mm,
	696.2*mm,
	627*mm,
	557.9*mm,
	488.7*mm
      };
    
    int    fidList[13]={
      0,
      1,
      2,
      3,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11
    };
    double fwList[13]=
      {
	width,
	width,
	width,
	34.5*mm,
	width-34.5*mm,
	width,
	width,
	width,
	width,
	width,
	width,
	width,
	width
      };
    double fl1List[13]=
      {
	1000.4*mm,
	1000.4*mm,
	1000.4*mm,
	1000.4*mm,
	1000.4*mm,
	986*mm,
	957.4*mm,
	928.7*mm,
	862.1*mm,
	793.0*mm,
	723.8*mm,
	648.4*mm,
	723.2*mm
      };
    double fl2List[13]=
      {
	1000.4*mm,
	1000.4*mm,
	1000.4*mm,
	1000.4*mm,
	986.1*mm,
	957.4*mm,
	928.8*mm,
	862.3*mm,
	793.1*mm,
	724.0*mm,
	648.8*mm,
	481.8*mm,
	390.1*mm
      };
    double fdList[13]=
      {
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	166.55*mm
      };
    
    double xCenter=0;
    double yCenter=0;
    double zCenter=0;

    
    double yBase=frontY0;
    int ind=0;
    std::vector<double> paramVec;

    std::list<TVector3> vListStore[4];
    
    for(int i=0;i<13;i++) {
      fgetCenter(fwList[i],fl1List[i],fl2List[i],fdList[i],
		 xCenter,yCenter,zCenter);
      fmakeParameter(fwList[i],fl1List[i],fl2List[i],fdList[i],paramVec);
      paramVec[0]=paramVec[0]*widthFactor;

      if(i==12) frontX0=-xCenter;
      double xPos=frontX0+xCenter;
      if(i>0) {
	if(i==4) {
	  yBase+=fwList[i-1];
	} else {
	  yBase+=fwList[i-1]+yOffset;
	}
      }
      double yPos=yBase+yCenter;
      double zPos=frontZ0+zCenter;


      TVector3 vTrans[4];
      vTrans[0]=TVector3(xPos,yPos,zPos);
      vTrans[1]=TVector3(-yPos,xPos,zPos);
      vTrans[2]=TVector3(-xPos,-yPos,zPos);
      vTrans[3]=TVector3(yPos,-xPos,zPos);
      
      TVector3 vRot[4];
      vRot[0]=TVector3(0,0,0);
      vRot[1]=TVector3(0,0,90*deg);
      vRot[2]=TVector3(0,0,180*deg);
      vRot[3]=TVector3(0,0,270*deg);

      //i==3,4 => merge
      for(int j=0;j<4;j++) {
	std::list<TVector3> vList=convTrapParam(paramVec);
	if(i==3) {
	  vListStore[j]=vList;
	  continue;
	}

	
	
	if(i==4) {
	  TVector3 vShift3(+4.487,-34.5,0);
	  for(std::list<TVector3>::iterator it=vListStore[j].begin();
	      it!=vListStore[j].end();it++) {
	    (*it)=(*it)+vShift3;
	  }
	  
	  std::list<TVector3> vList4;
	  std::list<TVector3>::iterator
	    it4=vList.begin();//it4-1st
	  vList4.push_back( (*it4) );
	  it4++;//it4-2nd
	  vList4.push_back( (*it4) );
	  it4++;//it4-3nd
	  it4++;//it4-4th
	  
	  std::list<TVector3>::iterator
	    it3=vListStore[j].begin();//1st
	  it3++;//2nd
	  it3++;//3nd
	  vList4.push_back( (*it3) );
	  it3++;//4th
	  vList4.push_back( (*it3) );
	  vList4.push_back( (*it4) );



	  
	  it4++;//5th
	  vList4.push_back( (*it4) );
	  it4++;//6th
	  vList4.push_back( (*it4) );
	  it4++;//7th
	  it4++;//8th

	  it3++;//5th
	  it3++;//6nd
	  it3++;//7th
	  vList4.push_back( (*it3) );
	  it3++;//8th
	  vList4.push_back( (*it3) );
	  vList4.push_back( (*it4) );
	  vList=vList4;
	}
	
	rotateAndTranslate(vList,vRot[j],vTrans[j]);

	//Divide it into two for both end readout
	//absolute coordinate
	//j==0 xp (x==?)
	//j==1 xp (y==?)
	//j==2 xp (x==?)
	//j==3 xp (y==?)
	std::list<TVector3> vListBoth[2];
	if(j==0) {
	  makeHalfDivision(vList,"X",vListBoth[0],vListBoth[1]);
	} else if (j==1) {
	  makeHalfDivision(vList,"Y",vListBoth[0],vListBoth[1]);
	} else if (j==2) {
	  makeHalfDivision(vList,"X",vListBoth[1],vListBoth[0]);
	} else if (j==3) {
	  makeHalfDivision(vList,"Y",vListBoth[1],vListBoth[0]);
	}
	
	if(i!=3) {
	  for(int k=0;k<2;k++) {
	    fillEnvelope(vListBoth[k],ind);
	    int id=fidList[i]*2+12*2*j+k;
	    m_moduleID[ind]=id;
	    m_mapIDtoIndex.insert(std::make_pair(id,ind) );
	    ind++;
	  }
	}
      }
    }
    yBase=rearY0;
    for(int i=0;i<11;i++) {
      rgetCenter(rwList[i],rlList[i],
		 xCenter,yCenter,zCenter);
      rmakeParameter(rwList[i],rlList[i],paramVec);
      paramVec[0]=paramVec[0]*widthFactor;
      
      double xPos=rearX0+xCenter;
      if(i>0) {
	yBase+=-rwList[i-1]-yOffset;;
      }
      double yPos=yBase+yCenter;
      double zPos=rearZ0+zCenter;
      
      TVector3 vTrans[4];
      vTrans[0]=TVector3(xPos,yPos,zPos);
      vTrans[1]=TVector3(-yPos,xPos,zPos);
      vTrans[2]=TVector3(-xPos,-yPos,zPos);
      vTrans[3]=TVector3(yPos,-xPos,zPos);
      
      TVector3 vRot[4];
      vRot[0]=TVector3(0,0,0);
      vRot[1]=TVector3(0,0,90*deg);
      vRot[2]=TVector3(0,0,180*deg);
      vRot[3]=TVector3(0,0,270*deg);

      double sf[4]={3,0,1,2};

      for(int j=0;j<4;j++) {
	std::list<TVector3> vList=convTrapParam(paramVec);
	rotateAndTranslate(vList,vRot[j],vTrans[j]);
	//Divide it into two for both end readout
	//absolute coordinate
	//j==0 xp (x==?)
	//j==1 xp (y==?)
	//j==2 xp (x==?)
	//j==3 xp (y==?)

	std::list<TVector3> vListBoth[2];
	if(j==0) {
	  makeHalfDivision(vList,"X",vListBoth[0],vListBoth[1]);
	} else if (j==1) {
	  makeHalfDivision(vList,"Y",vListBoth[0],vListBoth[1]);
	} else if (j==2) {
	  makeHalfDivision(vList,"X",vListBoth[1],vListBoth[0]);
	} else if (j==3) {
	  makeHalfDivision(vList,"Y",vListBoth[1],vListBoth[0]);
	}
	
	for(int k=0;k<2;k++) {
	  fillEnvelope(vListBoth[k],ind);
	  int id=100+ridList[i]*2+11*2*sf[j]+k;
	  m_moduleID[ind]=id;
	  m_mapIDtoIndex.insert(std::make_pair(id,ind) );
	  ind++;
	}
      }
    }
  }
  
  HnEveDisCV::~HnEveDisCV()
  {
    ;
  }


  void HnEveDisCV::fgetCenter(double width,double l1,double l2,double d,
			      double& xCenter,double& yCenter,double& zCenter)
  {
    const double mm = 1.;
    double thick = 3*mm;
    //
    //    d             l2
    //  <----><------------------>
    //  |
    //  | width
    //  |
    //  <------------------------------->
    //                 l1
    
    double cx=(l1/2.+(d+l2/2.))/2.;
    double cy=width/2.;
    double cz=0;
    
    double L1=l1+thick/sqrt(3.)- (l1-l2)/width* (thick/sqrt(3.));
    if(l1==l2) {
      L1=l1+thick/sqrt(3.)+thick/sqrt(3.);
    }
    double dxR=0;
    double dxL=0;
    if(d!=0) {
      dxL=d/width* (thick/sqrt(3.));
      dxR=(l1-(d+l2))/width* (thick/sqrt(3.));
      L1=l1-dxR-dxL;
    }
    double L2=l2+thick/sqrt(3.)- (l1-(d+l2))/width* (thick/sqrt(3.));
    if(l1==l2) {
      L2=l2+thick/sqrt(3.)+thick/sqrt(3.);
    }
    if(d!=0) {
      L2=l2-dxR-dxL;
    }
    //double A=-std::atan(((-thick/sqrt(3)+dxL+L1/2.)-(-thick/sqrt(3)+d+dxL+L2/2.))/width)*rad;
    double CX=(-thick/sqrt(3)+dxL+L1/2.+(-thick/sqrt(3)+d+dxL+L2/2.))/2.;
    double CY=width/2.+thick/sqrt(3.);
    double CZ=-thick;
    if(d!=0) {
      //A=-std::atan(((dxL+L1/2.)-(d+dxL+L2/2.))/width)*rad;
      CX=(dxL+L1/2.+(d+dxL+L2/2.))/2.;
      CY=width/2.+thick/sqrt(3.);
      CZ=-thick;
    }
    
    xCenter=(CX+cx)/2.;
    yCenter=(CY+cy)/2.;
    zCenter=(CZ+cz)/2.;
  }
  
  void HnEveDisCV::fmakeParameter(double width,double l1,double l2,double d,std::vector<double>& paramVec)
  {
    const double mm = 1.;
    const double rad=1;
    double thick = 3*mm;
    //
    //    d             l2
    //  <----><------------------>
    //  |
    //  | width
    //  |
    //  <------------------------------->
    //                 l1
    
    double a=-std::atan((l1/2.-(d+l2/2.))/width)*rad;
    double cx=(l1/2.+(d+l2/2.))/2.;
    double cy=width/2.;
    double cz=0;
    
    double L1=l1+thick/sqrt(3.)- (l1-l2)/width* (thick/sqrt(3.));
    if(l1==l2) {
      L1=l1+thick/sqrt(3.)+thick/sqrt(3.);
    }
    double dxR=0;
    double dxL=0;
    if(d!=0) {
      dxL=d/width* (thick/sqrt(3.));
      dxR=(l1-(d+l2))/width* (thick/sqrt(3.));
      L1=l1-dxR-dxL;
    }
    double L2=l2+thick/sqrt(3.)- (l1-(d+l2))/width* (thick/sqrt(3.));
    if(l1==l2) {
      L2=l2+thick/sqrt(3.)+thick/sqrt(3.);
    }
    if(d!=0) {
      L2=l2-dxR-dxL;
    }
    double A=-std::atan(((-thick/sqrt(3)+dxL+L1/2.)-(-thick/sqrt(3)+d+dxL+L2/2.))/width)*rad;
    double CX=(-thick/sqrt(3)+dxL+L1/2.+(-thick/sqrt(3)+d+dxL+L2/2.))/2.;
    double CY=width/2.+thick/sqrt(3.);
    double CZ=-thick;
    if(d!=0) {
      A=-std::atan(((dxL+L1/2.)-(d+dxL+L2/2.))/width)*rad;
      CX=(dxL+L1/2.+(d+dxL+L2/2.))/2.;
      CY=width/2.+thick/sqrt(3.);
      CZ=-thick;
    }
    
    double dx=(cx-CX);
    double dy=(cy-CY);
    double dz=(cz-CZ);
    
    
    
    double theta=std::atan(sqrt(dx*dx+dy*dy)/dz)*rad;
    double phi=std::atan2(dy,dx)*rad;
    
    paramVec.clear();
    paramVec.push_back(thick);
    paramVec.push_back(theta);
    paramVec.push_back(phi);
    paramVec.push_back(width);
    paramVec.push_back(L1);
    paramVec.push_back(L2);
    paramVec.push_back(A);
    paramVec.push_back(width);
    paramVec.push_back(l1);
    paramVec.push_back(l2);
    paramVec.push_back(a);
  }


  void HnEveDisCV::rgetCenter(double width,double l,
			      double& xCenter,double& yCenter,double& zCenter)
  {
    const double mm = 1.;
    double thick = 3*mm;
    //
    //  0               l
    //  <------------------------------->
    //  |
    //  | width
    //  |
    //  <------------------------------->
    //                 l
    
    double cx=l/2.;
    double cy=-width/2.;
    double cz=0;
    
    double L=l+thick/sqrt(3.);
    double CX=L/2.-thick/sqrt(3.);
    double CY=-width/2.-thick/sqrt(3.);
    double CZ=-thick;
    
    xCenter=(CX+cx)/2.;
    yCenter=(CY+cy)/2.;
    zCenter=(CZ+cz)/2.;
  }
  
  void HnEveDisCV::rmakeParameter(double width,double l,std::vector<double>& paramVec)
  {
    const double mm = 1.;
    const double rad=1;
    double thick = 3*mm;
    //
    //                 l
    //  <------------------------------->
    //  |
    //  | width
    //  |
    //  <------------------------------->
    //                 l
    
    double a=0;
    double cx=l/2.;
    double cy=-width/2.;
    double cz=0;
    
    double L=l+thick/sqrt(3.);
    double A=0;
    double CX=L/2.-thick/sqrt(3.);
    double CY=-width/2.-thick/sqrt(3.);
    double CZ=-thick;
    
    double dx=(cx-CX);
    double dy=(cy-CY);
    double dz=(cz-CZ);
    
    double theta=atan(sqrt(dx*dx+dy*dy)/dz)*rad;
    double phi=std::atan2(dy,dx)*rad;
    
    
    paramVec.clear();
    paramVec.push_back(thick);
    paramVec.push_back(theta);
    paramVec.push_back(phi);
    paramVec.push_back(width);
    paramVec.push_back(L);
    paramVec.push_back(L);
    paramVec.push_back(A);
    paramVec.push_back(width);
    paramVec.push_back(l);
    paramVec.push_back(l);
    paramVec.push_back(a);
    
  }

  bool HnEveDisCV::setFrontVisible(bool isVisible)
  {
    m_isFrontVisible=isVisible;
    if(!g_hPoly) return false;
    TList* list = g_hPoly->GetBins();
    if(!list) return false;
    for(int i=0;i<m_nIndex;i++) {
      int id = m_moduleID[i];
      if(id<100) {
	//Front
	TH2PolyBin* polybin = (TH2PolyBin*)list->At(i);
	if(!polybin) return false;
	if(!isVisible)
	  polybin->SetDrawOption("p");
	TObject* obj=polybin->GetPolygon();
	if(!obj) return false;
	TGraph* gr = (TGraph*)obj;
	if(isVisible) {
	  gr->SetFillStyle(1001);
	  gr->SetLineStyle(1);
	  gr->SetLineColor(1);
	  gr->SetLineColorAlpha(1,1);
	} else {
	  gr->SetFillStyle(0);
	  gr->SetLineStyle(0);
	  gr->SetLineColor(kWhite);
	  gr->SetLineColorAlpha(kWhite,0);
	}
      }
    }
    return true;
  }
  
  bool HnEveDisCV::setRearVisible(bool isVisible)
  {
    m_isRearVisible=isVisible;
    if(!g_hPoly) return false;
    TList* list = g_hPoly->GetBins();
    if(!list) return false;
    for(int i=0;i<m_nIndex;i++) {
      int id = m_moduleID[i];
      if(id>=100) {
	//Rear
	TH2PolyBin* polybin = (TH2PolyBin*)list->At(i);
	if(!polybin) return false;
	if(!isVisible)
	  polybin->SetDrawOption("p");
	TObject* obj=polybin->GetPolygon();
	if(!obj) return false;
	TGraph* gr = (TGraph*)obj;
	if(isVisible) {
	  gr->SetFillStyle(1001);
	  gr->SetLineStyle(1);
	  gr->SetLineColor(1);
	  gr->SetLineColorAlpha(1,1);
	} else {
	  gr->SetFillStyle(0);
	  gr->SetLineStyle(0);
	  gr->SetLineColor(kWhite);
	  gr->SetLineColorAlpha(kWhite,0);
	}
      }
    }
    return true;
  }

  bool HnEveDisCV::setSpecificModuleVisible(std::vector<int> v_VisibleModule)
  {
    //    m_isRearVisible=isVisible;
    if(!g_hPoly) return false;
    TList* list = g_hPoly->GetBins();
    if(!list) return false;
    bool isModuleIDVisible=false;
    for(int i=0;i<m_nIndex;i++) {
      int id = m_moduleID[i];
      isModuleIDVisible=false;
      for(UInt_t imod=0;imod<v_VisibleModule.size();imod++){
	if( id==v_VisibleModule.at(imod) ){
	  isModuleIDVisible=true;
	  break;
	}
      }

      TH2PolyBin* polybin = (TH2PolyBin*)list->At(i);
      if(!polybin) return false;
      if(!isModuleIDVisible)
	polybin->SetDrawOption("p");
      TObject* obj=polybin->GetPolygon();
      if(!obj) return false;
      TGraph* gr = (TGraph*)obj;
      if(isModuleIDVisible) {
	gr->SetFillStyle(1001);
	gr->SetLineStyle(1);
	gr->SetLineColor(1);
	gr->SetLineColorAlpha(1,1);
      } else {
	gr->SetFillStyle(0);
	gr->SetLineStyle(0);
	gr->SetLineColor(kWhite);
	gr->SetLineColorAlpha(kWhite,0);
      }

    }
    return true;
  }

  void HnEveDisCV::drawModuleID(std::string option)
  {
    reset(-1);
    for(int i=0;i<m_nIndex;i++) {
      int id=m_moduleID[i];
      if( (id<100 && m_isFrontVisible) ||
	  (id>=100 && m_isRearVisible)
	  ) {
	setModuleContent(id,id);
      }
    }
    
    if(g_hPoly) {
      g_hPoly->Draw(option.c_str());
    }
  }
  
  void HnEveDisCV::resetFront(double val)
  {
    if(g_hPoly) {
      for(int i=0;i<m_nIndex;i++) {
	int id=m_moduleID[i];
	if(id<100) 
	  g_hPoly->SetBinContent(i+1,val);
      }
    }
  }

  void HnEveDisCV::resetRear(double val)
  {
    if(g_hPoly) {
      for(int i=0;i<m_nIndex;i++) {
	int id=m_moduleID[i];
	if(id>=100) 
	  g_hPoly->SetBinContent(i+1,val);
      }
    }
  }

}


