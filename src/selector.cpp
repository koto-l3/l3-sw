

#include "selector.h"
#include "Detectors.h"
#include <string>
#include <math.h> // Remove this later if possible
#include <fstream>
#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <sstream>

#if D_USE_ROOT
#include "HnEveDisCSI.h"
#include "HnEveDisCV.h"
#include "TPad.h"
#include "TStyle.h"
#include "TROOT.h"
#endif

// #include "histos.h"
// #include "plotter.h"
#include "common.h"
#include "CLUEAlgoGPU.h"
#include "nvtx3/nvToolsExt.h"

struct Clue_Points
{
    std::vector<int> x;        // x index on the grid (int)
    std::vector<int> y;        // y index on the grid (int)
    std::vector<float> weight; // = Energy

    std::vector<float> rho;                  // local density
    std::vector<float> delta;                // distance to nearest with higher density
    std::vector<int> nearestHigher;          // index of nearest with higher density
    std::vector<int> clusterIndex;           // index of cluster it belongs to
    std::vector<std::vector<int>> followers; // indices of points with lower density
    std::vector<int> isSeed;                 // one seed per cluster
    // why use int instead of bool?
    // https://en.cppreference.com/w/cpp/container/vector_bool
    // std::vector<bool> behaves similarly to std::vector, but in order to be space efficient, it:
    // Does not necessarily store its elements as a contiguous array (so &v[0] + n != &v[n])

    int n;

    void clear()
    {
        x.clear();
        y.clear();
        weight.clear();

        rho.clear();
        delta.clear();
        nearestHigher.clear();
        clusterIndex.clear();
        followers.clear();
        isSeed.clear();

        n = 0;
    }
};

// class LayerTiles
//{
//     public:
//
//         float xMin = 0;
//         float xMax = 140;
//         float yMin = 0;
//         float yMax = 140;
//
//         int nColumns = 28;
//         int nRows = 28;
//
//         LayerTiles(){
//             layerTiles_.resize(nColumns * nRows);
//         }
//
//         void fill(float x, float y, int i)
//         {
//             //printf("len %d; getGlobalBin %d\n", layerTiles_.size(), getGlobalBin(x,y));
//             layerTiles_[getGlobalBin(x,y)].push_back(i);
//
//         }
//
//         int getXBin(float x)
//         {
//             int xBin = (x - xMin) * nColumns / (xMax - xMin);
//             xBin = std::min(xBin, nColumns-1);
//             xBin = std::max(xBin, 0);
//             return xBin;
//         }
//         int getYBin(float y)
//         {
//             int yBin = (y - yMin) * nRows / (yMax - yMin);
//             yBin = std::min(yBin, nRows-1);
//             yBin = std::max(yBin, 0);
//             return yBin;
//         }
//
//
//         int getGlobalBin(float x, float y)
//         {
//             int xBin = getXBin(x);
//             int yBin = getYBin(y);
//             return xBin + yBin * nColumns;
//         }
//
//         std::array<int,4> searchBox(float xMin, float xMax, float yMin, float yMax){
//         int xBinMin = getXBin(xMin);
//         int xBinMax = getXBin(xMax);
//         int yBinMin = getYBin(yMin);
//         int yBinMax = getYBin(yMax);
//         return std::array<int, 4>({{ xBinMin,xBinMax,yBinMin,yBinMax }});
//     }
//
//     std::vector<int>& operator[](int globalBinId) {
//       return layerTiles_[globalBinId];
//     }
//
//     private:
//         std::vector< std::vector<int>> layerTiles_;
//
// };

// class Clue_Algo
//{
//     public:
//     // constructor
//     Clue_Algo(float dc, float deltao, float deltac, float rhoc, bool verbose=false ){
//       dc_ = dc;
//       deltao_ = deltao;
//       deltac_ = deltac;
//       rhoc_ = rhoc;
//       dm_ = std::max(deltao_, deltac_);
//       verbose_ = verbose;
//
//     }
//     // distrcutor
//     ~Clue_Algo(){}
//
//     // public variables
//     float dc_, dm_, deltao_, deltac_, rhoc_;
//     bool verbose_;
//     Clue_Points points_;
//
//     // public methods
//     void setPoints(int n, float* x, float* y, float* weight) {
//       points_.clear();
//       points_.n = n;
//       // input variables
//       points_.x.assign(x, x + n);
//       points_.y.assign(y, y + n);
//       points_.weight.assign(weight, weight + n);
//       // result variables
//       points_.rho.resize(n,0);
//       points_.delta.resize(n,std::numeric_limits<float>::max());
//       points_.nearestHigher.resize(n,-1);
//       points_.isSeed.resize(n,0);
//       points_.followers.resize(n);
//       points_.clusterIndex.resize(n,-1);
//     }
//
//     void makeClusters();
//
//     private:
//     void prepareDataStructures(LayerTiles & layerTile_);
//     void calculateLocalDensity(LayerTiles & layerTile_);
//     void calculateDistanceToHigher(LayerTiles & layerTile_);
//     void findAndAssignClusters();
//
//
// };

// void Clue_Algo::makeClusters()
//{
//     LayerTiles layerTiles;
//     prepareDataStructures(layerTiles);
//
//     calculateLocalDensity(layerTiles);
//
//     calculateDistanceToHigher(layerTiles);
//
//     findAndAssignClusters();
// }

// void Clue_Algo::prepareDataStructures(LayerTiles & layerTile_)
//{
//     for (int i_point=0; i_point<points_.n; i_point++) {
//         layerTile_.fill(points_.x[i_point], points_.y[i_point], i_point);
//     }
// }

// void Clue_Algo::calculateLocalDensity(LayerTiles & layerTile_)
//{
//     for (int i_point = 0; i_point < points_.n; i_point++) {
//
//         std::array<int,4> search_box = layerTile_.searchBox(points_.x[i_point] - dc_, points_.x[i_point] + dc_, points_.y[i_point] - dc_, points_.y[i_point] + dc_);
//
//         for(int xBin = search_box[0]; xBin < search_box[1]+1; ++xBin) {
//             for(int yBin = search_box[2]; yBin < search_box[3]+1; ++yBin) {
//
//                 int binID = xBin + yBin * layerTile_.nColumns;
//                 int binSize = layerTile_[binID].size();
//
//                 // iterate inside this bin
//                 for (int i_bin2 = 0; i_bin2 < binSize; i_bin2++) {
//                     int j_point = layerTile_[binID][i_bin2];
//                     // query N_dc
//                     //float dist_ij = std::sqrt(pow(points_.x[i_point] - points_.x[j_point], 2) + pow(points_.y[i_point] - points_.y[j_point], 2));
//
//                     //printf("points_.x[i_point]: %d, points_.x[j_point]: %d, points_.y[i_point]: %d, points_.y[j_point]: %d\n", points_.x[i_point], points_.x[j_point], points_.y[i_point], points_.y[j_point]);
//                     int dist_ij_x = abs(points_.x[i_point] - points_.x[j_point]);
//                     int dist_ij_y = abs(points_.y[i_point] - points_.y[j_point]);
//                     int dist_ij = std::max({dist_ij_x, dist_ij_y});
//
//
//                     //printf("dist_ij_x: %d, dist_ij_y: %d, dist_ij_x + dist_ij_y: %d\n", dist_ij_x, dist_ij_y, dist_ij_x + dist_ij_y);
//                     //if (dist_ij < dc_) {
//                     if (dist_ij < dc_ && dist_ij_y < -dist_ij_x + 5/3*dc_) {
//                         points_.rho[i_point] += points_.weight[j_point];
//                     }
//                 }
//             }
//         }
//     }
// }

// void Clue_Algo::calculateDistanceToHigher(LayerTiles & lt)
//{
//   for(int i = 0; i < points_.n; i++) {
//     // default values of delta and nearest higher for i
//     //float maxDelta = std::numeric_limits<float>::max();
//     //float delta_i = maxDelta;
//
//     float maxDelta = std::numeric_limits<float>::max();
//     float delta_i = maxDelta;
//
//     int nearestHigher_i = -1;
//
//
//     // get search box
//     std::array<int,4> search_box = lt.searchBox(points_.x[i]-dm_, points_.x[i]+dm_, points_.y[i]-dm_, points_.y[i]+dm_);
//
//     // loop over all bins in the search box
//     for(int xBin = search_box[0]; xBin < search_box[1]+1; ++xBin) {
//       for(int yBin = search_box[2]; yBin < search_box[3]+1; ++yBin) {
//
//         // get the id of this bin
//         int binId = xBin + yBin * lt.nColumns;
//         // get the size of this bin
//         int binSize = lt[binId].size();
//
//         // iterate inside this bin
//         for (int binIter = 0; binIter < binSize; binIter++) {
//           int j = lt[binId][binIter];
//             if (i == j) continue; // skip the same point
//
//           // query N'_{dm_}(i)
//           bool foundHigher = (points_.rho[j] > points_.rho[i]);
//           // in the rare case where rho is the same, use detid
//           foundHigher = foundHigher || ((points_.rho[j] == points_.rho[i]) && (j>i) );
//             float dist_ij_x = abs(points_.x[i] - points_.x[j]);
//             float dist_ij_y = abs(points_.y[i] - points_.y[j]);
//
//             float dist_ij = std::max({dist_ij_x, dist_ij_y});
//
//
//
//             //printf("i %d, j %d, rho_i %f, rho_j %f, foundHigher %d, dist_ij %f\n", i, j, points_.rho[i], points_.rho[j], foundHigher, dist_ij);
//           if(foundHigher && dist_ij <= dm_ && dist_ij_y < -dist_ij_x + 5/3*dm_) { // definition of N'_{dm_}(i)
//             // find the nearest point within N'_{dm_}(i)
//             //printf("dist_ij %d, delta_i %d\n", dist_ij, delta_i);
//             if (dist_ij < delta_i) {
//
//               // update delta_i and nearestHigher_i
//               delta_i = dist_ij;
//               nearestHigher_i = j;
//             }
//           }
//         } // end of interate inside this bin
//       }
//     } // end of loop over bins in search box
//
//     points_.delta[i] = delta_i;
//     points_.nearestHigher[i] = nearestHigher_i;
//   } // end of loop over points
// }

// void Clue_Algo::findAndAssignClusters()
//{
//     int nClusters = 0;
//   // find cluster seeds and outlier
//   std::vector<int> localStack;
//   // loop over all points
//   for(unsigned int i = 0; i < points_.n; i++) {
//     // initialize clusterIndex
//     points_.clusterIndex[i] = -1;
//     // determine seed or outlier
//     bool isSeed = (points_.delta[i] > deltac_) && (points_.rho[i] >= rhoc_);
//     bool isOutlier = (points_.delta[i] > deltao_) && (points_.rho[i] < rhoc_);
//
//     //printf("i = %d, isSeed = %d, isOutlier = %d, rho = %f, delta = %f\n", i, isSeed, isOutlier, points_.rho[i], points_.delta[i]);
//
//     if (isSeed) {
//         float min = -875.0;
//         //printf("Seed coordinate: %f, %f\n", points_.x[i]*50/4 + min, points_.y[i]*50/4 + min);
//         //m_ChIdX.at(i_ch) = (getChPosX(i_ch) - minX)*4/50; // each large crystal is 4 units large
//
//       // set isSeed as 1
//       points_.isSeed[i] = 1;
//       // set cluster id
//       points_.clusterIndex[i] = nClusters;
//       // increment number of clusters
//       nClusters++;
//       // add seed into local stack
//       localStack.push_back(i);
//     } else if (!isOutlier) {
//       // register as follower at its nearest higher
//       points_.followers[points_.nearestHigher[i]].push_back(i);
//     }
//   }
//
//   // expend clusters from seeds
//   while (!localStack.empty()) {
//     int i = localStack.back();
//     auto& followers = points_.followers[i];
//     localStack.pop_back();
//
//     // loop over followers
//     for( int j : followers){
//       // pass id from i to a i's follower
//       points_.clusterIndex[j] = points_.clusterIndex[i];
//       // push this follower to localStack
//       localStack.push_back(j);
//     }
//   }
//     //printf("nClusters = %d\n", nClusters);
//
//
// }

Selector::Selector(const Config &config)
{
    m_config = &config;
    //    hs = new Histos();
    //    pl = new Plotter();
    //    hs->CreateHistogram("cv1", 100, 0, 2);
    //    m_histData.open("hist.dat");

    m_gpuData = new GPUData();
    m_gpuData->AllocateDeviceMemory(m_config->csi_n_channels);
    m_tmpOut.open("tmp.dat");
}

Selector::~Selector()
{
    m_gpuData->FreeDeviceMemory();
}

std::vector<DetectorData *> &Selector::GetDetectorDataList()
{
    return m_detectorDataList;
}

DetectorData *Selector::GetDetectorData(std::string name)
{
    for (auto &det : m_detectorDataList)
    {
        if (det->m_info->m_name == name)
        {
            return det;
        }
    }
    printf("ERROR: Detector %s not found.\n", name.c_str());
    return nullptr;
}

float Selector::CalcPedestal(const uint16_t wfm[], bool useOnline) //, float &pedestal, float &pedestal_sigma)
{
    //    float pedestal;//, pedestal_sigma;
    //
    //    const int n_used_samples = 10;
    //
    //    float Ped1 = 0; // First n_used_samples
    //    float RMS1 = 0; // First n_used_samples
    //
    //    for (int i_sample=0, n=0+n_used_samples; i_sample<n; i_sample++) {
    //        Ped1 += (float)wfm[i_sample]/(double)n_used_samples;
    //    }
    //
    //    for (int i_sample=0, n=0+n_used_samples; i_sample<n; i_sample++) {
    //        RMS1 += pow( (float)wfm[i_sample] - Ped1, 2 );
    //    }
    //    RMS1 = sqrt( RMS1 );
    //
    //    pedestal       = Ped1;
    //    //pedestal_sigma = RMS1;
    //    return pedestal;
    int sample_offset = 1;
    int wfmLen = 64;

    // check for nullptr
    if (wfm == nullptr)
    {
        return 0;
    }

    if (useOnline)
        return (float)wfm[0];

    const int n_used_samples = 10 - sample_offset;

    float Ped1 = 0; // First n_used_samples
    float Ped2 = 0; // last n_used_samples
    float RMS1 = 0; // First n_used_samples
    float RMS2 = 0; // last n_used_samples

    for (int i_sample = sample_offset, n = sample_offset + n_used_samples; i_sample < n; i_sample++)
    {
        Ped1 += (float)wfm[i_sample] / (double)n_used_samples;
        Ped2 += (float)wfm[wfmLen - 1 - i_sample] / (double)n_used_samples;
    }
    for (int i_sample = sample_offset, n = sample_offset + n_used_samples; i_sample < n; i_sample++)
    {
        RMS1 += pow((float)wfm[i_sample] - Ped1, 2);
        RMS2 += pow((float)wfm[wfmLen - 1 - i_sample] - Ped2, 2);
    }
    RMS1 = sqrt(RMS1);
    RMS2 = sqrt(RMS2);

    float pedestal = Ped1;
    //float pedestal_sigma = RMS1;
    if (RMS2 < RMS1)
    {
        pedestal = Ped2;
        //pedestal_sigma = RMS2;
    }

    // printf("pedestalOnlineOffline %d %f\n", wfm[0], pedestal);

    return pedestal;
}

float Selector::CalcIntegratedADC(const uint16_t wfm[], float pedestal, int nSamples)
{

    // check for nullptr
    if (wfm == nullptr)
    {
        return 0;
    }

    if (pedestal == -1)
    {
        pedestal = wfm[0];
    }

    float integratedADC = 0;
    for (int i_sample = 0; i_sample < nSamples; i_sample++)
    {
        integratedADC += wfm[i_sample];
    }

    // printf("Integrated ADC: %f; pedestal*nSamples: %f\n", integratedADC, pedestal*nSamples);
    // sleep(4);

    return integratedADC - pedestal * nSamples;
}


float calcConstantFractionTime(const uint16_t *wfm, int pedestal, int peak_time)
{
    // Constant fraction threshold
    double threshold = ( pedestal + wfm[peak_time] )/2.;

    // Search the crossing time from top point to small direction.
    for (int i_sample=(int)peak_time; i_sample>0; i_sample--) {
        if( wfm[i_sample-1] < threshold && threshold<=wfm[i_sample]){
	        return (float)( threshold-wfm[i_sample-1] ) / ( wfm[i_sample] - wfm[i_sample-1] ) + i_sample - 1;
        }
    }
    return -1;
}


int Selector::CalcTime(const uint16_t wfm[], int nSamples)
{

    // check for nullptr
    if (wfm == nullptr)
    {
        return -1;
    }

    int maxSample = 0;
    int time = 0;
    for (int i_sample = 0; i_sample < nSamples; i_sample++)
    {
        if (maxSample < wfm[i_sample])
        {
            maxSample = wfm[i_sample];
            time = i_sample;
        }
    }
    // sleep(4);

    return time;
}

int CalcPeakHeight(uint16_t wfm[], int pedestal, int nSamples)
{
    short MaxValue = 0;
    // int MaxValTime = 0;
    for (int i_sample = 0; i_sample < nSamples; i_sample++)
    {
        if (MaxValue < wfm[i_sample])
        {
            MaxValue = wfm[i_sample];
            // MaxValTime = i_sample;
        }
    }

    int peak_heigh = MaxValue - (int)pedestal;
    // int peak_time = MaxValTime;
    return peak_heigh;
}

float Selector::CalcEnergy(DetectorData const &det, int i_evt, int ich, int mode)
{
    float energy;

    DetectorInfo *detInfo = det.m_info;
    int nChannels = detInfo->m_nChannels;

    energy = 0;

    if (mode == m_UsePeakHeigh)
    {
        // printf("ich: %d, peak_heigh_size: %d, gainMean_size: %d, MeVCoeff_size: %d \n", ich, det->m_peak_heigh.size(), det->getGainMean().size(), det->getMeVCoeff().size());
        energy = det.m_peak_heigh.at(i_evt * nChannels + ich) / detInfo->getGainMean().at(ich) * detInfo->getMeVCoeff().at(ich);
    }
    else if (mode == m_UseIntegratedADC)
        energy = det.m_integratedADC.at(i_evt * nChannels + ich) / detInfo->getGainMean().at(ich) * detInfo->getMeVCoeff().at(ich);

    return energy;
}

void Selector::ComputeMinClusDistance(int i_evt, Clusters *clusters, float &minClusDistance)
{

    float minDistance = 999999;

    for (int i_clus = 0; i_clus < clusters->n[i_evt]; i_clus++)
    {
        for (int j_clus = i_clus + 1; j_clus < clusters->n[i_evt]; j_clus++)
        {
            float distance = sqrt(pow(clusters->x[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus] - clusters->x[i_evt * MAX_CSI_CLUS_PER_EVT + j_clus], 2) + pow(clusters->y[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus] - clusters->y[i_evt * MAX_CSI_CLUS_PER_EVT + j_clus], 2));
            if (distance < minDistance)
            {
                minDistance = distance;
            }
        }
    }

    minClusDistance = minDistance;
}

void Selector::ComputeClusECOE(int i_evt, Clusters *clusters, float &clusE, float &_minClusE, float &clusCOE, float &_minXY, float &_maxR)
{
    float totalE = 0;
    double coe[2] = {0, 0};

    float minClusE = 999999;

    float minXY = 999999;
    float maxR = 0;


    for (int i_clus = 0; i_clus < clusters->n[i_evt]; i_clus++)
    {
        totalE += clusters->E[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus];

        if (clusters->E[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus] < minClusE)
        {
            minClusE = clusters->E[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus];
        }

        //printf("i_clus: %d, x: %f, y: %f, nHitCrystals: %d, E: %f\n", i_clus, clusters->x[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus], clusters->y[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus], clusters->nHitCrystals[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus], clusters->E[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus]);

        // printf("i_clus: %d, E: %f\n", i_clus, clusters->E[i_evt*MAX_CSI_CLUS_PER_EVT + i_clus]);
        coe[0] += clusters->E[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus] * clusters->x[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus];
        coe[1] += clusters->E[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus] * clusters->y[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus];

        float r = sqrt(pow(clusters->x[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus], 2) + pow(clusters->y[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus], 2));
        if (std::max(abs(clusters->x[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus]), abs(clusters->y[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus])) < minXY)
        {
            minXY = std::max(abs(clusters->x[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus]), abs(clusters->y[i_evt * MAX_CSI_CLUS_PER_EVT + i_clus]));
        }
        if (r > maxR)
        {
            maxR = r;
        }
    }

    clusCOE = sqrt(pow(coe[0], 2) + pow(coe[1], 2)) / totalE;
    clusE = totalE;
    _minClusE = minClusE;
    _minXY = minXY;
    _maxR = maxR;
}

void Selector::ClearVariables()
{
    m_clusterList.clear();

    for (DetectorData *det : m_detectorDataList)
    {
        det->m_totalE = 0;
    }
}

void Selector::FinalProcess()
{
    m_histData.close();
    m_tmpOut.close();
    //    pl->SetOutputFileName("cv1");
    //    pl->Stack(hs, "cv1");
    //    printf("nentries: %d\n", hs->GetHisto("cv1")->GetEntries());
    //
    //    //hs->GetHisto("cv1")->Reset("ICESM");
    //    printf("nentries: %d\n", hs->GetHisto("cv1")->GetEntries());
}

/**
 * @brief Does something
 *
 * @return int
 */
void Selector::Select(std::vector<DetectorData> &detDataList, int nEvents, std::vector<uint16_t> &eventIDs, uint16_t *eventTag, int spillNo, uint16_t *nCDTClusters, int *gpuNClus, float *gpuClusX, float *gpuClusY)
{

    // TODO: Fix this
    DetectorData csi = detDataList.at(0);
    // DetectorData cv = detDataList.at(1);

    DetectorInfo csiInfo = *csi.m_info;
    // DetectorInfo cvInfo = *cv.m_info;

    ////////////t.StartLap(11);

    // CSI Reconstruction

    int nChannels = csiInfo.m_nChannels;

    csi.m_coe[0] = 0;
    csi.m_coe[1] = 0;
    csi.m_totalE = 0;
    ////////////t.StartLap(12);
    //nvtxRangePush("CalcIntADCAndEnergy");


    int i_selectedEvent = 0;

    for (int i_event = 0; i_event < nEvents; i_event++)
    {

        float totalE = 0;
        int totalIntegratedADC = 0;
        int nHitChs = 0;
        for (int i_ch = 0; i_ch < nChannels; i_ch++) // this excludes MPPCs
        {
            // std::cout << m_wfmData.size() << std::endl;

            // if peak heigh is not neccessary, don't calculate it

            // printf("csi pedestal size: %d, csi wfmdata size: %d\n", csi.m_pedestal.size(), csi.m_wfmData.size());

            // printf("m_integratedADC size: %d, m_pedestal size: %d, m_wfmData size: %d\n", csi.m_integratedADC.size(), csi.m_pedestal.size(), csi.m_wfmData.size());
            // csi.m_integratedADC.at(i_ch) = CalcIntegratedADC(csi.m_wfmData.at(i_ch), csi.m_pedestal.at(i_ch), csiInfo.GetSamplesPerWfm());

            // 2310-PedestalCompOnlineOffline
            // > > > > > > > > > > > > > > > >
            float ped_online = CalcPedestal(csi.m_wfmData.at(i_event * nChannels + i_ch), 1);
            //float ped_offline = CalcPedestal(csi.m_wfmData.at(i_event * nChannels + i_ch), 0);

            // printf("awkdata %d %d %f %f\n", eventIDs.at(i_event), i_ch, ped_online, ped_offline);
            //  < < < < < < < < < < < < < < < <

            csi.m_pedestal.at(i_event * nChannels + i_ch) = ped_online;
            csi.m_integratedADC.at(i_event * nChannels + i_ch) = CalcIntegratedADC(csi.m_wfmData.at(i_event * nChannels + i_ch), csi.m_pedestal.at(i_event * nChannels + i_ch), csiInfo.GetSamplesPerWfm());
            csi.m_times.at(i_event * nChannels + i_ch) = CalcTime(csi.m_wfmData.at(i_event * nChannels + i_ch), csiInfo.GetSamplesPerWfm());
            // csi.m_peak_heigh.at(i_ch) = CalcPeakHeight(csi.m_wfmData.at(i_ch), csi.m_pedestal.at(i_ch), csiInfo.GetSamplesPerWfm());



            // printf("integralADC: %f; pedestal*64: %f\n", csi.m_integratedADC.at(i_event * nChannels + i_ch), csi.m_pedestal.at(i_event * nChannels + i_ch) * csiInfo.GetSamplesPerWfm());

            float E = CalcEnergy(csi, i_event, i_ch, m_UseIntegratedADC);
            // if (i_ch == 2385)
            //{
            //     // print whole waveform
            //     printf("Complete wfm\n");
            //     for (int i_sample = 0; i_sample < csiInfo.GetSamplesPerWfm(); i_sample++)
            //     {
            //         printf("%d ", csi.m_wfmData.at(i_event * nChannels + i_ch)[i_sample]);
            //     }
            //     printf("\n");
            //     printf("i_event: %d; eventID: %d\n", i_event, eventIDs.at(i_event));
            //     printf("E: %f\n", E);
            //     printf("pedestal: %f\n", csi.m_pedestal.at(i_event * nChannels + i_ch));
            //     printf("intADC: %f\n", csi.m_integratedADC.at(i_event * nChannels + i_ch));
            //     printf("time: %d\n", csi.m_times.at(i_event * nChannels + i_ch));
            // }

            // float E = CalcEnergy(csi, i_ch, m_UsePeakHeigh);
            csi.m_Energies.at(i_event * nChannels + i_ch) = E;
            if (E > 3)
            {
                totalIntegratedADC += csi.m_integratedADC.at(i_event * nChannels + i_ch);
                nHitChs++;
                if (csi.m_times.at(i_event * nChannels + i_ch) > m_config->wfm_time_window[0] && csi.m_times.at(i_event * nChannels + i_ch) < m_config->wfm_time_window[1])
                {
                    totalE += E;
                }
            }
            // printf("i_event: %d, i_ch: %d, E: %f, time: %d\n", i_event, i_ch, E, csi.m_times.at(i_ch));
        }

        for (int i_ch = 0; i_ch < nChannels; i_ch++) // this excludes MPPCs
        {
            
            if (csi.m_Energies.at(i_event * nChannels + i_ch) > 3 && nHitChs < 1000)
            {
                //float cft = calcConstantFractionTime(csi.m_wfmData.at(i_event * nChannels + i_ch), csi.m_pedestal.at(i_event * nChannels + i_ch), csi.m_times.at(i_event * nChannels + i_ch));
                //printf("awkdata %d %f\n", csi.m_times.at(i_event * nChannels + i_ch), cft);
            }

        }


#if D_USE_ROOT
        
        if (eventIDs.at(i_event) == 12934 || eventIDs.at(i_event) == 4990 || eventIDs.at(i_event) == 1971 || eventIDs.at(i_event) == 13267)
        {
            gStyle->SetPalette(1);
            gStyle->SetPadLeftMargin(0.15);
            gStyle->SetPadRightMargin(0.15);
            gStyle->SetPadTopMargin(0.05);
            TH2D *hh = new TH2D("hh", "", 10, -1e3, 1e3, 10, -1e3, 1e3);
            TH2D *hh_clue = new TH2D("hh_clue", "", 1000, -1e3, 1e3, 1000, -1e3, 1e3);
            hh_clue->SetStats(0);

            // TH2D* hh_e14 = new TH2D("hh_e14","", 1000, -1e3, 1e3, 1000, -1e3, 1e3);
            hh->SetStats(0);
            hh->SetXTitle("X [mm]");
            hh->SetYTitle("Y [mm]");

            HnLib::HnEveDisCSI *buu = new HnLib::HnEveDisCSI(0);
            buu->setTitle("");
            buu->generateGlobalEnvelope();
            buu->generatePolyXY();
            buu->reset(-1);

            // buu->drawModuleID();

            buu->setMarkerSize(1);

            for (int i_ch = 0; i_ch < csiInfo.m_nChannels; i_ch++)
            { // Exclude MPPCs
                double x;
                double y;
                buu->getModuleXY(i_ch, x, y);
            }

            // for (int i = 0; i<m_Energies.size(); i++){
            //     buu->setModuleContent(m_IdxWithHit[i], m_Energies[i]);
            // }

            for (int i_ch = 0; i_ch < csiInfo.m_nChannels; i_ch++)
            { // Exclude MPPCs

                float E = csi.m_Energies.at(i_event * nChannels + i_ch);
                // float P = csi.m_pedestal.at(i_ch);

                // buu->setModuleContent(i_ch, i_ch);
                if (E > 3)
                {

                    //printf("chID: %d, E: %f;        ", i_ch, E);

                    buu->setModuleContent(i_ch, csi.m_Energies.at(i_event * nChannels + i_ch));
                    //buu->setModuleContent(i_ch, csi.m_times.at(i_event * nChannels + i_ch));
                    // buu->setModuleContent(2561, 50);
                    // buu->setModuleContent(i_ch, i_ch);//, csi.m_Energies.at(i_event * nChannels + i_ch));
                    // buu->setModuleContent(i_ch, E);
                    // csi.m_IdxWithHit.push_back(i_ch);
                    // csi.m_IdxWithHitVec.push_back(i_ch);
                }
                // printf("csiInfo.GetFadcChIds().at(i_ch): %d\n", csiInfo.GetFadcChIds().at(i_ch));
            }

            for (int i_clue_clus = 0; i_clue_clus < gpuNClus[i_selectedEvent]; i_clue_clus++)
            {
                // printf("evtID %d; clusID %d; clusX %f; clusY %f\n", eventIDs.at(i_event), i_clue_clus, gpuClusX[i_selectedEvent * MAX_CSI_CLUS_PER_EVT + i_clue_clus], gpuClusY[i_selectedEvent * MAX_CSI_CLUS_PER_EVT + i_clue_clus]);

                hh_clue->Fill(gpuClusX[i_selectedEvent * MAX_CSI_CLUS_PER_EVT + i_clue_clus], gpuClusY[i_selectedEvent * MAX_CSI_CLUS_PER_EVT + i_clue_clus]);
            }

            hh_clue->SetMarkerStyle(53);
            hh_clue->SetMarkerSize(10);
            hh_clue->SetMarkerColor(kRed);
            hh_clue->SetMarkerColor(kPink);

            //        hh_e14->SetMarkerStyle(53);
            //        hh_e14->SetMarkerSize(20);
            //        hh_e14->SetMarkerColor(kRed);

            buu->setZTitle("E [MeV]");
            //buu->setZTitle("Time [125 MHz clks]");
            buu->setMinimum(0);
            // buu->setMaximum(64);

            printf("Event %d has %d clusters\n", eventIDs.at(i_event), gpuNClus[i_selectedEvent]);


            std::string title = "nCDT: " + std::to_string(nCDTClusters[i_event]) + ", recTag: " + std::to_string(eventTag[i_event]);
            hh->SetTitle(title.c_str());

            hh->Draw();
            buu->draw("L COLZ same");
            hh_clue->Draw("P scat same");
            // hh_e14->Draw("P scat same");
            gPad->SetCanvasSize(6200, 5000);

            if (1)
            {
                // gPad->Print(Form("%d_csi.png", eventIDs.at(i_event)));
                gPad->Print(Form("e_%d_%d_csi.pdf", spillNo, eventIDs.at(i_event)));
                //gPad->Print(Form("t_%d_%d_csi.pdf", spillNo, eventIDs.at(i_event)));
                // gPad->Print(Form("%d_%d_csi.svg", spillNo, eventIDs.at(i_event)));
            }

            delete buu;
            delete hh_clue;
        }
#endif

        i_selectedEvent++;
    }
    return;
}