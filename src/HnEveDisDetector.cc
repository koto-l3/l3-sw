#include "HnEveDisDetector.h"
#include "TH2Poly.h"
#include "TVector3.h"
#include "TMath.h"
#include "TObject.h"
#include "TGraph.h"
#include "TPaveText.h"

//#include "CLHEP/Evaluator/Evaluator.h"


#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <list>

namespace HnLib
{
  HnEveDisDetector::HnEveDisDetector()
  {
    ;
  }
  
  HnEveDisDetector::HnEveDisDetector(std::string detectorName,std::string macroName,int nIndex)
  {
    m_detectorName=detectorName;
    m_macroName=macroName;
    m_nIndex=nIndex;
    m_moduleID       = new int[m_nIndex];

    m_localEnvelopeUS = new std::vector<TVector3>[m_nIndex];
    m_localEnvelopeDS = new std::vector<TVector3>[m_nIndex];
    m_globalEnvelopeUS = new std::vector<TVector3>[m_nIndex];
    m_globalEnvelopeDS = new std::vector<TVector3>[m_nIndex];
    
    m_shiftPosition.SetXYZ(0,0,0);
    m_isGlobalEnvelopeFilled=0;

    g_hPoly = 0;
    m_userFlag=0;
    for(int i=0;i<3;i++) {
      m_stat[i]=0;
    }
    m_resetValue=0;
    m_paveStat=0;
    m_paveInfo=0;
    m_label = new TText*[m_nIndex];
    for(int i=0;i<m_nIndex;i++) {
      m_label[i]=0;
    }
    m_IDtoValueMap.clear();
  }
  
  HnEveDisDetector::~HnEveDisDetector()
  {
    delete g_hPoly;
    
    delete [] m_localEnvelopeDS;
    delete [] m_localEnvelopeUS;
    delete [] m_globalEnvelopeDS;
    delete [] m_globalEnvelopeUS;

    delete [] m_moduleID;

    delete m_paveStat;
    delete m_paveInfo;
    
    for(int i=0;i<m_nIndex;i++) {
      if(m_label[i]) delete m_label[i];
    }
    delete [] m_label;
    
  }

  bool HnEveDisDetector::readShiftFromMacroFile(std::string macroFileName)
  {
    std::ifstream fi(macroFileName.c_str());
    std::string buf;
    std::string cmd;
    std::string nam;
    std::string mother;
    std::string sx,sy,sz;
    while( fi && std::getline(fi,buf) ) {
      std::istringstream stri(buf);
      if(stri >> cmd >> nam >> mother >> sx >> sy >> sz) {
	
	if (nam == m_macroName &&
	    mother == "/world/e14") {
            printf("Ugh we have problems\n");

//	  HepTool::Evaluator eval;
//	  eval.setStdMath();
//	  eval.setSystemOfUnits(
//				1.e+3,               //   meter 
//				1./1.60217733e-25,   //   kilogram
//				1.e+9,               //   second
//				1./1.60217733e-10,   //   ampere
//				1.0,                 //   Kelvin
//				1.0,                 //   mole
//				1.0                  //   candela
//				);
//	  
//	  double x = eval.evaluate(sx.c_str());
//	  double y = eval.evaluate(sy.c_str());
//	  double z = eval.evaluate(sz.c_str());
//
//	  m_shiftPosition.SetXYZ(x,y,z);
//	  
//	  return true;
	}
      }
    }
    return false;
  }
  
  void HnEveDisDetector::generateGlobalEnvelope()
  {
    for(int i=0;i<m_nIndex;i++) {
      m_globalEnvelopeDS[i].clear();
      for(std::vector<TVector3>::iterator it=m_localEnvelopeDS[i].begin();
	  it!=m_localEnvelopeDS[i].end();it++) {
	m_globalEnvelopeDS[i].push_back( (*it) +m_shiftPosition );
      }
    }
    for(int i=0;i<m_nIndex;i++) {
      m_globalEnvelopeUS[i].clear();
      for(std::vector<TVector3>::iterator it=m_localEnvelopeUS[i].begin();
	  it!=m_localEnvelopeUS[i].end();it++) {
	m_globalEnvelopeUS[i].push_back( (*it) +m_shiftPosition );
      }
    }
    m_isGlobalEnvelopeFilled=true;
  }
      
  void HnEveDisDetector::generatePolyXY()
  {
    if(!m_isGlobalEnvelopeFilled) {
      std::cerr << "Global envelope was not filled." << std::endl;
      std::cerr << "Run generateGlobalEnvelope()" << std::endl;
      return;
    }
    bool isStored=false;
    if(g_hPoly) {
      storePolyAtt();
      isStored=true;
      delete g_hPoly;
    }
    g_hPoly = new TH2Poly();
    g_hPoly->SetStats(0);
    g_hPoly->SetContour(100);
    
    double xPoints[100]={0};
    double yPoints[100]={0};
    for(int i=0;i<m_nIndex;i++) {
      int j=0;
      std::vector<TVector3>::iterator it=m_globalEnvelopeDS[i].begin();
      for(;it!=m_globalEnvelopeDS[i].end();it++) {
	xPoints[j]=(*it).X();
	yPoints[j]=(*it).Y();
	j++;
      }
      it=m_globalEnvelopeDS[i].begin();
      xPoints[j]=(*it).X();
      yPoints[j]=(*it).Y();
      j++;
      g_hPoly->AddBin(j, xPoints, yPoints);
    }
    if(isStored) {
      restorePolyAtt();
    }
  }

  void HnEveDisDetector::generatePolyZY()
  {
    if(!m_isGlobalEnvelopeFilled) {
      std::cerr << "Global envelope was not filled." << std::endl;
      std::cerr << "Run generateGlobalEnvelope()" << std::endl;
      return;
    }
    bool isStored=false;
    if(g_hPoly) {
      storePolyAtt();
      isStored=true;
      delete g_hPoly;
    }
    g_hPoly = new TH2Poly();
    g_hPoly->SetStats(0);
    g_hPoly->SetContour(100);
    
    double xPoints[100]={0};
    double yPoints[100]={0};
    for(int i=0;i<m_nIndex;i++) {

      double ymax[2]={-9999,-9999};
      double ymin[2]={+9999,+9999};
      double z_ymax[2]={0,0};
      double z_ymin[2]={0,0};

      
      std::vector<TVector3>::iterator itDS=m_globalEnvelopeDS[i].begin();
      std::vector<TVector3>::iterator itUS=m_globalEnvelopeUS[i].begin();
      for(;itDS!=m_globalEnvelopeDS[i].end();itDS++,itUS++) {
	if ( (*itDS).Y() > ymax[0]) {
	  ymax[0]=(*itDS).Y();
	  ymax[1]=(*itUS).Y();
	  z_ymax[0]=(*itDS).Z();
	  z_ymax[1]=(*itUS).Z();
	}

	if ( (*itDS).Y() < ymin[0]) {
	  ymin[0]=(*itDS).Y();
	  ymin[1]=(*itUS).Y();
	  z_ymin[0]=(*itDS).Z();
	  z_ymin[1]=(*itUS).Z();
	}
      }

      
      xPoints[0]=z_ymax[1];
      yPoints[0]=ymax[1];
      xPoints[1]=z_ymax[0];
      yPoints[1]=ymax[0];
      xPoints[2]=z_ymin[0];
      yPoints[2]=ymin[0];
      xPoints[3]=z_ymin[1];
      yPoints[3]=ymin[1];
      xPoints[4]=z_ymax[1];
      yPoints[4]=ymax[1];

      g_hPoly->AddBin(5, xPoints, yPoints);
    }
    if(isStored) {
      restorePolyAtt();
    }
  }


  void HnEveDisDetector::storePolyAtt()
  {
    if(!g_hPoly) return;
    TList* list = g_hPoly->GetBins();
    if(!list) return;
	
    m_title=g_hPoly->GetTitle();
    m_xTitle=g_hPoly->GetXaxis()->GetTitle();
    m_yTitle=g_hPoly->GetYaxis()->GetTitle();
    m_zTitle=g_hPoly->GetZaxis()->GetTitle();
    m_minimum=g_hPoly->GetMinimum();
    m_maximum=g_hPoly->GetMaximum();
    
    m_lineWidthMap.clear();
    m_lineColorMap.clear();
    m_fillStyleMap.clear();
    m_markerSizeMap.clear();
    m_valueMap.clear();
    
    for(int ind=0;ind<m_nIndex;ind++) {
      m_valueMap.insert(std::make_pair(ind,g_hPoly->GetBinContent(ind+1)));
      TH2PolyBin* polybin = (TH2PolyBin*)list->At(ind);
      if(!polybin) return;
      TObject* obj=polybin->GetPolygon();
      if(!obj) return;
      TGraph* gr = (TGraph*)obj;
      m_lineWidthMap.insert(std::make_pair(ind,gr->GetLineWidth()));
      m_lineColorMap.insert(std::make_pair(ind,gr->GetLineColor()));
      m_fillStyleMap.insert(std::make_pair(ind,gr->GetFillStyle()));
      m_markerSizeMap.insert(std::make_pair(ind,gr->GetMarkerSize()));
    }
  }

  void HnEveDisDetector::restorePolyAtt()
  {
    if(!g_hPoly) return;
    TList* list = g_hPoly->GetBins();
    if(!list) return;
    
    setTitle(m_title);
    setXTitle(m_xTitle);
    setYTitle(m_yTitle);
    setZTitle(m_zTitle);
    setMinimum(m_minimum);
    setMaximum(m_maximum);

    for(int ind=0;ind<m_nIndex;ind++) {
      g_hPoly->SetBinContent(ind+1,m_valueMap.at(ind));
      TH2PolyBin* polybin = (TH2PolyBin*)list->At(ind);
      if(!polybin) return;
      TObject* obj=polybin->GetPolygon();
      if(!obj) return;
      TGraph* gr = (TGraph*)obj;
      gr->SetLineWidth(m_lineWidthMap.at(ind));
      gr->SetLineColor(m_lineColorMap.at(ind));
      gr->SetFillStyle(m_fillStyleMap.at(ind));
      gr->SetMarkerSize(m_markerSizeMap.at(ind));
    }
  }

  bool HnEveDisDetector::setModuleContent(int modID,double w)
  {
    if(!g_hPoly) {
      std::cerr << "TH2Poly is not created" << std::endl;
      return false;
    }

    int nHit=m_mapIDtoIndex.count(modID);
    if(nHit == 0) {
      std::cerr << "No such module ID: " << modID << std::endl;
      return false;
    }

    std::multimap<int,int>::iterator it=m_mapIDtoIndex.find(modID);
    for(int i=0;i<nHit;i++) {
      int index=(*it).second;
      if( !(index>=0 && index<m_nIndex) ) {
	std::cerr << "Module id " << modID << " is larger than # of module " << m_nIndex << "."<< std::endl;
	return false;
      }
      g_hPoly->SetBinContent(index+1,w);
      it++;
    }
    m_IDtoValueMap.insert(std::make_pair(modID,w));
    return true;
  }

  bool HnEveDisDetector::addModuleContent(int modID,double w)
  {
    if(!g_hPoly) {
      std::cerr << "TH2Poly is not created" << std::endl;
      return false;
    }

    int nHit=m_mapIDtoIndex.count(modID);
    if(nHit == 0) {
      std::cerr << "No such module ID: " << modID << std::endl;
      return false;
    }
    
    std::multimap<int,int>::iterator it=m_mapIDtoIndex.find(modID);
    for(int i=0;i<nHit;i++) {
      int index=(*it).second;
      if( !(index>=0 && index<m_nIndex) ) {
	std::cerr << "Module id " << modID << " is larger than # of module " << m_nIndex << "."<< std::endl;
	return false;
      }

      double w0=g_hPoly->GetBinContent(index+1);
      g_hPoly->SetBinContent(index+1,w0+w);
      
      it++;
    }
    m_IDtoValueMap.insert(std::make_pair(modID,w));
    
    return true;
  }


  void HnEveDisDetector::draw(std::string option)
  {
    if(!m_isGlobalEnvelopeFilled) {
      std::cerr << "Global envelope was not filled." << std::endl;
      std::cerr << "Run generateGlobalEnvelope()" << std::endl;
      return;
    }
    
    if(g_hPoly)
      g_hPoly->Draw(option.c_str());
  }
  

  void HnEveDisDetector::drawModuleID(std::string option)
  {
    if(!m_isGlobalEnvelopeFilled) {
      std::cerr << "Global envelope was not filled." << std::endl;
      std::cerr << "Run generateGlobalEnvelope()" << std::endl;
      return;
    }
    reset(-1);
    for(int i=0;i<m_nIndex;i++) {
      int id=m_moduleID[i];
      setModuleContent(id,id);
    }
    
    if(g_hPoly) {
      g_hPoly->Draw(option.c_str());
    }
  }

  void HnEveDisDetector::reset(double val)
  {
    m_resetValue=val;
    if(g_hPoly) {
      for(int i=0;i<m_nIndex;i++) {
	g_hPoly->SetBinContent(i+1,val);
      }
    }
    m_IDtoValueMap.clear();
  }

  void HnEveDisDetector::drawStats()
  {
    if(!g_hPoly) return;
    
    double maxV=g_hPoly->GetMaximum();
    double minV=g_hPoly->GetMinimum();
    for(int k=0;k<3;k++) {
      m_stat[k]=0;
    }
    
    TList* list = g_hPoly->GetBins();
    if(!list) return;

    int Nbin=list->GetEntries();
    for(int i=0;i<Nbin;i++) {
      double v=g_hPoly->GetBinContent(i+1);
      if(v==m_resetValue) continue;
      if(v>=maxV) {
	//overflow
	m_stat[2]+=1;
      } else if(v<minV) {
	//underflow
	m_stat[0]+=1;
      } else {
	//within the range
	m_stat[1]+=1;
      }
    }

    
    if(m_paveStat) {
      m_paveStat->Clear();
    } else {
      m_paveStat = new TPaveText(0.82,0.8,0.98,0.98,"NDCB");
      m_paveStat->SetBorderSize(1);
      m_paveStat->SetFillStyle(0);
    }
    char txt[4][256];
    std::sprintf(txt[0],"# of modules");
    std::sprintf(txt[1],"Integral : %d",m_stat[1]);
    std::sprintf(txt[2],"Underflow : %d",m_stat[0]);
    std::sprintf(txt[3],"Overflow : %d",m_stat[2]);
    for(int k=0;k<4;k++) {
      TText* tt =m_paveStat->AddText(txt[k]);
      tt->SetTextAlign(12);
    }
    m_paveStat->Draw();
  }

  void HnEveDisDetector::drawInfo()
  {
    if(!g_hPoly) return;

    if(m_paveInfo) {
      m_paveInfo->Clear();
    } else {
      m_paveInfo = new TPaveText(0.82,0.1,0.98,0.78,"NDCB");
      m_paveInfo->SetBorderSize(1);
      m_paveInfo->SetFillStyle(0);
    }
    char txt[256];
    {
      TText* tt =m_paveInfo->AddText("Entries");
      tt->SetTextAlign(12);
    }


    for(std::multimap<int,double>::iterator it=m_IDtoValueMap.begin();
	it!=m_IDtoValueMap.end();it++) {
      int   id=(*it).first;
      double v=(*it).second;
      std::sprintf(txt,"%d:%3.2f",id,v);
      
      TText* tt =m_paveInfo->AddText(txt);
      tt->SetTextAlign(12);
    }
    
    
    // TList* list = g_hPoly->GetBins();
    // if(!list) return;
    
    // int Nbin=list->GetEntries();
    // for(int i=0;i<Nbin;i++) {
    //   double v=g_hPoly->GetBinContent(i+1);
    //   if(v==m_resetValue) continue;
    //   std::sprintf(txt,"%d:%3.2f",
    // 		   m_moduleID[i],
    // 		   v);
    //   TText* tt =m_paveInfo->AddText(txt);
    //   tt->SetTextAlign(12);
    // }
    m_paveInfo->Draw();
  }

  std::vector<int> HnEveDisDetector::getModuleIDXY(double x, double y)
  {
    double xPoints[5]={0};
    double yPoints[5]={0};

    std::vector<int> vID;
    for(int i=0;i<m_nIndex;i++) {
      int j=0;
      std::vector<TVector3>::iterator it=m_globalEnvelopeDS[i].begin();
      for(;it!=m_globalEnvelopeDS[i].end();it++) {
	xPoints[j]=(*it).X();
	yPoints[j]=(*it).Y();
	j++;
      }
      it=m_globalEnvelopeDS[i].begin();
      xPoints[j]=(*it).X();
      yPoints[j]=(*it).Y();
      j++;
      
      if( TMath::IsInside(x,y,j,xPoints,yPoints) ) {
	int id = m_moduleID[i];
	vID.push_back(id);
      }
    }
    return vID;
  }

  bool HnEveDisDetector::getModuleXY(int modID, double& x, double& y)
  {
    int nHit=m_mapIDtoIndex.count(modID);
    if(nHit == 0) {
      std::cerr << "No such module ID: " << modID << std::endl;
      return false;
    }

    
    std::multimap<int,int>::iterator it=m_mapIDtoIndex.find(modID);
    x=0;
    y=0;
    for(int i=0;i<nHit;i++) {
      int index=(*it).second;
      if( !(index>=0 && index<m_nIndex) ) {
	std::cerr << "Module id " << modID << " is larger than # of module " << m_nIndex << "."<< std::endl;
	return false;
      }

      int N=m_globalEnvelopeDS[index].size();

      std::vector<TVector3>::iterator it=m_globalEnvelopeDS[index].begin();
      for(;it!=m_globalEnvelopeDS[index].end();it++) {
	x+=(*it).X()/(N*1.);
	y+=(*it).Y()/(N*1.);
      }
    }
    x/=(nHit*1.);
    y/=(nHit*1.);
    
    return true;
  }

  std::list<TGraph*> HnEveDisDetector::getGraph(int modID)
  {
    std::list<TGraph*> grList;

    int nHit=m_mapIDtoIndex.count(modID);
    if(nHit == 0) {
      std::cerr << "No such module ID: " << modID << std::endl;
      return grList;
    }


    
    std::multimap<int,int>::iterator it=m_mapIDtoIndex.find(modID);
    for(int i=0;i<nHit;i++) {
      int index=(*it).second;
      if( !(index>=0 && index<m_nIndex) ) {
	std::cerr << "Module index " << index << " is larger than # of module " << m_nIndex << "."<< std::endl;
	return grList;
      }
      TList* list = 0;
      if(g_hPoly) list = g_hPoly->GetBins();
      if(!list) return grList;
      
      TH2PolyBin* polybin = (TH2PolyBin*)list->At(index);
      if(!polybin) return grList;

      TObject* obj=polybin->GetPolygon();
      if(!obj) return grList;
      
      TGraph* gr = (TGraph*)obj;
      grList.push_back(gr);
    }
    return grList;
  }

  bool HnEveDisDetector::setLineWidth(int modID, double width)
  {
    std::list<TGraph*> grList = getGraph(modID);
    if(grList.size()==0) return false;
    for(std::list<TGraph*>::iterator it =grList.begin();
	it!=grList.end();it++) {
      (*it)->SetLineWidth(width);
    }
    return true;
  }

  bool HnEveDisDetector::setLineColor(int modID, int iColor)
  {
    std::list<TGraph*> grList = getGraph(modID);
    if(grList.size()==0) return false;
    for(std::list<TGraph*>::iterator it =grList.begin();
	it!=grList.end();it++) {
      (*it)->SetLineColor(iColor);
    }
    return true;
  }

  bool HnEveDisDetector::setFillStyle(int modID, int iStyle)
  {
    std::list<TGraph*> grList = getGraph(modID);
    if(grList.size()==0) return false;
    for(std::list<TGraph*>::iterator it =grList.begin();
	it!=grList.end();it++) {
      (*it)->SetFillStyle(iStyle);
    }
    return true;
  }

  std::list<TVector3> HnEveDisDetector::convTrapParam(std::vector<double> parameterArray) {
      
    double dz=parameterArray[0]/2.;
    double pTheta=parameterArray[1];
    double pPhi=parameterArray[2];
    double pDy1=parameterArray[3]/2.;
    double pDx1=parameterArray[4]/2.;
    double pDx2=parameterArray[5]/2.;
    double pAlp1=parameterArray[6];
    double pDy2=parameterArray[7]/2.;
    double pDx3=parameterArray[8]/2.;
    double pDx4=parameterArray[9]/2.;
    double pAlp2=parameterArray[10];
    
    double r=dz/cos(pTheta);
    
    double z=-dz;
    double xCenter = -r*sin(pTheta)*cos(pPhi);
    double yCenter = -r*sin(pTheta)*sin(pPhi);

    std::list<TVector3> vList;
    vList.push_back( TVector3( xCenter+tan(pAlp1)*pDy1+pDx2,yCenter+pDy1,z) );
    vList.push_back( TVector3( xCenter+tan(pAlp1)*pDy1-pDx2,yCenter+pDy1,z) );
    vList.push_back( TVector3( xCenter-tan(pAlp1)*pDy1-pDx1,yCenter-pDy1,z) );
    vList.push_back( TVector3( xCenter-tan(pAlp1)*pDy1+pDx1,yCenter-pDy1,z) );
    
    z=+dz;
    xCenter = r*sin(pTheta)*cos(pPhi);
    yCenter = r*sin(pTheta)*sin(pPhi);
    vList.push_back( TVector3( xCenter+tan(pAlp2)*pDy2+pDx4,yCenter+pDy2,z) );
    vList.push_back( TVector3( xCenter+tan(pAlp2)*pDy2-pDx4,yCenter+pDy2,z) );
    vList.push_back( TVector3( xCenter-tan(pAlp2)*pDy2-pDx3,yCenter-pDy2,z) );
    vList.push_back( TVector3( xCenter-tan(pAlp2)*pDy2+pDx3,yCenter-pDy2,z) );

    return vList;
  }

  std::list<TVector3> HnEveDisDetector::convBoxParam(std::vector<double> parameterArray) {
      
    double dx=parameterArray[0]/2.;
    double dy=parameterArray[1]/2.;
    double dz=parameterArray[2]/2.;
    
    double z=-dz;
    std::list<TVector3> vList;
    vList.push_back( TVector3( +dx,+dy,z) );
    vList.push_back( TVector3( -dx,+dy,z) );
    vList.push_back( TVector3( -dx,-dy,z) );
    vList.push_back( TVector3( +dx,-dy,z) );
    
    z=+dz;
    vList.push_back( TVector3( +dx,+dy,z) );
    vList.push_back( TVector3( -dx,+dy,z) );
    vList.push_back( TVector3( -dx,-dy,z) );
    vList.push_back( TVector3( +dx,-dy,z) );
    

    return vList;
  }

  void HnEveDisDetector::rotateAndTranslate(std::list<TVector3>& vList,TVector3 rot, TVector3 pos)
  {
    for(std::list<TVector3>::iterator it=vList.begin();
	it!=vList.end();it++) {
      (*it).RotateX( rot.X() );
      (*it).RotateY( rot.Y() );
      (*it).RotateZ( rot.Z() );
      (*it)=(*it)+pos;
    }
  }

  void HnEveDisDetector::sortEnvelope(std::list<TVector3>& vList)
  {
    std::multimap<double,TVector3> zmap;
    int N=vList.size()/2;
    int k=0;
    for(std::list<TVector3>::iterator it=vList.begin();
	it!=vList.end();it++,k++) {
      double z=(*it).Z();
      zmap.insert( std::make_pair(z,(*it) ));
    }
    double xc[2]={0,0};
    double yc[2]={0,0};
    std::multimap<double,TVector3>::iterator itmap=zmap.begin();
    itmap=zmap.begin();
    for(k=0;k<2*N;k++) {
      TVector3 v=(*itmap).second;
      xc[k/N]+=v.X()/(N*1.);
      yc[k/N]+=v.Y()/(N*1.);
      itmap++;
    }

    std::multimap<double,TVector3> phimap[2];
    
    itmap=zmap.begin();
    for(k=0;k<2*N;k++) {
      TVector3 v=(*itmap).second;
      double dx=v.X()-xc[k/N];
      double dy=v.Y()-yc[k/N];
      double phi=TMath::ATan2(dy,dx);
      if(dy<0) {
	phi=TMath::Pi()*2+phi;
      }
      phimap[k/N].insert( std::make_pair(phi,v) );
      itmap++;
    }

    std::multimap<double,TVector3>::iterator it0=phimap[0].begin();
    std::multimap<double,TVector3>::iterator it1=phimap[1].begin();
    k=0;
    for(std::list<TVector3>::iterator it=vList.begin();
	it!=vList.end();it++,k++) {
	
      if(k<N) {
	(*it)= (*it0).second;
	it0++;
      } else {
	(*it)= (*it1).second;
	it1++;
      }
    }
  }

  void HnEveDisDetector::fillEnvelope(std::list<TVector3>& vList,int index)
  {
    int k=0;
    int N=vList.size()/2;
    for(std::list<TVector3>::iterator it=vList.begin();
	it!=vList.end();it++,k++) {
      if(k<N) {
	m_localEnvelopeDS[index].push_back(TVector3((*it).X(),(*it).Y(),(*it).Z()));
      } else {
	m_localEnvelopeUS[index].push_back(TVector3((*it).X(),(*it).Y(),(*it).Z()));
      }
    }
  }

  void HnEveDisDetector::makeHalfDivision(std::list<TVector3>& vList,std::string axisWord,
					  std::list<TVector3>& vList0,std::list<TVector3>& vList1)
  {
    if(! (axisWord=="X" ||
	  axisWord=="Y" ||
	  axisWord=="Z")
       ) {
      std::cerr << "Wrong axisWorld for " << axisWord << ". X, Y, or Z should be assigned.";
    }

    vList0.clear();
    vList1.clear();
    
    
    double vmin=999;
    double vmax=-999;
    for(std::list<TVector3>::iterator it=vList.begin();
	it!=vList.end();it++) {
      double vp=0;
      if(axisWord=="X")vp=(*it).X();
      if(axisWord=="Y")vp=(*it).Y();
      if(axisWord=="Z")vp=(*it).Z();
      if(vp<vmin) vmin=vp;
      if(vp>vmax) vmax=vp;
    }
    double vCenter=(vmin+vmax)/2.;

    //Check if crossing point exist other than the boundary of two plane
    int cnt=0;
    bool withCrossingPoint=false;
    double vpre=-999;
    int nPoint=vList.size()/2;
    for(std::list<TVector3>::iterator it=vList.begin();
	it!=vList.end();it++,cnt++) {
      double vp=0;
      if(axisWord=="X")vp=(*it).X();
      if(axisWord=="Y")vp=(*it).Y();
      if(axisWord=="Z")vp=(*it).Z();

      if(vpre==-999) {
	vpre=vp;
	continue;
	//skip the 1st point
      }
      if( cnt!=nPoint && (vpre-vCenter)*(vp-vCenter) < 0) {
	withCrossingPoint=true;
      }
      vpre=vp;
    }

    vpre=-999;
    if(withCrossingPoint) {
      TVector3 preV;
      std::list<TVector3> vList2;
      std::list<TVector3> vListBoth[2];
      TVector3 resV;
      
      cnt=0;
      for(std::list<TVector3>::iterator it=vList.begin();
	  it!=vList.end();it++,cnt++) {
	double vp=0;
	if(axisWord=="X")vp=(*it).X();
	if(axisWord=="Y")vp=(*it).Y();
	if(axisWord=="Z")vp=(*it).Z();

	if(vp>vCenter) {
	  vListBoth[0].push_back( (*it) );
	} else {
	  vListBoth[1].push_back( (*it) );
	}

	
	if(cnt==0 || cnt==nPoint) resV=(*it);
	vList2.push_back( (*it) );

	//Add inital loop point at the last
	if(cnt==nPoint-1 || cnt==2*nPoint-1) {
	  vList2.push_back( resV );
	}
      }

      //check crossing point
      cnt=0;
      vpre=-999;
      for(std::list<TVector3>::iterator it=vList2.begin();
	  it!=vList2.end();it++,cnt++) {
	double vp=0;
	if(axisWord=="X")vp=(*it).X();
	if(axisWord=="Y")vp=(*it).Y();
	if(axisWord=="Z")vp=(*it).Z();
	
	if(vpre==-999) {
	  preV=(*it);
	  vpre=vp;
	  continue;
	  //skip the 1st point
	}

	if( (vpre-vCenter)*(vp-vCenter) < 0) {
	  //crossing boundary
	  TVector3 midV=( (*it)+preV )*0.5;
	  vListBoth[0].push_back( midV );
	  vListBoth[1].push_back( midV );
	}
	//preparation for the next
	preV=(*it);
	vpre=vp;
      }

      
      vList0=vListBoth[0];
      vList1=vListBoth[1];
      sortEnvelope(vList0);
      sortEnvelope(vList1);
      
    } else {
      //Without crossing point
      //Typically for division in Z axis
      cnt=0;
      for(std::list<TVector3>::iterator it=vList.begin();
	  it!=vList.end();it++,cnt++) {
	if(cnt<nPoint) {
	  vList0.push_back( (*it) );
	} else {
	  TVector3 newP=(*it);
	  if(axisWord=="X") newP.SetX( vCenter );
	  if(axisWord=="Y") newP.SetY( vCenter );
	  if(axisWord=="Z") newP.SetZ( vCenter );
	  vList1.push_back( newP );
	}
      }

      cnt=0;
      for(std::list<TVector3>::iterator it=vList.begin();
	  it!=vList.end();it++,cnt++) {
	if(cnt<nPoint) {
	  TVector3 newP=(*it);
	  if(axisWord=="X") newP.SetX( vCenter );
	  if(axisWord=="Y") newP.SetY( vCenter );
	  if(axisWord=="Z") newP.SetZ( vCenter );
	  vList0.push_back( newP );
	} else {
	  vList1.push_back( (*it) );
	}
      }
    }
  }
}



