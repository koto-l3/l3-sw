/**
 * @file capture.cpp
 * @brief Implementation of the 40G Capturer class and related functions for packet capture. Using Netmap
 * 
 */

#include <sys/poll.h>
#include <fstream>

#include "Capture.h"



#define NETMAP_WITH_LIBS
extern "C" {
    #include "net/netmap_user.h"
    #include "libnetmap.h"
    #include "net/netmap.h"
}


/*
* Global arguments for all threads
*/
struct glob_arg {

    int nthreads;
    int cpus;
	struct nmport_d *nmd;
	int main_fd;
    int wait_link;
	char ifname[512];
	int forever;
    uint32_t orig_mode;
    int affinity;
    int someThreadHasFinished = 0; // Non-zero if some thread has finished capturing. Used only for debugging
};

/* counters to accumulate statistics */
struct stats {
	uint64_t pkts, bytes, events;
	uint64_t min_space;
	struct timeval t;
};


/*
 * Arguments for a new thread. The same structure is used by
 * the source and the sink
 */
struct targ {
	struct glob_arg *g;
	int used;
	int completed;
	int cancel;
	int fd;
	int affinity;
    int id;

	struct nmport_d *nmd;
	struct stats ctr;
};

// Circular buffer class
CircularBufferManager::CircularBufferManager(int size)
{
    m_capacity = size;
    m_head = 0;
    m_tail = 0;
}



void CircularBufferManager::pop(int nPkts)
{

    if (GetNPackets() < nPkts)
    {
        /**
         * This could happen, since GetNPackets() doesn't work
         * atomically. Race conditions leading to this situations could happen
         * if for other reasons the event rejection is extremely high. (very
         * high corrupted-packet rate, etc.)
         *
         * The likelyhood for this to happen in normal (low evt corruption)
         * conditions is virtually zero (has never been observed), so
         * GetNPackets() is kept non-atomic to improve performance at the
         * capturer-side.
         */


        l3log.log(L3_QUIET, L3_ERROR, "Trying to pop %d packets, but buffer contains only %d packets\n", nPkts, GetNPackets());
        
        // reset buffer
        m_tail = 0;
    }

    if (m_tail + nPkts > m_capacity)
    {
        // Chunk to be removed crosses the buffer boundaries
        int first_part = m_capacity - m_tail;
        int second_part = nPkts - first_part;
        l3log.log(L3_VERBOSE, L3_INFO, "Reached the buffer boundaries. Tail becomes %d\n", nPkts, second_part);
        m_tail = second_part;
    }
    else if (m_tail + nPkts == m_capacity)
    {
        // chunk to be removed ends exactly at the buffer boundary
        m_tail = 0;
    }
    else
    {
        // chunk to be removed is within the buffer boundaries
        m_tail = (m_tail + nPkts);
    }
}



int CircularBufferManager::GetNPackets()
{

    int head = m_head;
    int tail = m_tail;
    
    if (head >= tail){

        // head = tail means that the buffer is empty.
        // Buffer is completely filled when head = tail - 1
        // (so maximum capacity is m_capacity - 1)
        return head - tail;
    }
    else{
        return m_capacity - tail + head;
    }
}


void CircularBufferManager::clear()
{
    m_head = 0;
    m_tail = 0;
}



Capturer::Capturer(Config * cfg)
{
    m_config = cfg;

    m_rawPktDataBuffer.resize(m_config->n_pcap_threads);
    m_rawPktHdrBuffer.resize(m_config->n_pcap_threads);
    m_circularBufferManager.resize(m_config->n_pcap_threads);
    
    // Thread to allocate memory buffers in the correct NUMA node
    std::thread timmy;
    l3log.log(L3_VERBOSE, L3_DEBUG, "Launching mem. alloc. thread\n");

    timmy = std::thread(&Capturer::InitMemory, this);
    SetAffinity(timmy.native_handle(), m_config->mem_alloc_cpu_id);

    l3log.log(L3_VERBOSE, L3_DEBUG, "Set affinity of mem alloc thread to CPU %d\n", m_config->mem_alloc_cpu_id);
    
    timmy.join();
}


int Capturer::SetAffinity(pthread_t me, int i) 
{
    if (i < 0)
        return 1;

    cpu_set_t cpumask;

    CPU_ZERO(&cpumask);
    CPU_SET(i, &cpumask);

    int s = pthread_setaffinity_np(me, sizeof(cpu_set_t), &cpumask);

    if (s != 0)
    {
        D("Unable to set affinity (err %d): %s", s, strerror(s));
        return 1;
    }

    return 0;
}


void Capturer::ClearStats()
{
	for (u_int i = 0; i < m_config->n_pcap_threads; i++) {
	    targs[i].ctr = stats();
    }

    m_total_packets = 0;
}


void Capturer::Debug()
{

    printf("DEBUG: Entering Capturer::Debug()! --------------------\n");
    printf("Waiting 5 s for buffer initialization...\n");
    sleep(5);

    std::ofstream fout;
    fout.open("pcap_raw.txt");

    unsigned long int total_written_pkts = 0;
    while (1)
    {
        for (u_int i_thrd = 0; i_thrd < m_config->n_pcap_threads; i_thrd++)
        {
            // printf("DEBUG: Thread %d received %lu packets\n", i_thrd, targs[i_thrd].ctr.pkts);

            if (m_circularBufferManager.size() != m_config->n_pcap_threads)
            {
                // has not been initialized yet
                printf("DEBUG: Circular buffer not initialized yet!\n");
                sleep(3);
                continue;
            }
            if (m_circularBufferManager.at(i_thrd)->GetNPackets() == 0)
            {
                sleep(1);
                continue;
            }

            int tail_idx = m_circularBufferManager.at(i_thrd)->GetTail();
            for (int i_pkt = 0; i_pkt < m_circularBufferManager.at(i_thrd)->GetNPackets(); i_pkt++)
            {
                if (m_circularBufferManager.at(i_thrd)->GetNPackets() > 0)
                {
                    while (m_circularBufferManager.at(i_thrd)->GetNPackets() > 0)
                    {

                        // BEGIN PROCESS PACKET
                        // Dump it to a file, compare its contents with a reference, etc.

                        //fout << i_iface << " " << i_thrd << " ";
                        for (int i_byte = 0; i_byte < PKT_SIZE; i_byte++)
                        {
                            fout << std::hex << std::setfill('0') << std::setw(2) << (int)m_rawPktDataBuffer.at(i_thrd)->data()[tail_idx][i_byte] << " ";
                        }
                        fout << std::endl;

                        // END PROCESS PACKET

                        // Move ptr to the next packet
                        m_circularBufferManager.at(i_thrd)->pop(1);
                        tail_idx = m_circularBufferManager.at(i_thrd)->GetTail();
                        total_written_pkts++;
                        if (total_written_pkts % 1000 == 0)
                            l3log.log(L3_VERBOSE, L3_DEBUG, "(thread %d): %d packets written to file...\n", i_thrd, total_written_pkts);
                            //printf("DEBUG (thread %d): %d packets written to file...\n", i_thrd, total_written_pkts);
                    }
                    printf("DEBUG: %lu packets written to file...\n", total_written_pkts);
                }
            }
        }
    
        usleep(10000);
    }
    fout.close();

    printf("DEBUG: Exiting Capturer::Debug()! --------------------\n");
}


void * Capturer::receiver_body(void *targ_)
{

    struct targ *targ = (struct targ *) targ_;

    struct pollfd pfd = { .fd = targ->fd, .events = POLLIN, .revents = 0 };
    struct netmap_if *nifp;
    struct netmap_ring *ring;
    struct stats cur;
    memset(&cur, 0, sizeof(cur));

    D("reading from %s fd %d", targ->g->ifname, targ->fd);


    nifp = targ->nmd->nifp;
    int ri = targ->nmd->first_rx_ring;
    ring = NETMAP_RXRING(nifp, ri);


    int pollRet = 0;
    int pollPrevState = 0;


    /* Not the actual ThreadID, but just a tag */
    const int thisThreadID = targ->id;


    const int circBufCapacity = m_circularBufferManager.at(thisThreadID)->m_capacity;
    int circBufHead = m_circularBufferManager.at(thisThreadID)->m_head;

    while (1){

        pollRet = poll(&pfd, 1, 1000);
        if (pollRet <= 0) 
        {
            // If we haven't got any packet now
            bool flag = false;
            if (pollPrevState > 0) {
                // If we got packets before, but not now
                flag = true;
            }
            else if (m_config->m_someThreadHasFinished > 0){
                // if this thread hasn't got anything, but some other thread has
                // already finished capturing
                l3log.log(L3_NORMAL, L3_WARN, "Thread %d hasn't captured any packets this spill (yet)\n", thisThreadID);
                flag = true;
            }
            if (flag && m_config->exit_after_first_spill){
                break;
            }
        }
        pollPrevState = pollRet;

        if (!nm_ring_empty(ring)) {

            u_int head = ring->head;
            int packetsInRing = nm_ring_space(ring);
            int rx;

            if (packetsInRing > 1000){
                // Max capacity of a ring is ~2048 packets, but if pcap is fast
                // enough packetsInRing should not even reach 1000.
                l3log.log(L3_QUIET, L3_WARN, "(thread %d) nPackets in Netmap Ring reached: %d\n", thisThreadID, packetsInRing);
            }


            int badPackets = 0;

            if (m_circularBufferManager.at(thisThreadID)->GetNPackets() + packetsInRing >= circBufCapacity)
            {
                l3log.log(L3_QUIET, L3_ERROR, "(thread %d) Not enough space in PCAP buffers to offload current Netmap ring. Dropping packets like there's no tomorrow! (PCAP is faster than Processer and/or buffers are too small)\n", thisThreadID);

                while (circBufCapacity - m_circularBufferManager.at(thisThreadID)->GetNPackets() <= PKTS_PER_EVT*10){
                    l3log.log(L3_QUIET, L3_DEBUG, "(thread %d) nPkts in Buffer: %d / %d\n", thisThreadID, m_circularBufferManager.at(thisThreadID)->GetNPackets(), m_circularBufferManager.at(thisThreadID)->m_capacity);
                    sleep(10); // (de perdidos al río)
                }
            }

            // Loop through packets (non empty slots) in each ring.
            for (rx = 0; rx < packetsInRing; rx++) {
                u_int idx = ring->slot[head].buf_idx;
                u_char *buf = (u_char *)NETMAP_BUF(ring, idx);

                //printf("tid %d, buf_idx: %d, idx: %d, len %d, head %d, rx %d, tail %d, packetsInRing %d\n", thisThreadID, ring->slot[head].buf_idx, idx, ring->slot[head].len, ring->head, rx, ring->tail, packetsInRing);
                //printf("evtid: %d, rx:%d, head:%d, packetsInRing:%d, ring->head: %d, idx:%d, buf:%p\n", buf[15] << 8 | buf[16], rx, head, packetsInRing, ring->head, idx, buf);

                if (ring->slot[head].len != PKT_SIZE && ring->slot[head].len != LAST_PKT_SIZE)
                {
                    l3log.log(0, L3_WARN, "slot->len (incoming packet length): %d B, which is not equal to %d or %d\n", ring->slot[head].len, PKT_SIZE, LAST_PKT_SIZE);
                    head = nm_ring_next(ring, head);

                    badPackets++;
                    continue;
                }

                if (circBufHead == circBufCapacity)
                {
                    l3log.log(L3_VERBOSE, L3_DEBUG, "Reached end of circular buffer %d! Resetting head to 0\n", thisThreadID);
                    circBufHead = 0;
                }

                // Get stuff from OFC2 header
                m_rawPktHdrBuffer[thisThreadID]->at(circBufHead).pktNo = buf[19];

                // Get ADC data
                std::copy(buf+OFC2_HDR_SIZE, buf+ring->slot[head].len, m_rawPktDataBuffer[thisThreadID]->at(circBufHead));

                circBufHead++;
                head = nm_ring_next(ring, head);
            }

            m_circularBufferManager.at(thisThreadID)->m_head = circBufHead;

            /* prepare to recv next ring */
            ring->head = ring->cur = head;

            //printf("Thread %d: %d packets in ring, %d bad packets, head: %d\n", thisThreadID, packetsInRing, badPackets, head);

            // update stats
            targ->ctr.pkts += packetsInRing - badPackets;
        }
    }

    // This point is never reached duting normal operation
    m_config->m_someThreadHasFinished++;
    return 0;
}


void * Capturer::reporting_thread(void *_g) {


    glob_arg g = *(glob_arg *) _g;

    struct targ *t;

    int prevNPkts = 0;
    while (true) {
        
        sleep(1);
        m_total_packets = 0;

        for (int i = 0; i < g.nthreads; i++) {
        	t = &targs[i];
            l3log.log(L3_VERBOSE, L3_INFO, "- Thread %d: %d packets\n", i, t->ctr.pkts);
            m_total_packets += t->ctr.pkts;
        }
        l3log.log(L3_QUIET, L3_INFO, "Total: %d packets\n", m_total_packets);

        if (prevNPkts == m_total_packets && m_total_packets > 0)
        {
            l3log.log(L3_VERBOSE, L3_INFO, "No packets received in the last second!\n");

            if (m_config->do_profiling || m_config->exit_after_first_spill)
            {
                break;
            }
        }

        prevNPkts = m_total_packets;
    }

    return 0;
}



int Capturer::StartThreads(glob_arg &g) {


    // Init Capturer Thread Pool
    m_threads = std::make_unique<std::thread[]>(m_config->n_pcap_threads);

    targs = (struct targ *) calloc(g.nthreads, sizeof(*targs));

    struct targ *t;
    for (int i = 0; i < g.nthreads; i++) 
    {
		t = &targs[i];
		bzero(t, sizeof(*t));

		t->fd = -1; /* default, with pcap */
		t->g = &g;
        t->id = i; /* Thread ID */


        if (i > 0)
        {
            /* the first thread uses the fd opened by the main
			 * thread, the other threads re-open /dev/netmap
			 */

            t->nmd = nmport_clone(g.nmd);
            if (t->nmd == NULL)
				return -1;

            int j = i;

            t->nmd->reg.nr_ringid = j & NETMAP_RING_MASK;
            t->nmd->reg.nr_flags |= NETMAP_NO_TX_POLL;

            // Register interface
			if (nmport_open_desc(t->nmd) < 0) {
				nmport_undo_prepare(t->nmd);

                l3log.log(L3_QUIET, L3_ERROR, "Unable to open netmap port %s. Have you run env.sh in all the nodes?\n", g.ifname);

				t->nmd = NULL;
				return -1;
			}
        }
        else
        {
            // Recycle the main thread's nmd
            t->nmd = g.nmd;
        }
        t->fd = t->nmd->fd;
    

        if (g.affinity >= 0) {
            // bind the thread to a specific CPU
            t->affinity = m_config->first_capture_cpu_id + (g.affinity + i*2) % g.cpus;
            l3log.log(L3_QUIET, L3_INFO, "Thread %d of iface %s bound to CPU %d\n", i, g.ifname, t->affinity);
        }
        else{
            t->affinity = -1;
        }
    }


    /* Wait for PHY reset. */
	D("Wait %d secs for phy reset", g.wait_link);
	sleep(g.wait_link);
    D("Ready...");

    // Create reporting thread
    std::thread repo = std::thread(&Capturer::reporting_thread, this, &g);

    // Create capturing threads
    for (int i = 0; i < g.nthreads; i++)
    {
        t = &targs[i];

        l3log.log(L3_QUIET, L3_INFO, "Creating thread %d for iface %s\n",  i, g.ifname);
        m_threads[i] = std::thread(&Capturer::receiver_body, this, t);

        if (SetAffinity(m_threads[i].native_handle(), t->affinity) == 1)
        {
            l3log.log(L3_QUIET, L3_ERROR, "Did not set thread affinity!\n");
        }
    }

    // Join reporting thread. During normal operation, we won't get past this line
    repo.join();

    if (m_config->debug_40G || m_config->exit_after_first_spill || m_config->do_profiling)
    {
        l3log.log(L3_QUIET, L3_DEBUG, "Finished packet capture.\n");
        m_pcap_finished = true;
    }
    return 0;
}



void * Capturer::InitMemory()
{

    // Sleep some time so we have time to set the affinity of this thread to the
    // 40G local NUMA node before it starts allocating memory. (1s is way more
    // tha enonugh). There is no need for more elegant solutions here.
    sleep(1);

    // get this thread's affinity
    int cpu = sched_getcpu();
    l3log.log(L3_QUIET, L3_INFO, "CPU %d is allocating memory to hold %lu packets (%lu events) per thread.\n", cpu, m_config->mem_per_thread_buff/PKT_SIZE, m_config->mem_per_thread_buff/PKT_SIZE/67);

    // init raw packet data buffer
    for (u_int i = 0; i < m_config->n_pcap_threads; i++){
        m_rawPktDataBuffer[i] = std::make_shared<std::array<u_char[PKT_SIZE-OFC2_HDR_SIZE], N_PKTS_PER_THREAD+1>>();
    }

    // init raw packet header buffer
    for (u_int i = 0; i < m_config->n_pcap_threads; i++){
        m_rawPktHdrBuffer[i] = std::make_shared<std::array<struct pcap_pkthdr, N_PKTS_PER_THREAD>>();
    }

    // init circular buffer
    for (u_int i = 0; i < m_config->n_pcap_threads; i++){
        m_circularBufferManager[i] = std::make_shared<CircularBufferManager>(N_PKTS_PER_THREAD);
    }

    return 0;
}



int Capturer::Run() {

    //struct glob_arg g;
    glob_arg g;

    g.main_fd = -1;
    g.nthreads = m_config->n_pcap_threads;
    g.cpus = 40;
    g.wait_link = 2;
    g.forever = 1;
    g.affinity = 0; //0 to enable, -1 to disable

    // device name
    strcpy(g.ifname, ("netmap:" + m_config->ifname_40G).c_str());

    l3log.log(L3_QUIET, L3_INFO, "Opening %s for packet capture...\n", ("netmap:" + m_config->ifname_40G).c_str());
    g.nmd = nmport_prepare(g.ifname);
    if (g.nmd == NULL)
    {
        l3log.log(L3_QUIET, L3_ERROR, "Unable to prepare netmap port %s\n", g.ifname);
        exit(1);
    }

    /* If using many threads, map one thread to one single HW ring */
    g.orig_mode = g.nmd->reg.nr_mode;
    if (g.nthreads > 1)
    {
        switch (g.orig_mode)
        {
        case NR_REG_ALL_NIC:
        case NR_REG_NIC_SW: // This is ours
            g.nmd->reg.nr_mode = NR_REG_ONE_NIC;
            break;
        case NR_REG_SW:
            g.nmd->reg.nr_mode = NR_REG_ONE_SW;
            break;
        default:
            break;
        }
        g.nmd->reg.nr_ringid = 0;
    }

    if (nmport_open_desc(g.nmd) < 0)
    {
        l3log.log(L3_QUIET, L3_ERROR, "Unable to open netmap port %s. Have you run env.sh in all the nodes?\n", g.ifname);
        exit(1);
    }

    // get num of queues in rx. TODO: check if this is useful
    int devqueues = g.nmd->reg.nr_rx_rings + g.nmd->reg.nr_host_rx_rings;
    printf("INFO: nr_rx_rings: %d, nr_host_rx_rings: %d, devqueues: %d\n", g.nmd->reg.nr_rx_rings, g.nmd->reg.nr_host_rx_rings, devqueues);

    // release lock, so Raw2Wfm can start
    l3log.log(L3_QUIET, L3_INFO, "Hi, capture here. Releasing lock, so Raw2Wfm can start!\n");
    cv.notify_one();

    /* Initialize capturing threads */
	if (StartThreads(g) < 0){
        fprintf(stderr, "Unable to start all threads.\n");
        return 1;
    }

    nmport_close(g.nmd);
    return 0;
}