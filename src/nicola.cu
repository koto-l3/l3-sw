#include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
// #include <time.h>
#include <sys/time.h>
#include <chrono>
// #include <thread>
#include <fstream>
#include <unistd.h>
// #include <string>

#include "nicola.h"
#include "nvtx3/nvToolsExt.h"
#include "common.h"

#define USEGPU 1
#define DOTAKOLA 0

#define gpuErrchk(ans)                        \
    {                                         \
        gpuAssert((ans), __FILE__, __LINE__); \
    }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
    if (code != cudaSuccess)
    {
        fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (abort)
            exit(code);
    }
}





std::chrono::steady_clock::time_point cpuTimer()
{
    struct timeval tp;
    gettimeofday(&tp, NULL);

    std::chrono::steady_clock::time_point t = std::chrono::steady_clock::now();
    return (t);
}

void compress_cpu(u_int16_t *d_idata, u_int8_t *h_odata, unsigned int wfmLen, unsigned int nWfms, int *h_wfmInfo)
{

    // printf("nWfms: %d\n", nWfms);
    // printf("h_wfmInfo[(nWfms-1)*3+2]: %d\n", h_wfmInfo[(nWfms-1)*3+2]);
    // exit(1);

    // Useful for dbg, but not needed
    for (u_int i = 0; i < h_wfmInfo[(nWfms - 1) * 3 + 2] + h_wfmInfo[(nWfms - 1) * 3 + 1] * wfmLen; i++)
    {
        h_odata[i] = 99;
    }
    for (u_int i = 0; i < h_wfmInfo[(nWfms - 1) * 3 + 2] + h_wfmInfo[(nWfms - 1) * 3 + 1] * wfmLen / 8 + 3; i++)
    {
        h_odata[i] = 0;
    }

    for (u_int wfmN = 0; wfmN < nWfms; wfmN++)
    {

        u_int16_t *idata = d_idata + wfmN * 64;

        int r = h_wfmInfo[wfmN * 3 + 1];
        int min = h_wfmInfo[wfmN * 3];
        int outIdx = h_wfmInfo[wfmN * 3 + 2];


        // int byte = wfmN*wfmLen*2 + wfmN;
        h_odata[outIdx + 0] = r;
        h_odata[outIdx + 1] = (min & 0xFF00) >> 8;
        h_odata[outIdx + 2] = (min & 0x00FF);

        int bit = 0;
        if (r != 16)
        {
            for (u_int s = 0; s < wfmLen; s++)
            {
                outIdx = h_wfmInfo[wfmN * 3 + 2] + 3 + int(s * r / 8);
                // printf("byte1=  %d\n", outIdx);

                bit = s * r % 8;
                if (8 - r - bit >= 0)
                {
                    // printf("n, bit = [%d, %d]\n", n, bit);
                    h_odata[outIdx] += ((idata[s] - min) << (8 - r - bit));

                    // bit += r;
                }
                else
                {
                    uint8_t byte1 = ((idata[s] - min) >> (r + bit - 8));
                    uint8_t byte2 = (((idata[s] - min) << (8 - (r + bit - 8))) & 0x00FF);

                    h_odata[outIdx] += byte1;
                    h_odata[outIdx + 1] += byte2;

                }
            }
        }
        else
        {
            int byte = h_wfmInfo[wfmN * 3 + 2];

            h_odata[byte + 0] = 16; // r

            for (u_int s = 0; s < wfmLen; s++)
            {
                h_odata[(byte + 1) + 2 * s] = (uint8_t)(idata[s] >> 8);
                h_odata[(byte + 1) + 2 * s + 1] = (uint8_t)(idata[s] & 0x00FF);
            }
        }
    }

}

// Kernel to transpose
__global__ void transpose(const uint16_t *d_idata, uint16_t *d_odata)
{
    // dimGrid = 288, 1, 1 (nMatrices)
    // dimBlock = 16, 64, 1 (nx, ny)

    __shared__ uint16_t tile[16][64]; // each f the individual matrices

    // pointer to input element of the input matrix for each thread
    // (matrixID * matrixSize)
    int index_in = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;

    // int index_in = x + width;
    int index_out = blockIdx.x * blockDim.x * blockDim.y + threadIdx.x * blockDim.y + threadIdx.y;

    tile[threadIdx.x][threadIdx.y] = d_idata[index_in];

    __syncthreads();

    d_odata[index_out] = tile[threadIdx.x][threadIdx.y];
}


__device__ void do_fiducial_selection(Clusters d_clusters, uint16_t *d_recoTag, float minXY, float maxR, int evtid)
{

    int evtID = blockIdx.x;
    int nClusters = d_clusters.n[evtID];
    int tid = threadIdx.x;

    if (nClusters == 0)
    {
        // nClusters is also set to 0 when nClusters > MAX_CSI_CLUS_PER_EVT
        //printf("0 (or too many) clusters in i_event %d\n", evtID);
        return;
    }
    else if (tid < nClusters)
    {
        float x = d_clusters.x[evtID * MAX_CSI_CLUS_PER_EVT + tid];
        float y = d_clusters.y[evtID * MAX_CSI_CLUS_PER_EVT + tid];


        if (abs(x) < minXY && abs(y) < minXY)
        {
            d_recoTag[evtID] |= REJ_MINXY_BIT;
            //printf("rejected by minXY fiducial selection. x = %f, y = %f\n", x, y);
        }
        if (x * x + y * y > maxR * maxR)
        {
            d_recoTag[evtID] |= REJ_MAXR_BIT;
            //printf("rejected by maxR fiducial selection. x = %f, y = %f\n", x, y);
        }

    }

}

__device__ void do_pi0ee_selection(Clusters d_clusters, uint16_t *d_recoTag, float minE, float maxCOE)
{
    int evtID = blockIdx.x;
    int nClusters = d_clusters.n[evtID];
    int tid = threadIdx.x;

    __shared__ float COE[2];
    __shared__ float E;
    E = 0;
    COE[0] = 0;
    COE[1] = 0;


    __syncthreads();

    if (nClusters == 0)
    {
        // nClusters is also set to 0 when nClusters > MAX_CSI_CLUS_PER_EVT
        return;
    }
    else if (tid < nClusters)
    {
        if (0)
        {
            return;
        }
        else
        {
            float x = d_clusters.x[evtID * MAX_CSI_CLUS_PER_EVT + tid];
            float y = d_clusters.y[evtID * MAX_CSI_CLUS_PER_EVT + tid];

            atomicAdd(&E, d_clusters.E[evtID * MAX_CSI_CLUS_PER_EVT + tid]);
            atomicAdd(&COE[0], x * d_clusters.E[evtID * MAX_CSI_CLUS_PER_EVT + tid]);
            atomicAdd(&COE[1], y * d_clusters.E[evtID * MAX_CSI_CLUS_PER_EVT + tid]);

            __syncthreads();

            if (E < minE)
            {
                d_recoTag[evtID] |= REJ_TOTALE_BIT;
                //printf("rejected by totalE selection. E = %f\n", E);
            }
            
            float coe = (COE[0] * COE[0] + COE[1] * COE[1]) / (E * E);
            if (coe > maxCOE*maxCOE)
            {
                d_recoTag[evtID] |= REJ_COE_BIT;
                //printf("rejected by COE selection. coe = %f\n", coe);
            }
        }
    }
}

__device__ void do_kp_selection(Clusters d_clusters, uint16_t *d_recoTag, float minE)
{
    int evtID = blockIdx.x;
    int nClusters = d_clusters.n[evtID];
    int tid = threadIdx.x;

    __shared__ float E;
    E = 0;

    __syncthreads();

    if (nClusters == 0)
    {
        // nClusters is also set to 0 when nClusters > MAX_CSI_CLUS_PER_EVT
        return;
    }
    else if (tid < nClusters)
    {

        atomicAdd(&E, d_clusters.E[evtID * MAX_CSI_CLUS_PER_EVT + tid]);

        __syncthreads();

        if (E < minE)
        {
            d_recoTag[evtID] |= REJ_TOTALE_BIT;
        }
    }
}

__device__ void do_5g_selection(Clusters d_clusters, uint16_t *d_recoTag, float minE, float minCOE)
{
    int evtID = blockIdx.x;
    int nClusters = d_clusters.n[evtID];
    int tid = threadIdx.x;

    __shared__ float COE[2];
    __shared__ float E;
    E = 0;
    COE[0] = 0;
    COE[1] = 0;


    __syncthreads();

    if (nClusters == 0)
    {
        // nClusters is also set to 0 when nClusters > MAX_CSI_CLUS_PER_EVT
        return;
    }
    else if (tid < nClusters)
    {

        float x = d_clusters.x[evtID * MAX_CSI_CLUS_PER_EVT + tid];
        float y = d_clusters.y[evtID * MAX_CSI_CLUS_PER_EVT + tid];

        atomicAdd(&E, d_clusters.E[evtID * MAX_CSI_CLUS_PER_EVT + tid]);
        atomicAdd(&COE[0], x * d_clusters.E[evtID * MAX_CSI_CLUS_PER_EVT + tid]);
        atomicAdd(&COE[1], y * d_clusters.E[evtID * MAX_CSI_CLUS_PER_EVT + tid]);

        __syncthreads();

        if (E < minE)
        {
            d_recoTag[evtID] |= REJ_TOTALE_BIT;
            //printf("rejected by totalE selection. E = %f\n", E);
        }

        float coe = (COE[0] * COE[0] + COE[1] * COE[1]) / (E * E);
        if (coe < minCOE*minCOE)
        {
            d_recoTag[evtID] |= REJ_COE_BIT;
            //printf("rejected by COE selection. coesq = %f\n", coe);
        }
    }
}


__global__ void kernel_event_selection(Clusters d_clusters, uint16_t *d_recoTag)
{

    int evtID = blockIdx.x;


    if ((d_recoTag[evtID] & 0x00ff) == REC_RECO_5G) // do 5g selection
    {
        do_fiducial_selection(d_clusters, d_recoTag, SEL_5G_MIN_XY, SEL_5G_MAX_R, evtID);
        __syncthreads();

        do_5g_selection(d_clusters, d_recoTag, SEL_5G_MIN_E, SEL_5G_MIN_COE);
    }
    else if ((d_recoTag[evtID] & 0x00ff) == REC_RECO_KP) // do Kp selection
    {
        do_fiducial_selection(d_clusters, d_recoTag, SEL_KP_MIN_XY, SEL_KP_MAX_R, evtID);
        __syncthreads();

        do_kp_selection(d_clusters, d_recoTag, SEL_KP_MIN_E);
    }
    else if ((d_recoTag[evtID] & 0x00ff) == REC_RECO_PI0EE) // do pi0ee selection
    {       
        do_fiducial_selection(d_clusters, d_recoTag, SEL_PI0EE_MIN_XY, SEL_PI0EE_MAX_R, evtID);
        __syncthreads();

        do_pi0ee_selection(d_clusters, d_recoTag, SEL_PI0EE_MIN_E, SEL_PI0EE_MAX_COE);
    }
    return;
}

void launchSelectKernel(Clusters *d_clusters, uint16_t *d_recoTag, cudaStream_t stream, int nEvents)
{
    //nvtxRangePush("launchSelectKernel");
    
    dim3 blockSize = 128; // Threads per block
    dim3 gridSize = nEvents;


    if (nEvents == 0)
    {
        return;
    }

    kernel_event_selection<<<gridSize, blockSize, 0, stream>>>(*d_clusters, d_recoTag);
}

__device__ float CalcPedestal(uint16_t *wfm)
{
    // This is the offline algorithm. Not optimized at all, terribly inefficient
    // on GPU. Used just for testing.

    int sample_offset = 1;
    int wfmLen = 64;

    // check for nullptr
    if (wfm == nullptr)
    {
        return 0;
    }

    const int n_used_samples = 10 - sample_offset;

    float Ped1 = 0; // First n_used_samples
    float Ped2 = 0; // last n_used_samples
    float RMS1 = 0; // First n_used_samples
    float RMS2 = 0; // last n_used_samples

    for (int i_sample = sample_offset, n = sample_offset + n_used_samples; i_sample < n; i_sample++)
    {
        Ped1 += (float)wfm[i_sample] / (double)n_used_samples;
        Ped2 += (float)wfm[wfmLen - 1 - i_sample] / (double)n_used_samples;
    }
    for (int i_sample = sample_offset, n = sample_offset + n_used_samples; i_sample < n; i_sample++)
    {
        RMS1 += pow((float)wfm[i_sample] - Ped1, 2);
        RMS2 += pow((float)wfm[wfmLen - 1 - i_sample] - Ped2, 2);
    }
    RMS1 = sqrt(RMS1);
    RMS2 = sqrt(RMS2);

    float pedestal = Ped1;
    if (RMS2 < RMS1)
    {
        pedestal = Ped2;
    }

    return pedestal;
}


__device__ void calc_integrated_adc(float *integratedADC, uint16_t this_wfm[][64])
{


    // thread id inside wfm (32 threads / wfm)
    unsigned int id_thr_inside_waveform = threadIdx.x % 32;
    unsigned int id_wfm_inside_block = threadIdx.x / 32; // wfm number (0 to 8)

    //float pedestal = 0;
    //if (id_thr_inside_waveform == 0)
    //{
    //    pedestal = CalcPedestal(this_wfm[id_wfm_inside_block]);
    //    //printf("id_wfm_inside_block = %d\n", id_wfm_inside_block);
    //}
    //__syncthreads();


    // Calculate Integrated ADC
    __shared__ int local_sum_shared[8][32];

    __syncthreads();

    // first 32 samples
    local_sum_shared[id_wfm_inside_block][id_thr_inside_waveform] = this_wfm[id_wfm_inside_block][id_thr_inside_waveform];

    // last 32 samples
    local_sum_shared[id_wfm_inside_block][id_thr_inside_waveform] += this_wfm[id_wfm_inside_block][id_thr_inside_waveform + 32];

    __syncthreads();

    // reduce local_sum_shared

    if (id_thr_inside_waveform < 16)
    {
        local_sum_shared[id_wfm_inside_block][id_thr_inside_waveform] += local_sum_shared[id_wfm_inside_block][id_thr_inside_waveform + 16];
    }
    __syncthreads();
    if (id_thr_inside_waveform < 8)
    {
        local_sum_shared[id_wfm_inside_block][id_thr_inside_waveform] += local_sum_shared[id_wfm_inside_block][id_thr_inside_waveform + 8];
    }
    __syncthreads();
    if (id_thr_inside_waveform < 4)
    {
        local_sum_shared[id_wfm_inside_block][id_thr_inside_waveform] += local_sum_shared[id_wfm_inside_block][id_thr_inside_waveform + 4];
    }
    __syncthreads();
    if (id_thr_inside_waveform < 2)
    {
        local_sum_shared[id_wfm_inside_block][id_thr_inside_waveform] += local_sum_shared[id_wfm_inside_block][id_thr_inside_waveform + 2];
    }
    __syncthreads();
    if (id_thr_inside_waveform == 0)
    {
        integratedADC[id_wfm_inside_block] = local_sum_shared[id_wfm_inside_block][0] + local_sum_shared[id_wfm_inside_block][0 + 1] - this_wfm[id_wfm_inside_block][0]*64;
        //integratedADC[id_wfm_inside_block] = local_sum_shared[id_wfm_inside_block][0] + local_sum_shared[id_wfm_inside_block][0 + 1] - CalcPedestal(this_wfm[id_wfm_inside_block])*64;
    }
    __syncthreads();

}

__device__ void load_wfms_into_shared_data( unsigned int id_detCh, uint16_t *this_evt_data, const uint16_t *d_CSIDetChIdToADCChId, uint16_t this_wfm[][64], unsigned int id_wfm_inside_block, unsigned int id_thr_inside_waveform)
{

    // Load waveform into shared memory
    int d_idata_wfm_idx = d_CSIDetChIdToADCChId[id_detCh] * 64 + id_thr_inside_waveform;

    // load first 32 samples
    this_wfm[id_wfm_inside_block][id_thr_inside_waveform] = this_evt_data[d_idata_wfm_idx];

    // load last 32 samples
    this_wfm[id_wfm_inside_block][id_thr_inside_waveform + 32] = this_evt_data[d_idata_wfm_idx + 32];
}

__global__ void kernel_reconstruct_csi(uint16_t *d_idata, uint16_t *d_wfmInfo, float *d_csi_energies, uint16_t *d_csiTimes, const uint16_t *d_CSIDetChIdToADCChId, const float *d_gainMeanCoefs, const float *d_ADCMevCoefs, int csi_n_channels, uint16_t *d_recoTag, uint8_t *d_csiChsMaskForReco)
{

    unsigned int eventID = blockIdx.y; // inside grid (0 to events)

    // return if this event doesn't need to be reconstructed
    if ((d_recoTag[eventID] & 0x00ff) > SEL_THRESHOLD)
    {
        return;
    }


    // blockIdx.x: 0 to 340
    // blockDim.x: 256
    // threadIdx.x: 0 to 255
    // 32 threads per waveform
    unsigned int id_detCh = (blockIdx.x * blockDim.x + threadIdx.x) / 32; 
    if (id_detCh >= csi_n_channels)
    {
        return;
    }


    // thread id inside wfm (32 threads / wfm)
    unsigned int id_thr_inside_waveform = threadIdx.x % 32;
    unsigned int id_wfm_inside_block = threadIdx.x / 32; // wfm number (0 to 8)

    // 32 threads (1 warp) per waveform.
    // One block = 8 waveforms.

    //if (threadIdx.x == 0 && blockIdx.x ==0)
    //{
    //    printf("Reconstructing i_event %d, recoTag = %d\n", eventID, d_recoTag[eventID]);
    //}




    //__shared__ uint16_t this_wfm[8][64];

    uint16_t *this_evt_data = d_idata + 288 * 16 * 64 * eventID;

    // offline pedestal, not for production
    // > > > >
    //__shared__ float pedestal[8];
    //__syncthreads();
    //if (id_thr_inside_waveform == 0)
    //{
    //    pedestal[id_wfm_inside_block] = CalcPedestal(this_evt_data + d_CSIDetChIdToADCChId[id_detCh] * 64);
    //}
    //__syncthreads();
    // < < < <

    __syncthreads();


    // Load waveform into shared memory
    __shared__ uint16_t this_wfm[8][64];
    load_wfms_into_shared_data(id_detCh, this_evt_data, d_CSIDetChIdToADCChId, this_wfm, id_wfm_inside_block, id_thr_inside_waveform);


    __syncthreads();

    // get integrated ADC
    __shared__ float integratedADC[8];
    calc_integrated_adc(integratedADC, this_wfm);


    __syncthreads();

    // Calculate timing
    __shared__ volatile int local_max_vals[8][32];
    __shared__ volatile int local_max_idxs[8][32];
    int this_thread_val = INT_MIN;
    int this_thread_idx = -1;

    // first 32 samples
    this_thread_val = this_wfm[id_wfm_inside_block][id_thr_inside_waveform];
    this_thread_idx = id_thr_inside_waveform;

    // last 32 samples
    this_thread_val = max(this_thread_val, this_wfm[id_wfm_inside_block][id_thr_inside_waveform + 32]);
    this_thread_idx = (this_thread_val == this_wfm[id_wfm_inside_block][id_thr_inside_waveform + 32]) ? id_thr_inside_waveform + 32 : this_thread_idx;

    __syncthreads();
    local_max_vals[id_wfm_inside_block][id_thr_inside_waveform] = this_thread_val;
    local_max_idxs[id_wfm_inside_block][id_thr_inside_waveform] = this_thread_idx;

    for (int i = 16; i > 1; i >>= 1)
    {
        if (id_thr_inside_waveform < i)
        {
            if (local_max_vals[id_wfm_inside_block][id_thr_inside_waveform] < local_max_vals[id_wfm_inside_block][id_thr_inside_waveform + i])
            {
                local_max_vals[id_wfm_inside_block][id_thr_inside_waveform] = local_max_vals[id_wfm_inside_block][id_thr_inside_waveform + i];
                local_max_idxs[id_wfm_inside_block][id_thr_inside_waveform] = local_max_idxs[id_wfm_inside_block][id_thr_inside_waveform + i];
            }
        }
        __syncthreads();
    }



    if (id_thr_inside_waveform == 0)
    {

        if (d_csiChsMaskForReco[id_detCh] == 1)
        {
            d_csi_energies[eventID * csi_n_channels + id_detCh] = 0;
            d_csiTimes[eventID * csi_n_channels + id_detCh] = 0;
        }
        else
        {


        d_csiTimes[eventID * csi_n_channels + id_detCh] = (local_max_vals[id_wfm_inside_block][0] > local_max_vals[id_wfm_inside_block][1]) ? local_max_idxs[id_wfm_inside_block][0] : local_max_idxs[id_wfm_inside_block][1];
        
        float peakHeigh = max(local_max_vals[id_wfm_inside_block][0], local_max_vals[id_wfm_inside_block][1]) - this_wfm[id_wfm_inside_block][0];


        //if (id_thr_inside_waveform == 0)
        //{
        //    if (integratedADC[id_wfm_inside_block] < 64 && this_wfm[id_wfm_inside_block][0] - this_wfm[id_wfm_inside_block][1] > 0)
        //    {
        //        integratedADC[id_wfm_inside_block] += 64 * (this_wfm[id_wfm_inside_block][0] - this_wfm[id_wfm_inside_block][1]);
        //    }
        //}

        float ene = integratedADC[id_wfm_inside_block] / d_gainMeanCoefs[id_detCh] * d_ADCMevCoefs[id_detCh];

        if (peakHeigh > 4000){

            if (peakHeigh < 13000)
            {
                float linf = 1 - 4e-14*(peakHeigh*peakHeigh*peakHeigh);
                ene = ene/linf;
            }
            else
            {
                float linf = 0.86;
                ene = ene/linf;
            }
            //printf("ene = %f; linf = %f\n", ene, linf);
        }
        

        d_csi_energies[eventID * csi_n_channels + id_detCh] = ene;


        //d_tmp_csi_pedestals[eventID * csi_n_channels + id_detCh] = CalcPedestal(this_wfm[id_wfm_inside_block]);
        //d_tmp_csi_pedestals[eventID * csi_n_channels + id_detCh] = this_wfm[id_wfm_inside_block][0];
        //d_tmp_csi_intADCs[eventID * csi_n_channels + id_detCh] = integratedADC[id_wfm_inside_block];
        //d_tmp_csi_peakHeight[eventID * csi_n_channels + id_detCh] = max(local_max_vals[id_wfm_inside_block][0], local_max_vals[id_wfm_inside_block][1]) - d_tmp_csi_pedestals[eventID * csi_n_channels + id_detCh];
        }
    }
}

__global__ void kernel_print_ET(float* d_energies, float *d_csi_energies, uint16_t *d_csiTimes, int csi_n_channels, int nEvents)
{

    __shared__ float totalE;

    for (int i_evt = 0; i_evt < nEvents; i_evt++)
    {
        totalE = 0;
        int nHitChs = 0;
        __syncthreads();
        for (int i = 0; i < csi_n_channels; i++)
        {
            if (d_csi_energies[i_evt * csi_n_channels + i] > 3)
            {
                if (d_csiTimes[i_evt * csi_n_channels + i] > 24 && d_csiTimes[i_evt * csi_n_channels + i] < 30)
                {
                    totalE += d_csi_energies[i_evt * csi_n_channels + i];
                    nHitChs++;
                }
            }
        }
        //printf("evtID = %d; totalE = %f; nHitChs = %d\n", i_evt, totalE, nHitChs);
        d_energies[i_evt] = totalE;
    }

    __syncthreads();
}

void LaunchReconstructionKernel(uint16_t *d_idata, uint16_t *d_wfmInfo, float *d_csi_energies, uint16_t *d_csiTimes, const uint16_t *d_CSIDetChIdToADCChId, const float *d_gainMeanCoefs, const float *d_ADCMevCoefs, int csi_n_channels, cudaStream_t stream, int nLoadedEvents, uint16_t *d_recoTag, uint8_t* d_csiChsMaskForReco)
{
    //nvtxRangePush("LaunchReconstructionKernel");

    // 256 threads per block
    dim3 blockSize = 256;
    //dim3 gridSize = dim3((csi_n_channels / (blockSize.x / 32)) + 1, nLoadedEvents, 1); // Blocks in grid
    
    // max events that can be processed in parallel is 32. Some of those will
    // not be reconstructed, depending on their trigger tag
    //if (nLoadedEvents > 32)
    //{
    //    printf("WARNING: nLoadedEvents > 32. Maybe increase the number of blocks in the grid\n");
    //}

    // blocks per event = csi_n_channels / (blockSize.x / 32) + 1 = 340. Each block gets 8 wfms. One wfm gets 32 threads.
    dim3 gridSize = dim3(340, nLoadedEvents, 1); // Blocks in grid

    //nvtxRangePush("kernel_reconstruct_csi");
    kernel_reconstruct_csi<<<gridSize, blockSize, 0, stream>>>(d_idata, d_wfmInfo, d_csi_energies, d_csiTimes, d_CSIDetChIdToADCChId, d_gainMeanCoefs, d_ADCMevCoefs, csi_n_channels, d_recoTag, d_csiChsMaskForReco);
    // sleep for 2 seconds
    //nvtxRangePop();

//    dim3 b_blockSize = 1;
//    dim3 b_gridSize = 1;
//    kernel_print_ET<<<b_gridSize, b_blockSize, 0, stream>>>(d_energies, d_csi_energies, d_csiTimes, csi_n_channels, nLoadedEvents);
//
    //nvtxRangePop();
}

void launchCalcTotalEKernel(float* d_energies, float* d_csi_energies, uint16_t* d_csiTimes, int csi_n_channels, int nEvents, cudaStream_t stream)
{
    nvtxRangePush("launchCalcTotalEKernel");

    dim3 blockSize = 1;
    dim3 gridSize = 1;

    kernel_print_ET<<<gridSize, blockSize, 0, stream>>>(d_energies, d_csi_energies, d_csiTimes, csi_n_channels, nEvents);

    cudaStreamSynchronize(stream);
    nvtxRangePop();
}



void launchTransposeKernel(const uint16_t *d_idata, uint16_t *d_odata, cudaStream_t stream)
{
    const int nx = 16;
    const int ny = 64;
    const int nMatrices = 288;

    dim3 dimGrid(nMatrices, 1, 1);
    dim3 dimBlock(nx, ny, 1);

    transpose<<<dimGrid, dimBlock, 0, stream>>>(d_idata, d_odata);
    cudaStreamSynchronize(stream);
}


__global__ void kernel_pedestal_suppression(uint16_t *d_perFADCWfmData,  uint16_t* d_wfmInfo, const uint16_t *d_CSIDetChIdToADCChId, const float *d_gainMeanCoefs, const float *d_ADCMevCoefs, int csi_n_channels, uint16_t *d_recoTag, uint8_t *d_csiChsMaskForPedSup)
{


    unsigned int i_evt = blockIdx.y; // inside grid (0 to events)

    // blockIdx.x: 0 to 340
    // blockDim.x: 256
    // threadIdx.x: 0 to 255
    // 32 threads per waveform
    unsigned int id_detCh = (blockIdx.x * blockDim.x + threadIdx.x) / 32; 
    

    // thread id inside wfm (32 threads / wfm)
    unsigned int id_thr_inside_waveform = threadIdx.x % 32;
    unsigned int id_wfm_inside_block = threadIdx.x / 32; // wfm number (0 to 8)

    

    if ((d_recoTag[i_evt] & 0x00ff) > REC_PEDSUP && (d_recoTag[i_evt] && !(d_recoTag[i_evt] & REC_LUCKY_BIT)))
    {
        // no pedsup all events that are not set to be reconstructed AND
        // no pedsup all events that lucky
        // i.e.
        // pedsup all events that are set to be reconstructed/pedsuppressed AND are lucky

        if (id_thr_inside_waveform == 0 && id_detCh < csi_n_channels){
            d_wfmInfo[i_evt * (288 * 16 * 3) + d_CSIDetChIdToADCChId[id_detCh] * 3 + 0] = WFMHDR::RESET_TAG;    // will be filled later with min
            d_wfmInfo[i_evt * (288 * 16 * 3) + d_CSIDetChIdToADCChId[id_detCh] * 3 + 1] = WFMHDR::RESET_TAG;
            d_wfmInfo[i_evt * (288 * 16 * 3) + d_CSIDetChIdToADCChId[id_detCh] * 3 + 2] = WFMHDR::RESET_TAG;    // will be filled later with r    
        }

        return;
    }

    

    if (id_detCh >= csi_n_channels)
    {
        return;
    }

    uint16_t *this_evt_data = d_perFADCWfmData + 288 * 16 * 64 * i_evt;

    // To calculate peak height
    __shared__ volatile int local_max_vals[8][32];
    int this_thread_val = INT_MIN;

    __syncthreads();

    // Load waveform into shared memory
    __shared__ uint16_t this_wfm[8][64];
    load_wfms_into_shared_data(id_detCh, this_evt_data, d_CSIDetChIdToADCChId, this_wfm, id_wfm_inside_block, id_thr_inside_waveform);

    __syncthreads();

    // get integrated ADC
    __shared__ float integratedADC[8];
    calc_integrated_adc(integratedADC, this_wfm);



    // Calculate Peak Height

    // first 32 samples
    this_thread_val = this_wfm[id_wfm_inside_block][id_thr_inside_waveform];

    // last 32 samples
    this_thread_val = max(this_thread_val, this_wfm[id_wfm_inside_block][id_thr_inside_waveform + 32]);

    __syncthreads();
    local_max_vals[id_wfm_inside_block][id_thr_inside_waveform] = this_thread_val;

    for (int i = 16; i > 1; i >>= 1)
    {
        if (id_thr_inside_waveform < i)
        {
            if (local_max_vals[id_wfm_inside_block][id_thr_inside_waveform] < local_max_vals[id_wfm_inside_block][id_thr_inside_waveform + i])
            {
                local_max_vals[id_wfm_inside_block][id_thr_inside_waveform] = local_max_vals[id_wfm_inside_block][id_thr_inside_waveform + i];
            }
        }
        __syncthreads();
    }

    if (id_thr_inside_waveform == 0)
    {
        if (d_csiChsMaskForPedSup[id_detCh] == 1)
        {
            d_wfmInfo[i_evt * (288 * 16 * 3) + d_CSIDetChIdToADCChId[id_detCh] * 3 + 0] = WFMHDR::RESET_TAG;    // will be filled later with min
            d_wfmInfo[i_evt * (288 * 16 * 3) + d_CSIDetChIdToADCChId[id_detCh] * 3 + 1] = WFMHDR::RESET_TAG;
            d_wfmInfo[i_evt * (288 * 16 * 3) + d_CSIDetChIdToADCChId[id_detCh] * 3 + 2] = WFMHDR::RESET_TAG;    // will be filled later with r
        }
        else
        {

        float E = (integratedADC[id_wfm_inside_block]) / d_gainMeanCoefs[id_detCh] * d_ADCMevCoefs[id_detCh];
        float peakHeigh = max(local_max_vals[id_wfm_inside_block][0], local_max_vals[id_wfm_inside_block][1]) - this_wfm[id_wfm_inside_block][0];

        if (E < 1 && E > -2 && peakHeigh < 10)
        {
            if (DEBUG_PEDSUP)
            {
                // tag the waveform to let the compression stage know
                d_wfmInfo[i_evt * (288 * 16 * 3) + d_CSIDetChIdToADCChId[id_detCh] * 3 + 0] = WFMHDR::RESET_TAG; // will be filled later with "min"
                d_wfmInfo[i_evt * (288 * 16 * 3) + d_CSIDetChIdToADCChId[id_detCh] * 3 + 1] = WFMHDR::PDSUP_DBG_TAG;
                d_wfmInfo[i_evt * (288 * 16 * 3) + d_CSIDetChIdToADCChId[id_detCh] * 3 + 2] = WFMHDR::RESET_TAG; // will be filled later with "r"
            }
            else
            {
                // Fill d_wfmInfo accordingly
                d_wfmInfo[i_evt * (288 * 16 * 3) + d_CSIDetChIdToADCChId[id_detCh] * 3 + 0] = WFMHDR::RESET_TAG; // will be filled later with pedestal
                d_wfmInfo[i_evt * (288 * 16 * 3) + d_CSIDetChIdToADCChId[id_detCh] * 3 + 1] = WFMHDR::PDSUP_TAG;
                d_wfmInfo[i_evt * (288 * 16 * 3) + d_CSIDetChIdToADCChId[id_detCh] * 3 + 2] = WFMHDR::RESET_TAG; // will be filled later with r
            }
        }
        else
        {
            d_wfmInfo[i_evt * (288 * 16 * 3) + d_CSIDetChIdToADCChId[id_detCh] * 3 + 0] = WFMHDR::RESET_TAG; // will be filled later with min
            d_wfmInfo[i_evt * (288 * 16 * 3) + d_CSIDetChIdToADCChId[id_detCh] * 3 + 1] = WFMHDR::RESET_TAG; //
            d_wfmInfo[i_evt * (288 * 16 * 3) + d_CSIDetChIdToADCChId[id_detCh] * 3 + 2] = WFMHDR::RESET_TAG; // will be filled later with r
        }
    }
    }
    else{

    }
    //__syncthreads();

    //__syncthreads();
    //if (id_thr_inside_waveform == 0)
    //{
    //    float E = (integratedADC[id_wfm_inside_block]) / d_gainMeanCoefs[id_detCh] * d_ADCMevCoefs[id_detCh];
    //    tmp_wfms[i_evt * csi_n_channels * 65 + id_detCh * 65 + 0] = E;
    //}
    //tmp_wfms[i_evt * csi_n_channels * 65 + id_detCh * 65 + 1+id_thr_inside_waveform] = this_wfm[id_wfm_inside_block][id_thr_inside_waveform];
    //tmp_wfms[i_evt * csi_n_channels * 65 + id_detCh * 65 + 33+id_thr_inside_waveform] = this_wfm[id_wfm_inside_block][id_thr_inside_waveform+32];
    //__syncthreads();
}





__global__ void kernel_pre_compress(uint16_t *d_wfmData, uint16_t *d_wfmInfo, uint32_t *d_compressedBytes, uint16_t *d_recoTag)
{

    // set thread ID
    unsigned int tid = threadIdx.x; // inside block, 0 to 256

    unsigned int i_event = blockIdx.y;     // inside grid
    unsigned int i_fadc = blockIdx.x;    // 0 to 288
    uint16_t i_fadc_ch = tid / 16; // 64 wfms in total, 16 wfms per block
    // thread id inside wfm (16 threads / wfm)
    unsigned int i_sample16 = tid % 16;





    // 16 threads per waveform.
    // One block = 16 waveforms.

    __shared__ int block_mins[16];
    __shared__ int block_maxs[16];
    __shared__ int r[16];
    __shared__ uint16_t *this_wfm[16]; // pointer to input data for each wfm
    __shared__ uint32_t compressed_bytes[16];



    __syncthreads();

    // return if this event was rejected and is not lucky
    if (d_recoTag[i_event] > REJ_THRESHOLD && !(d_recoTag[i_event] & REC_LUCKY_BIT))
    {
        return;
    }



    block_mins[i_fadc_ch] = 60000;
    block_maxs[i_fadc_ch] = 0;
    r[i_fadc_ch] = 0;
    this_wfm[i_fadc_ch] = d_wfmData + i_event * 288 * 16 * 64 + i_fadc * 16 * 64 + i_fadc_ch * 64;


    __syncthreads();

    // set d_compressedBytes to 0
    d_compressedBytes[i_event * (288 * 16 + 1) + 0] = 0;
    d_compressedBytes[i_event * (288 * 16 + 1) + 1 + i_fadc * 16 + i_fadc_ch] = 0;

    __syncthreads();

    if (d_wfmInfo[i_event * (288 * 16 * 3) + i_fadc * 16 * 3 + i_fadc_ch * 3 + 1] == WFMHDR::PDSUP_TAG)
    {
        d_compressedBytes[i_event * (288 * 16 + 1) + 1 + i_fadc * 16 + i_fadc_ch] = 3;

        d_wfmInfo[i_event * (288 * 16 * 3) + i_fadc * 16 * 3 + i_fadc_ch * 3 + 0] = this_wfm[i_fadc_ch][0];
        //d_wfmInfo[i_event * (288 * 16 * 3) + i_fadc * 16 * 3 + i_fadc_ch * 3 + 1] = ;
        d_wfmInfo[i_event * (288 * 16 * 3) + i_fadc * 16 * 3 + i_fadc_ch * 3 + 2] = WFMHDR::PDSUP_R;

    }
    __syncthreads();
    if (d_wfmInfo[i_event * (288 * 16 * 3) + i_fadc * 16 * 3 + i_fadc_ch * 3 + 1] == WFMHDR::PDSUP_TAG)
    {
        return;
    }
    __syncthreads();


    int local_min = 60000;
    int local_max = 0;
    for (int i_s = 0; i_s < 4; i_s++)
    {
        uint16_t idataTid = this_wfm[i_fadc_ch][i_sample16 * 4 + i_s];
        //uint16_t idataTid = this_wfm[i_fadc_ch][0];
        //uint16_t idataTid = this_wfm[0][0];
        local_min = min(local_min, idataTid);
        local_max = max(local_max, idataTid);
    }

    __syncthreads();

    atomicMin(&block_mins[i_fadc_ch], local_min);
    atomicMax(&block_maxs[i_fadc_ch], local_max);

    __syncthreads();

    r[i_fadc_ch] = 32 - __clz(block_maxs[i_fadc_ch] - block_mins[i_fadc_ch]);
    r[i_fadc_ch] = (r[i_fadc_ch] < 9) ? r[i_fadc_ch] : 16;

    __syncthreads();
    if (i_sample16 == 0)
    {
        // printf("bmax = %d; bmin = %d; r = %d\n", block_maxs[i_fadc_ch], block_mins[i_fadc_ch], r[i_fadc_ch]);
    }

    if (r[i_fadc_ch] != 16)
    {

        if ((r[i_fadc_ch] < 0 || r[i_fadc_ch] > 16) && i_sample16 == 0)
        {
            printf("i_event %d; i_fadc_ch = %d; r = %d; block_maxs = %d; block_mins = %d\n", i_event, i_fadc_ch, r[i_fadc_ch], block_maxs[i_fadc_ch], block_mins[i_fadc_ch]);
        }

        compressed_bytes[i_fadc_ch] = 3 + 64 * r[i_fadc_ch] / 8;
    }
    else
    {
        compressed_bytes[i_fadc_ch] = 129;
    }

    __syncthreads();


    // load results into device arrays
    d_wfmInfo[i_event * (288 * 16 * 3) + i_fadc * 16 * 3 + i_fadc_ch * 3 + 0] = block_mins[i_fadc_ch];
    //d_wfmInfo[i_event * (288 * 16 * 3) + i_fadc * 16 * 3 + i_fadc_ch * 3 + 1] = block_maxs[i_fadc_ch];
    d_wfmInfo[i_event * (288 * 16 * 3) + i_fadc * 16 * 3 + i_fadc_ch * 3 + 2] = r[i_fadc_ch];

    d_compressedBytes[i_event * (288 * 16 + 1) + 1 + i_fadc * 16 + i_fadc_ch] = compressed_bytes[i_fadc_ch];
}

__global__ void kernel_reduce_2(uint32_t *d_compressedBytes, uint32_t* dh_perEvtSizeCompressed, uint16_t *d_recoTag)
{
    // First 256 elements are aready done.
    // Next 256 elements are what they are + d_data[255]
    // THEN, Next 256 elements are what they are + d_data[255+256 * 1]
    // THEN, Next 256 elements are what they are + d_data[255+256 * 2]
    // etc. (18 times)



    // set thread ID
    unsigned int wfmID = threadIdx.x; // inside block
    unsigned int evtID = blockIdx.y;



    __syncthreads();

    // return if this event was rejected
    if (d_recoTag[evtID] > REJ_THRESHOLD  && !(d_recoTag[evtID] & REC_LUCKY_BIT))
    {
        if (wfmID == 0)
        {
            dh_perEvtSizeCompressed[evtID] = L3_EVT_HDR_SIZE; // EVT_HDR without ADC hdrs
        }
    }
    else
    {

        __syncthreads();
        
        for (unsigned int i_iter = 1; i_iter < 18; i_iter++) // (iter "0" is already done)
        {
            __syncthreads();
            d_compressedBytes[evtID * (18 * 256 + 1) + 1 + i_iter * 256 + wfmID] += d_compressedBytes[evtID * (18 * 256 + 1) + 1 + (i_iter - 1) * 256 + 255];
        }
        __syncthreads();
        if (wfmID == 0)
        {
            dh_perEvtSizeCompressed[evtID] = EVT_HDR_SIZE + d_compressedBytes[evtID * (18 * 256 + 1) + 1 + 18 * 256 - 1];
        }
    }
}

__global__ void kernel_reduce_1(uint16_t *d_wfmInfo, uint32_t *d_compressedBytes, uint16_t *d_recoTag)
{

    // Not exactly reduce. We want the ith element to be the sum of the first i elements.
    // Here, each block reduces 256 elements to 16 elements.

    // Note: nBlocks * nThreads/Block = 18 * 256 = 4608
    // Note: nWfms = 288 * 16 = 4608

    // set thread ID
    unsigned int wfmID = threadIdx.x; // inside block, 0 to 255
    unsigned int evtID = blockIdx.y;
    unsigned int bid = blockIdx.x;  // inside grid, 0 to 17
    unsigned int bdim = blockDim.x; // 256

    // return if this event was rejected
    if (d_recoTag[evtID] > REJ_THRESHOLD && !(d_recoTag[evtID] & REC_LUCKY_BIT))
    {
        return;
    }


    //__syncthreads();
    // print d_recoTag and d_compressedBytes for all events and all waveforms
    //if (d_recoTag[evtID] != REJECTED && d_recoTag[evtID] != REC_PASS && d_recoTag[evtID] != REC_RECO_5G && d_recoTag[evtID] != REC_RECO_KP && d_recoTag[evtID] != REC_LUCKY && d_recoTag[evtID] != REC_PEDSUP)
    //{
    //    printf("d_recoTag[%d] = %d\n", evtID, d_recoTag[evtID]);
    //}

    //printf("awkdataGPU %d %d %d %d\n", d_recoTag[evtID], bid, wfmID, d_compressedBytes[evtID * (288 * 16 + 1) + 1 + bid * bdim + wfmID]);

    __syncthreads();



    // copy data to shared memory
    // 
    __shared__ uint32_t sdata[256]; // 256 waveforms per block

    sdata[wfmID] = d_compressedBytes[evtID * (288 * 16 + 1) + 1 + bid * bdim + wfmID];
    //printf("sdata[%d] = %d\n", wfmID, sdata[wfmID]);


    __syncthreads();

    if (d_compressedBytes[evtID * (288 * 16 + 1) + 1 + bid * bdim + wfmID] > 288 * 16 * 129 || d_compressedBytes[evtID * (288 * 16 + 1) + 1 + bid * bdim + wfmID] <= 0)
    {
        printf("huge error compressedBytes[%d] = %d\n", evtID * (288 * 16 + 1) + 1 + bid * bdim + wfmID, d_compressedBytes[evtID * (288 * 16 + 1) + 1 + bid * bdim + wfmID]);
    }

    if (sdata[wfmID] > 288 * 16 * 129 || sdata[wfmID] <= 0)
    {
        printf("huge error sdata[%d] = %d\n", wfmID, sdata[wfmID]);
    }
    
    // Scan sum in shared memory
    uint32_t tmp = 0;
    __syncthreads();
    for (unsigned int stride = 1; stride < bdim; stride *= 2)
    {
        if (wfmID >= stride)
        {
            tmp = sdata[wfmID - stride];
            if (sdata[wfmID] > 288 * 16 * 129)
                printf("evtID = %d; tmp = %d; wfmID = %d; stride = %d; sdata[wfmID-stride] = %d\n", evtID, tmp, wfmID, stride, sdata[wfmID]);

        }
        __syncthreads();
        if (wfmID >= stride)
        {
            sdata[wfmID] += tmp;
        }
        __syncthreads();
    }
    __syncthreads();

    d_compressedBytes[evtID * (288 * 16 + 1) + 1 + bid * bdim + wfmID] = sdata[wfmID];
}

__global__ void kernel_write_evt_hdr(uint32_t* dh_perEvtIdxCompressed, uint32_t* dh_perEvtSizeCompressed, uint8_t *d_perEvtHeaderData,  uint8_t *d_odata, int nLoadedEvents, uint16_t *d_recoTag)
{
    // Note that this is the slowest kernel of the whole l3. 

    unsigned int i_evt = blockIdx.x; // inside grid (0 to events)
    unsigned int tid = threadIdx.x; // inside block (0 to 128)


    // insert d_recotag into d_perEvtHeaderData
    if (tid ==32)
    {
        d_perEvtHeaderData[i_evt * EVT_HDR_SIZE + L3_TAG_O] = d_recoTag[i_evt] >> 8;
        d_perEvtHeaderData[i_evt * EVT_HDR_SIZE + L3_TAG_O + 1] = d_recoTag[i_evt] & 0xFF;
    }


    // write event size
    if (tid < COMP_EVT_SIZE_S)
    {
        // most significative byte first
        d_odata[dh_perEvtIdxCompressed[i_evt] + tid] = dh_perEvtSizeCompressed[i_evt] >> (8 * (COMP_EVT_SIZE_S - tid - 1));
    }

    __syncthreads();
    
    if (d_recoTag[i_evt] > REJ_THRESHOLD && !(d_recoTag[i_evt] & REC_LUCKY_BIT))
    {
        // if it has been rejected and it is not lucky, write only the header
        // write only L3_EVT_HDR
        if (tid < L3_EVT_HDR_SIZE - COMP_EVT_SIZE_S)
        {
            d_odata[dh_perEvtIdxCompressed[i_evt] + COMP_EVT_SIZE_S + tid] = d_perEvtHeaderData[i_evt * EVT_HDR_SIZE + COMP_EVT_SIZE_S + tid];
        }
    }
    else
    {
        // write L3_EVT_HDR + ADC headers
        while (tid < EVT_HDR_SIZE - COMP_EVT_SIZE_S)
        {
            d_odata[dh_perEvtIdxCompressed[i_evt] + COMP_EVT_SIZE_S + tid] = d_perEvtHeaderData[i_evt * EVT_HDR_SIZE + COMP_EVT_SIZE_S + tid];
            tid += blockDim.x;
        }
    }
}


void LaunchPedestalSuppressionKernel(uint16_t *d_perFADCWfmData, uint16_t* d_wfmInfo, const uint16_t *d_CSIDetChIdToADCChId, const float *d_gainMeanCoefs, const float *d_ADCMevCoefs, int csi_n_channels, cudaStream_t stream, int nLoadedEvents, uint16_t *d_recoTag, uint8_t *d_csiChsMaskForPedSup)
{

    // 256 threads per block (8 wfms per block)
    dim3 blockSize = 256;

    // blocks per event = csi_n_channels / (blockSize.x / 32) + 1 = 340. Each block gets 8 wfms. One wfm gets 32 threads.
    dim3 gridSize = dim3(340, nLoadedEvents, 1); // Blocks in grid
    //l3log.log(L3_QUIET, L3_DEBUG, "LaunchPedestalSuppressionKernel: nLoadedEvents = %d\n", nLoadedEvents);

    kernel_pedestal_suppression<<<gridSize, blockSize, 0, stream>>>(d_perFADCWfmData, d_wfmInfo, d_CSIDetChIdToADCChId, d_gainMeanCoefs, d_ADCMevCoefs, csi_n_channels, d_recoTag, d_csiChsMaskForPedSup);

}






uint32_t launchPreCompressKernel(uint16_t *d_perFADCWfmData, uint32_t* d_perEvtSizeCompressed, uint32_t* d_perEvtIdxCompressed, 
                                                             uint32_t* h_perEvtSizeCompressed, uint32_t* h_perEvtIdxCompressed, uint16_t *d_wfmInfo, uint32_t *d_compressedBytes, cudaStream_t stream, int nLoadedEvents, uint16_t *d_recoTag)
{
    //nvtxRangePush("launchPreCompressKernel");

    // dim3 gridSize = 288;  // N blocks
    dim3 gridSize = dim3(288, nLoadedEvents, 1); // Blocks in grid
    dim3 blockSize = dim3(256);                      // Threads per block


    kernel_pre_compress<<<gridSize, blockSize, 0, stream>>>(d_perFADCWfmData, d_wfmInfo, d_compressedBytes, d_recoTag);

    // reduce inside each ADC
    gridSize = dim3(18, nLoadedEvents);
    kernel_reduce_1<<<gridSize, 256, 0, stream>>>(d_wfmInfo, d_compressedBytes, d_recoTag);

    // reduce inside each event
    gridSize = dim3(1, nLoadedEvents);
    kernel_reduce_2<<<gridSize, 256, 0, stream>>>(d_compressedBytes, d_perEvtSizeCompressed, d_recoTag);
    

    //printf("dh_perEvtSizeCompressed[%d] = %d\n", 0, dh_perEvtSizeCompressed[0]);
    //printf("Event 0 starts at %d and ends at %d\n", dh_perEvtIdxCompressed[0], dh_perEvtIdxCompressed[0] + dh_perEvtSizeCompressed[0]);
   
    cudaMemcpy(h_perEvtSizeCompressed, d_perEvtSizeCompressed, nLoadedEvents * sizeof(uint32_t), cudaMemcpyDeviceToHost);
    for (int i_evt = 1; i_evt < nLoadedEvents; i_evt++)
    {
        //printf("dh_perEvtSizeCompressed[%d] = %d\n", i_evt, dh_perEvtSizeCompressed[i_evt]);
        h_perEvtIdxCompressed[i_evt] = h_perEvtSizeCompressed[i_evt-1] + h_perEvtIdxCompressed[i_evt - 1];
        //printf("Event %d starts at %d and ends at %d\n", i_evt, dh_perEvtIdxCompressed[i_evt], dh_perEvtIdxCompressed[i_evt] + dh_perEvtSizeCompressed[i_evt]);
    }
    cudaMemcpy(d_perEvtIdxCompressed, h_perEvtIdxCompressed, nLoadedEvents * sizeof(uint32_t), cudaMemcpyHostToDevice);

    //nvtxRangePop();
    return h_perEvtIdxCompressed[nLoadedEvents - 1] + h_perEvtSizeCompressed[nLoadedEvents - 1];;
}


/**
 * @brief More powerful than Nicola's on waveforms with a peak, but less
   powerful on pedestal waveforms. Might be worth using it together with the
   ped. sup.
*/
__global__ void kernel_compress_takola(u_int16_t *d_idata, uint8_t *d_odata, unsigned int wfmLen, unsigned int nWfms, u_int8_t *bytesPerWfm, int *d_wfmInfo)
{
    // set thread ID
    unsigned int chBlockID = threadIdx.x; // inside block
    unsigned int fadcid = blockIdx.x;     // inside grid

    unsigned int wfmID = chBlockID / 16;

    // thread id inside wfm (16 threads / wfm)
    int wfm_tid = chBlockID % 16;

    //__shared__ int cacheMax[64]; // block size, for the moment
    //__shared__ int cacheMin[64]; // block size, for the moment

    // 16 threads per wfm
    // 1 block per 16 wfms

    // boundary check
    // if(blockIdx.x >= nWfms) return;

    // printf("chBlockID: %d\n", chBlockID);
    // return;
    // if(chBlockID >= wfmLen) return;

    __shared__ int compressedWfms[16 * 129];

    // Set all to 0
    if (chBlockID == 0)
    {
        for (int i = 0; i < 16 * 129; i++)
        {
            compressedWfms[i] = 0;
        }
    }

    // compressedWfm[chBlockID+3] = 0;

    __shared__ int tmpWfms[16][64];

    // Data for all wfms in a block
    __shared__ int min[16];
    __shared__ int r[16];
    __shared__ int outIdx[16];       // index in output array
    __shared__ u_int16_t *idata[16]; // pointer to input data for each wfm

    // Init everything
    if (chBlockID % 16 == 0)
    {

        min[wfmID] = 600000;
        r[wfmID] = 0;
        outIdx[wfmID] = d_wfmInfo[fadcid * 16 * 3 + wfmID * 3 + 2];

        idata[wfmID] = d_idata + fadcid * 16 * wfmLen + wfmID * wfmLen;
    }

    __syncthreads(); // after initializations

    // r = 32 - __clz(minMax[1] - min);
    // r = (r < 9) ? r : 16;

    // if (chBlockID==0)
    //     printf("min = %d, r = %d, wfmN = %d, \n", min, r, blockIdx.x);

    if (chBlockID % 16 != 0) // if not the first thread in wfm
    {
        for (int i = 0; i < 4; i++)
        {
            tmpWfms[wfmID][wfm_tid * 4 + i] = idata[wfmID][wfm_tid * 4 + i] - idata[wfmID][wfm_tid * 4 + i - 1];
        }
    }
    else
    {
        tmpWfms[wfmID][0] = 0;
        for (int i = 1; i < 4; i++)
        {
            tmpWfms[wfmID][wfm_tid * 4 + i] = idata[wfmID][wfm_tid * 4 + i] - idata[wfmID][wfm_tid * 4 + i - 1];
        }
    }
    if (chBlockID == 0)
    {
        for (int i = 0; i < wfmLen; i++)
        {
            // printf("wfm %d, tmpWfm[%d] = %d\n", blockIdx.x, i, tmpWfm[i]);
        }
    }

    __syncthreads(); // after initializations

    // We have 16 threads per wfm (64 bins). Each thread finds the local min among its 4 bins
    // and then we find the global min among all 16 threads.

    int localMin = 600000;
    for (int i = 0; i < 4; i++)
    {
        if (tmpWfms[wfmID][wfm_tid * 4 + i] < localMin)
            localMin = tmpWfms[wfmID][wfm_tid * 4 + i];
    }
    atomicMin(&min[wfmID], localMin);

    __syncthreads(); // after initializations

    // Now we shift the wfm upwards so that the min is 0
    for (int i = 0; i < 4; i++)
    {
        tmpWfms[wfmID][wfm_tid * 4 + i] = tmpWfms[wfmID][wfm_tid * 4 + i] - min[wfmID];
    }

    __syncthreads(); // after initializations

    __shared__ int min_ts[16]; // FIX THIS
    __shared__ int max_ts[16];

    for (int i = 0; i < 16; i++)
    {
        min_ts[i] = 10000;
        max_ts[i] = 0;
    }
    __syncthreads();

    for (int i = 0; i < 4; i++)
    {
        atomicMin(&min_ts[wfmID], tmpWfms[wfmID][wfm_tid * 4 + i]);
        atomicMax(&max_ts[wfmID], tmpWfms[wfmID][wfm_tid * 4 + i]);
    }

    // printf("buu, tmpWfm[%d] = %d\n", chBlockID, tmpWfm[chBlockID]);

    __syncthreads();

    if (chBlockID % 16 == 0)
    {
        // printf("min_ts = %d, max_ts = %d\n",min_ts, max_ts);
        r[wfmID] = 32 - __clz(max_ts[wfmID] - min_ts[wfmID]);
        r[wfmID] = (r[wfmID] < 9) ? r[wfmID] : 16;
        // printf("r[wfmID] = %d\n", r[wfmID]);
        compressedWfms[wfmID * 129] = r[wfmID];
        d_wfmInfo[fadcid * 16 * 3 + wfmID * 3 + 1] = r[wfmID];

        for (int i = 1; i < 129; i++)
        {
            compressedWfms[wfmID * 129 + i] = 0;
        }

        // int bytesPerWfm;
        // if (r[wfmID] == 16)
        //     bytesPerWfm = 129;
        // else
        //     bytesPerWfm = 3 + 64*r[wfmID]/8;

        // printf("fadcID = %d, wfmID = %d, bytesPerWfm = %d\n", fadcid, wfmID, bytesPerWfm);
    }
    // r=4;

    __syncthreads(); // after initializations
    // if (wfm_tid == 0)
    //     printf("min = %d, r = %d, wfmN(glbl) = %d, \n", min_ts[wfmID], r[wfmID], fadcid*16+wfmID);

    if (r[wfmID] == 16 && wfm_tid == 0)
    {
        // printf("r = 16, wfmID = %d, wfmN (global) = %d\n", wfmID, fadcid*16+wfmID);
    }
    __syncthreads();

    if (r[wfmID] != 16)
    {
        for (int s = 0; s < 4; s++)
        {
            int byte = 3 + int(wfm_tid * 4 + s) * r[wfmID] / 8;
            // int byte = 3 + int(chBlockID  *r[wfmID]/8);
            int bit = (wfm_tid * 4 + s) * r[wfmID] % 8;

            compressedWfms[wfmID * 129 + 1] = (idata[wfmID][0] & 0xFF00) >> 8;
            compressedWfms[wfmID * 129 + 2] = (idata[wfmID][0] & 0x00FF);
            if (8 - r[wfmID] - bit >= 0)
            {
                atomicAdd(&compressedWfms[wfmID * 129 + byte], ((tmpWfms[wfmID][wfm_tid * 4 + s]) << (8 - r[wfmID] - bit)));
            }
            else
            {
                uint8_t byte1 = ((tmpWfms[wfmID][wfm_tid * 4 + s]) >> (r[wfmID] + bit - 8));
                uint8_t byte2 = (((tmpWfms[wfmID][wfm_tid * 4 + s]) << (8 - (r[wfmID] + bit - 8))) & 0x00FF);

                atomicAdd(&compressedWfms[wfmID * 129 + byte], byte1);
                atomicAdd(&compressedWfms[wfmID * 129 + byte + 1], byte2);
            }
        }
    }
    if (r[wfmID] == 16)
    {

        for (int s = 0; s < 4; s++)
        {
            compressedWfms[wfmID * 129 + 1 + 2 * (wfm_tid * 4 + s)] = (uint8_t)(idata[wfmID][wfm_tid * 4 + s] >> 8);
            compressedWfms[wfmID * 129 + 1 + 2 * (wfm_tid * 4 + s) + 1] = (uint8_t)(idata[wfmID][wfm_tid * 4 + s] & 0x00FF);
            if (fadcid * 16 + wfmID == 101 && wfm_tid == 0)
            {
                printf("compressedWfms index = %d; wfmN = %d\n", 1 + 2 * (wfm_tid * 4 + s) + 1, fadcid * 16 + wfmID);
            }
        }
    }
    __syncthreads();

    if (r[wfmID] == 16 && wfm_tid == 0 && fadcid * 16 + wfmID == 100)
    {
        for (int i = 0; i < 64; i++)
        {
            // printf("idata[%d] = %d, compressedWfms[%d] = %d\n", i, idata[wfmID][i], i, compressedWfms[wfmID*129 + 1 + i]);
            // printf("%d: %u \n", 2*i, compressedWfms[wfmID*129 + 1 +  2*i]);
            // printf("%d: %u \n", 2*i+1, compressedWfms[wfmID*129 + 1 +  2*i+1]);
        }
    }

    if (fadcid * 16 + wfmID == 101 && wfm_tid == 0)
    {
        // for (int s = 0; s < 4; s++)
        //     printf("compressedWfms[%d]= %d\n", wfmID*129 + 1 + s, compressedWfms[wfmID*129 + 1 + s]);
    }

    if (r[wfmID] == 16)
    {
        // d_odata has 64*2 + 1 elements
        for (int s = 0; s < 4; s++)
        {
            d_odata[outIdx[wfmID] + wfm_tid * 4 + s] = compressedWfms[wfmID * 129 + wfm_tid * 4 + s];
            d_odata[outIdx[wfmID] + 64 + wfm_tid * 4 + s] = compressedWfms[wfmID * 129 + 64 + wfm_tid * 4 + s];
            d_odata[outIdx[wfmID] + 128] = compressedWfms[wfmID * 129 + 128];
        }
    }
    else
    {
        for (int s = 0; s < 4; s++)
        {
            if (wfm_tid * 4 + s < 3 + 64 * r[wfmID] / 8)
            {
                d_odata[outIdx[wfmID] + wfm_tid * 4 + s] = compressedWfms[wfmID * 129 + wfm_tid * 4 + s];
            }
        }
        if (r[wfmID] == 8)
        {
            // d_odata has 64 + 3 elements
            d_odata[outIdx[wfmID] + 64] = compressedWfms[wfmID * 129 + 64];
            d_odata[outIdx[wfmID] + 65] = compressedWfms[wfmID * 129 + 65];
            d_odata[outIdx[wfmID] + 66] = compressedWfms[wfmID * 129 + 66];
        }
    }


    if (fadcid * 16 + wfmID == 101 && wfm_tid == 0)
    {
        // printf("r[%d] = %d, min[%d] = %d\n", wfmID, r[wfmID], wfmID, min[wfmID], wfmID);
        for (int i = 0; i < 129; i++)
        {
            // printf("sssh_odata[%d] = %d\n", i, d_odata[outIdx[wfmID] + i]);
        }
    }
    // printf("d_odata[%d]:%d, bid=%d\n", outIdx+chBlockID, d_odata[outIdx+chBlockID], blockIdx.x);
}

__global__ void kernel_compress_nicola(uint16_t *d_perFADCWfmData, u_int8_t* d_perEvtHeaderData, uint8_t *d_odata, unsigned int wfmLen, unsigned int nWfms, u_int16_t *d_wfmInfo, uint32_t* dh_perEvtIdxCompressed, u_int32_t *d_outputIdxInEvt, int nLoadedEvents, uint16_t *d_recoTag)
{


    // set thread ID
    unsigned int tid = threadIdx.x; // inside block (0 to 255)

    unsigned int i_fadc = blockIdx.x;    // inside grid (0 to 287)
    unsigned int i_fadc_ch = tid / 16; // 0 to 15
    unsigned int i_evt = blockIdx.y;    // inside grid (0 to events)

    // thread id inside wfm (16 threads / wfm)
    unsigned int i_sample16 = tid % 16;

    // This has to be int because there is no AtomicAdd for uint8_t
    __shared__ int compressedWfms[16 * 129]; // the last 129 is added to make the memset simpler


    __shared__ int block_mins[16];
    __shared__ int r[16];
    __shared__ int pdsupTag[16];
    __shared__ u_int32_t outIdx[16];
    __shared__ u_int16_t *idata[16]; // pointer to input data for each wfm

    //for (int i_evt = 0; i_evt < nLoadedEvents; i_evt++)
    {
        __syncthreads();

        if (d_recoTag[i_evt] > REJ_THRESHOLD && !(d_recoTag[i_evt] & REC_LUCKY_BIT))
        {
            return;
        }

        // Set compressedWfms[] to all zeros
        int i_bin = tid;
        while (i_bin < 16 * 129)
        {
            compressedWfms[i_bin] = 0;
            i_bin += blockDim.x;
        }

        if (tid < 16)
        {
            block_mins[tid] = d_wfmInfo[i_evt * 288 * 16 * 3 + i_fadc * 16 * 3 + tid * 3 + 0];
        }
        else if (tid < 32)
        {
            r[tid - 16]        = d_wfmInfo[i_evt * 288 * 16 * 3 + i_fadc * 16 * 3 + (tid - 16) * 3 + 2];
            pdsupTag[tid - 16] = d_wfmInfo[i_evt * 288 * 16 * 3 + i_fadc * 16 * 3 + (tid - 16) * 3 + 1];
        }
        else if (tid < 48)
        {
            outIdx[tid - 32] = dh_perEvtIdxCompressed[i_evt] + EVT_HDR_SIZE + d_outputIdxInEvt[i_evt * (288 * 16 + 1) + i_fadc * 16 + (tid - 32)];

            if (outIdx[tid - 32] > 288 * 16 * 129 * MAX_NEVENTS_PER_STREAM)
            {
                printf("debug: dh_perEvtIdxCompressed[%d] = %d, d_outputIdxInEvt[%d] = %d, outIdx[%d] = %d\n", i_evt, dh_perEvtIdxCompressed[i_evt], i_evt * (288 * 16 + 1) + i_fadc * 16 + (tid - 32), d_outputIdxInEvt[i_evt * (288 * 16) + i_fadc * 16 + (tid - 32)], tid - 32, outIdx[tid - 32]);
                printf("debug: d_outputIdxInEvt[0]: %d\n", d_outputIdxInEvt[0]);
            }
        }
        else if (tid < 64)
        {
            idata[tid - 48] = d_perFADCWfmData + EVT_WFMS_SIZE * i_evt + i_fadc * N_CH_PER_FADC * N_SAMPLES_PER_CH + (tid - 48) * N_SAMPLES_PER_CH;
        }

        __syncthreads();


        if (i_sample16 == 0)
        {
            compressedWfms[i_fadc_ch * 129] = r[i_fadc_ch];
        }

        __syncthreads();

        if (pdsupTag[i_fadc_ch] == WFMHDR::PDSUP_TAG)
        {

            // pedestal suppressed wfms
            //compressedWfms[i_fadc_ch * 129 + 1] = (block_mins[i_fadc_ch] & 0xFF00) >> 8;    // pedestal
            //compressedWfms[i_fadc_ch * 129 + 2] = (block_mins[i_fadc_ch] & 0x00FF);         // pedestal

            d_odata[outIdx[i_fadc_ch]] = WFMHDR::PDSUP_R;
            d_odata[outIdx[i_fadc_ch] + 1] = (block_mins[i_fadc_ch] & 0xFF00) >> 8;
            d_odata[outIdx[i_fadc_ch] + 2] = (block_mins[i_fadc_ch] & 0x00FF);

            return;
        }
        else if (r[i_fadc_ch] != 16)
        {

            compressedWfms[i_fadc_ch * 129 + 1] = (block_mins[i_fadc_ch] & 0xFF00) >> 8;
            compressedWfms[i_fadc_ch * 129 + 2] = (block_mins[i_fadc_ch] & 0x00FF);

            // 4 samples per thread
            for (int s = 0; s < 4; s++)
            {

                // int byte = 3 + int(chBlockID  *r[wfmID]/8);
                int byte = 3 + int(i_sample16 * 4 + s) * r[i_fadc_ch] / 8;
                int bit = (i_sample16 * 4 + s) * r[i_fadc_ch] % 8;

                if (8 - r[i_fadc_ch] - bit >= 0)
                {
                    atomicAdd(&compressedWfms[i_fadc_ch * 129 + byte], ((idata[i_fadc_ch][i_sample16 * 4 + s] - block_mins[i_fadc_ch]) << (8 - r[i_fadc_ch] - bit)));
                }
                else
                {
                    uint8_t byte1 = ((idata[i_fadc_ch][i_sample16 * 4 + s] - block_mins[i_fadc_ch]) >> (r[i_fadc_ch] + bit - 8));
                    uint8_t byte2 = (((idata[i_fadc_ch][i_sample16 * 4 + s] - block_mins[i_fadc_ch]) << (8 - (r[i_fadc_ch] + bit - 8))) & 0x00FF);

                    atomicAdd(&compressedWfms[i_fadc_ch * 129 + byte], byte1);
                    atomicAdd(&compressedWfms[i_fadc_ch * 129 + byte + 1], byte2);
                }
            }
        }
        else if (r[i_fadc_ch] == 16)
        {
            for (int s = 0; s < 4; s++)
            {
                compressedWfms[i_fadc_ch * 129 + 1 + 2 * (i_sample16 * 4 + s)] = (uint8_t)(idata[i_fadc_ch][i_sample16 * 4 + s] >> 8);
                compressedWfms[i_fadc_ch * 129 + 1 + 2 * (i_sample16 * 4 + s) + 1] = (uint8_t)(idata[i_fadc_ch][i_sample16 * 4 + s] & 0x00FF);
            }
        }

        __syncthreads();

        if (pdsupTag[i_fadc_ch] == WFMHDR::PDSUP_DBG_TAG)
        {
            // two most significant bits of "min" are set to PDSUP_DBG_2MSBS
            compressedWfms[i_fadc_ch * 129 + 1] = compressedWfms[i_fadc_ch * 129 + 1] | (PDSUP_DBG_2MSBS << 6);
        }

        __syncthreads();


        if (r[i_fadc_ch] == 16)
        {
            // d_odata has 64*2 + 1 elements
            for (int s = 0; s < 4; s++)
            {
                d_odata[outIdx[i_fadc_ch] + i_sample16 * 4 + s] = compressedWfms[i_fadc_ch * 129 + i_sample16 * 4 + s];
                d_odata[outIdx[i_fadc_ch] + 64 + i_sample16 * 4 + s] = compressedWfms[i_fadc_ch * 129 + 64 + i_sample16 * 4 + s];
                d_odata[outIdx[i_fadc_ch] + 128] = compressedWfms[i_fadc_ch * 129 + 128];
            }
        }
        else
        {
            for (int s = 0; s < 4; s++)
            {
                if (i_sample16 * 4 + s < 3 + 64 * r[i_fadc_ch] / 8)
                {

                    if (outIdx[i_fadc_ch] + i_sample16 * 4 + s >= MAX_NEVENTS_PER_STREAM * 288 * 16 * 129)
                    {
                        printf("i_evt = %d, i_fadc = %d, i_fadc_ch = %d, tid = %d, i_sample16 = %d, outIdx = %d, r = %d\n", i_evt, i_fadc, i_fadc_ch, tid, i_sample16, outIdx[i_fadc_ch], r[i_fadc_ch]);
                    }

                    d_odata[outIdx[i_fadc_ch] + i_sample16 * 4 + s] = compressedWfms[i_fadc_ch * 129 + i_sample16 * 4 + s];

                }
            }

            if (r[i_fadc_ch] == 8)
            {
                // d_odata has 64 + 3 elements
                d_odata[outIdx[i_fadc_ch] + 64] = compressedWfms[i_fadc_ch * 129 + 64];
                d_odata[outIdx[i_fadc_ch] + 65] = compressedWfms[i_fadc_ch * 129 + 65];
                d_odata[outIdx[i_fadc_ch] + 66] = compressedWfms[i_fadc_ch * 129 + 66];
            }

        }

        //__syncthreads();
        //if (i_sample16 == 0)
        //{
        //    // print r in d_odata
        //    //printf("wfmN = %d, r = %d; compressdeWfms[%d] = %d; d_odata[%d] = %d\n", i_fadc * 16 + i_fadc_ch, r[i_fadc_ch], i_fadc_ch * 129, compressedWfms[i_fadc_ch * 129], outIdx[i_fadc_ch], d_odata[outIdx[i_fadc_ch]]);
        //}
    }

    /* Different ways to calculate min

    //	// reduction (not used)
    //
    //    int tid2 = chBlockID-wfmLen/2;
    //    // First iteration
    //    if(chBlockID<wfmLen/2){
    //        cacheMax[chBlockID] = max(idata[chBlockID], idata[chBlockID + wfmLen/2]);
    //    }
    //    else if (tid2 >= 0 && tid2 < wfmLen/2){
    //        cacheMin[tid2] = min(idata[tid2], idata[tid2 + wfmLen/2]);
    //    }
    //
    //    for(int stride=wfmLen/4; stride>0; stride>>=1)
    //    {
    //        if(chBlockID<stride){
    //            cacheMax[chBlockID] = max(cacheMax[chBlockID], cacheMax[chBlockID + stride]);
    //        }
    //        else if (tid2 >= 0 && tid2 < stride){
    //            cacheMin[tid2] = min(cacheMin[tid2], cacheMin[tid2 + stride]);
    //        }
    //        __syncthreads();
    //    }
    //
    //    if(chBlockID == 0){
    //        minMax[1] = max(cacheMax[0], cacheMax[1]);
    //        min = min(cacheMin[0], cacheMin[wfmLen-1]);
    //    }
    //    //else if (chBlockID==1)
    //
    //	__syncthreads();


    //    // slow and safe approach (not used)
    //    if (chBlockID == 0){
    //
    //        int min = 65536;
    //        for(int i = 0; i < wfmLen; i += 1)
    //        {
    //            if (idata[i] < min)
    //                min = idata[i];
    //        }
    //        min = min;
    //    }
    //    else if (chBlockID == 1){
    //        int max = 0;
    //        for(int i = 0; i < wfmLen; i += 1)
    //        {
    //            if (idata[i] > max)
    //                max = idata[i];
    //        }
    //        minMax[1] = max;
    //    }

    //    // fastest way (not used)
    //    atomicMax(&minMax[1], idataTid);
    //    atomicMin(&min, idataTid);

    */

}


void Decompress(void *_h_idata, uint8_t *h_odata, int nWfms, int i_evt)
{
    nvtxRangePush("Decompress");

    int wfmLen = 64;

    uint16_t *h_idata = (uint16_t *)_h_idata;



    // Decompress
    // > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >
    // uint16_t *data16_uncompressed = (uint16_t *)malloc(bytes);
    // gpuErrchk( cudaHostAlloc((void**)&h_idata, bytes, cudaHostAllocMapped) );

    // int *data16_uncompressed = (int *)malloc(bytes);
    // Allog page-locked memory on the host
    int *data16_uncompressed = (int *)malloc(wfmLen * nWfms * sizeof(int));

    int byteNow = 0;
    int thisWfmLength = 0;
    int nNonPedSuppWfms = 0;
    for (int wfmN = 0; wfmN < nWfms; wfmN++)
    {

        int r = h_odata[byteNow];

        //printf("i_evt = %d, wfmN = %d, r = %d\n", i_evt, wfmN, r);
        //usleep(500);
        //printf("h_odata_0 = %d\n", h_odata[byteNow]);
        //printf("h_odata_1 = %d\n", h_odata[byteNow + 1]);
        //printf("h_odata_2 = %d\n", h_odata[byteNow + 2]);


        //printf("wfmN = %d, r = %d\n", wfmN, r);
        //usleep(10000);


    
        if (r == WFMHDR::PDSUP_R)
        {
            byteNow += 3;

            for (int b = 0; b < wfmLen; b++)
            {
                data16_uncompressed[wfmN * wfmLen + b] = 12345;
            }

            continue;
        }
        else{
            //printf("Ped. not suppressed! i_evt = %d, wfmN = %d, r = %d\n", i_evt, wfmN, r);
        }

        //printf("Decompressing wfmN = %d\n", wfmN);


        if (r != 16)
        {
            thisWfmLength = 3 + 64 * r / 8;
        }
        else
        {
            thisWfmLength = 129;
        }


        if (DEBUG_PEDSUP)
        {
            uint8_t pedsup = (h_odata[byteNow + 1] & 0xC0) >> 6;
            if (pedsup == PDSUP_DBG_2MSBS)
            {
                //printf("i_evt = %d, wfmN = %d, pedsup = %d\n", i_evt, wfmN, pedsup);
            }
            else{
                //printf("zeroo i_evt = %d, wfmN = %d, pedsup = %d\n", i_evt, wfmN, pedsup);
            }
            h_odata[byteNow + 1] = h_odata[byteNow + 1] & 0x3F;
        }

        uint16_t min = (h_odata[byteNow + 1] & 0x00FF) << 8 | h_odata[byteNow + 2] & 0x00FF;

        int i = byteNow + 3; // Where the SMP data starts in the 8 bit array

        int bit = 0;
        int byte = wfmN * wfmLen;
        //printf("bytenow+0 = %d; bytenow+1 = %d; bytenow+2 = %d\n", h_odata[byteNow], h_odata[byteNow + 1], h_odata[byteNow + 2]);


        if (r > 8)
        {
            i = i - 2;
            while (byte < wfmN * wfmLen + wfmLen)
            {
                data16_uncompressed[byte] = ((h_odata[i] << 8) | (h_odata[i + 1]));
                i += 2;
                byte++;
            }
        }
        else
        {
            if (DOTAKOLA)
            {
                printf("NEEDS REVISION\n");
                while (byte < wfmN * wfmLen + wfmLen)
                {
                    if (8 - r - bit >= 0) // if the element fits into this byte
                    {
                        data16_uncompressed[byte] = ((h_odata[i] >> (8 - r - bit)) & ((1 << r) - 1));
                        bit += r;
                        byte++;
                    }
                    else
                    {
                        i++;
                        data16_uncompressed[byte] = ((h_odata[i - 1] & ((1 << (8 - bit)) - 1)) << (r + bit - 8) | h_odata[i] >> (8 - (r + bit - 8)));
                        byte++;
                        bit = bit + r - 8;
                    }
                    // printf("data16_uncompressed[%d] = %u\n", byte-1, data16_uncompressed[byte-1]);
                }

                for (int b = 1; b < wfmLen; b++)
                {
                    data16_uncompressed[wfmN * wfmLen + b] = data16_uncompressed[wfmN * wfmLen + b] - data16_uncompressed[wfmN * wfmLen];
                    // printf("data16_uncompressed[%d] = %d\n", b, data16_uncompressed[wfmN*wfmLen + b]);
                }
                data16_uncompressed[wfmN * wfmLen] = min;
                for (int b = 1; b < wfmLen; b++)
                {
                    data16_uncompressed[wfmN * wfmLen + b] = data16_uncompressed[wfmN * wfmLen + b - 1] + data16_uncompressed[wfmN * wfmLen + b];
                }
            }
            else
            {

                while (byte < wfmN * wfmLen + wfmLen)
                {
                    if (8 - r - bit >= 0) // if the element fits into this byte
                    {
                        data16_uncompressed[byte] = ((h_odata[i] >> (8 - r - bit)) & ((1 << r) - 1)) + min;
                        bit += r;
                        byte++;
                    }
                    else
                    {
                        i++;
                        data16_uncompressed[byte] = ((h_odata[i - 1] & ((1 << (8 - bit)) - 1)) << (r + bit - 8) | h_odata[i] >> (8 - (r + bit - 8))) + min;
                        byte++;
                        bit = bit + r - 8;
                    }
                    if (i_evt == 1)
                    {
                        //printf("h_odata[%d] = %d\n", i, h_odata[i]);
                        //usleep(100000);
                    }

                    if (data16_uncompressed[byte-1] != h_idata[byte-1]){
                        //printf("burrai_evt=%d, i=%d; byte=%d; unComp=%d; original = %d\n", i_evt, i, byte-1, data16_uncompressed[byte-1], h_idata[byte-1]);
                        //usleep(100000);
                    }
                }
                // printf("data16_uncompressedNico[%d] = %d\n", byte-1, data16_uncompressed[byte-1]);
            }
        }
        // byteNow += bytesPerWfm[wfmN];
        byteNow += thisWfmLength;

        // Comprobation
        // printf("data16_uncompressed[%d] = %d\n", 0, data16_uncompressed[0]);

        // if (wfmN  == 72){
        //     for(int j=0 ; j<64 ; j++)
        //         printf("wfmN:%d, j=%d; unComp=%d; original = %d, h_odata[j] =  %d\n", wfmN, j, data16_uncompressed[wfmN*wfmLen+j], h_idata[wfmN*wfmLen+j], h_odata[wfmN*(wfmLen*2) + wfmN]);
        // }


        //if (wfmN == 0){
        //printf("Printing wfm0:\n");
        //for (int k = 0; k < 64; k++)
        //{
        //    printf("err: wfmN:%d, k=%d; unComp=%d; original = %d, h_odata[k] =  %d\n", wfmN, k, data16_uncompressed[wfmN * wfmLen + k], h_idata[wfmN * wfmLen + k], h_odata[wfmN * (wfmLen * 2) + wfmN]);
        //}
        //continue;   
        //}


        //if (r > 5)
        //    printf("decompressed i_evt = %d, wfmN = %d, r = %d, min = %d\n", i_evt, wfmN, r, min);
        for (int j = 0; j < 64; j++)
        {
            //if (j==0)
            //    printf("buu: wfmN:%d, j=%d; unComp=%d; original = %d, h_odata[j] =  %d\n", wfmN, j, data16_uncompressed[wfmN * wfmLen + j], h_idata[wfmN * wfmLen + j], h_odata[wfmN * (wfmLen * 2) + wfmN]);



            if (data16_uncompressed[wfmN * wfmLen + j] != h_idata[wfmN * wfmLen + j])
            //if (h_idata[wfmN * wfmLen + j] == 469 && h_idata[wfmN * wfmLen + j+1] == 469 && h_idata[wfmN * wfmLen + j + 2] == 471 && j < 62)
            //if (wfmN == 0)
            {
                printf("Error... i_evt=%d, wfmN=%d, j=%d; unComp=%d; original = %d, r = %d, min = %d\n", i_evt, wfmN, j, data16_uncompressed[wfmN * wfmLen + j], h_idata[wfmN * wfmLen + j], r, min);

                printf("Printing last 64 elements of h_odata:\n");
                for (int k = 0; k < 64; k++)
                {
                    printf("err: wfmN:%d, k=%d; unComp=%d; original = %d, h_odata[k] =  %d\n", wfmN, k, data16_uncompressed[wfmN * wfmLen + k], h_idata[wfmN * wfmLen + k], h_odata[wfmN * (wfmLen * 2) + wfmN]);
                }
                exit(1);
                return;
            }
        }
        //printf("Wfm %d is OK\n", wfmN);
        nNonPedSuppWfms++;

    }

    //l3log.log(L3_VERBOSE, L3_DEBUG, "Decompressed %d wfms\n", nNonPedSuppWfms);

    // Extra comprobation
    for (int i = 0; i < nWfms * wfmLen; i++)
    {

        if (data16_uncompressed[i] == 12345)
        {
            continue;
        }
        else if (data16_uncompressed[i] != h_idata[i])
        {
            printf("i=%d; unComp=%d; original = %d\n", i, data16_uncompressed[i], h_idata[i]);
            printf("error!\n");

            return;
        }
    }

    // free memory so CPU doesn't explode
    free(data16_uncompressed);

    nvtxRangePop();
}


void LaunchCompressKernel(uint16_t *d_perFADCWfmData, uint8_t* d_perEvtHeaderData, uint8_t *d_odata, uint16_t *d_wfmInfo, uint32_t* d_perEvtIdxCompressed, uint32_t* d_perEvtSizeCompressed, uint32_t *d_compressedBytesPerWfm, cudaStream_t stream, int nLoadedEvents, uint16_t *d_recoTag)
{
    //nvtxRangePush("LaunchCompressKernel");

    l3log.log(L3_DEBUG, L3_INFO, "Launch kernel_write_evt_hdr with %d events\n", nLoadedEvents);

    // reduce all events
    dim3 gridSize = dim3(nLoadedEvents);
    kernel_write_evt_hdr<<<gridSize, 128, 0, stream>>>(d_perEvtIdxCompressed, d_perEvtSizeCompressed, d_perEvtHeaderData, d_odata, nLoadedEvents, d_recoTag);


    int nWfms = 288 * 16;
    unsigned int wfmLen = 64;

    gridSize = dim3(288, 1, 1); // same kernel processes all nLoadedEvents, but one by one please.
    gridSize = dim3(288, nLoadedEvents, 1); // same kernel processes all nLoadedEvents, but one by one please.
    dim3 blockSize = 256; // Threads per block

    kernel_compress_nicola<<<gridSize, blockSize, 0, stream>>>(d_perFADCWfmData, d_perEvtHeaderData, d_odata, wfmLen, nWfms, d_wfmInfo, d_perEvtIdxCompressed, d_compressedBytesPerWfm, nLoadedEvents, d_recoTag);

    //nvtxRangePop();
}

// make -j15 && ./main 0 | awk '/awkdata/ {printf ("%f %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d\n", $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26, $27, $28, $29, $30, $31, $32, $33, $34, $35, $36, $37, $38, $39, $40, $41, $42, $43, $44, $45, $46, $47, $48, $49, $50, $51, $52, $53, $54, $55, $56, $57, $58, $59, $60, $61, $62, $63, $64, $65, $66)}' > out.txt