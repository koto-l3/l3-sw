
#include "Detectors.h"
#include <fstream>
#include <iostream>
#include <algorithm>

DetectorInfo::DetectorInfo(const Config &config)
{
    m_config = config;

    m_nChannels = 0;
    m_samplesPerWfm = 0;
}


int DetectorInfo::fillChMaskFromCfg()
{
    for (u_int i_ch_to_mask = 0; i_ch_to_mask < m_config.csi_chids_to_mask_reco.size(); i_ch_to_mask++)
    {
        if (m_config.csi_chids_to_mask_reco.at(i_ch_to_mask) >= m_nChannels)
        {
            l3log.log(L3_QUIET, L3_ERROR, "CSI CH ID %d set to be masked in cfg, but CSI has only %d channels.\n", m_config.csi_chids_to_mask_reco.at(i_ch_to_mask), m_nChannels);
            return -1;
        }
        m_chMaskForReco.at(m_config.csi_chids_to_mask_reco.at(i_ch_to_mask)) = 1;
    }



    std::ifstream ifile;
    ifile.open(m_config.csi_chids_to_mask_pedsup_file.c_str());

    if (!ifile)
    {
        std::cerr << "cannot open file '" << m_config.csi_chids_to_mask_pedsup_file << "'" << std::endl;
        return -1;
    }

    std::string line;
    while (std::getline(ifile, line))
    {
        std::replace(std::begin(line), std::end(line), '\t', ' ');
        std::vector<std::string> words = split(line, ' ');

        if (words.size() == 0)
        {
            continue;
        }
        else if (words.size() == 1)
        {
            int chid = stoi(words.at(0));
            if (chid >= m_nChannels)
            {
                l3log.log(L3_QUIET, L3_ERROR, "CSI CH ID %d set to be masked in %s, but CSI has only %d channels.\n", chid, m_config.csi_chids_to_mask_pedsup_file.c_str(), m_nChannels);
                return -1;
            }

            m_chMaskForPedSup.at(chid) = 1;
        }
        else
        {
            l3log.log(L3_QUIET, L3_ERROR, "Please write only one channel per line in %s\n", m_config.csi_chids_to_mask_pedsup_file.c_str());
            printf("%s\n", line.c_str());
        }
    }

    ifile.close();
    return 0;
}

int DetectorInfo::fillMapperListFromTxt()
{
    std::ifstream ifile;
    ifile.open(m_ext_map_file.c_str());

    if (!ifile)
    {
        std::cerr << "cannot open file '" << m_ext_map_file << "'" << std::endl;
        return -1;
    }

    l3log.log(L3_VERBOSE, L3_INFO, "Opening mapper file %s for detector %s\n", m_ext_map_file.c_str(), m_name.c_str());

    std::string line;
    while (std::getline(ifile, line))
    {
        std::replace(std::begin(line), std::end(line), '\t', ' ');
        std::vector<std::string> words = split(line, ' ');

        if (words.size() == 0)
        {
            continue;
        }
        else if (words.size() == 1)
        {
            continue;
        }
        else if (words.size() == 4)
        {
            int det_ch_id = stoi(words.at(0));
            if (det_ch_id >= m_nChannels && m_nChannels != 0)
            {
                l3log.log(L3_VERBOSE, L3_WARN, "WARNING: ChID %d (read from mapper file) >= %d (read from cfg)\n", det_ch_id, m_nChannels);
                continue;
            }
            AddChannelData(det_ch_id, stoi(words.at(1)), stoi(words.at(2)), stoi(words.at(3)));
        }
        else
        {
            printf("Come and do something about this.\n");
            printf("%s\n", line.c_str());
        }
    }

    ifile.close();
    return 0;
}

void DetectorInfo::PrepareVectors()
{
    // Constant for all events
    m_detChID.resize(m_nChannels, -1);
    m_fadcCrateIDs.resize(m_nChannels, -1);
    m_fadcModuleIDs.resize(m_nChannels, -1);
    m_fadcChIDs.resize(m_nChannels, -1);
    m_AbsChID.resize(m_nChannels, -1);

    m_ChIdX.resize(m_nChannels, -1);
    m_ChIdY.resize(m_nChannels, -1);

    m_GainMean.resize(m_nChannels, -1);
    m_GainSigma.resize(m_nChannels, -1);
    m_MeVCoeff.resize(m_nChannels, -1);
    m_chMaskForReco.resize(m_nChannels, 0);
    m_chMaskForPedSup.resize(m_nChannels, 0);

    // Variable event by event
    // m_pedestal.resize(m_nChannels);
    // m_integratedADC.resize(m_nChannels);
    // m_peak_heigh.resize(m_nChannels);
    // m_Energies.resize(m_nChannels);
}

void DetectorInfo::AddChannelData(int det_ch_id, int fadcCrateID, int fadcModuleID, int fadcChID)
{

    if (std::find(m_detChID.begin(), m_detChID.end(), det_ch_id) != m_detChID.end())
    {
        printf("WARNING: det_ch_id %d is duplicated in the provided mapper file.\n", det_ch_id);
    }
    else
    {

        if (det_ch_id >= m_nChannels)
        {
            printf("WARNING: ChID %d >= nChannels read from file (%d). Redefining nChannels to %d...\n", det_ch_id, m_nChannels, det_ch_id + 1);
            m_nChannels = det_ch_id + 1;
            PrepareVectors();
        }

        m_detChID.at(det_ch_id) = det_ch_id;
        m_fadcCrateIDs.at(det_ch_id) = fadcCrateID;
        m_fadcModuleIDs.at(det_ch_id) = fadcModuleID;
        m_fadcChIDs.at(det_ch_id) = fadcChID;
        m_AbsChID.at(det_ch_id) = fadcCrateID * N_FADCS_PER_CRATE * N_CH_PER_FADC + fadcModuleID * N_CH_PER_FADC + fadcChID;

        // Fuck this sideways with the rusty anchor of an aircraft carrier
        // if (m_fadcCrateIDs.at(det_ch_id)==10){
        //    m_fadcModuleIDs.at(det_ch_id) -= 1;
        //}
        // if (m_fadcCrateIDs.at(det_ch_id)==15){
        //    if (m_fadcModuleIDs.at(det_ch_id) > 8){
        //        m_fadcModuleIDs.at(det_ch_id) -= 1;
        //    }
        //}

        // printf("Added det_ch_id: %d, crateid: %d, fadcid: %d, chid: %d\n", det_ch_id, fadcCrateID, fadcModuleID, fadc);
    }
}

int DetectorInfo::ReadCalibrationData()
{
    // e14calibrator l88

    std::ifstream ifile;
    ifile.open(m_calibFile.c_str());
    if (!ifile)
    {
        return 1;
    }
    l3log.log(L3_QUIET, L3_INFO, "Reading calibration data from %s\n", m_calibFile.c_str());

    m_GainMean.clear();
    m_GainSigma.clear();
    m_MeVCoeff.clear();

    std::string line;
    int i_line = -1;
    while (std::getline(ifile, line))
    {
        i_line ++;
        // std::cout << line << std::endl;
        std::replace(std::begin(line), std::end(line), '\t', ' ');
        std::vector<std::string> words = split(line, ' ');

        if (words.size() == 0)
        {
            continue;
        }
        else if (words.size() == 1)
        {
            // This relies on the format of the calib. file
            if (m_nChannels == 0)
                m_nChannels = stoi(words.at(0));
            PrepareVectors();
        }
        else if (words.size() == 2)
        {
            // AddDetector(words.at(0), words.at(1));
            printf("here be dragons");
        }
        else if (words.size() == 3)
        {
            printf("here be dragons too");
        }
        else if (words.size() == 4)
        {
            if (stoi(words.at(0)) >= m_nChannels)
            {
                printf("WARNING: Detector %s appears to have more channels than the number of channels read from the cfg, %d.\n", words.at(0).c_str(), m_nChannels);
                break;
                // printf("WARNING: ChID %d >= nChannels read from file (%d). Redefining nChannels to %d...\n", stoi(words.at(0)), m_nChannels, stoi(words.at(0))+1);
                // m_nChannels = stoi(words.at(0))+1;
                // PrepareVectors();
            }
            m_fadcChIDs.at(stoi(words.at(0))) = stoi(words.at(0));
            m_GainMean.at(stoi(words.at(0))) = stof(words.at(1));
            m_GainSigma.at(stoi(words.at(0))) = stof(words.at(2));
            m_MeVCoeff.at(stoi(words.at(0))) = stof(words.at(3));
        }
        else
        {
            l3log.log(L3_QUIET, L3_ERROR, "Hi!! There is something weid in your calib cfg file (~line %d)\n", i_line);
        }
    }
    ifile.close();
    return 0;
}

int DetectorInfo::ReadCHPositions()
{

    std::ifstream ifile;
    ifile.open(m_chPosFile.c_str());
    if (!ifile)
    {
        std::cerr << "ERROR: cannot open file '" << m_chPosFile << "'" << std::endl;
        return 1;
    }

    std::string line;
    while (std::getline(ifile, line))
    {
        // std::cout << line << std::endl;
        std::replace(std::begin(line), std::end(line), '\t', ' ');
        std::vector<std::string> words = split(line, ' ');

        if (words.size() == 0)
        {
            continue;
        }
        else if (words.at(0).rfind("#", 0) == 0)
        {
            continue;
        }
        else if (words.size() == 1)
        {
            printf("here be dragons");
        }
        else if (words.size() == 2)
        {
            // AddDetector(words.at(0), words.at(1));
            printf("here be dragons");
        }
        else if (words.size() == 3)
        {

            // std::cout << line << std::endl;
            m_chPosX.push_back(stof(words.at(1)));
            m_chPosY.push_back(stof(words.at(2)));
            // m_chPosX.push_back(((tmp++)%850*2) - 850);
            // m_chPosY.push_back(100);
        }
        else
        {
            printf("Hi!! There is something weird in your chPos cfg file\n");
        }
    }

    if ((int)m_chPosX.size() != m_nChannels || (int)m_chPosY.size() != m_nChannels)
    {
        printf("WARNING: Number of channels in chpos file (%d) does not match number of channels set in cfg (%d)\n", (int)m_chPosX.size(), m_nChannels);
        printf("Filling remaining channel pos. with 0s...\n");
        for (int i = m_chPosX.size(); i < m_nChannels; i++)
        {
            m_chPosX.push_back(0);
            m_chPosY.push_back(0);
        }
    }

    ifile.close();

    // Fill chIDs in X and Y

    // min and max X and Y
    float minX = *std::min_element(m_chPosX.begin(), m_chPosX.end());
    float minY = *std::min_element(m_chPosY.begin(), m_chPosY.end());
    //float maxX = *std::max_element(m_chPosX.begin(), m_chPosX.end());
    //float maxY = *std::max_element(m_chPosY.begin(), m_chPosY.end());

    for (int i_ch = 0; i_ch < m_nChannels; i_ch++)
    {
        m_ChIdX.at(i_ch) = (getChPosX(i_ch) - minX) * 4 / 50; // each large crystal is 4 units large
        m_ChIdY.at(i_ch) = (getChPosY(i_ch) - minY) * 4 / 50; // each large crystal is 4 units large

        // so ChPosX = m_ChIdX.at(i_ch)*50/4 + minX; min = -875.000000

    }

    // printf("posX ch 46: %f; posX ch 47: %f\n", getChPosX(46), getChPosX(47));

    return 0;
}

DetectorData::DetectorData(DetectorInfo *info)
{
    m_info = info;
    m_totalE = 0;
    m_wfmData.resize(MAX_NEVENTS_PER_STREAM * m_info->m_nChannels);
    m_pedestal.resize(MAX_NEVENTS_PER_STREAM * m_info->m_nChannels);
    m_integratedADC.resize(MAX_NEVENTS_PER_STREAM * m_info->m_nChannels);
    m_peak_heigh.resize(MAX_NEVENTS_PER_STREAM * m_info->m_nChannels);
    m_Energies.resize(MAX_NEVENTS_PER_STREAM * m_info->m_nChannels);
    m_times.resize(MAX_NEVENTS_PER_STREAM * m_info->m_nChannels);
}
