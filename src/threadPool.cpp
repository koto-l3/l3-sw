
#include "threadPool.h"
#include "common.h"
#include <future>
#include <unistd.h>

ThreadPool::ThreadPool(int numThreads)
    : m_stop()
    , m_busy()
{
    for (int i=0; i<numThreads; ++i)
        m_threads.emplace_back(std::bind(&ThreadPool::thread_proc, this));
}



void ThreadPool::thread_proc()
{
    while (true)
    {
        std::unique_lock<std::mutex> latch(m_queueMutex);
        m_condition.wait(latch, [this](){ return m_stop || !m_tasks.empty(); });
        if (!m_tasks.empty())
        {
            // got work. set busy.
            ++m_busy;

            // pull from queue
            auto fn = m_tasks.front();
            m_tasks.pop_front();

            // release lock. run async
            latch.unlock();

            // run function outside context
            fn();
            //++processed;

            latch.lock();
            --m_busy;
            m_finished.notify_one();
        }
        else if (m_stop)
        {
            break;
        }
    }
}


//ThreadPool::ThreadPool(std::string label, int numThreads): m_stop(false), m_busy(0), m_label(label)
//{
//
//    for (int i = 0; i < numThreads; i++)
//    {
//        m_threads.emplace_back([this] {
//            while (true)
//            {
//                std::function<void()> task;
//                {
//
//                    std::unique_lock<std::mutex> latch(this->m_queueMutex);
//                    this->m_condition.wait(latch, [this] { return this->m_stop || !this->m_tasks.empty(); });
//                    if (this->m_stop && this->m_tasks.empty()){
//                        return;
//                    }
//                    else{
//                        m_busy++;
//                        task = std::move(this->m_tasks.front());
//                        this->m_tasks.pop();
//                        latch.unlock();
//                        task();
//                        latch.lock();
//                        m_busy--;
//                        m_finished.notify_one();
//                    }
//                }
//            }
//        });
//    }
//    printf("ThreadPool initialized with %d threads.\n", numThreads);
//}


// waits until the queue is empty.
void ThreadPool::waitFinished()
{
    std::unique_lock<std::mutex> lock(m_queueMutex);
    m_finished.wait(lock, [this](){ return m_tasks.empty() && (m_busy == 0); });
}



ThreadPool::~ThreadPool()
{
    {
        std::unique_lock<std::mutex> lock(this->m_queueMutex);
        this->m_stop = true;
        lock.unlock();
    }
    this->m_condition.notify_all();
    for (std::thread& thread : this->m_threads)
        thread.join();
}
