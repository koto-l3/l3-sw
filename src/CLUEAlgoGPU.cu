#include <math.h>
#include <limits>
#include <iostream>

// GPU Add
#include <cuda_runtime.h>
#include <cuda.h>
// for timing
#include <chrono>
#include <ctime>
// user include
#include <thrust/copy.h>

#include "CLUEAlgoGPU.h"
#include "LayerTilesGPU.h"
#include "GPUData.h"
#include "common.h"

__global__ void clue_fill_tiles(LayerTilesGPU *d_hist,
                                const PointsPtr d_points)
{

    printf("DEPRECATED\n");

    int nPoints = *d_points.n;

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < nPoints)
    {
        // push index of points into tiles
        // d_hist[d_points.i_evt[i]].fill(d_points.x[i], d_points.y[i], i);
        d_hist[d_points.i_evt[i]].fill(d_points.x[i], d_points.y[i], i);
    }

    // d_seeds[d_points.i_evt[i]].reset();
    // d_followers[d_points.i_evt[i]].reset();
    //__syncthreads();

} // kernel

__global__ void clue_compute_density(LayerTilesGPU *d_hist,
                                     PointsPtr d_points,
                                     uint16_t *d_recoTag,
                                     ParamsPtr d_params)
{

    int nPoints = *d_points.n;
    float local_rho = 0;

    int i_point = blockIdx.x * blockDim.x + threadIdx.x;
    if (i_point < nPoints)
    {
        int i_evt = d_points.i_evt[i_point];
        uint8_t this_evt_reco_mode = d_recoTag[i_evt] & 0x00ff;

        if (this_evt_reco_mode > SEL_THRESHOLD || this_evt_reco_mode >= MAX_N_RECO_MODES)
        {
            return;
        }

        float xi = d_points.x[i_point];
        float yi = d_points.y[i_point];

        // get dc, dm, deltac, deltao, rhoc
        float dc = d_params.dc[this_evt_reco_mode];

        // get search box
        int4 search_box = d_hist[i_evt].searchBox(xi - dc, xi + dc, yi - dc, yi + dc);

        // loop over bins in the search box
        for (int xBin = search_box.x; xBin < search_box.y + 1; ++xBin)
        {
            for (int yBin = search_box.z; yBin < search_box.w + 1; ++yBin)
            {

                // get the id of this bin
                int binId = d_hist[i_evt].getGlobalBinByBin(xBin, yBin);
                // get the size of this bin
                int binSize = d_hist[i_evt][binId].size();

                // interate inside this bin
                for (int binIter = 0; binIter < binSize; binIter++)
                {
                    int j_point = d_hist[i_evt][binId][binIter];
                    // query N_{dc_}(i)
                    // float xj = d_points.x[j_point];
                    // float yj = d_points.y[j_point];
                    // float dist_ij_tmp = std::sqrt((xi-xj)*(xi-xj) + (yi-yj)*(yi-yj));

                    int dist_ij_x = abs(d_points.x[i_point] - d_points.x[j_point]);
                    int dist_ij_y = abs(d_points.y[i_point] - d_points.y[j_point]);
                    int dist_ij = max(dist_ij_x, dist_ij_y);

                    if (max(dist_ij_x, dist_ij_y) < dc && dist_ij_y < -dist_ij_x + 5 / 3 * dc)
                    {
                        // d_points.rho[i_point] += d_points.weight[j_point];
                        local_rho += d_points.weight[j_point];
                    }

                } // end of interate inside this bin
            }
        } // end of loop over bins in search box
        d_points.rho[i_point] = local_rho;
        // printf("ld_i_point = %d, rhoi = %f, dc = %f\n", i_point, local_rho, dc);
    }
} // kernel

__global__ void clue_compute_distanceToHigher(LayerTilesGPU *d_hist,
                                              PointsPtr d_points,
                                              uint16_t *d_recoTag,
                                              ParamsPtr d_params)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int nPoints = *d_points.n;

    if (i < nPoints)
    {
        int i_evt = d_points.i_evt[i];
        uint8_t this_evt_reco_mode = d_recoTag[i_evt] & 0x00ff;

        if (this_evt_reco_mode > SEL_THRESHOLD || this_evt_reco_mode >= MAX_N_RECO_MODES)
        {
            return;
        }

        // float deltai = std::numeric_limits<float>::max();
        float deltai = 999999;
        int nearestHigheri = -1;
        float xi = d_points.x[i];
        float yi = d_points.y[i];
        float rhoi = d_points.rho[i];

        // get deltac and deltao
        float deltac = d_params.deltac[this_evt_reco_mode];
        float deltao = d_params.deltao[this_evt_reco_mode];
        float dm = max(deltac, deltao);

        // get search box
        int4 search_box = d_hist[i_evt].searchBox(xi - dm, xi + dm, yi - dm, yi + dm);

        // loop over all bins in the search box
        for (int xBin = search_box.x; xBin < search_box.y + 1; ++xBin)
        {
            for (int yBin = search_box.z; yBin < search_box.w + 1; ++yBin)
            {
                // get the id of this bin
                int binId = d_hist[i_evt].getGlobalBinByBin(xBin, yBin);
                // get the size of this bin
                int binSize = d_hist[i_evt][binId].size();

                // interate inside this bin
                for (int binIter = 0; binIter < binSize; binIter++)
                {
                    int j = d_hist[i_evt][binId][binIter];
                    // if (binId == 0){
                    //   printf("binSize = %d, binId = %d, binIter = %d, j = %d\n", binSize, binId, binIter, j);
                    // }
                    //  query N'_{dm}(i)
                    // float xj = d_points.x[j];
                    // float yj = d_points.y[j];
                    // float dist_ij_tmp = std::sqrt((xi-xj)*(xi-xj) + (yi-yj)*(yi-yj));

                    float dist_ij_x = abs(d_points.x[i] - d_points.x[j]);
                    float dist_ij_y = abs(d_points.y[i] - d_points.y[j]);

                    float dist_ij = max(dist_ij_x, dist_ij_y);

                    bool foundHigher = (d_points.rho[j] > rhoi);

                    // in the rare case where rho is the same, use detid
                    foundHigher = foundHigher || ((d_points.rho[j] == rhoi) && (j > i));

                    if (foundHigher && dist_ij <= dm && dist_ij_y < -dist_ij_x + 5 / 3 * dm)
                    { // definition of N'_{dm}(i)
                        // find the nearest point within N'_{dm}(i)
                        if (dist_ij <= deltai)
                        {
                            // update deltai and nearestHigheri if the new candidate has higher rho
                            if (nearestHigheri == -1)
                            {
                                deltai = dist_ij;
                                nearestHigheri = j;
                            }
                            else if (d_points.rho[j] > d_points.rho[nearestHigheri])
                            {
                                deltai = dist_ij;
                                nearestHigheri = j;
                            }
                        }
                    }
                } // end of interate inside this bin
            }
        } // end of loop over bins in search box
        d_points.delta[i] = deltai;
        d_points.nearestHigher[i] = nearestHigheri;
        //printf("i = %d, deltai = %f, nearestHigheri = %d, rhoi = %f\n", i, deltai, nearestHigheri, rhoi);
    }
} // kernel

__global__ void clue_find_clusters(GPU::VecArray<int, maxNSeeds> *d_seeds,
                                   GPU::VecArray<int, maxNFollowers> *d_followers,
                                   PointsPtr d_points,
                                   uint16_t *d_recoTag,
                                   ParamsPtr d_params)
{
    int nPoints = *d_points.n;
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < nPoints)
    {
        d_followers[i].reset();
    }
    __syncthreads();

    if (i < nPoints)
    {

        int i_evt = d_points.i_evt[i];
        uint8_t this_evt_reco_mode = d_recoTag[i_evt] & 0x00ff;
        
        if (this_evt_reco_mode > SEL_THRESHOLD || this_evt_reco_mode >= MAX_N_RECO_MODES)
        {
            return;
        }

        float deltac = d_params.deltac[this_evt_reco_mode];
        float deltao = d_params.deltao[this_evt_reco_mode];
        float rhoc = d_params.rhoc[this_evt_reco_mode];

        // initialize clusterIndex
        d_points.clusterIndex[i] = -1;
        // determine seed or outlier
        float deltai = d_points.delta[i];
        float rhoi = d_points.rho[i];
        bool isSeed = (deltai > deltac) && (rhoi >= rhoc);
        bool isOutlier = (deltai > deltao) && (rhoi < rhoc);

        if (isSeed)
        {
            // set isSeed as 1
            d_points.isSeed[i] = 1;
            d_seeds[d_points.i_evt[i]].push_back(i); // head of d_seeds
                                                     // printf("Found seed %d\n", i);
        }
        else
        {
            d_points.isSeed[i] = 0;
            if (!isOutlier)
            {
                assert(d_points.nearestHigher[i] < nPoints);
                // register as follower at its nearest higher
                d_followers[d_points.nearestHigher[i]].push_back(i);
            }
        }
    }
} // kernel

__global__ void clue_compute_clusterXYE(const GPU::VecArray<int, maxNSeeds> *d_seeds,
                                        const GPU::VecArray<int, maxNFollowers> *d_followers,
                                        PointsPtr d_points,
                                        Clusters d_clusters)
{

    float thisClusterEnergy = 0;
    float thisClusterX = 0;
    float thisClusterY = 0;

    int i_seed = blockIdx.x * blockDim.x + threadIdx.x; // inside layer
    int i_layer = blockIdx.y;

    const auto &seeds = d_seeds[i_layer];
    u_int nSeeds = seeds.size();

    __shared__ int nClus;
    nClus = 0;

    __syncthreads();

    d_clusters.evtID[i_layer] = d_points.i_evt[seeds[0]];

    if (nSeeds > MAX_CSI_CLUS_PER_EVT)
    {
        //printf("nSeeds = %d\n", nSeeds);
    }
    else if (i_seed < nSeeds)
    {

        int nFollowersThisSeed = d_followers[seeds[i_seed]].size();
        uint16_t i_clus;

        if (nFollowersThisSeed != 0)
        {
            i_clus = atomicAdd(&nClus, 1);

            int localStack[localStackSizePerSeed] = {-1};
            int localStackSize = 0;

            // assign cluster to seed[i_clus]
            int idxThisSeed = seeds[i_seed];

            thisClusterEnergy += d_points.weight[idxThisSeed];
            thisClusterX += d_points.x[idxThisSeed] * d_points.weight[idxThisSeed];
            thisClusterY += d_points.y[idxThisSeed] * d_points.weight[idxThisSeed];

            d_points.clusterIndex[idxThisSeed] = i_clus;
            // push_back idThisSeed to localStack
            localStack[localStackSize] = idxThisSeed;
            localStackSize++;
            // process all elements in localStack
            while (localStackSize > 0)
            {
                // get last element of localStack
                int idxEndOflocalStack = localStack[localStackSize - 1];

                // int temp_clusterIndex = d_points.clusterIndex[idxEndOflocalStack];
                //  pop_back last element of localStack
                localStack[localStackSize - 1] = -1;
                localStackSize--;

                // printf("clusterID %d; Nfloowers = %d\n",i_clus,  d_followers[idxEndOflocalStack].size());
                //  loop over followers of last element of localStack
                for (int j : d_followers[idxEndOflocalStack])
                {
                    // // pass id to follower
                    // push_back follower to localStack
                    if (localStackSize >= localStackSizePerSeed)
                    {
                        //printf("localStackSize = %d\n", localStackSize);
                        // If the cluster has this many hits, consider only the MAX_CSI_HITS_PER_CLUSTER most energetic ones.
                        break;
                    }
                    localStack[localStackSize] = j;
                    localStackSize++;

                    thisClusterEnergy += d_points.weight[j];
                    thisClusterX += d_points.x[j] * d_points.weight[j];
                    thisClusterY += d_points.y[j] * d_points.weight[j];

                    if (isnan(d_points.weight[j]))
                    {
                        printf("i_layer %d ClusterID %d; x = %f, y = %f; j = %d; Energy = %f\n", i_layer, i_clus, d_points.x[j], d_points.y[j], j, d_points.weight[j]);
                    }

                    // printf("j = %d, temp_clusterIndex = %d; idxthisseed = %d\n", j, i_clus, idxThisSeed);
                }
            }

            // printf("i_layer %d ClusterID %d; Energy = %f\n", i_layer, i_clus, thisClusterEnergy);
            d_clusters.E[i_layer * MAX_CSI_CLUS_PER_EVT + i_clus] = thisClusterEnergy;
            d_clusters.x[i_layer * MAX_CSI_CLUS_PER_EVT + i_clus] = thisClusterX / thisClusterEnergy * 50.3 / 4 - 880.25;
            d_clusters.y[i_layer * MAX_CSI_CLUS_PER_EVT + i_clus] = thisClusterY / thisClusterEnergy * 50.3 / 4 - 880.25;
        }
    }
    __syncthreads();
    d_clusters.n[i_layer] = nClus;

} // kernel

__global__ void clue_set_points_from_gpu(int csi_n_channels, float *d_csi_energies, uint16_t *d_csi_times, int *d_csi_x, int *d_csi_y, PointsPtr d_points, LayerTilesGPU *d_hist, uint16_t *d_recoTag, GPU::VecArray<int, maxNSeeds> *d_seeds, u_int time_win_min, u_int time_win_max)
{
    int csi_ch_id = threadIdx.x;
    int i_evt = blockIdx.x;

    if (csi_ch_id == 0)
        d_seeds[i_evt].reset();
    // d_hist[i_evt].clear();

    if ((d_recoTag[i_evt] & 0x00ff) > SEL_THRESHOLD)
    {
        return;
    }
    __syncthreads();

    while (csi_ch_id < csi_n_channels)
    {
        if (d_csi_energies[i_evt * csi_n_channels + csi_ch_id] > 3)
        {
            //printf("awkdata %d\n", d_csi_times[i_evt * csi_n_channels + csi_ch_id]);
            // printf("buuu %d\n", d_csi_times[i_evt * csi_n_channels + csi_ch_id]);
            // if (d_csi_times[i_evt * csi_n_channels + csi_ch_id] >= 26 && d_csi_times[i_evt * csi_n_channels + csi_ch_id] <= 32) // run 87
            if (d_csi_times[i_evt * csi_n_channels + csi_ch_id] > time_win_min && d_csi_times[i_evt * csi_n_channels + csi_ch_id] < time_win_max) // run 90
            {
                int point_id = atomicAdd(d_points.n, 1);

                if (point_id >= MAX_CSI_HITS_PER_STREAM)
                {
                    if (threadIdx.x == 0)
                        printf("Too many CSI hit crystls in this block of events. Consider increasing MAX_CSI_HITS_PER_STREAM\n");

                    d_recoTag[i_evt] = REC_WEIRD;
                    return;
                }

                // printf("d_points.n = %d\n", point_id);
                d_points.weight[point_id] = d_csi_energies[i_evt * csi_n_channels + csi_ch_id];

                d_points.x[point_id] = (float)d_csi_x[csi_ch_id];
                d_points.y[point_id] = (float)d_csi_y[csi_ch_id];


                d_points.i_evt[point_id] = i_evt;
                // push index of points into tiles
                d_hist[i_evt].fill(d_points.x[point_id], d_points.y[point_id], point_id);
            }
        }
        csi_ch_id += blockDim.x;
    }
}

void CLUEAlgoGPU::SetPointsFromGPU(GPUData *gpudata, GPUDetectorInfo *csi_info, int csi_n_channels, cudaStream_t stream, int nEvents, uint16_t *d_recoTag, u_int time_win[2])
{
    // Prepare for clustering

    dim3 blockSize2 = 1024;               // Threads per block
    dim3 gridSize2 = dim3(nEvents, 1, 1); // Blocks in grid

    cudaMemset(d_points.n, 0, sizeof(int));

    cudaMemset(d_hist, 0x00, sizeof(LayerTilesGPU) * MAX_NEVENTS_PER_STREAM);
    clue_set_points_from_gpu<<<gridSize2, blockSize2, 0, stream>>>(csi_n_channels, gpudata->d_csi_energies, gpudata->d_csi_times, csi_info->d_csi_x, csi_info->d_csi_y, d_points, d_hist, d_recoTag, d_seeds, time_win[0], time_win[1]);
}

void CLUEAlgoGPU::makeClusters(cudaStream_t stream, Clusters *d_clusters, uint16_t *d_recoTag, int nEvtsForClustering)
{

    // copy_todevice();
    clear_set();

    // d_hist = new LayerTilesGPU[nEvtsForClustering];

    // printf("Size tile 0: %d\n", d_hist[0][0].size());

    // exit(1);

    ////////////////////////////////////////////
    // calcualte rho, delta and find seeds
    // 1 point per thread
    ////////////////////////////////////////////
    const dim3 blockSize(1024, 1, 1);
    const dim3 gridSize(1024, 1, 1); // ideally this should be the number of events
    // printf("gridSize = %d\n",points_.n);
    // clue_fill_tiles<<<gridSize, blockSize, 0, stream>>>(d_hist, d_points);
    // cudaDeviceSynchronize();
    // printf("dc_ = %f, dm_ = %f, deltac_ = %f, deltao_ = %f, rhoc_ = %f\n", dc_, dm_, deltac_, deltao_, rhoc_);
    clue_compute_density<<<gridSize, blockSize, 0, stream>>>(d_hist, d_points, d_recoTag, d_params);
    // cudaStreamSynchronize(stream);
    clue_compute_distanceToHigher<<<gridSize, blockSize, 0, stream>>>(d_hist, d_points, d_recoTag, d_params);
    // cudaStreamSynchronize(stream);
    clue_find_clusters<<<gridSize, blockSize, 0, stream>>>(d_seeds, d_followers, d_points, d_recoTag, d_params);
    // cudaStreamSynchronize(stream);

    ////////////////////////////////////////////
    // assign clusters
    // 1 point per seeds
    ////////////////////////////////////////////
    const dim3 gridSize_nseeds(1, nEvtsForClustering, 1);
    const dim3 blockSize_nseeds(1024, 1, 1);
    clue_compute_clusterXYE<<<gridSize_nseeds, blockSize_nseeds, 0, stream>>>(d_seeds, d_followers, d_points, *d_clusters);

}
