
// make -j15 && mpirun --mca pml ucx -x UCX_TLS=sm,cuda_copy,cuda_ipc,tcp -x UCX_NET_DEVICES=eno1np0,ens7f1np1 --bind-to none --hostfile ../config/mpi_hosts ./main 0

#include <iostream>
#include <string>
#include <sys/stat.h>
#include <vector>
//#include "TFile.h"
#include "nlohmann/json.hpp"



#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include "mpi.h"


#include <iostream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <cstdio>
#include <vector>

//#include "E14L2Mapper.h"
#include "common.h"
#include "timer.h"
#include "Capture.h"
#include "writeToDisk.h"


#ifdef IS_SPILLNODE
#include "Process.h"
#if D_ENABLE_CPU_DEBUG
#include "selector.h"
#endif
#endif

#include "toml.hpp"

#if D_USE_ROOT
#include "TROOT.h"
#endif




/**
 *  Naming conventions in KOTO's L3 Software:
 * 
 *  Iterators start with "i_", e.g. i_fadc
 *  Short loops, or loops in which the iterator is not explicitely used might use i, j, k... as an iterator, e.g. for (int i = 0; i < 10; i++)
 *  
 *  Member variables start with "m_", e.g. m_integratedADC
 *  Function parameters start with "_", e.g. _integratedADC
 *  GPU shared memory variables start with "s_", e.g. s_integratedADC
 */


// The L3 sw might be run with 
// mpirun  --bind-to none -n 2 ./main
// mpirun  --bind-to none --hostfile ../config/mpi_hosts ./main





// it in Process.


// Global variables, common to many source files.
//Timer t = Timer();
L3log l3log = L3log();
//ThreadPool * tp;

//BS::thread_pool *tp2;


//ThreadPool * tp = new ThreadPool(10);

int rank, size;



bool FileExists(const std::string& name)
{
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}



template<typename T>
void Parse(T &result, std::optional<T> source, std::string tag)
{
    if (source.has_value())
    {
        result = source.value();
    }
    else
    {
        printf("Error: %s not found in config file. Exiting.\n", tag.c_str());
        exit(1);
    }
}


int main(int argc, char **argv)
{

    // Parse config file
    toml::table tbl;
    try
    {
        tbl = toml::parse_file("/home/koto/git-repos/l3-sw/config/config.toml");
            //std::cout << tbl << "\n";
    }
    catch (const toml::parse_error& err)
    {
        std::cerr << "Parsing failed:\n" << err << "\n";
        return 1;
    }

    Config conf;
    
    // Get run number from the command line
    if (argc == 3){
        conf.run_number = atoi(argv[1]);
        conf.run_type = argv[2];
    }
    else{
        printf("usage: mpirun --mca pml ucx -x UCX_TLS=all -x UCX_NET_DEVICES=eno2np1,ens7f0np0 --bind-to none --hostfile ../config/mpi_hosts ./main <run_number> <run_type>\n");
        exit(0);
    }

    std::optional<bool> do_mpi = tbl["general"]["do_mpi"].value<bool>();
    Parse(conf.do_mpi, do_mpi, "do_mpi");
    std::optional<bool> do_physics = tbl["general"]["do_physics"].value<bool>();
    Parse(conf.do_physics, do_physics, "do_physics");
    std::optional<bool> do_decompress = tbl["general"]["do_decompress"].value<bool>();
    Parse(conf.do_decompress, do_decompress, "do_decompress");
    std::optional<bool> do_profiling = tbl["debugging"]["do_profiling"].value<bool>();
    Parse(conf.do_profiling, do_profiling, "do_profiling");
    std::optional<bool> do_single_thread = tbl["debugging"]["do_single_thread"].value<bool>();
    Parse(conf.do_single_thread, do_single_thread, "do_single_thread");
    std::optional<bool> exit_after_first_spill = tbl["general"]["exit_after_first_spill"].value<bool>();
    Parse(conf.exit_after_first_spill, exit_after_first_spill, "exit_after_first_spill");
    std::optional<bool> debug_40G = tbl["debugging"]["debug_40G"].value<bool>();
    Parse(conf.debug_40G, debug_40G, "debug_40G");
    std::optional<bool> debug_bin2raw = tbl["debugging"]["debug_bin2raw"].value<bool>();
    Parse(conf.debug_bin2raw, debug_bin2raw, "debug_bin2raw");
    std::optional<u_int> n_pcap_threads = tbl["performance"]["n_pcap_threads"].value<int>();
    Parse(conf.n_pcap_threads, n_pcap_threads, "n_pcap_threads");
    std::optional<int> adc_header_size = tbl["ofc2_packet_format"]["adc_header_size"].value<int>();
    Parse(conf.adc_header_size, adc_header_size, "adc_header_size");
    std::optional<int> adc_footer_size = tbl["ofc2_packet_format"]["adc_footer_size"].value<int>();
    Parse(conf.adc_footer_size, adc_footer_size, "adc_footer_size");
    std::optional<int> crate_footer_size = tbl["ofc2_packet_format"]["crate_footer_size"].value<int>();
    Parse(conf.crate_footer_size, crate_footer_size, "crate_footer_size");
    std::optional<int> verbose_level = tbl["general"]["verbose_level"].value<int>();
    Parse(conf.verbose_level, verbose_level, "verbose_level");

    std::optional<std::string> csi_calib_file = tbl["detectors"]["csi_calib_file"].value<std::string>();
    Parse(conf.csi_calib_file, csi_calib_file, "csi_calib_file");
    std::optional<std::string> csi_ch_pos_file = tbl["detectors"]["csi_ch_pos_file"].value<std::string>();
    Parse(conf.csi_ch_pos_file, csi_ch_pos_file, "csi_ch_pos_file");
    std::optional<std::string> cv_calib_file = tbl["detectors"]["cv_calib_file"].value<std::string>();
    Parse(conf.cv_calib_file, cv_calib_file, "cv_calib_file");
    std::optional<int> csi_n_channels = tbl["detectors"]["csi_n_channels"].value<int>();
    Parse(conf.csi_n_channels, csi_n_channels, "csi_n_channels");
    std::optional<int> cv_n_channels = tbl["detectors"]["cv_n_channels"].value<int>();
    Parse(conf.cv_n_channels, cv_n_channels, "cv_n_channels");

    std::optional<u_int> time_window_peak_i_sample_min = tbl["reconstruction"]["time_window_peak_i_sample_min"].value<u_int>();
    std::optional<u_int> time_window_peak_i_sample_max = tbl["reconstruction"]["time_window_peak_i_sample_max"].value<u_int>();

    if (time_window_peak_i_sample_min > time_window_peak_i_sample_max || time_window_peak_i_sample_max > 64)
    {
        l3log.log(L3_QUIET, L3_ERROR, "Time window not defined correctly in config file. Exiting.\n");
        l3log.log(L3_QUIET, L3_ERROR, "Time window in config (min, max): (%d, %d)\n", time_window_peak_i_sample_min, time_window_peak_i_sample_max);
        exit(1);
    }
    Parse(conf.wfm_time_window[0], time_window_peak_i_sample_min, "time_window_peak_i_sample_min");
    Parse(conf.wfm_time_window[1], time_window_peak_i_sample_max, "time_window_peak_i_sample_max");


    // Disk nodes
    std::optional<int> n_ranks_per_disk_node = tbl["disk_nodes"]["n_ranks_per_disk_node"].value<int>();
    Parse(conf.n_ranks_per_disk_node, n_ranks_per_disk_node, "n_ranks_per_disk_node");
    std::optional<int> disk_node_nbuffers_per_rank = tbl["disk_nodes"]["disk_node_nbuffers_per_rank"].value<int>();
    Parse(conf.disk_node_nbuffers_per_rank, disk_node_nbuffers_per_rank, "disk_node_nbuffers_per_rank");
    std::optional<uint64_t> disk_node_buffers_nbytes = tbl["disk_nodes"]["disk_node_nbytes_per_buffer"].value<uint64_t>();
    Parse(conf.disk_node_nbytes_per_buffer, disk_node_buffers_nbytes, "disk_node_nbytes_per_buffer");

    // Cluster variables
    std::optional<int> dc, deltao, deltac, rhoc;
    dc = tbl["reconstruction"]["Klpi0ee"]["cluster_dc"].value<int>();
    Parse(conf.clus_cfg_pi0ee.dc, dc, "cluster_dca");
    deltao = tbl["reconstruction"]["Klpi0ee"]["cluster_deltao"].value<int>();
    Parse(conf.clus_cfg_pi0ee.deltao, deltao, "cluster_deltao");
    deltac = tbl["reconstruction"]["Klpi0ee"]["cluster_deltac"].value<int>();
    Parse(conf.clus_cfg_pi0ee.deltac, deltac, "cluster_deltac");
    rhoc = tbl["reconstruction"]["Klpi0ee"]["cluster_rhoc"].value<int>();
    Parse(conf.clus_cfg_pi0ee.rhoc, rhoc, "cluster_rhoc");

    dc = tbl["reconstruction"]["Kl5g"]["cluster_dc"].value<int>();
    Parse(conf.clus_cfg_5g.dc, dc, "cluster_dcs");
    deltao = tbl["reconstruction"]["Kl5g"]["cluster_deltao"].value<int>();
    Parse(conf.clus_cfg_5g.deltao, deltao, "cluster_deltao");
    deltac = tbl["reconstruction"]["Kl5g"]["cluster_deltac"].value<int>();
    Parse(conf.clus_cfg_5g.deltac, deltac, "cluster_deltac");
    rhoc = tbl["reconstruction"]["Kl5g"]["cluster_rhoc"].value<int>();
    Parse(conf.clus_cfg_5g.rhoc, rhoc, "cluster_rhoc");

    dc = tbl["reconstruction"]["Kp"]["cluster_dc"].value<int>();
    Parse(conf.clus_cfg_kp.dc, dc, "cluster_dcd");
    deltao = tbl["reconstruction"]["Kp"]["cluster_deltao"].value<int>();
    Parse(conf.clus_cfg_kp.deltao, deltao, "cluster_deltao");
    deltac = tbl["reconstruction"]["Kp"]["cluster_deltac"].value<int>();
    Parse(conf.clus_cfg_kp.deltac, deltac, "cluster_deltac");
    rhoc = tbl["reconstruction"]["Kp"]["cluster_rhoc"].value<int>();
    Parse(conf.clus_cfg_kp.rhoc, rhoc, "cluster_rhoc");




    // Trigger ADC related variables
    std::optional<int> trigger_adc_ifadc = tbl["reconstruction"]["trigger_adc_ifadc"].value<int>();
    Parse(conf.trigger_adc_ifadc, trigger_adc_ifadc, "trigger_adc_ifadc");
    std::optional<int> scaled_trig_bit_ch = tbl["reconstruction"]["scaled_trig_bit_ch"].value<int>();
    Parse(conf.scaled_trig_bit_ch, scaled_trig_bit_ch, "scaled_trig_bit_ch");
    std::optional<int> scaled_trig_bit_sample = tbl["reconstruction"]["scaled_trig_bit_sample"].value<int>();
    Parse(conf.scaled_trig_bit_sample, scaled_trig_bit_sample, "scaled_trig_bit_sample");
    std::optional<int> n_cdt_clus_ch = tbl["reconstruction"]["n_cdt_clus_ch"].value<int>();
    Parse(conf.n_cdt_clus_ch, n_cdt_clus_ch, "n_cdt_clus_ch");
    std::optional<int> n_cdt_clus_sample = tbl["reconstruction"]["n_cdt_clus_sample"].value<int>();
    Parse(conf.n_cdt_clus_sample, n_cdt_clus_sample, "n_cdt_clus_sample");


    // Parse scaled_trig_bit_definitions
    std::optional<int> min_bias_trigger_bit = tbl["trigger_bits"]["min_bias_trigger_bit"].value<int>();
    Parse(conf.min_bias_trigger_bit, min_bias_trigger_bit, "min_bias_trigger_bit");

    //std::optional<std::string> run_type = tbl["trigger_bits"]["run_type"].value<std::string>();
    //Parse(conf.run_type, run_type, "run_type");

    for (unsigned int i = 0; i < conf.scaled_trig_bit_definitions.size(); i++)
    {
        // check if run_type is defined in the conf
        if (tbl.contains(conf.run_type) == false)
        {
            l3log.log(L3_QUIET, L3_ERROR, "run_type '%s' not found in config file. Using \"Default\"!.\n", conf.run_type.c_str());

            // sleep some time so that the user can see the error message
            sleep(2);
            conf.run_type = "Default";
        }

        std::string option_parsed;
        std::optional<std::string> option = tbl["trigger_bits"][conf.run_type][std::to_string(i)]["option"].value<std::string>();
        Parse(option_parsed, option, "selected conf[\"trigger_bits\"][run_type]");

        // No selection
        if (option_parsed == "compression"){
            conf.scaled_trig_bit_definitions[i] = REC_PASS;
        }
        else if (option_parsed == "pedestal_suppression"){
            conf.scaled_trig_bit_definitions[i] = REC_PEDSUP;
        }

        // Selection
        else if (option_parsed == "selection_5g"){
            conf.scaled_trig_bit_definitions[i] = REC_RECO_5G;
        }
        else if (option_parsed == "selection_kp"){
            conf.scaled_trig_bit_definitions[i] = REC_RECO_KP;
        }
        else if (option_parsed == "selection_pi0ee"){
            conf.scaled_trig_bit_definitions[i] = REC_RECO_PI0EE;
        }
        else{
            l3log.log(L3_QUIET, L3_ERROR, "Unknown command '%s' set for trigger_bits[%d]. Exiting.\n", option.value().c_str(), i);
            return 0;
        }
    }


    // Parse csi_chids_to_mask_reco
    toml::array* csi_chids_to_mask_reco_conf;
    csi_chids_to_mask_reco_conf = tbl["detectors"]["csi_chids_to_mask_reco"].as_array();
    conf.csi_chids_to_mask_reco.resize(csi_chids_to_mask_reco_conf->size());
    for (unsigned int i = 0; i < csi_chids_to_mask_reco_conf->size(); i++)
    {
        Parse(conf.csi_chids_to_mask_reco[i], csi_chids_to_mask_reco_conf->get(i)->value<int>(), "csi_chids_to_mask_reco");
    }
        // Parse csi_chids_to_mask_pedsup_file
    std::optional<std::string> csi_chids_to_mask_pedsup_file = tbl["detectors"]["csi_chids_to_mask_pedsup_file"].value<std::string>();
    Parse(conf.csi_chids_to_mask_pedsup_file, csi_chids_to_mask_pedsup_file, "csi_chids_to_mask_pedsup_file");



    conf.mem_per_thread_buff = (PKT_SIZE) * N_PKTS_PER_THREAD;
    conf.max_compressed_evt_size = 100 + 288*2064;


    //printf("doMPI: %d, debug40G: %d\n", conf.do_mpi, conf.debug_40G);

    int provided;
    if (conf.do_mpi){
        // This initialization is needed, because we are going to be making
        // parallel MPI calls from multiple threads.
        MPI_Init_thread( &argc, &argv, MPI_THREAD_MULTIPLE, &provided );
        //printf("MPI_Init_thread: provided = %d\n", provided);
        MPI_Comm_size( MPI_COMM_WORLD, &size );
        MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    }

    //printf("size: %d, rank: %d\n", size, rank);    


    conf.node_type = conf.UNKNOWN_NODE;


    // Get hostname
    char hostname[HOST_NAME_MAX];
    gethostname(hostname, HOST_NAME_MAX);

    // Init L3Logger
    l3log.set_hostname(hostname);
    l3log.set_verbose_level(conf.verbose_level);


    std::optional<int> n_spillnodes_conf = tbl["spill_nodes"]["n_spill_nodes"].value<int>();
    int n_spillnodes;
    Parse(n_spillnodes, n_spillnodes_conf, "n_spill_nodes");
    
    std::optional<int> n_disknodes_conf = tbl["disk_nodes"]["n_disk_nodes"].value<int>();
    int n_disknodes;
    Parse(n_disknodes, n_disknodes_conf, "n_disk_nodes");

    if (conf.do_mpi && (n_spillnodes + n_disknodes*conf.n_ranks_per_disk_node != size))
    {
        l3log.log(L3_QUIET, L3_ERROR, "n_spillnodes = %d; n_disknodes = %d; n_ranks_per_disk_node = %d (conf file); but a total of %d ranks registered (hosts file). Fix either the conf file or the hosts file.\n", n_spillnodes, n_disknodes, conf.n_ranks_per_disk_node, size);
        if (size == 1){
            printf("Or most likely, you either forgot to use 'mpirun' to launch the sw, or to disable mpi in the cfg file...\n");
        }
        return -1;
    }
    

    for (int i_spillnode = 0; i_spillnode < n_spillnodes; i_spillnode++)
    {
        //std::cout << tbl["spill_nodes"]["0"] << std::endl;
        std::optional<std::string> hostname_conf = tbl["spill_nodes"][std::to_string(i_spillnode)]["hostname"].value<std::string>(); 
        std::string hostname_str;
        Parse(hostname_str, hostname_conf, "hostname");



        if (hostname_str == hostname)
        //if (rank == 0)
        {
            conf.node_type = conf.SPILL_NODE;
            conf.hostname = hostname_str;

            // Get 40G interface names
            std::optional<std::string> spillnode_ifname40G = tbl["spill_nodes"][std::to_string(i_spillnode)]["ifname_40G"].value<std::string>();
            Parse(conf.ifname_40G, spillnode_ifname40G, "ifname_40G");
            printf("%s is a spill node. 40G interface: [%s]\n", hostname, conf.ifname_40G.c_str());
        
            // Get first capturing cpu id
            std::optional<int> first_capture_cpu_id = tbl["spill_nodes"][std::to_string(i_spillnode)]["first_capture_cpu_id"].value<int>();
            Parse(conf.first_capture_cpu_id, first_capture_cpu_id, "first_capture_cpu_id");

            // Get memory allocator cpu id
            std::optional<int> mem_alloc_cpu_id = tbl["spill_nodes"][std::to_string(i_spillnode)]["mem_alloc_cpu_id"].value<int>();
            Parse(conf.mem_alloc_cpu_id, mem_alloc_cpu_id, "mem_alloc_cpu_id");
        }

        //conf.spillnode_hostnames.push_back(hostname_str);
    }

    if (conf.node_type == conf.UNKNOWN_NODE)
    {
        for (int i_disknode = 0; i_disknode < n_disknodes; i_disknode++)
        {
            std::optional<std::string> hostname_conf = tbl["disk_nodes"][std::to_string(i_disknode)]["hostname"].value<std::string>();
            std::string hostname_str;
            Parse(hostname_str, hostname_conf, "hostname");

            if (hostname_str == hostname)
            //if (rank == 1)
            {
                conf.node_type = conf.DISK_NODE;
                conf.hostname = hostname_str;

                std::string out_base_dir_cfg_file;
                std::optional<std::string> _out_base_dir_cfg_file = tbl["disk_nodes"][std::to_string(i_disknode)]["out_base_dir_cfg_file"].value<std::string>();
                Parse(out_base_dir_cfg_file, _out_base_dir_cfg_file, "out_base_dir_cfg_file");

                // Get base path from file
                std::ifstream base_dir_file(out_base_dir_cfg_file);
                std::string base_dir;
                if (base_dir_file.is_open())
                {
                    std::getline(base_dir_file, base_dir);
                    base_dir_file.close();
                }
                else
                {
                    l3log.log(L3_QUIET, L3_ERROR, "Could not open file %s. Exiting.\n", out_base_dir_cfg_file.c_str());
                    return -1;
                }

                l3log.log(L3_QUIET, L3_INFO, "This run will be written to %s\n", base_dir.c_str());

                // get out dirs
                std::optional<std::string> data_out_dir = tbl["disk_nodes"][std::to_string(i_disknode)]["data_out_dir"].value<std::string>();
                Parse(conf.data_out_dir, data_out_dir, "data_out_dir");
                std::optional<std::string> stats_out_dir = tbl["disk_nodes"][std::to_string(i_disknode)]["stats_out_dir"].value<std::string>();
                Parse(conf.stats_out_dir, stats_out_dir, "stats_out_dir");
                std::optional<std::string> monitor_out_dir = tbl["disk_nodes"][std::to_string(i_disknode)]["monitor_out_dir"].value<std::string>();
                Parse(conf.monitor_out_dir, monitor_out_dir, "monitor_out_dir");

                conf.data_out_dir = base_dir + conf.data_out_dir;
                conf.stats_out_dir = base_dir + conf.stats_out_dir;
                conf.monitor_out_dir = base_dir + conf.monitor_out_dir;
            }
        }
    }
    if (conf.node_type == conf.UNKNOWN_NODE)
    {
        l3log.log(L3_QUIET, L3_ERROR, "Host %s is not defined in the config file. Exiting.\n", hostname);
        return -1;
    }

    if (conf.do_mpi)
    {
        if (conf.node_type == conf.SPILL_NODE){
            l3log.log(L3_QUIET, L3_INFO, "Hi! I am rank %d out of %d spill nodes\n", rank, n_spillnodes);
        }
        else if (conf.node_type == conf.DISK_NODE){
            l3log.log(L3_QUIET, L3_INFO, "Hi! I am rank %d out of %d disk nodes\n", rank, n_disknodes);
        }

        if (n_spillnodes + n_disknodes*conf.n_ranks_per_disk_node != size)
        {
            printf("ERROR: n_spillnodes = %d; n_disknodes = %d (conf file); but only %d ranks registered (hosts file). Fix either the conf file or the hosts file.\n", n_spillnodes, n_disknodes, size);
            return -1;
        }
        printf("INFO: Running on %d spill nodes (ranks 0-%d) and %d disk nodes (ranks %d-%d)\n", n_spillnodes, n_spillnodes-1, n_disknodes, size-n_disknodes, size-1);
    }
    




    l3log.log(L3_VERBOSE, L3_INFO, "This is an info message\n");
    l3log.log(L3_VERBOSE, L3_WARN, "This is a warning message\n");
    l3log.log(L3_VERBOSE, L3_ERROR, "This is an error message\n");
    l3log.log(L3_VERBOSE, L3_DEBUG, "This is a debug message\n");

    if (conf.do_mpi)
    {
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0)   
        {
            l3log.print_banner();
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }

    //if (rank == 1) {
    if (conf.node_type == conf.DISK_NODE) {

        if (!conf.do_mpi){
            l3log.log(L3_QUIET, L3_ERROR, "I am pretty sure you forgot either to enable mpi in the config, or to share the modified config with all the nodes.\n");
            throw std::runtime_error("Attempting to initialize a disk node, but MPI is not enabled.\n");
        }

        l3log.log(L3_QUIET, L3_INFO, "Starting writeToDisk...\n");
        writeToDisk * m_writeToDisk = new writeToDisk(conf);
        //m_writeToDisk->SetOutputDataDir(conf.data_out_dir);
        //m_writeToDisk->SetOutputStatsDir(conf.stats_out_dir);
        l3log.log(L3_QUIET, L3_INFO, "Started writeToDisk!\n");

        if (conf.do_mpi){
            MPI_Barrier(MPI_COMM_WORLD);
        }
        l3log.log(L3_QUIET, L3_INFO, "Entering m_writeToDisk->Run()\n");
        m_writeToDisk->Run();
        printf("Finished Writing To disk. This should happen only when testing!\n");
    }



#ifdef IS_SPILLNODE

#if D_USE_ROOT
    ROOT::EnableThreadSafety();
#endif


    l3log.log(L3_QUIET, L3_INFO, "Starting Capturer...\n");
    Capturer * capturer = new Capturer(&conf);
    l3log.log(L3_QUIET, L3_INFO, "Started Capturer!\n");

    if (conf.do_mpi){
        MPI_Barrier(MPI_COMM_WORLD);
    }

    l3log.log(L3_QUIET, L3_INFO, "Initializing Processer...\n");
    Process * m_Process = new Process(conf);

    m_Process->m_diskNodeRanks.resize(n_disknodes*conf.n_ranks_per_disk_node);
    for (int i = 0; i < n_disknodes*conf.n_ranks_per_disk_node; i++)
    {
        m_Process->m_diskNodeRanks[i] = size-n_disknodes*conf.n_ranks_per_disk_node+i;
    }



    {
        // Launch Capturer thread
        std::thread capturerThread(&Capturer::Run, capturer);

        if (conf.debug_40G){
            capturer->Debug();
            return 0;
        }

        m_Process->Run( capturer );
        //t.GenerateResultFile("timerResults.txt");


        capturerThread.join();
    }
#endif


    if (conf.do_mpi){
	    MPI_Finalize();    
    }


    return 0;
}