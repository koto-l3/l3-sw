

#include <mpi.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <unistd.h>
#include <sys/stat.h>

#include "writeToDisk.h"
#include "common.h"



int writeToDisk::GetSpillStatsIndex(int spillNo)
{

    int index = STATS_INDEX_NOT_FOUND;
    for (int i = 0; i < m_nSpillBuffers; i++)
    {
        if (m_stats_spillNo[i] == spillNo)
            index = i;
    }
    if (index == STATS_INDEX_NOT_FOUND)
    {
        for (int i = 0; i < m_nSpillBuffers; i++)
        {
            if (m_stats_spillNo[i] == STATS_INDEX_FREE)
            {
                m_stats_spillNo[i] = spillNo;
                index = i;
                break;
            }
        }
    }
    if (index < 0)
    {
        // This should never happen
        l3log.log(L3_QUIET, L3_ERROR, "No free index found for spill stats!\n");
        return m_nSpillBuffers-1;
    }
    return index;
}


writeToDisk::writeToDisk(const Config &config)//, int runNo, std::string data_out_dir, std::string stats_out_dir, std::string monitor_out_dir) : m_config.stats_out_dir(stats_out_dir), m_config.monitor_out_dir(monitor_out_dir)
{
    m_config = config;

    l3log.log(L3_QUIET, L3_INFO, "Creating %d buffers of size %llu bytes each\n", m_config.disk_node_nbuffers_per_rank, m_config.disk_node_nbytes_per_buffer);
    m_spill_buffer_size = m_config.disk_node_nbytes_per_buffer; // 1.5e9*15/2**30 = 20.95 GiB
    m_nSpillBuffers = m_config.disk_node_nbuffers_per_rank;
    m_spill_buffer.resize(m_nSpillBuffers, std::vector<uint8_t>(m_spill_buffer_size));
    m_buffer_busy_status.resize(m_nSpillBuffers, BUFFER_FREE);
    m_buffer_spillNo.resize(m_nSpillBuffers, 0);
    m_buffer_nbytes_written.resize(m_nSpillBuffers, 0);
    m_buffer_timestamp.resize(m_nSpillBuffers, 0);


    m_spill_stats.resize(m_nSpillBuffers);
    m_stats_spillNo.resize(m_nSpillBuffers, STATS_INDEX_FREE);

    m_RunNo = config.run_number;

    // check if output dirs exist
    struct stat dir_metadata;
    if (stat(config.data_out_dir.c_str(), &dir_metadata) != 0)
    {
        l3log.log(L3_QUIET, L3_ERROR, "Cannot access output data directory %s\n", config.data_out_dir.c_str());
        throw std::runtime_error("Cannot access output data directory");
        return;
    }
    if (stat(m_config.stats_out_dir.c_str(), &dir_metadata) != 0)
    {
        l3log.log(L3_QUIET, L3_ERROR, "Cannot access output stats directory %s\n", m_config.stats_out_dir.c_str());
        throw std::runtime_error("Cannot access output stats directory");
        return;
    }
    if (stat(m_config.monitor_out_dir.c_str(), &dir_metadata) != 0)
    {
        l3log.log(L3_QUIET, L3_ERROR, "Cannot access output monitor directory %s\n", m_config.monitor_out_dir.c_str());
        throw std::runtime_error("Cannot access output monitor directory");
        return;
    }

    // create a directory for this run, if it doesn't exist already
    std::string run_dir = config.data_out_dir + (std::ostringstream() << std::setfill('0') << std::setw(5) << std::to_string(m_RunNo)).str();
    if (stat(run_dir.c_str(), &dir_metadata) != 0)
    {
        l3log.log(L3_QUIET, L3_INFO, "Creating output directory for run %d\n", m_RunNo);
        mkdir(run_dir.c_str(), 0777);
    }

    m_config.data_out_dir = run_dir + "/";
}

writeToDisk::~writeToDisk()
{
}

void writeToDisk::WriteSpillStats(int dest_i_buffer, int spillNo)
{

    bool gogogo = false;
    // Loop through all the buffers to make sure there is no buffer busy with
    // this spill.
    while (!gogogo)
    {
        gogogo = true;
        for (int i_buffer = 0; i_buffer < m_nSpillBuffers; i_buffer++)
        {
            if (m_buffer_spillNo[i_buffer] == spillNo)
            {
                l3log.log(L3_VERBOSE, L3_DEBUG, "Buffer %d (status %s) is still busy with spill %d. Waiting for it to finish...\n", i_buffer, GetVerboseBufferStatus(m_buffer_busy_status[i_buffer]).c_str(), spillNo);
                gogogo = false;
                usleep(1000);
            }
        }
    }

    int thisSpillStatsIndex = GetSpillStatsIndex(spillNo);

    std::string filename = m_config.stats_out_dir + "run_" + (std::ostringstream() << std::setfill('0') << std::setw(5) << std::to_string(m_RunNo) << "_spill_" << std::setfill('0') << std::setw(4) << std::to_string(spillNo) << "_stats" << ".txt").str();

    l3log.log(L3_VERBOSE, L3_DEBUG, "rank %d; spill %d\n", rank, spillNo);
    // Check if file already exists
    struct stat buffer;
    if (stat(filename.c_str(), &buffer) == 0)
    {
        l3log.log(L3_QUIET, L3_ERROR, "Stats file %s already exists! NOT writing new stats to disk.\n", filename.c_str());
        m_spill_stats.at(thisSpillStatsIndex) = {};
        m_stats_spillNo.at(thisSpillStatsIndex) = STATS_INDEX_FREE;
        return;
    }

    // open file
    std::ofstream outfile(filename.c_str(), std::ios::out);

    // write spill stats, tabulated
    outfile << "# runNo SpillNo nProcessedEvents UnixTimestamp (ns)" << std::endl;
    outfile << "# REC_TAG_0: nAccEvts, nRejEvts_total, nRejEvts_MinXY, nRejEvts_MaxR, nRejEvts_TotalE, nRejEvts_COE" << std::endl;
    outfile << "# REC_TAG_1: nAccEvts, nRejEvts_total, nRejEvts_MinXY, nRejEvts_MaxR, nRejEvts_TotalE, nRejEvts_COE" << std::endl;
    outfile << "# REC_TAG_2: nAccEvts, nRejEvts_total, nRejEvts_MinXY, nRejEvts_MaxR, nRejEvts_TotalE, nRejEvts_COE" << std::endl;
    outfile << "# ..." << std::endl;
    outfile << "# REC_TAG_9: nAccEvts, nRejEvts_total, nRejEvts_MinXY, nRejEvts_MaxR, nRejEvts_TotalE, nRejEvts_COE" << std::endl;

    outfile << m_RunNo << "\t" << spillNo << "\t" << 
            m_spill_stats.at(thisSpillStatsIndex).nProcessedEvents << "\t" <<
            m_buffer_timestamp.at(thisSpillStatsIndex) << "\t" <<
            0 << "\t" <<
            0 << std::endl;

    for (int i = 0; i < 10; i++)
    {
        outfile << m_spill_stats.at(thisSpillStatsIndex).nAcceptedEvents[i] << "\t" <<
                m_spill_stats.at(thisSpillStatsIndex).nRejectedEvents_total[i] << "\t" <<
                m_spill_stats.at(thisSpillStatsIndex).nRejectedEvents_MinXY[i] << "\t" <<
                m_spill_stats.at(thisSpillStatsIndex).nRejectedEvents_MaxR[i] << "\t" <<
                m_spill_stats.at(thisSpillStatsIndex).nRejectedEvents_TotalE[i] << "\t" <<
                m_spill_stats.at(thisSpillStatsIndex).nRejectedEvents_COE[i] << std::endl;
    }


    outfile.close();
    l3log.log(L3_QUIET, L3_INFO, "Wrote stats file for spill %d\n", spillNo);

    m_spill_stats.at(thisSpillStatsIndex) = {};
    m_stats_spillNo.at(thisSpillStatsIndex) = STATS_INDEX_FREE;
}

void writeToDisk::WriteDataAndStats(int dest_i_buffer, int spillNo, int fileID, uint64_t nBytes)
{
    WriteBinaryFile(dest_i_buffer, spillNo, fileID, nBytes);
    WriteSpillStats(dest_i_buffer, spillNo);
}



void writeToDisk::WriteBinaryFile(int dest_i_buffer, int spillNo, int fileID, uint64_t nBytes)
{
    int thisSpillStatsIndex = GetSpillStatsIndex(spillNo);
 

    // open binary file
    std::string filename = m_config.data_out_dir + "run_" + (std::ostringstream() << std::setfill('0') << std::setw(5) << std::to_string(m_RunNo) << "_spill_" << std::setfill('0') << std::setw(4) << std::to_string(spillNo) << "_file_" << std::setfill('0') << std::setw(3) << std::to_string(fileID) << ".bin").str();
        

    if (m_buffer_busy_status.at(dest_i_buffer) == WRT_SPILL_RECVD)
    {

    }
    
    
    std::string filename_tmp = filename + ".tmp";

    l3log.log(L3_QUIET, L3_INFO, "Writing %llu bytes to file %s from buffer %d\n", nBytes, filename.c_str(), dest_i_buffer);
    
    // file header
    std::array<uint8_t, 8> header = {0, 1, 2, 3, 0xa, 0xb, 0xc, 0xd};


    std::ofstream outfile(filename_tmp.c_str(), std::ios::out | std::ios::binary);

    // write file header
    outfile.write((char*)&header, sizeof(header));

    // write data
    outfile.write((char*)&m_spill_buffer.at(dest_i_buffer).at(0), nBytes);
    outfile.close();

    l3log.log(L3_QUIET, L3_INFO, "Wrote %llu bytes to file %s\n", nBytes, filename_tmp.c_str());
    // remove the temporary file extension
    rename(filename_tmp.c_str(), filename.c_str());




    std::string monitorFname;
    std::ofstream monitorOutfile;

    if (fileID == 0)
    {
        // only one monitor file per spill
        monitorFname = m_config.monitor_out_dir + "monitor_run_" + (std::ostringstream() << std::setfill('0') << std::setw(5) << std::to_string(m_RunNo) << "_spill_" << std::setfill('0') << std::setw(4) << std::to_string(spillNo) << "_file_" << std::setfill('0') << std::setw(3) << std::to_string(fileID) << ".bin").str();
        monitorOutfile.open(monitorFname.c_str(), std::ios::out | std::ios::binary);
        monitorOutfile.write((char*)&header, sizeof(header));
    }

    uint8_t *thisEvent = &m_spill_buffer.at(dest_i_buffer).at(0);

    uint64_t nBytesRead = 0;
    int nMinBiasEvtsWritten = 0;
    int nRandomEvtsWritten = 0;
    int evt_count = 0;
    while (nBytesRead < nBytes)
    {
        // get event size from header
        uint32_t eventSize = (thisEvent[0] << 24) | (thisEvent[1] << 16) | (thisEvent[2] << 8) | thisEvent[3];

        if (eventSize > MAX_COMP_EVT_SIZE || eventSize == 0)
        {
            l3log.log(L3_QUIET, L3_ERROR, "Received corrupted event with compressed size %d!.\n", eventSize);
            l3log.log(L3_QUIET, L3_ERROR, "Ignoring remaining events in the received buffer.\n");
            break;
        }

        uint16_t recoTag = thisEvent[L3_TAG_O] << 8 | thisEvent[L3_TAG_O + 1];
        uint8_t  recoTagLSBs = recoTag & 0xFF;
        uint16_t recoTagLuckyMasked = recoTag & (~REC_LUCKY_BIT);

        if (recoTagLuckyMasked < REJ_THRESHOLD)
        {
            // This event has been accepted
            if (recoTagLuckyMasked == REC_RECO_5G)
            {
                m_spill_stats.at(thisSpillStatsIndex).nAcceptedEvents[REC_RECO_5G]++;
            }
            else if (recoTagLuckyMasked == REC_RECO_KP)
            {
                m_spill_stats.at(thisSpillStatsIndex).nAcceptedEvents[REC_RECO_KP]++;
            }
            else if (recoTagLuckyMasked == REC_RECO_PI0EE)
            {
                m_spill_stats.at(thisSpillStatsIndex).nAcceptedEvents[REC_RECO_PI0EE]++;
            }
        }
        else if (recoTagLuckyMasked > REJ_THRESHOLD)
        {
            // This event has been rejected
            m_spill_stats.at(thisSpillStatsIndex).nRejectedEvents_total[recoTagLSBs]++;
            if (recoTagLuckyMasked & REJ_MINXY_BIT)
            {
                m_spill_stats.at(thisSpillStatsIndex).nRejectedEvents_MinXY[recoTagLSBs]++;
            }
            if (recoTagLuckyMasked & REJ_MAXR_BIT)
            {
                m_spill_stats.at(thisSpillStatsIndex).nRejectedEvents_MaxR[recoTagLSBs]++;
            }
            if (recoTagLuckyMasked & REJ_TOTALE_BIT)
            {
                m_spill_stats.at(thisSpillStatsIndex).nRejectedEvents_TotalE[recoTagLSBs]++;
            }
            if (recoTagLuckyMasked & REJ_COE_BIT)
            {
                m_spill_stats.at(thisSpillStatsIndex).nRejectedEvents_COE[recoTagLSBs]++;
            }
        }

        //printf("Rejected minxy[0] %d\n", m_spill_stats.at(dest_i_buffer).nRejectedEvents_MinXY[recoTagLSBs]);
        //printf("Rejected maxr[0] %d\n", m_spill_stats.at(dest_i_buffer).nRejectedEvents_MaxR[recoTagLSBs]);
        //printf("Rejected totale[0] %d\n", m_spill_stats.at(dest_i_buffer).nRejectedEvents_TotalE[recoTagLSBs]);
        //printf("Rejected coe[0] %d\n", m_spill_stats.at(dest_i_buffer).nRejectedEvents_COE[recoTagLSBs]);

        if (fileID ==0)
        {
            // if it's a minimum bias event, write to monitor file
            uint8_t scaledTrigBit = thisEvent[TRIG_BIT_O];
            if (nMinBiasEvtsWritten < 100 && (scaledTrigBit & (1<<m_config.min_bias_trigger_bit)))
            {
                // Write all min. bias events
                monitorOutfile.write((char*)thisEvent, eventSize);
                nMinBiasEvtsWritten++;
            }
            else if ( nRandomEvtsWritten < 100 && evt_count % 500 == 0)
            {
                // Write one every 100 events
                monitorOutfile.write((char*)thisEvent, eventSize);
                nRandomEvtsWritten++;
            }
        }

        thisEvent += eventSize;
        nBytesRead += eventSize;
        evt_count ++;
    }
    m_spill_stats.at(thisSpillStatsIndex).nProcessedEvents += evt_count;
    //printf("Increasing processed events by %d to %d in buff %d (spill %d)\n", evt_count, m_spill_stats.at(dest_i_buffer).nProcessedEvents, dest_i_buffer, spillNo);

    if (fileID == 0)
    {
        monitorOutfile.close();
        l3log.log(L3_QUIET, L3_INFO, "Wrote %d min. bias events and %d randomly selected events to monitor file %s\n", nMinBiasEvtsWritten, nRandomEvtsWritten, monitorFname.c_str());
    }

    // reset buffer
    m_buffer_spillNo.at(dest_i_buffer) = -1;
    m_buffer_nbytes_written[dest_i_buffer] = 0;
    m_buffer_busy_status.at(dest_i_buffer) = BUFFER_FREE;
}




void writeToDisk::Run()
{

    l3log.log(L3_QUIET, L3_INFO, "Starting writeToDisk::Run\n");

    // keep a thread in idle until MPI packet is received
    bool alreadyGotSomePackets = false;
    while (true) 
    {
        MPI_Status status;
        int flag = 0;

        int iterationsWthoutPackets = 0;
        bool flushAllBuffers = false;

        while(1){
            MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
            //printf("iprobe flag: %d\n", flag);

            if (flag){
                //printf("got some packets\n");
                alreadyGotSomePackets = true;
                break;
            }
            else{
                iterationsWthoutPackets++;
                usleep(100); // This might be too small
                if (iterationsWthoutPackets > 60000 && alreadyGotSomePackets){
                    printf("writeToDisk: No packets received for > 6 seconds. Flushing all buffers.\n");
                    alreadyGotSomePackets = false;
                    flushAllBuffers = true;
                    break;
                }
            }
        }

        //int count2;
        //// Get size of packet
        //MPI_Get_count(&status, MPI_BYTE, &count2);
        //MPI_Recv(&m_spill_buffer[0][0], count2, MPI_BYTE, status.MPI_SOURCE, status.MPI_TAG, MPI_COMM_WORLD, &status);
        //continue;

        if (flushAllBuffers){
            for (int i_buffer = 0; i_buffer < m_nSpillBuffers; i_buffer++)
            {
                if (m_buffer_busy_status[i_buffer] == BUFFER_RECVING_SPILL)
                {

                    int fileID = m_spillNo_to_fileID[m_buffer_spillNo[i_buffer]] ++;


                    m_buffer_busy_status[i_buffer] = WRT_SPILL_RECVD;
                    //m_tpDisk->async(&writeToDisk::WriteBinaryFile, this, i_buffer, m_buffer_spillNo[i_buffer], fileID, m_buffer_nbytes_written[i_buffer]);
                    //m_tpDisk->async(&writeToDisk::WriteSpillStats, this, m_buffer_spillNo[i_buffer], m_spillNo_to_SpillStats[m_buffer_spillNo[i_buffer]]);
                
                    m_tpDisk->async(&writeToDisk::WriteDataAndStats, this, i_buffer, m_buffer_spillNo[i_buffer], fileID, m_buffer_nbytes_written[i_buffer]);

                    // Remove this spill from the maps
                    //m_spillNo_to_SpillStats.erase(m_buffer_spillNo[i_buffer]);
                    m_spillNo_to_fileID.erase(m_buffer_spillNo[i_buffer]);
                }
            }
            flushAllBuffers = false;
            continue;

            printf("writeToDisk: Flushed all buffers. Exiting! (not intended for production)\n");
            
            m_tpDisk->waitFinished();
            printf("writeToDisk: All tasks finished. Exiting!\n");
            return;
        }


        int count;
        // Get size of packet
        MPI_Get_count(&status, MPI_BYTE, &count);

        int dest_i_buffer = -1;
        int spillNo = status.MPI_TAG;

        if (spillNo > m_latestSpillNo){
            m_latestSpillNo = spillNo;
        }

        int thisSpillStatsIndex = GetSpillStatsIndex(spillNo);


        // loop throught the buffers to find the one that is free or already
        // assigned to the incoming spill, and has space for more packets
        for (int i_buffer = 0; i_buffer < m_nSpillBuffers; i_buffer++)
        {
            if (m_buffer_spillNo[i_buffer] == spillNo)
            {
                if (m_buffer_busy_status[i_buffer] == BUFFER_RECVING_SPILL)
                {
                    if ((uint64_t)count + m_buffer_nbytes_written[i_buffer] < (uint64_t)m_spill_buffer[i_buffer].size())
                    {
                        dest_i_buffer = i_buffer;
                        break;
                    }
                    else{
                        // There is no more space left in the buffer. Write it to disk.
                        l3log.log(L3_NORMAL, L3_INFO, "Buffer %d is full. Writing to disk.\n", i_buffer);
                        m_buffer_busy_status[i_buffer] = WRT_BUFFER_FULL;


                        int fileID = m_spillNo_to_fileID[m_buffer_spillNo[i_buffer]] ++;

                        m_tpDisk->async(&writeToDisk::WriteBinaryFile, this, i_buffer, spillNo, fileID, m_buffer_nbytes_written[i_buffer]);

                    }
                }
            }
        }
        if (dest_i_buffer == -1)
        {
            // Receiving either a new spill or a spill whose buffer has become full.
            // Look for a buffer that is not assigned to any spill
            for (int i_buffer = 0; i_buffer < m_nSpillBuffers; i_buffer++)
            {
                if (m_buffer_busy_status[i_buffer] == BUFFER_FREE)
                {
                    dest_i_buffer = i_buffer;
                    m_buffer_timestamp.at(thisSpillStatsIndex) = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

                    m_buffer_spillNo[dest_i_buffer] = spillNo;
                    
                    // check if this spillNo is already registered in the map
                    if (m_spillNo_to_fileID.find(m_buffer_spillNo[i_buffer]) == m_spillNo_to_fileID.end()){
                        l3log.log(L3_NORMAL, L3_INFO, "Receiving new spillNo %d.\n", m_buffer_spillNo[i_buffer]);
                        m_spillNo_to_fileID[m_buffer_spillNo[i_buffer]] = 0;

                    }

                    m_buffer_busy_status[dest_i_buffer] = BUFFER_RECVING_SPILL;
                    m_buffer_nbytes_written[dest_i_buffer] = 0;

                    break;
                }
            }
        }

        if (dest_i_buffer == -1)
        {
            l3log.log(L3_QUIET, L3_ERROR, "No free buffer found. Waiting for one to become free...\n");

            for (int i_buffer = 0; i_buffer < m_nSpillBuffers; i_buffer++)
            {
                l3log.log(L3_NORMAL, L3_WARN, "  Buffer %d: SpillNo %d, status: %s\n", i_buffer, m_buffer_spillNo[i_buffer], GetVerboseBufferStatus(m_buffer_busy_status[i_buffer]).c_str());
            }

            sleep(1);
            continue;
        }
        else
        {
            // Get the data
            MPI_Recv(&m_spill_buffer[dest_i_buffer][m_buffer_nbytes_written[dest_i_buffer]], count, MPI_BYTE, status.MPI_SOURCE, status.MPI_TAG, MPI_COMM_WORLD, &status);
            l3log.log(L3_VERBOSE, L3_DEBUG, "Received %d bytes from spill %d. Buffer %d now contains %d bytes.\n", count, spillNo, dest_i_buffer, m_buffer_nbytes_written[dest_i_buffer]);
            //l3log.log(L3_VERBOSE, L3_DEBUG, "Received buffer into address %p\n", &m_spill_buffer[dest_i_buffer][m_buffer_nbytes_written[dest_i_buffer]]);
        
            //uint16_t evtID = (m_spill_buffer[dest_i_buffer][m_buffer_nbytes_written[dest_i_buffer] + EVT_NO_O] << 8) | m_spill_buffer[dest_i_buffer][m_buffer_nbytes_written[dest_i_buffer] + EVT_NO_O+1];
            //l3log.log(L3_QUIET, L3_DEBUG, "First received event ID into buffer: %d\n", evtID);

            m_buffer_nbytes_written[dest_i_buffer] += count;
        }

        
        int nReceivingBuffers = 0;
        for (int i_buffer = 0; i_buffer < m_nSpillBuffers; i_buffer++)
        {

            if (m_buffer_spillNo[i_buffer] == spillNo && spillNo < m_latestSpillNo - (m_config.n_ranks_per_disk_node+1))
            {
                // If DN has received spills 1, 3, 5, 7, 9, ... and then it
                // receives 9-3 = 5 again, this is triggered to avoid writing
                // many small files from old spills. This could happen if the
                // writing speed to disk is for some time slowed down, and
                // eventually PCAP buffers at the SNs get full (and filled with
                // events from multiple spills). Once it recovers, all those
                // events will reach the DN and mess up things.


                // discard buffers that are less than 200 MiB.
                if (m_buffer_nbytes_written[i_buffer] < 200*1024*1024)
                {
                    l3log.log(L3_NORMAL, L3_WARN, "Discarding buffer %d from old spill %d containing %d bytes.\n", i_buffer, m_buffer_spillNo[i_buffer], m_buffer_nbytes_written[i_buffer]);

                    // check that no other buffer is busy with this spill
                    bool otherBufferBusyWithThisSpill = false;
                    for (int j_buffer = 0; j_buffer < m_nSpillBuffers; j_buffer++)
                    {
                        if (j_buffer != i_buffer && m_buffer_spillNo[j_buffer] == m_buffer_spillNo[i_buffer])
                        {
                            otherBufferBusyWithThisSpill = true;
                        }
                    }
                    if (!otherBufferBusyWithThisSpill)
                    {
                        m_stats_spillNo[GetSpillStatsIndex(spillNo)] = STATS_INDEX_FREE;
                        m_spill_stats.at(GetSpillStatsIndex(spillNo)) = {};
                        m_spillNo_to_fileID.erase(m_buffer_spillNo[i_buffer]);
                    }

                    m_buffer_nbytes_written[i_buffer] = 0;
                    m_buffer_spillNo[i_buffer] = -1;
                    m_buffer_busy_status[i_buffer] = BUFFER_FREE;
                    continue;
                }
            }

            if (m_buffer_busy_status[i_buffer] == BUFFER_RECVING_SPILL)
            {
                nReceivingBuffers ++;
            }
        }
        if (nReceivingBuffers > 6)
        {
            // loop through buffers to see if any contains a finished spill
            for (int i_buffer = 0; i_buffer < m_nSpillBuffers; i_buffer++)
            {
                if (m_buffer_busy_status[i_buffer] == BUFFER_RECVING_SPILL)
                {
                    // If the buffer corresponds to a spill that is more than 3 behind the current spillNo
                    if ((m_buffer_spillNo[i_buffer] < m_latestSpillNo - 3))
                    {
                        // This buffer contains a spill that should be completely received.
                        l3log.log(L3_NORMAL, L3_INFO, "Writing buffer %d (spill %d; current SpillNo %d).\n", i_buffer, m_buffer_spillNo[i_buffer], spillNo);
                        m_buffer_busy_status[i_buffer] = WRT_SPILL_RECVD;

                        int fileID = m_spillNo_to_fileID[m_buffer_spillNo[i_buffer]] ++;

                        // This should be the last buffer from this spill. Then, write stats too.
                        m_tpDisk->async(&writeToDisk::WriteDataAndStats, this, i_buffer, m_buffer_spillNo[i_buffer], fileID, m_buffer_nbytes_written[i_buffer]);
                        m_spillNo_to_fileID.erase(m_buffer_spillNo[i_buffer]);
                    }
                }
            }
        }
    }
}


