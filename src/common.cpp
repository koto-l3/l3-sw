


#include "common.h"


#include "stdarg.h"
#include <stdlib.h>




L3log::L3log() {}
L3log::~L3log() {}


//uint16_t GetRecoTagFromScaledTiggerBit(uint8_t scaledTrigBit)
//{
//
//    uint16_t l3tag;
//
//    if (scaledTrigBit & (1 << 0)) { return(REC_PASS); }
//    if (scaledTrigBit & (1 << 1)) { return(REC_PASS); }
//    if (scaledTrigBit & (1 << 2)) { return(REC_PEDSUP); }
//    if (scaledTrigBit & (1 << 3)) { return(REC_RECO_KP); }
//    if (scaledTrigBit & (1 << 4)) { return(REC_RECO_KP); }
//    if (scaledTrigBit & (1 << 5)) { return(REC_PEDSUP); }
//    if (scaledTrigBit & (1 << 6)) { return(REC_PEDSUP); }
//    if (scaledTrigBit & (1 << 7)) { return(REC_RECO_5G); }
//    if (scaledTrigBit & (1 << 8)) { return(REC_RECO_PI0EE); }
//    else
//        // if no bit is enabled
//        return(REC_PASS);
//
//}


void L3log::log(int verbose_level, int severity, const char * format, ...)
{
    if (verbose_level > m_verbose_level){
        return;
    }

    if (severity == L3_INFO){
        // info, green
        printf("\033[1;32mSHIT\033[0m \033[1m[\033[0m%s\033[1m]\033[0m: ", m_hostname.c_str());
    }
    else if (severity == L3_WARN)
    {
        // warning, orange
        printf("\033[1;33mDAMN\033[0m \033[1m[\033[0m%s\033[1m]\033[0m: ", m_hostname.c_str());
    }
    else if (severity == L3_ERROR)
    {
        // error, red
        printf("\033[1;31mFUCK\033[0m \033[1m[\033[0m%s\033[1m]\033[0m: ", m_hostname.c_str());
    }
    else if (severity == L3_DEBUG)
    {
        // debug, blue
        printf("\033[1;34mIDFC\033[0m \033[1m[\033[0m%s\033[1m]\033[0m: ", m_hostname.c_str());
    }

    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
}


void L3log::print_banner()
{
    printf(
        "\n"\
        "+=============================================================================+\n"
        "+.................................kotokotokot.................................+\n"
        "+................................kotokotokotok................................+\n"
        "+................................kotokotokotok................................+\n"
        "+................................kotokotokotok................................+\n"
        "+...............................kotokotokotoko................................+\n"
        "+...............................kotokotokotokot...............................+\n"
        "+..............................kotokotokotokoto...............................+\n"
        "+.............................kotokotokotokotoko..............................+\n"
        "+.............................kotokotokotokotokot.............................+\n"
        "+............................kotokotokotokotokotok............................+\n"
        "+...........................kotokotokotokotokotokot...........................+\n"
        "+.........................kotokotokotokotokotokotoko..........................+\n"
        "+.......................kotokotokotokotokotokotokotoko........................+\n"
        "+.....................kotokotokotokotokotokotokotokotokot.....................+\n"
        "+.................kotokotokotokotokotokotokotokotokotokotokot.................+\n"
        "+.................kotokotokotokotokotokotokotokotokotokotokot.................+\n"
        "+................kotokotokotokotokotokotokotokotokotokotokotok................+\n"
        "+...............kotokotokotokotokotokotokotokotokotokotokotoko................+\n"
        "+..............kotokotokotokotokotokotokotokotokotokotokotokoto...............+\n"
        "+.............kotokotokotokotok................kotokotokotokotok..............+\n"
        "+............kotokotokotokot......................kotokotokotokot.............+\n"
        "+............kotokotokotok..........................kotokotokotoko............+\n"
        "+...........kotokotokotok.............................kotokotokoto............+\n"
        "+..........kotokotokoto........000.......000000........kotokotokoto...........+\n"
        "+.........kotokotokoto.........000.....000...0000.......kotokotokoto..........+\n"
        "+.......kotokotokotoko.........000............000........kotokotokotok........+\n"
        "+......kotokotokotoko..........000.........0000..........kotokotokotoko.......+\n"
        "+.....kotokotokotokot..........000...........0000.........kotokotokotoko......+\n"
        "+....kotokotokotokot...........000.....000....000.........kotokotokotokot.....+\n"
        "+...kotokotokotokoto...........000......00...0000.........kotokotokotokotok...+\n"
        "+.kotokotokotokotoko...........00000000..000000...........kotokotokotokotoko..+\n"
        "+kotokotokotokotokok......................................kotokotokotokotokoto+\n"
        "+=============================================================================+\n"
        "                            Welcome to KOTO's L3 sw!                           \n"
        "\n");
}