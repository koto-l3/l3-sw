#pragma once


#include <string>
#include <vector>
#include <list>


#include <fstream>
#include "common.h"
#include "GPUData.h"

class Histos;
class Plotter;
class Clusters;

class DetectorData;

class Selector{

    public:

        Selector(const Config & config);
        ~Selector();

        //void SetTimestamp(long int timestamp) { m_timestamp = timestamp; }


        void Select(std::vector<DetectorData> &detDataList, int nEvents, std::vector<uint16_t> &eventIDs, u_int16_t * eventTag, int spillNo, uint16_t * nCDTCLusters, int * gpuNClus, float * gpuClusX, float * gpuClusY);
        void ClearVariables();

        void SetRawEntryID(int x) {m_rawEntryID = x;}

        std::vector<DetectorData *> & GetDetectorDataList();
        DetectorData * GetDetectorData(std::string name);

        float CalcPedestal(const uint16_t wfm[], bool useOnline);
        float CalcIntegratedADC(const uint16_t wfm[], float pedestal, int nSamples);
        int CalcTime(const uint16_t wfm[], int nSamples);
        void FinalProcess();

        void ComputeMinClusDistance(int i_evt, Clusters * clusters, float & minClusDistance);
        void ComputeClusECOE(int i_evt, Clusters *clusters, float &clusE, float &_minClusE, float &clusCOE, float &_minXY, float &_maxR);

        // TODO: Maybe move this to the CSI cluster, inherited from detector.
        struct Cluster{
            double posx;
            double posy;
            double E;
            double coex;
            double coey;
        };

        std::vector<Cluster> m_clusterList = {};

        float CalcEnergy(const DetectorData &det, int i_evt, int ich, int mode);
        enum {
            m_UsePeakHeigh = 10,
            m_UseIntegratedADC = 11,
        };

        GPUData * m_gpuData;






    private:

        const Config * m_config;

        // TODO: Make sure this is not being copied across objects of this class.
        std::vector<DetectorData *> m_detectorDataList;

        //long int m_timestamp;
        std::ofstream m_histData;
        std::ofstream m_tmpOut; // for debugging
        //Histos * hs;
        //Plotter * pl;
        int m_rawEntryID;

};