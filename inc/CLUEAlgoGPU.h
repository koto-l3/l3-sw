#ifndef CLUEAlgoGPU_h
#define CLUEAlgoGPU_h

#include "CLUEAlgo.h"
#include "LayerTilesGPU.h"
#include "nicola.h"
#include "common.h"
#include "GPUData.h"

static const int maxNSeeds = MAX_CSI_CLUS_PER_EVT;
static const int maxNFollowers = MAX_CSI_HITS_PER_CLUSTER;
static const int localStackSizePerSeed = MAX_CSI_HITS_PER_CLUSTER; // maximum number of followers per seed

struct PointsPtr
{
  float *x;
  float *y;
  int *i_evt;
  //int *i_layer;
  float *weight;

  float *rho;
  float *delta;
  int *nearestHigher;
  int *clusterIndex;
  int *isSeed;

  int *n;
};

struct ParamsPtr
{
  float *dc;
  float *deltao;
  float *deltac;
  float *rhoc;
};




class CLUEAlgoGPU : public CLUEAlgo
{
  // inheriate from CLUEAlgo

public:
  // constructor
  CLUEAlgoGPU() : CLUEAlgo(0, 0, 0, 1)
  {
    init_device();
  }
  // distrcutor
  ~CLUEAlgoGPU()
  {
    free_device();
  }

  // public methods

  void SetPointsFromGPU(GPUData *gpudata, GPUDetectorInfo *csi_info, int csi_n_channel, cudaStream_t stream, int nEvents, u_int16_t *eventTag, u_int time_win[2]);
  void makeClusters(cudaStream_t stream, Clusters *d_clusters, uint16_t *d_recoTag, int nLoadedEvents); // overwrite base class

  void SetClusterParams(int mode, float dc, float deltao, float deltac, float rhoc)
  {

    if (mode < 0 || mode >= MAX_N_RECO_MODES)
    {
      printf("ERROR: RecoTag %d < 0 or >= %d. Fix that in the config.\n", mode, MAX_N_RECO_MODES);
      throw std::runtime_error("RecoTag out of range");
    }



    cudaMemcpyAsync(&d_params.dc[mode], &dc, sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpyAsync(&d_params.deltao[mode], &deltao, sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpyAsync(&d_params.deltac[mode], &deltac, sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpyAsync(&d_params.rhoc[mode], &rhoc, sizeof(float), cudaMemcpyHostToDevice);

  }

private:
  // private variables

  // #ifdef __CUDACC__
  // // CUDA functions

  // algorithm internal variables
  PointsPtr d_points;
  ParamsPtr d_params;
  // Clusters d_clusters;
  LayerTilesGPU *d_hist;
  u_int8_t *d_modes; // KP, PI0EE, 5G, PASS
  GPU::VecArray<int, maxNSeeds> *d_seeds;
  GPU::VecArray<int, maxNFollowers> *d_followers;


  // private methods

  
  void init_device()
  {
    // input variables

    // for (int i_stream = 0; i_stream < N_CUDA_STREAMS; i_stream++)
    //{
    cudaMalloc(&d_points.x, sizeof(float) * MAX_CSI_HITS_PER_STREAM);
    cudaMalloc(&d_points.y, sizeof(float) * MAX_CSI_HITS_PER_STREAM);
    cudaMalloc(&d_points.i_evt, sizeof(int) * MAX_CSI_HITS_PER_STREAM);
    //cudaMalloc(&d_points.i_layer, sizeof(int) * reserve);
    cudaMalloc(&d_points.weight, sizeof(float) * MAX_CSI_HITS_PER_STREAM);
    // result variables
    cudaMalloc(&d_points.rho, sizeof(float) * MAX_CSI_HITS_PER_STREAM);
    cudaMalloc(&d_points.delta, sizeof(float) * MAX_CSI_HITS_PER_STREAM);
    cudaMalloc(&d_points.nearestHigher, sizeof(int) * MAX_CSI_HITS_PER_STREAM);
    cudaMalloc(&d_points.clusterIndex, sizeof(int) * MAX_CSI_HITS_PER_STREAM);
    cudaMalloc(&d_points.isSeed, sizeof(int) * MAX_CSI_HITS_PER_STREAM);
    cudaMalloc(&d_points.n, sizeof(int));
    cudaMemset(d_points.n, 0x00, sizeof(int));

    cudaMalloc(&d_params.dc, sizeof(float) * MAX_N_RECO_MODES);
    cudaMalloc(&d_params.deltao, sizeof(float) * MAX_N_RECO_MODES);
    cudaMalloc(&d_params.deltac, sizeof(float) * MAX_N_RECO_MODES);
    cudaMalloc(&d_params.rhoc, sizeof(float) * MAX_N_RECO_MODES);

    // algorithm internal variables
    cudaMalloc(&d_hist, sizeof(LayerTilesGPU) * MAX_NEVENTS_PER_STREAM);
    cudaMalloc(&d_modes, sizeof(u_int8_t) * MAX_NEVENTS_PER_STREAM);
    cudaMalloc(&d_seeds, sizeof(GPU::VecArray<int, maxNSeeds>) * MAX_NEVENTS_PER_STREAM);
    cudaMalloc(&d_followers, sizeof(GPU::VecArray<int, maxNFollowers>) * MAX_CSI_HITS_PER_STREAM);
    //}
  }

  

  void free_device()
  {
    // input variables
    // for (int i_stream = 0; i_stream < N_CUDA_STREAMS; i_stream++)
    //{
    cudaFree(d_points.x);
    cudaFree(d_points.y);
    cudaFree(d_points.i_evt);
    cudaFree(d_points.weight);
    // result variables
    cudaFree(d_points.rho);
    cudaFree(d_points.delta);
    cudaFree(d_points.nearestHigher);
    cudaFree(d_points.clusterIndex);
    cudaFree(d_points.isSeed);
    // algorithm internal variables
    cudaFree(d_hist);
    cudaFree(d_seeds);
    cudaFree(d_followers);
    //}
  }

  void copy_todevice()
  {
    //      // input variables
    //      cudaMemcpy(d_points.x, points_.x.data(), sizeof(float)*points_.n, cudaMemcpyHostToDevice);
    //      cudaMemcpy(d_points.y, points_.y.data(), sizeof(float)*points_.n, cudaMemcpyHostToDevice);
    //      cudaMemcpy(d_points.weight, points_.weight.data(), sizeof(float)*points_.n, cudaMemcpyHostToDevice);
  }

  void clear_set()
  {
    // // result variables
    // for (int i_stream = 0; i_stream < N_CUDA_STREAMS; i_stream++)
    //{
    h_points_.n = 1024;
    //cudaMemset(d_points.rho, 0x00, sizeof(float) * MAX_NEVENTS_PER_STREAM * MAX_NPOINTS_PER_EVENT);
    //cudaMemset(d_points.delta, 0x00, sizeof(float) * MAX_NEVENTS_PER_STREAM * MAX_NPOINTS_PER_EVENT);
    //cudaMemset(d_points.nearestHigher, 0x00, sizeof(int) * MAX_NEVENTS_PER_STREAM * MAX_NPOINTS_PER_EVENT);
    //cudaMemset(d_points.clusterIndex, 0x00, sizeof(int) * MAX_NEVENTS_PER_STREAM * MAX_NPOINTS_PER_EVENT);
    //cudaMemset(d_points.isSeed, 0x00, sizeof(int) * MAX_NEVENTS_PER_STREAM * MAX_NPOINTS_PER_EVENT);
    // algorithm internal variables
    //cudaMemset(d_hist, 0x00, sizeof(LayerTilesGPU) * MAX_NEVENTS_PER_STREAM);
    //cudaMemset(d_seeds, 0x00, sizeof(GPU::VecArray<int, maxNSeeds>) * MAX_NEVENTS_PER_STREAM);
    //cudaMemset(d_followers, 0x00, sizeof(GPU::VecArray<int, maxNFollowers>) * MAX_NEVENTS_PER_STREAM * MAX_NPOINTS_PER_EVENT);
    //}
  }

  // #endif // __CUDACC__
};

#endif
