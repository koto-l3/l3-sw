#ifndef CLUEAlgo_h
#define CLUEAlgo_h

// C/C++ headers
#include <set>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include <chrono>

#include "LayerTiles.h"
#include "Points.h"
#include "common.h"

class CLUEAlgo
{

public:
  // constructor
  CLUEAlgo(float dc, float deltao, float deltac, float rhoc, bool verbose = false)
  {
    dc_ = dc;
    deltao_ = deltao;
    deltac_ = deltac;
    rhoc_ = rhoc;
    dm_ = std::max(deltao_, deltac_);
    verbose_ = verbose;
  }
  // distrcutor
  ~CLUEAlgo() {}

  // public variables
  float dc_, dm_, deltao_, deltac_, rhoc_;
  bool verbose_;
  Points h_points_;

  // public methods
  void setPoints(int n, float *x, float *y, float *weight)
  {
    for (int i_stream = 0; i_stream < N_CUDA_STREAMS; i_stream++)
    {

      h_points_.clear();
      h_points_.n = n;
      // input variables
      h_points_.x.assign(x, x + n);
      h_points_.y.assign(y, y + n);
      h_points_.weight.assign(weight, weight + n);
      // result variables
      h_points_.rho.resize(n, 0);
      h_points_.delta.resize(n, std::numeric_limits<float>::max());
      h_points_.nearestHigher.resize(n, -1);
      h_points_.isSeed.resize(n, 0);
      h_points_.followers.resize(n);
      h_points_.clusterIndex.resize(n, -1);
    }
  }

  void setPoints(int n)
  {

    h_points_.clear();
    h_points_.n = n;

    // result variables
    h_points_.rho.resize(n, 0);
    h_points_.delta.resize(n, std::numeric_limits<float>::max());
    h_points_.nearestHigher.resize(n, -1);
    h_points_.isSeed.resize(n, 0);
    h_points_.followers.resize(n);
    h_points_.clusterIndex.resize(n, -1);
  }

  void clearPoints()
  {
    for (int i_stream = 0; i_stream < N_CUDA_STREAMS; i_stream++)
    {
      h_points_.clear();
    }
  }

  void makeClusters();

private:
  // private variables

  // private member methods
  void prepareDataStructures(LayerTiles &);
  void calculateLocalDensity(LayerTiles &);
  void calculateDistanceToHigher(LayerTiles &);
  void findAndAssignClusters();
  inline float distance(int, int) const;
};

#endif
