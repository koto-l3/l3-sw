
#pragma once

#include <iostream>
#include <memory>


class compress_cpu
{
private:
    /* data */
public:
    compress_cpu(/* args */);
    ~compress_cpu();
    void GetWfmInfo(uint16_t *wfmBuffer, int * wfm_info);
    void Compress(u_int16_t wfmData[][16][64], uint8_t h_odata[], unsigned int wfmLen, int *h_wfmInfo, int fadcID0, int fadcID1);
};

