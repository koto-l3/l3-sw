


#pragma once





#include <chrono>
#include <string>
#include <vector>

#include "nlohmann/json.hpp"

class Timer
{
private:
    std::chrono::high_resolution_clock::time_point m_start_point;
    std::string m_label;
    int m_debug_mode;
    std::vector<std::chrono::high_resolution_clock::time_point> m_timers; // parallel timers (running)
    std::vector<int64_t> m_timers_results; // parallel timers (results)
    std::vector<std::string> m_labels; // parallel timers (results)
    std::vector<int> m_sub_timer_count;
    std::vector<std::vector<int>> m_parent_ids;
    std::vector<int> m_tmpPID;
    int m_nTimers;
    nlohmann::json m_json;
    float m_calibrationConstant;
    void Callibrate();

public:

    Timer();

    void Start();
    uint64_t GetDuration();
    void SetDebugMode(int x);
    void StartLap(int id);
    void FinishLap(int id);
    void CreateTimer(int id, std::string label, int tmpPID, int parentId1=-1, nlohmann::json buu={});
    void GetTimerCount(int id);
    void GenerateResultFile(std::string fileName);
    void IncreaseParentTimerMeasurementCount(int id);

};
