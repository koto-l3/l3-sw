

#include <string>
#include <vector>
#include <list>

#include "common.h"



template <typename Out>
void split(const std::string &s, char delim, Out result);
std::vector<std::string> split(const std::string &s, char delim);

class DetectorInfo{
    public:

        DetectorInfo(const Config& config);

        std::string m_calibFile;
        std::string m_name;
        std::string m_chPosFile;
        std::string m_ext_map_file;
        int ReadCalibrationData();
        int ReadCHPositions();
        int fillMapperListFromTxt();
        int fillChMaskFromCfg();

        std::vector<float> getGainMean() { return m_GainMean; };
        std::vector<float> getGainSigma() { return m_GainSigma; };
        std::vector<float> getMeVCoeff() { return m_MeVCoeff; };
        std::vector<uint8_t> getChMaskForReco() { return m_chMaskForReco; };
        std::vector<uint8_t> getChMaskForPedSup() { return m_chMaskForPedSup; };
        float getChPosX(int i) { return m_chPosX.at(i); };
        float getChPosY(int i) { return m_chPosY.at(i); };
        int getChIdX(int i) { return m_ChIdX.at(i); };
        int getChIdY(int i) { return m_ChIdY.at(i); };

        void SetIs500MHz(bool b) { m_is_500MHz = b; }
        bool Is500MHz() {return m_is_500MHz;}

        void SetSamplesPerWfm(int b) { m_samplesPerWfm = b; }
        uint16_t GetSamplesPerWfm() {return m_samplesPerWfm;}

        void AddModuleID(int modID) { m_modID.push_back(modID); }

        void PrepareVectors();
        void AddChannelData(int idx, int crateID, int FADCID, int CHID);
        std::vector<int> GetFadcChIds() { return m_fadcChIDs; };

        int GetCrateID( int n ) const { return m_fadcCrateIDs[n]; }
        int GetFadcModuleId( int n ) const { return m_fadcModuleIDs[n]; }
        int GetFadcChId( int n ) const { return m_fadcChIDs[n]; }
        uint16_t* GetAbsChIdPtr() { return m_AbsChID.data(); }

        // data
        int m_nChannels = 0;
        std::vector<int> m_timestamp;
        Config m_config;

        std::vector<int> m_ChIdX;
        std::vector<int> m_ChIdY;

    private:

        bool m_is_500MHz;
        uint16_t m_samplesPerWfm;

        // Geometry, callibration
        std::vector<float> m_GainMean;
        std::vector<float> m_GainSigma;
        std::vector<float> m_MeVCoeff;
        std::vector<float> m_chPosX;
        std::vector<float> m_chPosY;
        std::vector<uint8_t> m_chMaskForReco;
        std::vector<uint8_t> m_chMaskForPedSup;

        std::vector<int> m_modID;
        std::vector<int> m_detChID;
        std::vector<int> m_fadcCrateIDs;
        std::vector<int> m_fadcModuleIDs;
        std::vector<int> m_fadcChIDs;

        std::vector<uint16_t> m_AbsChID; // m_AbsChID[detector channel] = absolute fadc channel ID = crateID*(16*16) + FADCID*(16) + CHID. To be copied to the GPU
};

class DetectorData
{
    public:

        DetectorData(DetectorInfo* info);
        
        void SetChannelWfmPtr(int i_event, int channel, uint16_t wfm[]) { m_wfmData.at( i_event*m_info->m_nChannels + channel ) = wfm; }
        

        //TODO: Make these private
        std::vector<uint16_t*> m_wfmData;
        std::vector<float> m_pedestal;
        std::vector<float> m_peak_heigh;
        float m_totalE;
        std::vector<float> m_Energies;
        std::vector<uint16_t> m_times;
        float m_coe[2];
        std::vector<float> m_integratedADC;
        std::list<int> m_IdxWithHit;
        std::vector<int> m_IdxWithHitVec;

        DetectorInfo* m_info;
        Config m_config;

};




class CSI: public DetectorInfo{

    public: 
        
};