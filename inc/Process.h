#pragma once

#include <vector>
#include <iostream>
#include <map>
#include <memory>

#ifdef IS_SPILLNODE
#include <cuda_runtime.h>
#include "CLUEAlgoGPU.h"
#include "GPUData.h"
#endif

#include "Capture.h"
#include "common.h"

#if D_ENABLE_CPU_DEBUG
class Selector;
#endif

struct WfmData;
struct EventHeaderData;
struct SpillStats;

class DetectorData;
class DetectorInfo;
class compress_cpu;



struct FADCHeaderData
{



    // Trig. bit, evt No, spill No, run No, adc Hdrs
    std::array<uint8_t, MAX_NEVENTS_PER_STREAM*(EVT_HDR_SIZE)> evtHdrData;

    uint16_t crateNo[MAX_NEVENTS_PER_STREAM][288];
    uint16_t moduleID[MAX_NEVENTS_PER_STREAM][288];

    uint16_t data[MAX_NEVENTS_PER_STREAM][288][6];


    uint16_t nCDTClusters[MAX_NEVENTS_PER_STREAM];
    uint16_t triggerBits[MAX_NEVENTS_PER_STREAM][8];
    
    uint16_t recoTag[MAX_NEVENTS_PER_STREAM];
};



class Process
{

public:

    /**
     * @brief Initializes CPU and GPU memory. Initializes detectors and loads
     * all configuration parameters. This function runs only once.
     *
     * @param config 
     */
    Process(const Config & config);
    
    ~Process();


    std::mutex m_lock;
    std::vector<bool> m_thread_status;
    std::atomic<int> m_blockID;

    int Initialize();


    /**
     * @brief Get packets from Capturer's buffers, and prepare blocks to be
     * processed. Call "LaunchProcessBlock" when a block is ready. Check for
     * blocks whose processing is completed, and update Capturer's buffers
     * tail.
     *
     * @param capturer 
     * @return int 
     */
    int Run(Capturer * capturer);
    void SetWfmDataPerDetector(uint16_t wfmData[][16][64]);

    
    /**
     * @brief Loop through the raw packet buffers. Reject corrupted events.
     * Separate ADC data and ADC headers. Send ADC data to GPU for processing.
     *
     * @param totalProcessedPackets 
     * @param cudaStreamID 
     * @param nPackets 
     * @param firstPktIdx 
     * @param eventIDs 
     * @param scaledTrigBits 
     * @param FADCHeaderData 
     * @param prevSpillNo 
     * @return uint32_t 
     */
    uint32_t PrepareGPUData(uint32_t & totalProcessedPackets, int cudaStreamID, u_int nPackets, int firstPktIdx, std::vector<uint16_t> & eventIDs, std::vector<uint8_t> & scaledTrigBits, FADCHeaderData & FADCHeaderData, uint16_t& prevSpillNo);
    
    
    /**
     * @brief 
     * 
     * @param nLoadedEvents 
     * @param cudaStreamID 
     */
    void ReconstructAndSelect(int nLoadedEvents, int cudaStreamID);
    void DebugReconstruction(int nLoadedEvents, std::vector<uint16_t> &eventIDs, std::vector<uint8_t> &scaledTrigBits, int cudaStreamID, int spillNo, FADCHeaderData &FADCHeaderData);
    
    
    
    /**
     * @brief Main function that prepares GPU arrays of events and calls all GPU
     * kernels to reconstruct, select and compress the data.
     *
     * @param i_thread 
     * @param evtStart 
     * @param nPacketsInBlock 
     * @param blockID 
     * @return uint32_t 
     */
    uint32_t ProcessEventBlock(int i_thread, int evtStart, u_int nPacketsInBlock, int blockID);
    
    /**
     * @brief Align the start of a packet block with the start of an event and
     * launch "ProcessEventBlock"
     *
     * @param i_pcap_thread 
     * @param nPktsInBlock 
     * @param blockID 
     * @return * void 
     */
    void LaunchProcessBlock(int i_pcap_thread, int nPktsInBlock, int blockID);
    int LaunchCompress(uint16_t wfmData[][16][64], uint8_t h_odata[]);

    void WriteEvtHeader(uint8_t *_compressedDataPtr, uint8_t scaledTrigBit, int spillNo, uint16_t* EventHeaderDataPtr);

    void SetWfmData(DetectorData &det,
				//const IndexMap &index_map,
				//E14Mapper *mapper, 
                uint16_t *wfmData, int nLoadedEvents);

    //int loadADCData(uint16_t dst[][16][64], FADCHeaderData & FADCHeaderData, uint8_t * src, int nFADCs, int firstFADCIdx);
    int loadADCData(uint16_t *dst, FADCHeaderData & FADCHeaderData, uint8_t * src, int nFADCs, int firstFADCIdx, int i_event);

    void SetRawEntryID(int x) {m_rawEntryID = x;}

    void Debug(int i_pcap_thread, uint16_t perFADCWfmData[][16][64], FADCHeaderData & FADCHeaderData, int refEventID);

    void ComputingThreadGroupManager();

    int SetUpDetectors();


    void PrintGPUData(int nLoadedEvents, GPUData & gpudata, Clusters& clusters, std::vector<uint16_t> & eventIDs, uint16_t *d_recoTag);
    //std::vector<DetectorConfigData> & GetDetectorConfigData() { return m_DetectorConfigList; }

    //IndexMap &GetIndexMap() { return *m_IndexMap; }

    //uint8_t * GetCompressedData() { return m_h_odata;}
    //int GetNBytesPerFADC(int i_FADC) { return m_nBytesPerFADC[i_FADC]; }
    //void fillMapperListFromTxt(std::string detectorName, std::string FileName);


    uint32_t m_packetsBeingProcessed[8];


    uint16_t GetRecoTagFromScaledTiggerBit(uint8_t scaledTrigBit);

    int m_nThreadsSel = 20;

    std::vector<int> m_diskNodeRanks;

    std::vector<SpillStats> m_spillStats;


#ifdef IS_SPILLNODE
    cudaStream_t m_streams[8];

    std::array<uint16_t *, 8> m_d_perFADCWfmData;
    std::array<uint8_t *, 8> m_d_perEvtHeaderData;
    //std::array<uint32_t *, 8> m_dh_perEvtIdxCompressed;
    //std::array<uint32_t *, 8> m_dh_perEvtSizeCompressed;
    std::array<uint32_t *, 8> m_d_perEvtSizeCompressed;
    std::array<uint32_t *, 8> m_d_perEvtIdxCompressed;
    //std::array<uint16_t *, 8> m_d_nCDTClusters;
    std::array<uint16_t *, 8> m_d_triggerBits;
    std::array<uint16_t *, 8> m_d_recoTag;


    std::array<uint16_t *, 8> m_d_wfmInfo;
    std::array<uint32_t *, 8> m_d_compressedBytesPerWfm;
    std::array<uint8_t  *, 8> m_d_odata;
    std::array<uint8_t  *, 8> m_d_pedSuppressionFlag;

    std::array<uint16_t  *, 8> m_h_perFADCWfmData;
    std::array<uint8_t   *, 8> m_h_compressedDataBuffer;

    std::array<uint16_t  *, 8> m_h_compressedBytesPerWfm;

    std::array<uint32_t  *, 8> m_h_perEvtSizeCompressed;
    std::array<uint32_t  *, 8> m_h_perEvtIdxCompressed;

    //Clusters d_clusters;
    std::array<Clusters *, 8> m_d_clusters;
    std::array<Clusters *, 8> m_h_clusters;

    std::array<float *, 8> m_d_energies;



    // Detector Ch ID to FADC CH ID mapping
    uint16_t* m_d_CSIDetChIdToADCChId;
    uint8_t* m_d_csiChsMaskForReco;
    uint8_t* m_d_csiChsMaskForPedSup;
    float* m_d_gainMeanCoefs;
    float* m_d_ADCMevCoefs;




#endif


private:

    Config m_config;


    /**
     * @brief Called for all the detectors, even the ones that are not used for selection.
     * Only the detectors used for selection are moved to the selector's detector list.
     *
     * @param name
     * @param is_500MHz
     * @param ext_map_file
     */
    void AddDetector(const std::string name,
				 const bool is_500MHz,
				 const std::string ext_map_file);

    DetectorInfo* GetDetectorInfo(const std::string name);

    //IndexMap * m_IndexMap;
    //std::shared_ptr<IndexMap> m_IndexMap;


    // Capacity is N_PKTS_PER_THREAD+1. packet in buff[N_PKTS_PER_THREAD] is the
    // same as the one in buff[0], so when processing the last ADC in packet
    // buff[N_PKTS_PER_THREAD-1], the ADC can be read without weird memory
    // jumps.
    std::vector<std::shared_ptr<std::array<u_char[PKT_SIZE-OFC2_HDR_SIZE], N_PKTS_PER_THREAD+1>>> *m_rawPktDataBuffer;
    
    std::vector<std::shared_ptr<std::array<struct pcap_pkthdr, N_PKTS_PER_THREAD>>> *m_rawPktHdrBuffer;
    std::vector<std::shared_ptr<CircularBufferManager>> *m_circularBufferManager;

    DetectorInfo * m_csi_info;
    
#if D_ENABLE_CPU_DEBUG
    Selector * m_selector;
#endif

    int m_rawEntryID;
    u_int16_t m_spillNo;
    u_int16_t m_prevSpillNo;

    std::array<CLUEAlgoGPU  *, 8> m_clueAlgo;

    std::array<GPUData*, 8> m_gpuDataList;
    GPUDetectorInfo* m_gpu_csi_info;


    //std::vector<DetectorConfigData> m_DetectorConfigList;

    std::vector<DetectorInfo *> m_fullDetectorList; // Includes ALL detectors.

//    std::vector<E14Mapper*> m_MapperList;

    //std::vector<WfmData> m_perFADCWfmData;
    uint16_t m_perFADCWfmData[288][16][64];
    
    // Compressed array in the host
    //std::unique_ptr<uint8_t[]> h_odata;
    
    //int m_nCompressedBytes;
    int m_fadcHeaderSize = 10;
    int m_nBlocks;



    std::vector<std::queue<std::future<uint32_t>>> m_futures;

    std::vector<std::unique_ptr<BS::thread_pool>> m_threadPoolListForCUDA;
    
    // vector of shared ptrs to the events in m_rawPktDataBuffer that were selected
    //std::vector<uint8_t[MPI_BUFF_SIZE]> m_compressedDataBuffers;

    std::vector<std::shared_ptr<std::array<u_char[PKT_SIZE], N_PKTS_PER_THREAD>>> m_selectedEvents;
    
    std::condition_variable m_tmp_cv;
    std::mutex m_tmp_cv_m;


    std::vector<int> m_recvdPktsPerQueue;

    std::vector<int> m_shiftPerQueue;

    //uint8_t  (*h_odata)[2064];
    




};