# pragma once


#define N_CSI_CHANNELS  2716


// Do not print errors if an ADC with wrong crate No or module No is found.
#define MASK_ALL_ADCS 0


// The following defines would better fit in a config file. But passing them to
// the reconstruction GPU kernel is not trivial, so we have here instead.
#define SEL_5G_MIN_XY   140
#define SEL_5G_MAX_R    865
#define SEL_5G_MIN_E    0
#define SEL_5G_MIN_COE  0

#define SEL_KP_MIN_XY   140
#define SEL_KP_MAX_R    850
#define SEL_KP_MIN_E    600

#define SEL_PI0EE_MIN_XY  140
#define SEL_PI0EE_MAX_R   870
#define SEL_PI0EE_MIN_E   600
#define SEL_PI0EE_MAX_COE 9999


// When defined, no triggers are pedestal suppressed, but a "pedestal
// suppression flag" written into each waveform header.
#define DEBUG_PEDSUP 0

// When defined, reco and selection are performed but no event is rejected.
// The REC_LUCKY_BIT is turned on for all events.
#define DEBUG_SELECTION 0


#define DO_RDMA 1