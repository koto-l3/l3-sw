#ifndef HnLib_HnEveDisCSI_h
#define HnLib_HnEveDisCSI_h
#include "HnEveDisDetector.h"

namespace HnLib
{
  class HnEveDisCSI : public HnEveDisDetector
  {
  public:
    HnEveDisCSI(int userFlag);
    virtual ~HnEveDisCSI();

    virtual bool setZYPlaneVisible(bool isVisible);

  protected:
    HnEveDisCSI();

    bool m_isZYPlaneVisible;

  };
}

#endif // HnLib_HnEveDisCSI_h
