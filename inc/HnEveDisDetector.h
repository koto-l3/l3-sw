#ifndef HnLib_HnEveDisDetector_h
#define HnLib_HnEveDisDetector_h



#include "TVector3.h"
#include "TH2Poly.h"
#include "TText.h"
#include "TList.h"
#include <string>
#include <map>
#include <list>

class TGraph;
class TPaveText;

namespace HnLib
{
  /**
   *  @class HnEveDisDetector
   *  @brief HnEveDisDetector is the base class of event display
   */
  
  class HnEveDisDetector
  {
  public:
    /**
     *  @function HnEveDisDetector
     *  @brief Constructor of HnEveDisDetector
     *  @param detectorName The detector name for the event display.
     *  @param macroName    The corresponding name in the gsim4 macro.
     *  @param nIndex       The number of module idexes in the detector.
     */
    HnEveDisDetector(std::string detectorName,std::string macroName,int nModule);
    /**
     *  @function ~HnEveDisDetector
     *  @brief Destructor of HnEveDisDetector
     */
    virtual ~HnEveDisDetector();

    /**
     *  @function readShiftFromMacroFile
     *  @brief Read the detector translation shift from a gsim4 macro file.
     *  @param macroFileName gsim4 macro file name
     */
    bool readShiftFromMacroFile(std::string macroFileName);

    /**
     *  @function generateGlobalEnvelope
     *  @brief Generate global module shapes from local envelope and coordinate shift.
     */
    void generateGlobalEnvelope();

    /**
     *  @function generatePolyXY();
     *  @brief Generate TH2Poly object from global envelope
     */
    void generatePolyXY();

    /**
     *  @function generatePolyZY();
     *  @brief Generate TH2Poly object from global envelope
     */
    void generatePolyZY();

    void storePolyAtt();
    void restorePolyAtt();
    
    /**
     *  @function setModuleContent
     *  @brief Set corresponding bin contents for TH2Poly.
     *  @param modID module ID.
     *  @param w weight value filled in the TH2Poly.
     */
    bool setModuleContent(int modID, double w);

    /**
     *  @function setModuleContent
     *  @brief Add corresponding bin contents for TH2Poly.
     *  @param modID module ID.
     *  @param w weight value to be added in the TH2Poly.
     */
    bool addModuleContent(int modID, double w);

    /**
     *  @function draw
     *  @brief Draw TH2Poly
     *  @param option The draw option of TH2Poly (default is "l colz").
     */
    void draw(std::string option="l colz");

    /**
     *  @function drawModuleID
     *  @brief Fill ID and draw
     *  @param option The draw option of TH2Poly (default is "l text").
     */
    virtual void drawModuleID(std::string option="l text");

    /**
     *  @function reset
     *  @brief Reset all the bin contents to be the assigned value.
     *  @param val The reset value.
     */
    virtual void reset(double val=0);

    /// Draw pave for statistics
    virtual void drawStats();

    /// Draw pave for entry information
    virtual void drawInfo();
    
    void setTitle(std::string title  ) { if(g_hPoly) g_hPoly->SetTitle(title.c_str()); };
    void setXTitle(std::string title ) { if(g_hPoly) g_hPoly->SetXTitle(title.c_str()); };
    void setYTitle(std::string title ) { if(g_hPoly) g_hPoly->SetYTitle(title.c_str()); };
    void setZTitle(std::string title ) { if(g_hPoly) g_hPoly->SetZTitle(title.c_str()); };
    void setMinimum(double v         ) { if(g_hPoly) g_hPoly->SetMinimum(v); };
    void setMaximum(double v         ) { if(g_hPoly) g_hPoly->SetMaximum(v); };
    bool setLineWidth(int modID, double width);
    bool setLineColor(int modID, int iColor);
    bool setFillStyle(int modID, int iStyle);
    void setMarkerSize(double size) { if(g_hPoly) g_hPoly->SetMarkerSize(size); };

    

    /**
     *  @function getModuleIDXY
     *  @brief Get module ID to which (X mm,Y mm) point belongs.
     *  @param x X coordinate in mm.
     *  @param y Y coordinate in mm.
     */
    std::vector<int>  getModuleIDXY(double x, double y);
    
    /**
     *  @function getModuleXY
     *  @brief Get module center (X mm,Y mm) point corresponding to the module ID.
     *  @param modID The module ID.
     *  @param x X coordinate in mm.
     *  @param y Y coordinate in mm.
     */
    bool getModuleXY(int modID, double& x, double& y);

    

  public:
    /// The ROOT 2D histogram with variable bin shapes which use can define with polygon.
    TH2Poly* g_hPoly;
    
  protected:
    /// defualt constructor is hidden.
    HnEveDisDetector();

    /// Get TGraph corresponding to the TPolyBin
    std::list<TGraph*> getGraph(int modID);
    
  protected:
    /// The detector name in the event display.
    /// (mandatory in constructor)
    std::string m_detectorName;

    /// The corresponding detector name in the gsim4 macro.(mandatory in constructor)
    /// (mandatory in constructor)
    std::string m_macroName;

    /// Number of module indexes (detector parts) in the detector
    /// (mandatory in constructor)
    int m_nIndex;
    
    /// Lists of module IDs
    /// (mandatory in constructor)
    int* m_moduleID;
    
    /// Module Index from ID.
    /// (mandatory in constructor)
    /// Multiple modules with the same ID are allowd.
    /// Multiple ID with the same module are also allowd.
    std::multimap<int,int> m_mapIDtoIndex;
    
    ///The userflag used in the gsim4 to define the detector shapes or positions.
    //  (mandatory in constructor)
    int m_userFlag;
    

    /**
     *  @brief Module local shape with vector of TVector3 points.
     *
     *  std::vector<TVector3> m_localEnvelope[m_nModule];
     *  the unit is mm
     *  The upstream and downstream faces with vector of TVector3 positions each.
     *  The coordinate can be extracted from GsimE14Detector source codes.
     *  It should be aligned in the order of phi angle.
     *  (mandatory in constructor)
     *
     * The example below include 4 points.
     * Any nubmer of poitns larger than 2 is possible.
     * The number of poitns should be the same between upstream and downstream.
     *
     * [smaller Z]
     *         |Y or radial direction
     * (1)o    |    (0)o
     *         |
     *   --------------
     *         |      X
     *   (2)o  | (3)o
     *
     *         |
     * [larger Z]
     *         |Y or radial direction
     * (5)o    |    (4)o
     *         |
     *   --------------
     *         |      X
     *   (6)o  | (7)o
     *         |
     *
     *  
     */
    std::vector<TVector3>* m_localEnvelopeUS;
    std::vector<TVector3>* m_localEnvelopeDS;

    /**
     *  @brief Module global shape with vector of TVector3 points.
     *  (automatically generated in generateGlobalEvnelope)
     */
    std::vector<TVector3>* m_globalEnvelopeUS;
    std::vector<TVector3>* m_globalEnvelopeDS;

    /**
     *  @brief Module translation shift in gsim4
     *  (automatically assigned in readShiftFromMacroFile)
     */
    TVector3  m_shiftPosition;

    /// Check flag for gloal envelop construction.
    bool m_isGlobalEnvelopeFilled;


    
    /////////////////////////////////////
    /////Properties for TH2Poly
    std::string m_title;
    std::string m_xTitle;
    std::string m_yTitle;
    std::string m_zTitle;
    double m_minimum;
    double m_maximum;
    ///index => line width
    std::map<int,double> m_lineWidthMap;
    ///index => line color
    std::map<int,double> m_lineColorMap;
    ///index => fill style
    std::map<int,int>    m_fillStyleMap;
    ///index => marker size
    std::map<int,double> m_markerSizeMap;
    ///index => filled value
    std::map<int,double> m_valueMap;

    
    ////////////////////////////////////
    ///// Map for filled ID to value
    //ID => value map for PaveText information
    std::multimap<int,double> m_IDtoValueMap;

    

    /// Statistics : entriess under/inside/over the z range
    int m_stat[3];
    /// Pave for stat display
    TPaveText* m_paveStat;
    /// Value for reset to check content change
    double m_resetValue;
    /// Pave for entry information
    TPaveText* m_paveInfo;

    

    /// Utility to show text 
    TText** m_label;

    ///Convert to gsim4 trap to TVector3 list.
    /// The former half is for upstream, the latter half is for downstream.
    std::list<TVector3> convTrapParam(std::vector<double> parameterArray);

    ///Convert to gsim4 box to TVector3 list.
    /// The former half is for upstream, the latter half is for downstream.
    std::list<TVector3> convBoxParam(std::vector<double> parameterArray);

    ///Rotate and translate TVector3 for all the contents
    void rotateAndTranslate(std::list<TVector3>& vList,TVector3 rot, TVector3 pos);

    ///Sort with z and phi angle
    void sortEnvelope(std::list<TVector3>& vList);

    ///Fill local envelop with vList.
    /// The former half is for upstream, the latter half is for downstream.
    void fillEnvelope(std::list<TVector3>& vList,int index);

    ///Divide the vList into two for the axis defined by axisWord X, Y, or Z.
    void makeHalfDivision(std::list<TVector3>& vList,std::string axisWord,
			  std::list<TVector3>& vList0,std::list<TVector3>& vList1);
  };
}
#endif// HnLib_HnEveDisDetector_h
