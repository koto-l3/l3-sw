
#include <vector>
#include <array>

#include "threadPool.h"
#include "common.h"



// class to receive packets and write them to disk

class writeToDisk
{

public:

    writeToDisk(const Config & config);
    ~writeToDisk();

    void Run();

    void WriteBinaryFile(int destBuffer, int spillNo, int fileID, uint64_t nBytes);
    void WriteSpillStats(int dest_i_buffer, int spillNo);
    void WriteDataAndStats(int destBuffer, int spillNo, int fileID, uint64_t nBytes);

    void AddEvtToCompressedArray();

    // buffer status
    enum bufferStatus
    {
        BUFFER_FREE = 100,
        BUFFER_RECVING_SPILL,
        WRT_SPILL_RECVD,
        WRT_BUFFER_FULL,
    };


    /**
     * @brief There is an array containing SpillStats objects. Which is the
     * index in that array of the stats from a given spill? This function
     * returns that index. If the spill is not found, it is added to the array.
    */
    int GetSpillStatsIndex(int spillNo);

    enum statsStatus
    {
        STATS_INDEX_NOT_FOUND = -2,
        STATS_INDEX_FREE = -1,
    };

private:

    Config m_config;
    
    std::map<int, int> m_spillNo_to_fileID; // map spillNo to fileID

    int m_RunNo = 0;
    int m_latestSpillNo = 0;

    std::vector<std::vector<uint8_t>> m_spill_buffer;
    std::vector<int> m_buffer_busy_status; // rename me TODO
    std::vector<int> m_buffer_spillNo; // spillNo the buffer is containing
    int m_nSpillBuffers;
    uint64_t m_spill_buffer_size;

    std::vector<SpillStats> m_spill_stats;
    std::vector<int> m_stats_spillNo;

    std::vector<uint64_t> m_buffer_nbytes_written;
    std::vector<uint64_t> m_buffer_timestamp;

    std::string GetVerboseBufferStatus(int status)
    {
        switch (status)
        {
            case BUFFER_FREE:
                return "BUFFER_FREE";
            case BUFFER_RECVING_SPILL:
                return "BUFFER_RECVING_SPILL";
            case WRT_SPILL_RECVD:
                return "WRT_SPILL_RECVD";
            case WRT_BUFFER_FULL:
                return "WRT_BUFFER_FULL";
            default:
                return "UNKNOWN";
        }
    }


    ThreadPool* m_tpDisk = new ThreadPool(25); // thread pool to write to disk

};



