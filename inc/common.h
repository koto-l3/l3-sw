
# pragma once

#include "timer.h"
#include "threadPool.h"
#include "BS_thread_pool.hpp"
#include "config.h"




//extern Timer t; // Timer object


extern ThreadPool * tp; // ThreadPool object

extern int rank, size; // MPI rank and size




struct Config
{
    /**
    * @brief Set to 1 to use MPI. 0 is useful for debugging. Note: "0" will
    * disable the disk node-related code. When using MPI, run something like
    * "make -j10  && mpirun --bind-to none -n 2 ./main"
    */
    bool do_mpi;

    /**
     * @brief Debug 40G interface. Capture packets, test them 
     * (write them to file, compare them against reference, etc.) and exit
     * 
     */
    bool debug_40G;

    bool debug_Process;

    bool do_physics;

    std::vector<int> csi_chids_to_mask_reco;
    std::string csi_chids_to_mask_pedsup_file;


    /**
     * @brief Decompress the compressed data, and compare it against the
     * original to check that the compression works. Intended for debugging, not
     * for production.
     *
     */
    bool do_decompress;

    bool exit_after_first_spill;

    /**
     * @brief  Debug Bin2Raw, the program that will run at KEKCC on binary the
     * output of the Disk nodes.
     *
     */
    bool debug_bin2raw;

    bool do_single_thread;

    bool do_profiling;

    /**
     * @brief Number of capturing threads. Usually 1 is not 
     * enough to cope with the 40G.
     * 
     */
    u_int n_pcap_threads;

    int n_ranks_per_disk_node;
    int disk_node_nbuffers_per_rank;
    uint64_t disk_node_nbytes_per_buffer;


    /**
     * @brief Size of the headers in the incoming packets
     */
    //int pkt_size;

    int adc_header_size;
    int adc_footer_size;
    int crate_footer_size;

    int memory_cpu_id;


    std::string hostname;
    std::string ifname_40G;
    std::string data_out_dir;
    std::string stats_out_dir;
    std::string monitor_out_dir;
    int node_type;

    std::string csi_calib_file;
    std::string csi_ch_pos_file;
    int csi_n_channels;

    std::string cv_calib_file;
    uint16_t run_number;
    int cv_n_channels;

    u_int wfm_time_window[2];

    int verbose_level;


    int first_capture_cpu_id;
    int mem_alloc_cpu_id;

    enum NodeType {
        SPILL_NODE,
        DISK_NODE,
        UNKNOWN_NODE
    };


    int m_someThreadHasFinished = 0;

    // Not read from config file. Set in main.cpp
    uint64_t mem_per_thread_buff;
    u_int max_compressed_evt_size;



    // special trigger ADC
    int trigger_adc_ifadc;
    int scaled_trig_bit_ch;
    int scaled_trig_bit_sample;
    int n_cdt_clus_ch;
    int n_cdt_clus_sample;


    struct ClusterConfig
    {
        int mode;
        int dc;
        int deltao;
        int deltac;
        int rhoc;
    };

    // cluster variables
    ClusterConfig clus_cfg_pi0ee;
    ClusterConfig clus_cfg_kp;
    ClusterConfig clus_cfg_5g;



    int min_bias_trigger_bit;
    std::string run_type;
    std::array<int, 8> scaled_trig_bit_definitions;


};


#define MPI_BUFF_SIZE 80000000UL


// Constants to define arrays
//#define N_PKTS_PER_THREAD 10000UL//167500UL
//#define N_PKTS_PER_THREAD 16750UL//167500UL
#define N_PKTS_PER_THREAD 400000UL
#define PKT_SIZE 8800 // Bytes
#define LAST_PKT_SIZE 7840 // Bytes

#define OFC2_HDR_SIZE 40 // Bytes // TODO: Get this from config!!



#define PKTS_PER_EVT 68
#define N_CRATES 18
#define N_FADCS_PER_CRATE 16
#define N_CH_PER_FADC 16
#define N_SAMPLES_PER_CH 64



#define COMP_EVT_SIZE_S  4
#define TRIG_BIT_S  1
#define L3_TAG_S  2
#define EVT_NO_S  2
#define SPILL_NO_S  2
#define RUN_NO_S  2
#define ADC_HDRS_S  288*6*2
#define L3_EVT_HDR_SIZE  (COMP_EVT_SIZE_S + TRIG_BIT_S + L3_TAG_S + EVT_NO_S + SPILL_NO_S + RUN_NO_S)
#define EVT_HDR_SIZE  (L3_EVT_HDR_SIZE + ADC_HDRS_S)

#define MAX_COMP_EVT_SIZE (EVT_HDR_SIZE + N_CRATES*N_FADCS_PER_CRATE*N_CH_PER_FADC*129)


#define COMP_EVT_SIZE_O  0
#define TRIG_BIT_O  (COMP_EVT_SIZE_O + COMP_EVT_SIZE_S)
#define L3_TAG_O  (TRIG_BIT_O + TRIG_BIT_S)
#define EVT_NO_O    (L3_TAG_O + L3_TAG_S)
#define SPILL_NO_O  (EVT_NO_O + EVT_NO_S)
#define RUN_NO_O  (SPILL_NO_O + SPILL_NO_S)
#define ADC_HDRS_O  (RUN_NO_O + RUN_NO_S)






#define EVT_WFMS_SIZE (N_CRATES*N_FADCS_PER_CRATE*N_CH_PER_FADC*N_SAMPLES_PER_CH)


#define N_CUDA_STREAMS 8

#define MAX_NEVENTS_PER_STREAM 800
#define N_CSI_CHANNELS 2716

#define MAX_CSI_CLUS_PER_EVT 40
#define MAX_CSI_HITS_PER_CLUSTER 120
#define MAX_CSI_HITS_PER_STREAM (200 * MAX_NEVENTS_PER_STREAM)    // All events with more than this number of hits will be tagged as weird and accepted.
#define MAX_N_RECO_MODES 10

// Maximum size of the compressed data buffer in bytes. This corresponds to the
// case in which all events are accepted, and no ped. suppression or compression
// are performed.
#define MAX_COMP_ADC_DATA_SIZE (MAX_NEVENTS_PER_STREAM * 288 * 16 * 129)

// In reality, the L3 reduction factor is very high ~ x20, so a big buffer would
// be wasting a lot of memory. In practice we can use a smaller buffer. If
// D_COMP_ADC_DATA_BUFF_SIZE is set to MAX_COMP_DATA_SIZE, set
// MAX_EVENTS_PER_STREAM to  <= 50. Use nvidia-smi to check how much memory the GPU
// is taking at runtime with different configurations.
#define D_COMP_ADC_DATA_BUFF_SIZE (MAX_COMP_ADC_DATA_SIZE)





struct SpillStats
{
    // timestamp
    uint64_t spill_start_time = 0xFFFFFFFFFFFFFFFF;

    // stats
    uint32_t nAcceptedPackets = 0;
    uint32_t nAcceptedEvents[10] = {0};
    uint32_t nProcessedEvents = 0;

    uint32_t nRejectedEvents_total[10] = {0};
    // Rejected events by category
    uint32_t nRejectedEvents_MinXY[10] = {0};
    uint32_t nRejectedEvents_MaxR[10] = {0};
    uint32_t nRejectedEvents_TotalE[10] = {0};
    uint32_t nRejectedEvents_COE[10] = {0};

};

enum WFMHDR
{
    PDSUP_R = 100,
    PDSUP_DBG_TAG = 101,
    PDSUP_TAG = 102,
    PDSUP_DBG_2MSBS = 0b11,
    RESET_TAG = 999
};


enum l3LogLevel {
    L3_INFO  = 0,
    L3_WARN  = 1,
    L3_ERROR = 2,
    L3_DEBUG = 3
};

enum l3VerboseLevel {
    L3_QUIET = 0,
    L3_NORMAL = 1,
    L3_VERBOSE = 2,
};


class L3log
{
    public:
        L3log();
        ~L3log();
        void set_verbose_level(int verbose_level){ m_verbose_level = verbose_level; }
        void set_hostname(std::string hostname){ m_hostname = hostname; }
        void log(int verbose_level, int severity, const char * format, ...);
        void print_banner();
    private:
        int m_verbose_level;
        std::string m_hostname = "somenode";
}; 
extern L3log l3log; // L3log object

enum {
    REC_RECO_5G = 1,    // 5G RECO    + SEL + PEDSUP + COMPRESS
    REC_RECO_KP = 2,    // KP RECO    + SEL + PEDSUP + COMPRESS
    REC_RECO_PI0EE = 3, // PI0EE RECO + SEL + PEDSUP + COMPRESS

    SEL_THRESHOLD = 10, // Less than this value and will be selected.

    REC_PEDSUP = 50,    // PEDSUP + COMPRESS
    
    REC_PASS = 100,     // COMPRESS 
    REC_WEIRD = 101,    // Weird events that are accepted as they are. E.g. events where more than half of the CsI channels have hits

    REC_LUCKY_BIT =  (1<<8),    // will go through the selection, but if it has this bit enabled won't be rejected.
    
    REJ_THRESHOLD =  (1<<9)-1,  // events with tag > REJ_THRESHOLD have been rejected.
    REJ_MINXY_BIT =  (1<<9),    // rejected by MINXY cut
    REJ_MAXR_BIT =   (1<<10),   // rejected by MAXR cut
    REJ_TOTALE_BIT = (1<<11),   // rejected by TOTALE cut
    REJ_COE_BIT =    (1<<12)    // rejected by COE cut
};
