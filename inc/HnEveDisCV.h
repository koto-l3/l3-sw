#ifndef HnLib_HnEveDisCV_h
#define HnLib_HnEveDisCV_h

#include "HnEveDisDetector.h"


namespace HnLib
{
  class HnEveDisCV : public HnEveDisDetector
  {
  public:
    HnEveDisCV(int userFlag);
    virtual ~HnEveDisCV();

    bool setFrontVisible(bool isVisible);
    bool setRearVisible(bool isVisible);
    bool setSpecificModuleVisible(std::vector<int> v_VisibleModule);
    void resetFront(double val=0);
    void resetRear(double val=0);

    void drawModuleID(std::string option="l text");
    
  protected:
    HnEveDisCV();

    void fgetCenter(double width,double l1,double l2,double d,
		    double& xCenter,double& yCenter,double& zCenter);
    void fmakeParameter(double width,double l1,double l2,double d,
			std::vector<double>& paramVec);
    void rgetCenter(double width,double l,
		    double& xCenter,double& yCenter,double& zCenter);
    void rmakeParameter(double width,double l,
			std::vector<double>& paramVec);

    bool m_isFrontVisible;
    bool m_isRearVisible;
  };
}

#endif // HnLib_HnEveDisCV_h
