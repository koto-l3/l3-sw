#ifndef LayerTilesConstants_h
#define LayerTilesConstants_h

#include <cstdint>
#include <array>

#define NLAYERS 1

namespace LayerTilesConstants {

  constexpr int32_t ceil(float num) {
    return (static_cast<float>(static_cast<int32_t>(num)) == num) ? static_cast<int32_t>(num) : static_cast<int32_t>(num) + ((num > 0) ? 1 : 0);
  }

  constexpr float minX = 0;
  constexpr float maxX = 140;
  constexpr float minY = 0;
  constexpr float maxY = 140;
  constexpr int nColumns = 14;
  constexpr int nRows    = 14;
  constexpr int maxTileDepth = 25; // maximum number of hits per tile

  constexpr float rX = nColumns/(maxX-minX);
  constexpr float rY = nRows/(maxY-minY);

}

#endif // LayerTilesConstants_h

