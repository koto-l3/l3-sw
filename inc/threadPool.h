#pragma once

#include <mutex>
#include <queue>
#include <vector>
#include <future>
#include <thread>
#include <functional>
#include <condition_variable>



class ThreadPool {
public:
    ThreadPool(int numThreads);

    template<class F, class... Args>
    auto async(F&& f, Args&&... args) 
        -> std::future<typename std::result_of<F(Args...)>::type>;

    void waitFinished();
    void thread_proc();

    ~ThreadPool();

private:
    std::vector<std::thread> m_threads;
    //std::queue<std::function<void()>> m_tasks;
    std::deque< std::function<void()> > m_tasks;

    std::mutex m_queueMutex;
    std::condition_variable m_condition;
    std::condition_variable m_finished;
    std::string m_label;
    bool m_stop;
    std::atomic_uint m_busy;
};





template<class F, class... Args>
auto ThreadPool::async(F&& f, Args&&... args) 
    -> std::future<typename std::result_of<F(Args...)>::type>
{
    using return_type = typename std::result_of<F(Args...)>::type;

    auto task = std::make_shared< std::packaged_task<return_type()> >(
            std::bind(std::forward<F>(f), std::forward<Args>(args)...)
        );
        
    std::future<return_type> res = task->get_future();
    {
        std::unique_lock<std::mutex> lock(m_queueMutex);

        // don't allow enqueueing after stopping the pool
        if(m_stop)
            throw std::runtime_error("enqueue on stopped ThreadPool");

        m_tasks.emplace_back([task](){ (*task)(); });
        if (m_label == "tpSelector")
            printf("ThreadPool: Task added. Tasks Left: %lu\n", m_tasks.size());
    }
    m_condition.notify_one();
    return res;
}