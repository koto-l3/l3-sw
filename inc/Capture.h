
#pragma once
#include <vector>
#include <memory>
//#include <array>
#include <future> // for u_char

#include "common.h"



struct glob_arg;
struct targ;
struct pcap_pkthdr
{
    uint8_t pktNo;
};

class CircularBufferManager
{
    public:

    CircularBufferManager(int nPkts);


    /**
     * @brief Removes nPkts packets from the buffer by advancing m_tail
     * 
     * @param nPkts Number of packets to remove from the buffer
     */
    void pop(int nPkts);
    int GetCapacity() const { return m_capacity; }

    /**
     * Retrieves the number of packets in the buffer
     * 
     * @return int Number of packets in the buffer
    **/
    int GetNPackets();
    int GetHead() const { return m_head; }
    int GetTail() const { return m_tail; }
    int GetIdxFromTail(int i) const { return (m_tail + i) % m_capacity; }
    int GetIdxFromIdx(int orig, int i) const { return (orig + i) % m_capacity; }

    /**
     * @brief Resets the buffer by setting m_head and m_tail to 0
     * 
     */
    void clear();

    int m_capacity; // Note that these are not atomic for performance reasons.
    int m_head;
    std::atomic<int> m_tail;
};


class Capturer
{

    public:

    /**
     * @brief Initializes buffers.
     * 
     */
    Capturer(Config * config);

    /**
     * @brief Prepares the 40G interface and initializes everything needed for packet capture
     * 
     * @return int 
     */
    int Run();

    /**
     * @brief Initialize packet capture buffers.
     * 
     * @return void* 
     */
    void * InitMemory();

    /**
     * @brief Initializes the capturing and reporting threads
     * 
     * @param g (struct glob_arg) Global arguments for all threads
     * @return int 
     */
    int StartThreads(glob_arg &g);
   
    /**
     * @brief Retrieves packets from the Netmap rings, extracts the OFC2 headers and
     * stores them in the L3 circular buffer
     *
     * @param targ_ Pointer to the thread arguments (struct targ)
     * @return void* 
     */    
    void * receiver_body(void *data);

    /**
     * @brief Attaches a thread to a specific CPU core
     * 
     * @param me the thread to attach
     * @param i the CPU core to attach the thread to
     * @return int 0 if successful, 1 otherwise
     */
    static int SetAffinity(pthread_t me, int i);
  
    /**
     * @brief One thread, the "reporting thread" is responsible for printing the
     * statistics of all capturing threads
     *
     * @param _g Pointer to glob_arg
     * @return void* 
     */  
    void * reporting_thread(void *_g);

    /**
     * @brief Clears the statistics counters for all threads
     * 
     */
    void ClearStats();


    /**
     * @brief Writes to a file each raw packet (one per line) that has been
     * captured, so the contents of the packets can be manually checked.
     */
    void Debug();

    std::vector<std::shared_ptr<std::array<u_char[PKT_SIZE-OFC2_HDR_SIZE], N_PKTS_PER_THREAD+1>>>* GetThreadBuff() { return &m_rawPktDataBuffer; } 
    std::vector<std::shared_ptr<std::array<struct pcap_pkthdr, N_PKTS_PER_THREAD>>>* GetRawHdrBuff() {  return &m_rawPktHdrBuffer; }
    std::vector<std::shared_ptr<CircularBufferManager>>* GetCircularBuffer(){ return &m_circularBufferManager; }

    std::mutex m;
    std::condition_variable cv;
    
    int m_total_packets = 0;
    bool m_pcap_finished = false;

    private:

    targ* targs;

    Config * m_config;
    std::unique_ptr<std::thread[]> m_threads = nullptr;


    /**
     * Class to manage the circular buffers (add/remove elements, retrieve indexes, etc.)
     * 
     */
    std::vector<std::shared_ptr<CircularBufferManager>> m_circularBufferManager;


    /**
    *  Underlying Buffer to hold the captured packets. The vector contains
    *  pointers (one per thread), that are mapped to arrays (contigous memory)
    *  where the packets are written. The allocation is done later by a thread
    *  attached to the NIC 's local NUMA node, so the memory is allocated also
    *  on the local node.
    */
    std::vector<std::shared_ptr<std::array<u_char[PKT_SIZE-OFC2_HDR_SIZE], N_PKTS_PER_THREAD+1>>> m_rawPktDataBuffer;


    /**
     * This is to contain anything that could be retrieved from the OFC2
     * headers. Currently, the only useful thing is the packet No. within the
     * event, but in the future we might also get trigger information through
     * here.
     */
    std::vector<std::shared_ptr<std::array<struct pcap_pkthdr, N_PKTS_PER_THREAD>>> m_rawPktHdrBuffer;



};
