#pragma once

#include <cuda_runtime.h>
#include <common.h>


class GPUData {
    public:
        float    * d_csi_energies;
        uint16_t * d_csi_times;
        //float    * d_tmp_csi_pedestals;
        //float    * d_tmp_csi_intADCs;
        //float    * d_tmp_csi_peakHeight;


        void AllocateDeviceMemory(int csi_n_channels)
        {
            cudaMalloc((void **)&d_csi_energies, MAX_NEVENTS_PER_STREAM * csi_n_channels * sizeof(float));
            
            
            //cudaMalloc((void **)&d_tmp_csi_pedestals, MAX_NEVENTS_PER_STREAM * csi_n_channels * sizeof(float));
            //cudaMalloc((void **)&d_tmp_csi_intADCs, MAX_NEVENTS_PER_STREAM * csi_n_channels * sizeof(float));
            //cudaMalloc((void **)&d_tmp_csi_peakHeight, MAX_NEVENTS_PER_STREAM * csi_n_channels * sizeof(float));
            cudaMalloc((void **)&d_csi_times, MAX_NEVENTS_PER_STREAM * csi_n_channels * sizeof(uint16_t));
        }

        void FreeDeviceMemory()
        {
            cudaFree(d_csi_energies);
            cudaFree(d_csi_times);
            //cudaFree(d_tmp_csi_pedestals);
            //cudaFree(d_tmp_csi_intADCs);
            //cudaFree(d_tmp_csi_peakHeight);

        }
};


class GPUDetectorInfo {

    public: 
        int * d_csi_x;
        int * d_csi_y;

        void AllocateDeviceMemory(int csi_n_channels)
        {
            cudaMalloc((void **)&d_csi_x, csi_n_channels * sizeof(int));
            cudaMalloc((void **)&d_csi_y, csi_n_channels * sizeof(int));
        }
};