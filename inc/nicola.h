#pragma once

#include <cuda_runtime.h>
#include "common.h"

#include <vector>


#define TILE_DIM 32
#define BLOCK_ROWS 8

struct Clusters {
  float *x;
  float *y;
  float *E;
  int *n;
  int *evtID;
};


inline
cudaError_t checkCuda(cudaError_t result)
{
#if defined(DEBUG) || defined(_DEBUG)
  if (result != cudaSuccess) {
    fprintf(stderr, "CUDA Runtime Error: %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
#endif
  return result;
}


extern Config conf;



// Kernel to transpose d_perFADCWfmData
__global__ void transpose(const uint16_t *d_idata, uint16_t *d_odata);


void launchTransposeKernel(const uint16_t *d_idata, uint16_t *d_odata, cudaStream_t stream);
void LaunchPedestalSuppressionKernel(uint16_t *d_perFADCWfmData,  uint16_t* d_wfmInfo, const uint16_t *d_CSIDetChIdToADCChId, const float *d_gainMeanCoefs, const float *d_ADCMevCoefs, int csi_n_channels, cudaStream_t stream, int nLoadedEvents, uint16_t *d_recoTag, uint8_t *d_csiChsMaskForPedSup);
uint32_t launchPreCompressKernel(uint16_t *d_perFADCWfmData, uint32_t* d_perEvtSizeCompressed, uint32_t* d_perEvtIdxCompressed, u_int32_t* h_perEvtSizeCompressed, uint32_t* h_perEvtIdxCompressed, uint16_t *d_wfmInfo, uint32_t *d_compressedBytes, cudaStream_t stream, int nLoadedEvents, uint16_t *d_recoTag);
void LaunchCompressKernel(uint16_t *d_perFADCWfmData, uint8_t* d_perEvtHeaderData, uint8_t *d_odata, uint16_t *d_wfmInfo, uint32_t* dh_perEvtIdxCompressed, uint32_t* dh_perEvtSizeCompressed, uint32_t *d_compressedBytesPerWfm, cudaStream_t stream, int nLoadedEvents, uint16_t *d_recoTag);
void LaunchReconstructionKernel(uint16_t *d_idata, uint16_t *d_wfmInfo, float *d_csiEnergies, uint16_t *d_csiTimes, const uint16_t *d_CSIDetChIdToADCChId, const float *d_gainMeanCoefs, const float *d_ADCMevCoefs, int csi_n_channels, cudaStream_t stream, int nLoadedEvents, uint16_t *d_triggerBits, uint8_t *d_csiChsMaskForReco);
void launchSelectKernel(Clusters *d_clusters, uint16_t *d_recoTag, cudaStream_t stream, int nEvents);

void launchCalcTotalEKernel(float* d_energies, float* d_csi_energies, uint16_t* d_csiTimes, int csi_n_channels, int nEvents, cudaStream_t stream);

void Decompress(void *h_idata, uint8_t *h_odata, int nWfms, int i_evt);