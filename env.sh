
set -e

#sudo ethtool -U ens7f0 rx-flow-hash udp4 v


# Array of devices
DEVS=("ens7f0" "ens7f1")
DEVS=("ens6f1" "ens6f1")
DEVS=("ens6f0" "ens6f1")

DRV_VER="2.21.12"
DRV_VER="2.20.12"
DRV_VER="2.22.8"
DRV_VER="2.22.18"
DRV_VER="2.23.17"
NETMAP_BASE="/home/koto/local/netmap"

# Set Netmap affinity to local NUMA node, etc.
for DEV in "${DEVS[@]}"
do 
    ethtool -L $DEV combined 8
done

#$NETMAP_BASE/LINUX/i40e-$DRV_VER/scripts/set_irq_affinity -x 1,3,5,7,9,11,13,15 ${DEVS[0]}
#$NETMAP_BASE/LINUX/i40e-$DRV_VER/scripts/set_irq_affinity -x 17,19,21,23,25,27,29,31 ${DEVS[1]}

#$NETMAP_BASE/LINUX/i40e-$DRV_VER/scripts/set_irq_affinity -x 0,2,4,6,8,10,12,14 ${DEVS[0]}
#$NETMAP_BASE/LINUX/i40e-$DRV_VER/scripts/set_irq_affinity -x 16,18,20,22,24,26,28,30 ${DEVS[0]}
#$NETMAP_BASE/LINUX/i40e-$DRV_VER/scripts/set_irq_affinity -x 16,17,18,19,20,21,22,23 ${DEVS[0]}

$NETMAP_BASE/LINUX/i40e-$DRV_VER/scripts/set_irq_affinity -x local ${DEVS[0]}
#$NETMAP_BASE/LINUX/i40e-$DRV_VER/scripts/set_irq_affinity -x 0,2,4,6,8,10,12,14 $DEV
#$NETMAP_BASE/LINUX/i40e-$DRV_VER/scripts/set_irq_affinity -x 9,11,13,15 $DEV
#$NETMAP_BASE/LINUX/i40e-$DRV_VER/scripts/set_irq_affinity -x all $DEV

for DEV in "${DEVS[@]}"
do 
    # promiscuous mode
    ip link set $DEV promisc on
    sudo ip link set $DEV mtu 9000


    # ethtool defaults
    ethtool -C $DEV adaptive-rx on
    ethtool -G $DEV rx 512
    ethtool -A $DEV tx off rx off
    #ethtool -K $DEV tx off rx off gso off tso off gro off lro off
done

# To use netmap
sudo chmod o+rw /dev/netmap

for DEV in "${DEVS[@]}"
do 
    # Netmap reccomended
    ethtool -A $DEV tx off rx off
    ethtool -K $DEV tx off rx off gso off tso off gro off lro off
    
    # ethtool modified
    ethtool -C $DEV adaptive-rx on
    #ethtool -C $DEV rx-usecs 1000
    ethtool -G $DEV rx 2048
    ethtool -X $DEV hkey 00:00:00:00:0A:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
done
# Netmap
echo 36864 > /sys/module/netmap/parameters/ring_size
echo 163840 > /sys/module/netmap/parameters/buf_num

echo 9064 > /sys/module/netmap/parameters/buf_size
echo 1 > /sys/module/netmap/parameters/verbose
#echo 83840 > /sys/module/netmap/parameters/buf_num
#echo 147456 > /sys/module/netmap/parameters/ring_size


# sysctl defaults
/sbin/sysctl -w net.core.wmem_max=212992
/sbin/sysctl -w net.core.rmem_max=212992
/sbin/sysctl -w net.ipv4.tcp_rmem="4096	131072	6291456"
/sbin/sysctl -w net.ipv4.tcp_wmem="4096	131072	6291456"
/sbin/sysctl -w net.core.netdev_max_backlog=1000
/sbin/sysctl -w net.ipv4.tcp_timestamps=1
/sbin/sysctl -w net.ipv4.tcp_mtu_probing=0
/sbin/sysctl -w net.ipv4.route.flush=1
/sbin/sysctl -w net.ipv4.tcp_low_latency=0
/sbin/sysctl -w net.ipv4.tcp_sack=1
/sbin/sysctl -w net.core.netdev_max_backlog=250000
/sbin/sysctl -w net.core.netdev_budget=300

# sysctl modified
echo "" > /etc/sysctl.d/99-sysctl-custom.conf
echo "net.core.wmem_max = 104857600" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.core.rmem_max = 104857600" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.ipv4.tcp_rmem = 4096 524288 104857600" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.ipv4.tcp_wmem = 4096 524288 104857600" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.core.netdev_max_backlog = 250000" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.ipv4.tcp_timestamps = 0" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.ipv4.tcp_mtu_probing = 1" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.ipv4.tcp_low_latency = 1" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.ipv4.tcp_sack = 0" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.core.netdev_max_backlog=30000" >> /etc/sysctl.d/99-sysctl-custom.conf
echo "net.ipv4.route.flush = 1" >> /etc/sysctl.d/99-sysctl-custom.conf

# reload 
#sysctl -p /etc/sysctl.d/99-sysctl-custom.conf



