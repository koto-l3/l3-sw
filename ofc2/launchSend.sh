set -e

#g++ sendTestPkt.cpp -o send -lpcap $(root-config --glibs --cflags --libs)
g++ sendRawData.cpp -o send -lpcap $(root-config --glibs --cflags --libs) -g
#sudo chgrp pcap main
sudo chmod 750 send
sudo setcap cap_net_raw,cap_net_admin=eip send


./send $1 $2 $3
#./send $1 $2
