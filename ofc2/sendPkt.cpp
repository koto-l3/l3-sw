#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <vector>
#include <chrono>
#include <unistd.h>
#include <pcap.h>

#include "TFile.h"
#include "TTree.h"
#include "TChain.h"



struct counters{
    int sent_total = 0;
};
counters c;


int packetsPerChunk = 1000;
int packetLength = 9000;
int send_limit = 10000;
int64_t timestamp_0 = 0;





void deviceList()
{
    pcap_if_t *alldevs;
    pcap_if_t *d;
    int i = 0;
    char errbuf[PCAP_ERRBUF_SIZE];

    /* Retrieve the device list from the local machine */
    if (pcap_findalldevs(&alldevs, errbuf) == -1)
    {
        fprintf(stderr, "Error in pcap_findalldevs_ex: %s\n", errbuf);
        exit(1);
    }

    pcap_if_t *cur = alldevs;
    while (cur)
    {
        printf("Device: %s - %s\n", cur->name, cur->description ? cur->description : "");
        cur = cur->next;
    }

    /* Print the list */
    for (d = alldevs; d != NULL; d = d->next)
    {
        printf("%d. %s", ++i, d->name);
        if (d->description)
            printf(" (%s)\n", d->description);
        else
            printf(" (No description available)\n");
    }

    if (i == 0)
    {
        printf("\nNo interfaces found! sudo run me\n");
        return;
    }

    /* We don't need any more the device list. Free it */
    pcap_freealldevs(alldevs);
}


uint16_t swap(uint16_t x)
{ // swap the endianess
    uint16_t aa = (x & 0xff00) >> 8;
    uint16_t bb = ((x & 0x00ff) << 8) | aa;
    return bb;
}


int main()
{



    TChain *T = new TChain("T");
    
    // Add trees
    for (int i = 0; i < 7; i++){
        T->Add(Form("/home/koto/git-repos/l3-sw/ofc2/data/out_%d.root", i));
    }





    UChar_t packet[9000];
    T->SetBranchAddress("Packets", &packet);



    // open pcap device
    pcap_t *fp;
    char errbuf[PCAP_ERRBUF_SIZE];
    /* Open the output device */
    if ( (fp= pcap_open_live("ens6f1",         // name of the device
                        BUFSIZ,                // portion of the packet to capture (only the first 100 bytes)
                        PCAP_OPENFLAG_PROMISCUOUS,  // promiscuous mode
                        1000,               // read timeout
                        errbuf              // error buffer
                        ) ) == NULL)
    {
        printf("%s\n", errbuf);
        return -1;
    }


    int oldEvtID = -1;
    int packetNo = 0;

    int tmp = 0;
    //for (int iEntry = 67*1; T->LoadTree(iEntry) >= 0; iEntry+=0) {
    for (int iEntry = 0; T->LoadTree(iEntry) >= 0; iEntry++) {
        tmp++;


        T->GetEntry(iEntry);

        //packet[12] = 32;

        if (c.sent_total % packetsPerChunk == 0)
        {
            uint64_t counted_time = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();

            double time = (counted_time - timestamp_0); // ns
            double size = packetsPerChunk * packetLength * 8; // bits

            printf("Speed: %f Gbps\n", size / time);
            timestamp_0 = counted_time;
        }

        
        

        int evtID = packet[15] << 8 | packet[16];

        //packet[15] = packetNo >> 8;
        //packet[16] = packetNo & 0xff;



        //if (evtID != oldEvtID)
        //{
        //    // Print first wfm of event
        //    oldEvtID = evtID;
        //    printf("EventID: %d\n", evtID);
        //    for (int k = 18; k < 128+18; k+=2){
        //        printf("%d ", (packet[k+1] << 8) | packet[k]);
        //    }
        //    printf("\n");
        //}


        //if (oldEvtID == 7)
        //{
        //    printf("EventID: %d, packetNo = %d\n", evtID, packetNo);
        //    packetNo ++;
        //    for (int k = 0; k < 26; k++){
        //        printf("%d ", packet[k]);
        //    }
        //    //for (int k = 0; k < 32; k+=2){
        //    //    printf("%d ", (packet[k+1] << 8) | packet[k]);
        //    //}
        //    printf("\n");
        //}


        //if (iEntry == 0){
        //    for (int k = 18; k < 128+18; k+=2){
        //        printf("%d ", (packet[k+1] << 8) | packet[k]);
        //    }
        //    printf("\n");
        //}


        // Send down the packet
        if (pcap_sendpacket(fp, packet, packetLength /* size */) != 0)
        {
            fprintf(stderr, "\nError sending the packet: %s \n", pcap_geterr(fp));
            return -1;
        }

        packetNo ++;
        c.sent_total ++;


        if (iEntry > 67*12-2){
            break;
        }

    }
    printf("Sent %d packets, %d events\n", c.sent_total, c.sent_total/67);
    int nWfms = c.sent_total * 4 * 16;
    printf("Sent %d waveforms from %d fadcs\n", nWfms, c.sent_total/4);
    pcap_close(fp);
    return 0;
}





