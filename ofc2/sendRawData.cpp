#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <vector>
#include <chrono>
#include <unistd.h>
#include <pcap.h>

#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TClonesArray.h"





// OFC2 bus length 320 bits (40Gbps at 125 Mhz)
// OFC2 header and ETH header sent out first clock (1 320 bit word)
// ONE OFC2 packet contains 1 320 bit word header + 219 320 bit words data (i.e. 4380 16 bit words)

#define USE_SINGLE_MAC 1

#define ADC_HDR_SIZE 6 // 16 bit words
#define ADC_NCHANELS 16
#define ADC_NSAMPLES_PER_CH 64  // 1 sample is 16 bit
#define NCRATES 18
#define NADCS_PER_CRATE 16
#define ADC_FOOTER_SIZE 2 // 16 bit words

#define CRATE_FOOTER_SIZE 8 // 16 bit words

#define FULL_FADC_SIZE  (ADC_HDR_SIZE+ADC_NCHANELS*ADC_NSAMPLES_PER_CH+ADC_FOOTER_SIZE)
#define FULL_CRATE_SIZE (NADCS_PER_CRATE*FULL_FADC_SIZE+CRATE_FOOTER_SIZE)

// 40 [Gb/s] / 125[clks/10^6s]*10^9[b/Gb] = 320 [bits/clk]
// 320 [bits/clk] = 20 [16 bit words/clk] = 40 [bytes/clk]
// 8800 = 40 + 40*219. The 219 was fixed by God.
#define PKT_SIZE 8800 // bytes
#define OFC2_HDR_SIZE 40 // bytes (20 16 bit words)
#define OFC2_DATA_SIZE (PKT_SIZE - OFC2_HDR_SIZE) // bytes

#define NEVTS 75000



void writeADCHeader(uint16_t * adcHdr, uint16_t spillNo, uint8_t slotNo, uint8_t crateNo, uint16_t eventNo){

    //adcHdr[0] = 1, 1, spillNo[3, .., 0], slotNo[4, .., 0], crateNo[4, .., 0]
    //adcHdr[1] = 1, 1, eventNo[7, .., 0], spillNo[9, .., 4]
    //adcHdr[2] = 1, 1, timestamp[5, .., 0], eventNo[15, .., 8]
    //adcHdr[3] = 1, 1, timestamp[19, .., 6]
    //adcHdr[4] = 1, 1, clusBit[4, .., 0], timestamp[28, .., 20]
    //adcHdr[5] = 0, 0, 0, clusBit[15, .., 5]
    //printf("\n");

    uint32_t timestamp = 123456;
    uint16_t clusBit = 222;

    // TODO: Note that the indexes are inverted because of the little/big endian thing.
    adcHdr[0] = (1 << 15) + (1 << 14) + ((spillNo & 0xF) << 10) + ((slotNo & 0x1F) << 5) + (crateNo & 0x1F);
    adcHdr[1] = (1 << 15) + (1 << 14) + ((eventNo & 0xFF) << 6) + (spillNo >> 4 & 0x3F);
    adcHdr[2] = (1 << 15) + (1 << 14) + ((timestamp & 0x3F) << 8) + (eventNo >> 8 & 0xFF);
    adcHdr[3] = (1 << 15) + (1 << 14) + (timestamp >> 6 & 0x3FFF);
    adcHdr[4] = (1 << 15) + (1 << 14) + ((clusBit & 0x1F) << 9) + (timestamp >> 20 & 0x1FF);
    adcHdr[5] = (1 << 15) + (1 << 14) + (0 << 13) + (clusBit >> 5 & 0x7FF);

    //adcHdr[0] = 0xab;
    //adcHdr[1] = 0xab;
    //adcHdr[2] = 0xab;
    //adcHdr[3] = 0xab;
    //adcHdr[4] = 0xab;
    //adcHdr[5] = 0xab;

    //printf("adcHdr[0]: %x\n", adcHdr[0]);
    //printf("adcHdr[1]: %x\n", adcHdr[1]);
    //printf("adcHdr[2]: %x\n", adcHdr[2]);
    //printf("adcHdr[3]: %x\n", adcHdr[3]);
    //printf("adcHdr[4]: %x\n", adcHdr[4]);
    //printf("adcHdr[5]: %x\n", adcHdr[5]);

    //printf("spillNo: %d\n", ((adcHdr[0] & 0x3F) << 4) + ((adcHdr[1] >> 10) & 0xF));
    //printf("slotNo: %d\n", adcHdr[1] >> 5 & 0x1F);
    //printf("crateNo: %d\n", adcHdr[0] & 0x1F);
    //printf("eventNo: %d\n", ((adcHdr[3] & 0xFF) << 8) + (adcHdr[0] >> 6 & 0xFF));
    //printf("timestamp: %llu\n", ((adcHdr[5] & 0x1FF) << 20) + ((adcHdr[2] & 0x3FFF) << 6) + ((adcHdr[3] >> 8) & 0x3F));
    //printf("clusBit: %d\n", ((adcHdr[4] & 0x7FF) << 5) + ((adcHdr[5] >> 9) & 0x1F));
    //printf("--------------------\n");

}

class E14RawFADCDataRUN79 : public TObject
{
public:


    // Header data
    std::vector<UInt_t> HeaderWord;

    Int_t L2SlotID;
    Int_t L2FiberID;
    Int_t L2AR;
    Int_t CrateID;
    Int_t SlotNo;
    Int_t ModuleID;
    Int_t TimeStamp;
    Int_t L1TrigNo;
    Int_t L2TrigNo;
    Int_t SpillID;
    Int_t FADCFreqNo;
    Int_t CompressionFlag[16];

    // Waveform Data
    Short_t Data[16][128];
    Int_t CompressionMode[16];

    // CDT & PS bits
    std::vector<UInt_t> COEWord;

    UInt_t CDTBit;
    UInt_t PSBit;

    // Error flags
    Bool_t HeaderError;
    Bool_t ModuleIDError;
    Bool_t SpillIDError;
    Bool_t UnrecognizedModuleError;

};



int main(int argc, char *argv[])
{


    if (argc != 4) {
        printf("Usage: %s <rawData.root> <nEvts> <spillNo>\n", argv[0]);
        return 1;
    }

    std::string filename = argv[1];
    int nEvts = atoi(argv[2]);
    int spillNo = atoi(argv[3]);

    printf("filename: %s; nEvts: %d; spillNo: %d\n", filename.c_str(), nEvts, spillNo);

    int sizeOfFullADC = (ADC_HDR_SIZE + ADC_NCHANELS * ADC_NSAMPLES_PER_CH) * 2; // bytes
    printf("one full ADC (header + data) is %d bytes\n", sizeOfFullADC);
    int sizeOfAllADCs = sizeOfFullADC * NCRATES * NADCS_PER_CRATE;
    printf("One whole evt worth of ADC data is %d bytes\n", sizeOfAllADCs);
    int ADCDataSizePerOFC2Pkt = PKT_SIZE - OFC2_HDR_SIZE;

    uint16_t adcHdr[NCRATES][NADCS_PER_CRATE][ADC_HDR_SIZE];
    uint16_t adcEne[NCRATES][NADCS_PER_CRATE][ADC_NCHANELS][ADC_NSAMPLES_PER_CH];
    uint16_t adcFtr[ADC_FOOTER_SIZE];
    uint16_t adcData[NCRATES][NADCS_PER_CRATE * FULL_FADC_SIZE + CRATE_FOOTER_SIZE];


    uint16_t * adcDataPtr = &adcData[0][0];
    uint16_t * adcDataPtrEnd = &adcData[NCRATES-1][NADCS_PER_CRATE * FULL_FADC_SIZE + CRATE_FOOTER_SIZE - 1];
    uint8_t ofc2Header[OFC2_HDR_SIZE];

    //printf("Expected n of pkts: %d", (adcDataPtrEnd - adcDataPtr)/(OFC2_DATA_SIZE/2));

    // ADC Header
    for (int i_crate = 0; i_crate < NCRATES; i_crate++){
        for (int i_adc = 0; i_adc < NADCS_PER_CRATE; i_adc++){
            for (int i_hdr_wrd = 0; i_hdr_wrd < ADC_HDR_SIZE; i_hdr_wrd++){
                adcHdr[i_crate][i_adc][i_hdr_wrd] = 0;
            }
        }
    }

    // OFC2 header
    for (int i = 0; i < OFC2_HDR_SIZE; i++){
        ofc2Header[i] = i;
    }


    std::vector<std::array<uint8_t, 8>> snMacs = {
//        {0xf8, 0xf2, 0x1e, 0xe6, 0xd8, 0x00},
//        {0xf8, 0xf2, 0x1e, 0xe6, 0xcc, 0xb0},
//        {0xf8, 0xf2, 0x1e, 0xe6, 0xd3, 0x80},
//        {0xf8, 0xf2, 0x1e, 0xe6, 0xcf, 0x50},
//        {0xf8, 0xf2, 0x1e, 0xe6, 0xc7, 0xe0}
//        //{0xf8, 0xf2, 0x1e, 0xe6, 0xd4, 0x30}

        // ens6f1
        {0xf8, 0xf2, 0x1e, 0xe6, 0xd8, 0x01},
        {0xf8, 0xf2, 0x1e, 0xe6, 0xcc, 0xb1},
        {0xf8, 0xf2, 0x1e, 0xe6, 0xd3, 0x81},
        {0xf8, 0xf2, 0x1e, 0xe6, 0xcf, 0x51}
        //{0xf8, 0xf2, 0x1e, 0xe6, 0xc7, 0xe1}      

    };



    if (USE_SINGLE_MAC)
    {
        // dst MAC
        ofc2Header[0] = (uint8_t)0xf8;
        ofc2Header[1] = (uint8_t)0xf2;
        ofc2Header[2] = (uint8_t)0x1e;
        ofc2Header[3] = (uint8_t)0xdf;
        ofc2Header[4] = (uint8_t)0x63;
        ofc2Header[5] = (uint8_t)0x80;

        // sn1
//        ofc2Header[0] = (uint8_t)0xf8;
//        ofc2Header[1] = (uint8_t)0xf2;
//        ofc2Header[2] = (uint8_t)0x1e;
//        ofc2Header[3] = (uint8_t)0xe6;
//        ofc2Header[4] = (uint8_t)0xd8;
//        ofc2Header[5] = (uint8_t)0x00;
    }

    // src MAC (sn6, F8:F2:1E:E6:D4:30)
    ofc2Header[6] =  0xf8;
    ofc2Header[7] =  0xf2;
    ofc2Header[8] =  0x1e;
    ofc2Header[9] =  0xe6;
    ofc2Header[10] = 0xd4;
    ofc2Header[11] = 0x28;

    // eth type (set later for every packet)
    ofc2Header[12] = 0x00;
    ofc2Header[13] = 0x00;



    // ROOT
    TChain *T = new TChain("T");
    T->Add(filename.c_str());

    Short_t m_adc_data[288][16][64];
    Short_t m_adc_hdrs[288][6];


    T->SetBranchAddress("m_adc_data", &m_adc_data);
    T->SetBranchAddress("m_adc_hdrs", &m_adc_hdrs);


    // PCAP
    pcap_t *fp;
    char errbuf[PCAP_ERRBUF_SIZE];
    if ( (fp = pcap_open_live("ens7f0",         // name of the device
                        BUFSIZ,                // portion of the packet to capture (only the first 100 bytes)
                        PCAP_OPENFLAG_PROMISCUOUS,  // promiscuous mode
                        1000,               // read timeout
                        errbuf              // error buffer
                        ) ) == NULL)
    {
        printf("%s\n", errbuf);
        return -1;
    }


    uint8_t packet[PKT_SIZE];

    uint32_t sent_pkt_count = 0;

    int i_byte = 0;
    uint64_t t0 = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();

    for (int i_evt = 0; T->LoadTree(i_evt) >= 0; i_evt++) {

        T->GetEntry(i_evt);



        // set dst mac from list
        if (! USE_SINGLE_MAC)
        {
            ofc2Header[0] = snMacs[i_evt % 4][0];
            ofc2Header[1] = snMacs[i_evt % 4][1];
            ofc2Header[2] = snMacs[i_evt % 4][2];
            ofc2Header[3] = snMacs[i_evt % 4][3];
            ofc2Header[4] = snMacs[i_evt % 4][4];
            ofc2Header[5] = snMacs[i_evt % 4][5];
        }









        int evtNo = ((m_adc_hdrs[0*NADCS_PER_CRATE + 0][2] & 0xFF) << 8) + (m_adc_hdrs[0*NADCS_PER_CRATE + 0][1] >> 6 & 0xFF);
        //printf("evtNo: %d\n", evtNo);



        // prepare ADC data
        for (int i_crate = 0; i_crate < NCRATES; i_crate++){
            for (int i_adc = 0; i_adc < NADCS_PER_CRATE; i_adc++){

                Short_t * adcHdr = m_adc_hdrs[i_crate*NADCS_PER_CRATE + i_adc];

                //     adcHdr[0] = (1 << 15) + (1 << 14) + ((spillNo & 0xF) << 10) + ((slotNo & 0x1F) << 5) + (crateNo & 0x1F);
                //     adcHdr[1] = (1 << 15) + (1 << 14) + ((eventNo & 0xFF) << 6) + (spillNo >> 4 & 0x3F);

                // write spillNo to adcHdr
                // 1. clear the spillNo bits
                adcHdr[0] &= ~(0xF << 10);
                adcHdr[1] &= ~(0x3F);

                // 2. write spillNo
                adcHdr[0] |= (spillNo & 0xF) << 10;
                adcHdr[1] |= (spillNo >> 4 & 0x3F);

                


                //printf("spillNo: %d ( = %d)\n", ((adcHdr[1] & 0x3F) << 4) + ((adcHdr[0] >> 10) & 0xF), spillNo);
                //printf("slotNo: %d\n", adcHdr[0] >> 5 & 0x1F);
                //printf("crateNo: %d\n", adcHdr[0] & 0x1F);
                //printf("eventNo: %d\n", ((adcHdr[3] & 0xFF) << 8) + (adcHdr[0] >> 6 & 0xFF));
                //printf("timestamp: %llu\n", ((adcHdr[5] & 0x1FF) << 20) + ((adcHdr[2] & 0x3FFF) << 6) + ((adcHdr[3] >> 8) & 0x3F));
                //printf("clusBit: %d\n", ((adcHdr[4] & 0x7FF) << 5) + ((adcHdr[5] >> 9) & 0x1F));
                //printf("--------------------\n");

                //printf("adcHdr[0]: %d\n", adcHdr[0]);


                //writeADCHeader(&adcData[i_crate][i_adc][0], spillNo, i_adc, i_crate, i_evt);
                //writeADCHeader(&adcData[i_crate][i_adc*FULL_FADC_SIZE], spillNo, i_adc, i_crate, i_evt);
                adcData[i_crate][i_adc*FULL_FADC_SIZE + 0] = m_adc_hdrs[i_crate*NADCS_PER_CRATE + i_adc][0];
                adcData[i_crate][i_adc*FULL_FADC_SIZE + 1] = m_adc_hdrs[i_crate*NADCS_PER_CRATE + i_adc][1];
                adcData[i_crate][i_adc*FULL_FADC_SIZE + 2] = m_adc_hdrs[i_crate*NADCS_PER_CRATE + i_adc][2];
                adcData[i_crate][i_adc*FULL_FADC_SIZE + 3] = m_adc_hdrs[i_crate*NADCS_PER_CRATE + i_adc][3];
                adcData[i_crate][i_adc*FULL_FADC_SIZE + 4] = m_adc_hdrs[i_crate*NADCS_PER_CRATE + i_adc][4];
                adcData[i_crate][i_adc*FULL_FADC_SIZE + 5] = m_adc_hdrs[i_crate*NADCS_PER_CRATE + i_adc][5];
            }
        }



        // ADC Ene
        for (int i_crate = 0; i_crate < NCRATES; i_crate++){
            for (int i_adc = 0; i_adc < NADCS_PER_CRATE; i_adc++){

                for (int i_ch = 0; i_ch < ADC_NCHANELS; i_ch++){
                    for (int i_smpl = 0; i_smpl < ADC_NSAMPLES_PER_CH; i_smpl++){
                        adcEne[i_crate][i_adc][i_ch][i_smpl] = m_adc_data[i_crate*NADCS_PER_CRATE + i_adc][i_ch][i_smpl];
                    }
                }
            }
        }


        // total ADC
        for (int i_crate = 0; i_crate < NCRATES; i_crate++){
            for (int i_adc = 0; i_adc < NADCS_PER_CRATE; i_adc++){

                // data
                for (int i_ch = 0; i_ch < ADC_NCHANELS; i_ch++){
                    for (int i_sample = 0; i_sample < ADC_NSAMPLES_PER_CH; i_sample++){
                        //adcData[i_crate][i_adc][ADC_HDR_SIZE + i_sample*16 + i_ch] = adcEne[i_crate][i_adc][i_sample][i_ch];
                        adcData[i_crate][i_adc * FULL_FADC_SIZE + ADC_HDR_SIZE + i_ch*ADC_NSAMPLES_PER_CH + i_sample] = adcEne[i_crate][i_adc][i_ch][i_sample];
                    }
                }
                // ftr
                for (int i_ftr_wrd = 0; i_ftr_wrd < ADC_FOOTER_SIZE; i_ftr_wrd++){
                    //adcData[i_crate][i_adc][ADC_HDR_SIZE + ADC_NCHANELS * ADC_NSAMPLES_PER_CH + i_ftr_wrd] = 0;
                    adcData[i_crate][i_adc * FULL_FADC_SIZE + ADC_HDR_SIZE + ADC_NCHANELS * ADC_NSAMPLES_PER_CH + i_ftr_wrd] = 0xcccc;
                }
            }
            // crate ftr
            for (int i_ftr_wrd = 0; i_ftr_wrd < 8; i_ftr_wrd++){
                adcData[i_crate][NADCS_PER_CRATE * FULL_FADC_SIZE + i_ftr_wrd] = 0;
            }
        }



        //Build packets from the ADC data until we have covered all the ADCs (whole event)
        int i_pkt = 0;
        bool evtFinished = false;



        while (1)
        {
            // eth type
            ofc2Header[12] = 32 + i_evt % 8;

            // pktNo
            ofc2Header[19] = i_pkt & 0xff;

            // evtNo
            ofc2Header[20] = evtNo >> 8;
            ofc2Header[21] = evtNo & 0xff;

            // OFC2 Header (40 bytes)
            for (int i = 0; i < OFC2_HDR_SIZE; i++){
                packet[i] = ofc2Header[i];
            }
            i_byte += OFC2_HDR_SIZE;

            // OFC2 Data
            if (adcDataPtr + OFC2_DATA_SIZE/2 >= adcDataPtrEnd){
                // If this is the last packet of the evt
                //printf("adcDataPtrEnd %d\n", adcDataPtrEnd - &adcData[0][0]);
                for (int i_word = 0; i_word < adcDataPtrEnd+1 - adcDataPtr; i_word++){
                    packet[i_byte + i_word*2] = adcDataPtr[i_word] & 0xff;
                    packet[i_byte + i_word*2 + 1] = adcDataPtr[i_word] >> 8;
                }
                i_byte += (adcDataPtrEnd+1 - adcDataPtr)*2;

                evtFinished = true;
                //// print full packet
                //for (int i = 0; i < i_byte; i++){
                //    printf("%02x ", packet[i]);
                //}
                //printf("\n");
                //sleep(1);
            }
            else{
                for (int i_word = 0; i_word < OFC2_DATA_SIZE/2; i_word++){
                    packet[i_byte + i_word*2] = adcDataPtr[i_word] & 0xff;
                    packet[i_byte + i_word*2 + 1] = adcDataPtr[i_word] >> 8;
                }
                adcDataPtr += OFC2_DATA_SIZE/2;
                i_byte += OFC2_DATA_SIZE;
            }
            

            // skip some packets to simulate network traffic
            if (sent_pkt_count != -2000){

                // send pkt here
                if (pcap_sendpacket(fp, packet, i_byte ) != 0)
                {
                    fprintf(stderr, "\nError sending the packet: \n", pcap_geterr(fp));
                    return -1;
                }
            }
            sent_pkt_count ++;
            //printf("Packet %d sent, (%d bytes)\n", i_pkt, i_byte);

            i_pkt ++;
            i_byte = 0;
            if (evtFinished) break;





        }

        printf("i_evt %d sent (evtNo %d, spillNo %d)\n", i_evt, evtNo, spillNo);
        adcDataPtr = &adcData[0][0];

        if (i_evt == nEvts - 1) break;

    }
    uint64_t t1 = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();


    printf("Sent %d events, %d packets\n", nEvts, sent_pkt_count);

    double time = (t1 - t0); // in ns
    double sent_size = (double)nEvts * (OFC2_HDR_SIZE + 288*(ADC_HDR_SIZE*2 + ADC_NCHANELS*ADC_NSAMPLES_PER_CH*2 + ADC_FOOTER_SIZE*2)); // in bytes
    printf("Speed: %f Gbps\n", sent_size*8/time);

}





