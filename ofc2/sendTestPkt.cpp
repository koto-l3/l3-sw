
#include <iostream>
#include <pcap.h>
#include <chrono>
#include <vector>
#include <array>
#include <unistd.h>


// OFC2 bus length 320 bits (40Gbps at 125 Mhz)
// OFC2 header and ETH header sent out first clock (1 320 bit word)
// ONE OFC2 packet contains 1 320 bit word header + 219 320 bit words data (i.e. 4380 16 bit words)

#define ADC_HDR_SIZE 6 // 16 bit words
#define ADC_NCHANELS 16
#define ADC_NSAMPLES_PER_CH 64  // 1 sample is 16 bit
#define NCRATES 18
#define NADCS_PER_CRATE 16
#define ADC_FOOTER_SIZE 2 // 16 bit words

#define CRATE_FOOTER_SIZE 8 // 16 bit words

#define FULL_FADC_SIZE  (ADC_HDR_SIZE+ADC_NCHANELS*ADC_NSAMPLES_PER_CH+ADC_FOOTER_SIZE)
#define FULL_CRATE_SIZE (NADCS_PER_CRATE*FULL_FADC_SIZE+CRATE_FOOTER_SIZE)

// 40 [Gb/s] / 125[clks/10^6s]*10^9[b/Gb] = 320 [bits/clk]
// 320 [bits/clk] = 20 [16 bit words/clk] = 40 [bytes/clk]
// 8800 = 40 + 40*219. The 219 was fixed by God.
#define PKT_SIZE 8800 // bytes
#define OFC2_HDR_SIZE 40 // bytes (20 16 bit words)
#define OFC2_DATA_SIZE (PKT_SIZE - OFC2_HDR_SIZE) // bytes

#define NEVTS 75000



void writeADCHeader(uint16_t * adcHdr, uint16_t spillNo, uint8_t slotNo, uint8_t crateNo, uint16_t eventNo){

    //adcHdr[0] = 1, 1, spillNo[3, .., 0], slotNo[4, .., 0], crateNo[4, .., 0]
    //adcHdr[1] = 1, 1, eventNo[7, .., 0], spillNo[9, .., 4]
    //adcHdr[2] = 1, 1, timestamp[5, .., 0], eventNo[15, .., 8]
    //adcHdr[3] = 1, 1, timestamp[19, .., 6]
    //adcHdr[4] = 1, 1, clusBit[4, .., 0], timestamp[28, .., 20]
    //adcHdr[5] = 0, 0, 0, clusBit[15, .., 5]
    //printf("\n");

    uint32_t timestamp = 123456;
    uint16_t clusBit = 222;

    // TODO: Note that the indexes are inverted because of the little/big endian thing.
    adcHdr[0] = (1 << 15) + (1 << 14) + ((spillNo & 0xF) << 10) + ((slotNo & 0x1F) << 5) + (crateNo & 0x1F);
    adcHdr[1] = (1 << 15) + (1 << 14) + ((eventNo & 0xFF) << 6) + (spillNo >> 4 & 0x3F);
    adcHdr[2] = (1 << 15) + (1 << 14) + ((timestamp & 0x3F) << 8) + (eventNo >> 8 & 0xFF);
    adcHdr[3] = (1 << 15) + (1 << 14) + (timestamp >> 6 & 0x3FFF);
    adcHdr[4] = (1 << 15) + (1 << 14) + ((clusBit & 0x1F) << 9) + (timestamp >> 20 & 0x1FF);
    adcHdr[5] = (1 << 15) + (1 << 14) + (0 << 13) + (clusBit >> 5 & 0x7FF);

    //adcHdr[0] = 0xab;
    //adcHdr[1] = 0xab;
    //adcHdr[2] = 0xab;
    //adcHdr[3] = 0xab;
    //adcHdr[4] = 0xab;
    //adcHdr[5] = 0xab;

    //printf("adcHdr[0]: %x\n", adcHdr[0]);
    //printf("adcHdr[1]: %x\n", adcHdr[1]);
    //printf("adcHdr[2]: %x\n", adcHdr[2]);
    //printf("adcHdr[3]: %x\n", adcHdr[3]);
    //printf("adcHdr[4]: %x\n", adcHdr[4]);
    //printf("adcHdr[5]: %x\n", adcHdr[5]);

    //printf("spillNo: %d\n", ((adcHdr[0] & 0x3F) << 4) + ((adcHdr[1] >> 10) & 0xF));
    //printf("slotNo: %d\n", adcHdr[1] >> 5 & 0x1F);
    //printf("crateNo: %d\n", adcHdr[0] & 0x1F);
    //printf("eventNo: %d\n", ((adcHdr[3] & 0xFF) << 8) + (adcHdr[0] >> 6 & 0xFF));
    //printf("timestamp: %llu\n", ((adcHdr[5] & 0x1FF) << 20) + ((adcHdr[2] & 0x3FFF) << 6) + ((adcHdr[3] >> 8) & 0x3F));
    //printf("clusBit: %d\n", ((adcHdr[4] & 0x7FF) << 5) + ((adcHdr[5] >> 9) & 0x1F));
    //printf("--------------------\n");

}





int main(int argc, char *argv[])
{


    if (argc != 3) {
        printf("Usage: %s <nEvts> <spillNo>\n", argv[0]);
        return 1;
    }

    int nEvts = atoi(argv[1]);
    int spillNo = atoi(argv[2]);



    int sizeOfFullADC = (ADC_HDR_SIZE + ADC_NCHANELS * ADC_NSAMPLES_PER_CH) * 2; // bytes
    printf("one full ADC (header + data) is %d bytes\n", sizeOfFullADC);
    int sizeOfAllADCs = sizeOfFullADC * NCRATES * NADCS_PER_CRATE;
    printf("One whole evt worth of ADC data is %d bytes\n", sizeOfAllADCs);
    int ADCDataSizePerOFC2Pkt = PKT_SIZE - OFC2_HDR_SIZE;


    uint16_t adcHdr[NCRATES][NADCS_PER_CRATE][ADC_HDR_SIZE];
    uint16_t adcEne[NCRATES][NADCS_PER_CRATE][ADC_NCHANELS][ADC_NSAMPLES_PER_CH];
    uint16_t adcFtr[ADC_FOOTER_SIZE];
    uint16_t adcData[NCRATES][NADCS_PER_CRATE * FULL_FADC_SIZE + CRATE_FOOTER_SIZE];


    uint16_t * adcDataPtr = &adcData[0][0];
    uint16_t * adcDataPtrEnd = &adcData[NCRATES-1][NADCS_PER_CRATE * FULL_FADC_SIZE + CRATE_FOOTER_SIZE - 1];
    uint8_t ofc2Header[OFC2_HDR_SIZE];

    printf("buuuu");

    printf("Expected n of pkts: %d", (adcDataPtrEnd - adcDataPtr)/(OFC2_DATA_SIZE/2));

 
    // ADC Header
    for (int i_crate = 0; i_crate < NCRATES; i_crate++){
        for (int i_adc = 0; i_adc < NADCS_PER_CRATE; i_adc++){
            for (int i_hdr_wrd = 0; i_hdr_wrd < ADC_HDR_SIZE; i_hdr_wrd++){
                adcHdr[i_crate][i_adc][i_hdr_wrd] = 0;
            }
        }
    }

    // ADC Ene
    for (int i_crate = 0; i_crate < NCRATES; i_crate++){
        for (int i_adc = 0; i_adc < NADCS_PER_CRATE; i_adc++){

            if (i_crate == 14 && i_adc == 15)
            {
                // special trigger ADC
                for (int i_ch = 0; i_ch < ADC_NCHANELS; i_ch++){
                    for (int i_sample = 0; i_sample < ADC_NSAMPLES_PER_CH; i_sample++){
                        adcEne[i_crate][i_adc][i_ch][i_sample] = 0;
                    }
                }
                // scaled trigger bit
                //adcEne[i_crate][i_adc][8][0] = (i_crate*16 + i_adc) % 25;
            }
            else
            {
                for (int i_ch = 0; i_ch < ADC_NCHANELS; i_ch++){
                    for (int i_sample = 0; i_sample < ADC_NSAMPLES_PER_CH; i_sample++){
                        //adcEne[i_crate][i_adc][i_sample][i_ch] = (i_sample % 10) + 100;
                        //adcEne[i_crate][i_adc][i_sample][i_ch] = i_sample*i_ch*(i_crate*16 + i_adc) % 65536;
                        adcEne[i_crate][i_adc][i_ch][i_sample] = i_sample;
                    }
                }
            }
        }
    }

    // total ADC
    for (int i_crate = 0; i_crate < NCRATES; i_crate++){
        for (int i_adc = 0; i_adc < NADCS_PER_CRATE; i_adc++){
            
            // hdr is written evt by evt

            // data
            //for (int i_ch = 0; i_ch < ADC_NCHANELS; i_ch++){
            //    for (int i_sample = 0; i_sample < ADC_NSAMPLES_PER_CH; i_sample++){
            //        adcData[i_crate][i_adc][ADC_HDR_SIZE + i_ch*ADC_NSAMPLES_PER_CH + i_sample] = adcEne[i_crate][i_adc][i_sample][i_ch];
            //    }
            //}

            // data
            for (int i_ch = 0; i_ch < ADC_NCHANELS; i_ch++){
                for (int i_sample = 0; i_sample < ADC_NSAMPLES_PER_CH; i_sample++){
                    //adcData[i_crate][i_adc][ADC_HDR_SIZE + i_sample*16 + i_ch] = adcEne[i_crate][i_adc][i_sample][i_ch];
                    adcData[i_crate][i_adc * FULL_FADC_SIZE + ADC_HDR_SIZE + i_ch*ADC_NSAMPLES_PER_CH + i_sample] = adcEne[i_crate][i_adc][i_ch][i_sample];
                }
            }
            // ftr
            for (int i_ftr_wrd = 0; i_ftr_wrd < ADC_FOOTER_SIZE; i_ftr_wrd++){
                //adcData[i_crate][i_adc][ADC_HDR_SIZE + ADC_NCHANELS * ADC_NSAMPLES_PER_CH + i_ftr_wrd] = 0;
                adcData[i_crate][i_adc * FULL_FADC_SIZE + ADC_HDR_SIZE + ADC_NCHANELS * ADC_NSAMPLES_PER_CH + i_ftr_wrd] = 0xcccc;
            }
        }
        // crate ftr
        for (int i_ftr_wrd = 0; i_ftr_wrd < 8; i_ftr_wrd++){
            adcData[i_crate][NADCS_PER_CRATE * FULL_FADC_SIZE + i_ftr_wrd] = 0;
        }
    }



    // OFC2 header
    for (int i = 0; i < OFC2_HDR_SIZE; i++){
        ofc2Header[i] = i;
    }
    // dst MAC
    ofc2Header[0] = (uint8_t)0xf8;
    ofc2Header[1] = (uint8_t)0xf2;
    ofc2Header[2] = (uint8_t)0x1e;
    ofc2Header[3] = (uint8_t)0xdf;
    ofc2Header[4] = (uint8_t)0x63;
    ofc2Header[5] = (uint8_t)0x80;

    // src MAC
    ofc2Header[6] =  0xf8;
    ofc2Header[7] =  0xf2;
    ofc2Header[8] =  0x1e;
    ofc2Header[9] =  0xe6;
    ofc2Header[10] = 0xd8;
    ofc2Header[11] = 0x00;

    // eth type (set later for every packet)
    ofc2Header[12] = 0x00;
    ofc2Header[13] = 0x00;


    


    pcap_t *fp;
    char errbuf[PCAP_ERRBUF_SIZE];
    if ( (fp = pcap_open_live("ens7f0",         // name of the device
                        BUFSIZ,                // portion of the packet to capture (only the first 100 bytes)
                        PCAP_OPENFLAG_PROMISCUOUS,  // promiscuous mode
                        1000,               // read timeout
                        errbuf              // error buffer
                        ) ) == NULL)
    {
        printf("%s\n", errbuf);
        return -1;
    }





    std::vector<std::array<uint8_t, 6>> macs_ens6f0 = 
    {
        {0xf8, 0xf2, 0x1e, 0xdf, 0x63, 0x80}
        
        //{0xf8, 0xf2, 0x1e, 0xe6, 0xd8, 0x00},
        //{0xf8, 0xf2, 0x1e, 0xe6, 0xcc, 0xb0},
        //{0xf8, 0xf2, 0x1e, 0xe6, 0xd3, 0x80},
        //{0xf8, 0xf2, 0x1e, 0xe6, 0xcf, 0x50},
        //{0xF8, 0xF2, 0x1E, 0xE6, 0xC7, 0xE0},
        //{0xf8, 0xf2, 0x1e, 0xe6, 0xd4, 0x30}
    };

    std::vector<std::array<uint8_t, 6>> macs_ens6f1 = 
    {
        {0xf8, 0xf2, 0x1e, 0xe6, 0xd8, 0x01},
        {0xf8, 0xf2, 0x1e, 0xe6, 0xcc, 0xb1},
        {0xf8, 0xf2, 0x1e, 0xe6, 0xd3, 0x81},
        {0xf8, 0xf2, 0x1e, 0xe6, 0xcf, 0x51},
        {0xf8, 0xf2, 0x1e, 0xe6, 0xc7, 0xe1},
        {0xf8, 0xf2, 0x1e, 0xe6, 0xd4, 0x31},     
    };





    uint8_t packet[PKT_SIZE];

    uint32_t sent_pkt_count = 0;

    int i_byte = 0;
    uint64_t t0 = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
    for (int i_evt = 0; i_evt < nEvts; i_evt++)
    {
        


        // set dst mac
        //printf("Sending to spillnode %d\n", spillNo % macs_ens6f0.size());
        for (int i = 0; i < 6; i++){
            ofc2Header[i] = macs_ens6f0[spillNo % macs_ens6f0.size()][i];
        }


        // set trigg bit
        adcData[14][15 * FULL_FADC_SIZE + ADC_HDR_SIZE + 16*9] = i_evt % 25;


        // prepare ADC data
        for (int i_crate = 0; i_crate < NCRATES; i_crate++){
            for (int i_adc = 0; i_adc < NADCS_PER_CRATE; i_adc++){
                //writeADCHeader(&adcData[i_crate][i_adc][0], spillNo, i_adc, i_crate, i_evt);
                writeADCHeader(&adcData[i_crate][i_adc*FULL_FADC_SIZE], spillNo, i_adc, i_crate, i_evt);
            }
        }

        //// print full fadc data
        //for (int i_crate = 0; i_crate < NCRATES; i_crate++){
        //    printf("crate %d\n", i_crate);
        //    for (int i_sample = 0; i_sample < FULL_CRATE_SIZE; i_sample++){
        //        printf("%04x ", adcData[i_crate][i_sample]);
        //    }
        //    printf("\n");
        //}
        //exit(0);


        //Build packets from the ADC data until we have covered all the ADCs (whole event)
        int i_pkt = 0;
        bool evtFinished = false;
        while (1)
        {

            // eth type
            ofc2Header[12] = 32 + i_evt % 8;

            // pktNo
            ofc2Header[19] = i_pkt & 0xff;

            // evtNo
            ofc2Header[20] = i_evt >> 8;
            ofc2Header[21] = i_evt & 0xff;

            // OFC2 Header (40 bytes)
            for (int i = 0; i < OFC2_HDR_SIZE; i++){
                packet[i] = ofc2Header[i];
            }
            i_byte += OFC2_HDR_SIZE;

            // OFC2 Data
            if (adcDataPtr + OFC2_DATA_SIZE/2 >= adcDataPtrEnd){
                // If this is the last packet of the evt
                //printf("adcDataPtrEnd %d\n", adcDataPtrEnd - &adcData[0][0]);
                for (int i_word = 0; i_word < adcDataPtrEnd+1 - adcDataPtr; i_word++){
                    packet[i_byte + i_word*2] = adcDataPtr[i_word] & 0xff;
                    packet[i_byte + i_word*2 + 1] = adcDataPtr[i_word] >> 8;
                }
                i_byte += (adcDataPtrEnd+1 - adcDataPtr)*2;

                evtFinished = true;
                //// print full packet
                //for (int i = 0; i < i_byte; i++){
                //    printf("%02x ", packet[i]);
                //}
                //printf("\n");
                //sleep(1);
            }
            else{
                for (int i_word = 0; i_word < OFC2_DATA_SIZE/2; i_word++){
                    packet[i_byte + i_word*2] = adcDataPtr[i_word] & 0xff;
                    packet[i_byte + i_word*2 + 1] = adcDataPtr[i_word] >> 8;
                }
                adcDataPtr += OFC2_DATA_SIZE/2;
                i_byte += OFC2_DATA_SIZE;
            }
            


            // skip some packets to simulate network traffic
            if (sent_pkt_count != -2000){

                // send pkt here
                if (pcap_sendpacket(fp, packet, i_byte ) != 0)
                {
                    fprintf(stderr, "\nError sending the packet: \n", pcap_geterr(fp));
                    return -1;
                }
            }
            sent_pkt_count ++;
            //printf("Packet %d sent, (%d bytes)\n", i_pkt, i_byte);

            i_pkt ++;
            i_byte = 0;
            if (evtFinished) break;
        }

        printf("Evt %d finished\n", i_evt);    
        adcDataPtr = &adcData[0][0];
    }

    uint64_t t1 = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();


    printf("Sent %d events, %d packets\n", nEvts, sent_pkt_count);

    double time = (t1 - t0); // in ns
    double sent_size = (double)nEvts * (OFC2_HDR_SIZE + 288*(ADC_HDR_SIZE*2 + ADC_NCHANELS*ADC_NSAMPLES_PER_CH*2 + ADC_FOOTER_SIZE*2)); // in bytes
    printf("Speed: %f Gbps\n", sent_size*8/time);



}


